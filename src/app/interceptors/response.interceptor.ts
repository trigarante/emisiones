import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import {tap} from 'rxjs/operators';
import {NotificacionesService} from '../@core/data/services/others/notificaciones.service';

@Injectable()
export class ResponseInterceptor implements HttpInterceptor {

  constructor(private notificacionesService: NotificacionesService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      tap({
        next: () => this.notificacionesService.carga('Cargando'),
        complete: () => {
          for (const [key, value] of Object.entries(document.getAnimations())) {
            for (const a in value) {
              if (a === 'animationName'){
                if (value[a] === 'swal2-rotate-loading'){
                  this.notificacionesService.cerrar();
                }
              }
            }
          }
        },
        error: () => this.notificacionesService.error('Error al cargar los datos'),
      }));
  }
}
