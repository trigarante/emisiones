import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {Error404Component} from './modulos/error404/error404.component';
// import {SafePipe} from './safe.pipe';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {LoaderComponent } from './loader/loader.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {SocketIoModule} from 'ngx-socket-io';
import {environment} from "../environments/environment";
import {ResponseInterceptor} from './interceptors/response.interceptor';



@NgModule({
  declarations: [
    AppComponent,
    Error404Component,
    // SafePipe,
    LoaderComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule,
    MatTooltipModule,
    MatSnackBarModule,
    SocketIoModule,

    // StoreModule.forRoot({
    //   cliente: clienteReducer,
    //   productoCliente: clienteReducer
    // }),
    // StoreModule.forRoot( appReducers ),
    // StoreDevtoolsModule.instrument({
    //   maxAge: 25, // Retains last 25 states
    //   logOnly: environment.production, // Restrict extension to log-only mode
    //   autoPause: true, // Pauses recording actions and state changes when the extension window is not open
    // }),
  ],
  providers: [
    MatBottomSheet,
    { provide: HTTP_INTERCEPTORS, useClass: ResponseInterceptor, multi: true}
  ],
  exports: [
    LoaderComponent,
    // SafePipe,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
