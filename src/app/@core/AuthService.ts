import {Injectable, NgZone} from '@angular/core';
import {Router} from '@angular/router';
import {BehaviorSubject} from 'rxjs';
import Swal from 'sweetalert2';
import {TokenStorageService} from './data/services/login/token-storage.service';
import {environment} from '../../environments/environment';
import moment from 'moment-timezone';
import {EstadoUsuario} from './data/interfaces/usuario/EstadoUsuario';
import {HttpClient} from '@angular/common/http';

declare const gapi: any;


@Injectable({
    providedIn: 'root',
})


export class AuthService {
    baseUrl = environment.nodeLogin;
    permisos: string;
    idEmpleado: string;
    idUsuario: string;
    idSesion: string;
    idTipo: string;
    imagen: string;
    nombre: string;
    token: string;
    email: string;
    idGrupo: number;
    idSubarea: string;
  idDepartamento: string;
    estado: EstadoUsuario;
    code = 'vt442twgr43t54';
    extension: string;
    private loggedIn = new BehaviorSubject<boolean>(false); // {1}

    get isLoggedIn() {
        return this.loggedIn.asObservable(); // {2}
    }

    constructor(
        private router: Router,
        private tokenService: TokenStorageService,
        private tokenStorage: TokenStorageService,
        private ngZone: NgZone,
        private http: HttpClient
    ) {
        this.loggedIn.next(!!window.sessionStorage.getItem('logged'));
    }

    login() {
        this.loggedIn.next(true);
        window.sessionStorage.setItem('logged', 'chi');
        this.router.navigate(['modulos/dashboard']);
    }

    sendToken(token: string) {
        // @ts-ignore
      Swal.fire({
            title: 'Espere un momento',
            allowOutsideClick: false,
            allowEscapeKey: false,
            onBeforeOpen: () => {
                Swal.showLoading();
            },
        });

        let idPuesto: number;
        return this.http.post<{ ok: boolean, token: string, user: any, sesion: any }>(`${this.baseUrl}/auth1`,
            {token}, {headers: {Anonymous: 'auth'}})
            .subscribe(response => {
                if (response.user.idEmpleado === null) {
                    this.router.navigate(['login', 'error-empleado']);
                } else {
                  this.idEmpleado = response.user.idEmpleado;
                                   this.idUsuario = response.user.id;
                                   this.idGrupo = response.user.idGrupo;
                                    this.idDepartamento = response.user.idDepartamento;
                                    this.idSubarea = response.user.idSubarea;
                                    this.idSesion = response.sesion.id;
                                    this.permisos = response.user.permisos;
                                    this.token = response.token;
                                    this.nombre = response.user.nombre;
                                    this.imagen = response.user.imagenUrl;
                                    this.extension = response.user.extension;
                                    this.idTipo = response.user.idTipo;
                                    this.email = response.user.usuario;
                                    idPuesto = response.user.idPuesto;
                                    localStorage.setItem('sede', response.user.sede);
                                    localStorage.setItem('area', response.user.area);
                                    localStorage.setItem('depto', response.user.departamento);
                                    localStorage.setItem('puesto', response.user.puesto);
                                    localStorage.setItem('puestoE', response.user.puestoEspecifico);
                                    localStorage.setItem('nombreE', response.user.empleado);
                }
            }, async() => {
                await Swal.fire({
                    icon: 'error',
                    title: 'Usuario no encontrado',
                    text: 'No existe el correo o se encuentra desactivado',
                    allowOutsideClick: false,
                });
                this.tokenStorage.signOut();
                this.logOut();
            }, () => {
                if (this.idEmpleado) {
                    this.tokenStorage.saveCurrentUserPermissions(this.permisos);
                    this.tokenStorage.saveCurrentEmployee(this.idEmpleado, this.idUsuario, this.idDepartamento,
                        this.idSubarea, this.idTipo, this.idGrupo.toString(), this.email);
                    this.tokenStorage.saveTokenGoogle(this.token);
                    this.tokenStorage.saveCodeGoogle(this.code);
                    this.tokenStorage.saveCurrentUserData(this.nombre, this.imagen);
                    localStorage.setItem('sesion', this.idSesion);
                    localStorage.setItem('extension', this.extension);
                    window.sessionStorage.setItem('idEstadoSesion', '1');
                    window.sessionStorage.setItem('idPuesto', idPuesto.toString());
                    Swal.fire({
                        icon: 'success',
                        title: 'Sesión iniciada',
                        showConfirmButton: false,
                        timer: 4000,
                    });
                    this.ngZone.run(() => {
                        this.router.navigate(['modulos/dashboard']);
                    });
                }
                // this.postestadoUsuario();
            });
    }

    async logOut() {
        // const idSesion = window.sessionStorage.getItem('idSesion');
        if (!this.idSesion)
            this.idSesion = localStorage.getItem('sesion');
        // this.postEstadoCierre();
        window.localStorage.clear();
        window.sessionStorage.clear();
        const ahora = moment.tz('America/Mexico_City').format();
        this.http.put(`${this.baseUrl}/auth1/logout`, {id: this.idSesion, fechaFin: ahora},
            {headers: {Anonymous: 'auth'}}).subscribe();

        const auth2 = gapi.auth2.getAuthInstance();
        await auth2.signOut();
        this.ngZone.run(async() => {
          await this.router.navigateByUrl('/login');
          window.location.reload();
      });
    }
}
