import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FiltrosTablasService {
  filterSelect = [];

  createFilter() {
    let filterFunction = function (data: any, filter: string): boolean {
      let searchTerms = JSON.parse(filter);
      let isFilterSet = false;
      for (const col in searchTerms) {
        if (searchTerms[col].toString() !== '') {
          isFilterSet = true;
        } else {
          delete searchTerms[col];
        }
      }

      let nameSearch = () => {
        let found = false;
        if (isFilterSet) {
          // tslint:disable-next-line:forin
          for (const col in searchTerms) {

            searchTerms[col].trim().toLowerCase().split(' ').forEach(word => {
              if (data[col].toString().toLowerCase().indexOf(word) != -1 && isFilterSet) {
                found = true
              }
            });
          }
          return found;
        } else {
          return true;
        }
      }
      return nameSearch();
    }
    return filterFunction;
  }

  createFilterSelect(columnas) {
    columnas.forEach((columna) => {
      this.filterSelect.push({name: columna.nombre.toUpperCase(), columnProp: columna.id, options: [] = ['poliza', 'nombre', 'empleado', 'socio', 'subarea', 'estadoPoliza', 'estadoPago', 'estadoRecibo',
          'telefonoMovil', 'fechaRegistro', 'numeroSerie', 'acciones']});
    });
    return this.filterSelect;
  }

  getFilterObject(dataTable, key) {
    const uniqChk = [];
    dataTable.filter((obj) => {
      if (!uniqChk.includes(obj[key])) {
        uniqChk.push(obj[key]);
      }
      return obj;
    });
    return uniqChk;
  }
}
