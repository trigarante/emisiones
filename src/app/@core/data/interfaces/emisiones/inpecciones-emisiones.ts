import {Observable} from 'rxjs';

export interface InspeccionesEmisiones {
  id: number;
  idRegistro: number;
  idEstadoInspeccion: number;
  cp?: string;
  idColonia?: number;
  fechaInspeccion?: Date;
  kilometraje?: number;
  placas?: number;
  urlPreventa?: number;
  urlPostventa?: string;
  archivo?: string;
  comentarios?: string;
  estado?: string;
  asenta?: string;
  poliza?: string;
  primaNeta?: number;
}

export abstract class InspeccionesEmisionesData {

  abstract get(): Observable<InspeccionesEmisiones[]>;

  abstract post(Inspeccion): Observable<InspeccionesEmisiones>;

  abstract postOne(Inspeccion): Observable<InspeccionesEmisiones>;

  abstract getInspeccionById(idInspeccion): Observable<InspeccionesEmisiones>;

  abstract put(idInspeccion, Inspeccion): Observable<InspeccionesEmisiones>;

}
