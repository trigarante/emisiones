import {Observable} from 'rxjs';

export interface PagosEmisiones {
  id: number;
  idRecibo: number;
  idFormaPago: number;
  idEstadoPago: number;
  idEmpleado: number;
  fechaPago: Date;
  cantidad: number;
  archivo: string;
  datos: string;
  numero?: number;
  recibosCantidad?: number;
  formapagoDescripcion?: string;
  estadoPagoDescripcion?: string;
}

export abstract class PagosEmisionesData {
  abstract get(): Observable<PagosEmisiones[]>;
  abstract getPagoById(id): Observable<PagosEmisiones>;
  abstract getPagoByIdADM(id): Observable<PagosEmisiones>;
  abstract getByIdRecibo(idRecibo): Observable<PagosEmisiones>;
  abstract getByIdReciboADM(idRecibo): Observable<PagosEmisiones>;
  abstract post(pago): Observable<PagosEmisiones>;
  abstract postOne(pago): Observable<PagosEmisiones>;
  abstract put(id, pago): Observable<PagosEmisiones>;
  abstract putArchivo(id, archivo: string): Observable<PagosEmisiones>;
}
