import {Observable} from 'rxjs';

export interface StepsEmisionView {
  idEmisiones: number;
  idProductoCliente: number;
  idCliente: number;
  idRegistro: number;
  recibos: number;
  pagos: number;
}

export abstract class StepsEmisionViewData {
  abstract get(): Observable<StepsEmisionView[]>;
  abstract getById(idEmisiones: number): Observable<StepsEmisionView>;
  abstract getByIdRegistro(idRegistro: number): Observable<StepsEmisionView>;
}
