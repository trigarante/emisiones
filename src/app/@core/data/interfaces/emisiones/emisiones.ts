import {Observable} from 'rxjs';
import {AplicacionView} from '../finanzas/aplicacionView';
import {Recibos} from "../ventaNueva/recibos";


export interface Emisiones {
  id: number;
  idEmpleado: number;
  idRegistro: number;
  idTipoPago: number;
  idEstadoRenovacion: number;
  idProductoSocio: number;
  idSocio: number;
  idTipoBandejaRenovacion: number;
  idTipoRenovacion: number;
  idPeriodicidad: number;
  idEstadoPoliza: number;
  poliza: string;
  fechaInicio: Date;
  primaNeta: number;
  primerPago: number;
  montoSubsecuente: number;
  archivo: string;
  oficina: string;
}

export interface EmisionesViews {
  id: number;
  idRegistro: number;
  idEmpleado: number;
  idTipoRenovacion: number;
  idProductoSocio: number;
  // idProducto: number;
  idProductoCliente: number;
  idTipoPago: number;
  idEstadoRenovacion: number;
  // idTipoPoliza: number;
  idSocio: number;
  poliza: string;
  polizaVieja: string;
  fechaInicio: Date;
  primaNeta: number;
  primaNetaAnterior: number;
  variacion: number;
  fechaRegistro: Date;
  archivo: string;
  tipoPago: string;
  estadoRenovacion: string;
  nombreComercial: string;
  descripcionPoliza: string;
  idTipoBandejaRenovacion: number;
  oficina: number;
  idPeriodicidad: number;
  idRamo: number;
  idSubramo: number;
  idProducto: number;
  archivoViejo: string;
  idNuevoProducto: number;
  idNuevoRegistro: number;
  idNuevoRecibo: number;
  idEstadoIntentoPago: number;
  montoSubsecuente: number;
  primerPago: number;
  idSolicitud: number;
  idPago?: number;
  idNuevoPago?: number;
  correo?: string;
  telefonoMovil?: number;
  nombreEmpleadoRenovaciones?: string;
  apellidoPaternoEmpleadoRenovaciones?: string;
  apellidoMaternoEmpleadoRenovaciones?: string;
}

export interface DatosRegistroEmisiones {
  idRegistro: number;
  poliza: string;
  idTipoPago: number;
  idSocio: number;
  idRamo: number;
  idSubRamo: number;
  idProductoSocio: number;
  idTipoBandejaRenovacion: number;
  estado: string;
  fechaInicio: Date;
  idPeriodicidad: number;
  oficina: string;
  primaNeta: number;
  primerPago: number;
  pagoMensual: number;
  recibos: Recibos[];
}

export abstract class EmisionesData {
  abstract post(data): Observable<Emisiones>;
  abstract postInOne(data): Observable<any>;
  abstract getAll(): Observable<EmisionesViews[]>;
  abstract getCobranzaEmisionesRenovacion(): Observable<EmisionesViews[]>;
  abstract getAllEmisionesOutbound(): Observable<EmisionesViews[]>;
  abstract getAllEmisionesOutboundPeru(): Observable<EmisionesViews[]>;
  abstract getAllEmisionesCargoAutomaticoInterno(): Observable<EmisionesViews[]>;
  abstract getAllEmisionesCargoAutomaticoExterno(): Observable<EmisionesViews[]>;
  abstract getAllBandejas(): Observable<any[]>;
  abstract getFiltroFechaEmisiones(fecha1, fecha2): Observable<any[]>;
  abstract getFiltroFechaARenovar(fecha1, fecha2): Observable<any[]>;
  abstract getFiltroColores(valor): Observable<any[]>;
  abstract getById(id: number): Observable<EmisionesViews>;
  abstract getRegistroEmisionById(id: number): Observable<any>;
  abstract getRegistroEmision(idEmision: number, idRegistro: number): Observable<any>;
  abstract getByIdTipoBandeja(idTipoBandeja: number): Observable<EmisionesViews[]>;
  abstract getAllRegistros(): Observable<AplicacionView[]>;
  abstract getAllPendientes(): Observable<EmisionesViews[]>;
  abstract getAllRegistrosEmision(): Observable<AplicacionView[]>;
  abstract putEmision(id: number, emisiones: Emisiones): Observable<Emisiones>;
  abstract putEmisionModalRegistro(id: number, tipoRenovacion: number): Observable<any>;
  abstract putDatosRegistroEmisiones(idRegistro: number, datos: DatosRegistroEmisiones): Observable<any>;
  abstract getByIdTipoBandejaAndFecha(fecha1, fecha2, idTipoBandeja: number): Observable<any>;
  abstract getFiltroFechaCobranzaRenovacion(fecha1, fecha2): Observable<any>;
}
