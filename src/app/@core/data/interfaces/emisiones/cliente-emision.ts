import {ProductoCliente} from "../ventaNueva/producto-cliente";
import {Registro} from "../ventaNueva/registro";
import {Recibos} from "../ventaNueva/recibos";
import {Inspecciones} from "../ventaNueva/inspecciones";

export interface ClienteEmision {
  id: number;
  idPais: number;
  personaMoral: number;
  nombre: string;
  paterno: string;
  materno: string;
  cp: number;
  calle: string;
  numInt: string;
  numExt: string;
  idColonia: number;
  colonia: string;
  genero: string;
  telefonoFijo: string;
  telefonoMovil: string;
  correo: string;
  fechaNacimiento: string;
  fechaRegistro: string;
  curp: string;
  rfc: string;
  nombrePaises: string;
  razonSocial: string;
  archivo: string;
  archivoSubido: number;
  dni: string;
  ruc: string;
  idColoniaPeru: number;
}

export interface RegistroClienteEmision {
  productoCliente: ProductoCliente;
  registro: Registro;
  recibos: Recibos[];
  inpecciones: Inspecciones;
}
