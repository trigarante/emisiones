export default interface Menu{
  name: string,
  alias?: string,
  icon?: string,
  children?: Menu[],
  rutas?: string,
  grandChildren?: Menu[],
  permiso?: any
}
