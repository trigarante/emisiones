import {Observable} from 'rxjs';

export interface CarruselEmpleados {
  id: number;
  idEmpleado: number;
  idMedioDifusion: number;
  contador: number;
  permiso: number;
  pagina: string;
}

export abstract class CarruselEmpleadosData {
  abstract get(): Observable<CarruselEmpleados[]>;
  abstract getRegistroById(idCarrusel): Observable<CarruselEmpleados>;
  abstract post(Carrusel): Observable<CarruselEmpleados>;
  abstract put(idCarrusel, Carrusel): Observable<CarruselEmpleados>;
}
