export interface ImagenesSocios {
  id: number;
  idSocio: number;
  linkImagen: string;
  activo: number;
  nombreComercial: string;
}
