import {Observable} from 'rxjs';
import {ClientePeru} from '../cliente-peru';

export interface ClienteCorreccion {
  id: number;
  idPais: number;
  personaMoral: number;
  nombre: string;
  paterno: string;
  materno: string;
  cp: number;
  calle: string;
  numInt: string;
  numExt: string;
  idColonia: number;
  colonia: string;
  genero: string;
  telefonoFijo: string;
  telefonoMovil: string;
  correo: string;
  fechaNacimiento: string;
  fechaRegistro: string;
  curp: string;
  rfc: string;
  nombrePaises: string;
  razonSocial: number;
  archivo: string;
  archivoSubido: number;
  dni: string;
  ruc: string;
  idColoniaPeru: number;
}

export abstract class ClienteCorreccionData {
  abstract get(): Observable <ClienteCorreccion[]>;
  abstract getFinanzas(): Observable<ClienteCorreccion[]>;
  abstract getClienteFinanzas(): Observable<ClienteCorreccion[]>;
  abstract getClienteFinanzasById(idClienteFinanzas): Observable<ClienteCorreccion>;
  abstract getClienteById(idCliente): Observable <ClienteCorreccion>;
  abstract getClienteByIdADM(idCliente): Observable <ClienteCorreccion>;
  abstract getFinanzasClienteById(idCliente): Observable <ClienteCorreccion>;
  abstract getClientePeruById(idCliente: number): Observable<ClientePeru>;
  abstract getClienteFinPeruById(idCliente: number): Observable<ClientePeru>;
  abstract curpExist(curp);
  abstract rfcExist(rfc);
  abstract post(cliente): Observable <ClienteCorreccion>;
  abstract postClienteFinanzas(cliente): Observable <ClienteCorreccion>;
  abstract postSocket(json): Observable<JSON>;
  abstract put(idCliente, cliente): Observable <ClienteCorreccion>;
  abstract putCliente(idCliente, cliente): Observable <ClienteCorreccion>;
  abstract putArchivo(idCliente: number, idCarpetaDrive: string): Observable<any>;
  abstract putArchivoSubido(idCliente: number, idArchivoSubido: number): Observable<any>;
  abstract dniExist(dni: string): Observable<ClientePeru>;
}
