import {Observable} from 'rxjs';

export interface Registro {
  id: number;
  idEmpleado: number;
  idProducto: number;
  idTipoPago: number;
  idEstadoPoliza: number;
  idTipoPoliza: number;
  idProductoSocio: number;
  idFlujoPoliza: number;
  idPeriodicidad: number;
  periodicidad: string;
  productoSocio: string;
  tipoRamo: string;
  tipoSubramo: string;
  idSocio: number;
  idRamo: number;
  idSubramo: number;
  poliza: string;
  fechaInicio: Date;
  primaNeta: number;
  fechaRegistro: Date;
  datos: any;
  archivo: string;
  idCliente?: number;
  cantidadPagos: number;
  archivoSubido: number;
  fechaFin: Date;
  idPais: number;
  oficina?: string;
  idSubarea: number;
  fechaCierre: Date;
}

export interface RegistroSoloDatos {
  idSocio: number;
  idRamo: number;
  idSubRamo: number;
  idProductoSocio: number;
  fechaInicio: Date;
  idPeriodicidad: number;
  poliza: string;
  oficina?: string;
  primaNeta: number;
}

export interface RegistroComplemento {
  id: number;
  idRegistro: number;
  idEstadoRegistroComplemento: number;
  idEmpleado: number;
  idTipoPago: number;
  primaNeta: number;
  fechaInicio: Date;
  fechaRegistro: Date;
  idPeriodicidad: number;
  archivo: string;
}

export abstract class RegistrosData {
  abstract get(): Observable<Registro[]>;
  abstract getRegistroById(idRegistro): Observable<Registro>;
  abstract getRegistroComplementoById(idRegistro): Observable<RegistroComplemento>;
  abstract getRegistroRetenciones(idRegistro): Observable<Registro>;
  abstract getRegistroEmisionById(idRegistro): Observable<Registro>;
  abstract getRegistroByIdADM(idRegistro): Observable<Registro>;
  abstract getRegistroFinanzasById(idRegistro): Observable<Registro>;
  abstract getNoSerieExistente(numSerie: string): Observable<string>;
  abstract getNoSerieExistenteRenovaciones(numSerie: string): Observable<string>;
  abstract getPolizaExistente(poliza: string, idSocio: string, fechaInicio: string): Observable<string>;
  abstract post(registro): Observable<Registro>;
  abstract postRF(registro): Observable<Registro>;
  abstract postInOne(any): Observable<Registro>;
  abstract postAllFinanzas(any): Observable<any>;
  abstract postInOneComplemento(any): Observable<RegistroComplemento>;
  abstract put(idRegistro, registro): Observable<Registro>;
  abstract putRF(idRegistro, registro): Observable<Registro>;
  abstract updateArchivo(idRegistro: number, idCarpeta: string): Observable<any>;
  abstract updateArchivoComplemento(idRegistro: number, idCarpeta: string): Observable<any>;
  abstract updateEstadoPoliza(idRegistro: number, estado: number): Observable<any>;
  abstract updateFlujoPoliza(idRegistro: number, idFlujoPoliza: number): Observable<any>;
  abstract updateFlujoAndEstadoPoliza(idRegistro: number, idFlujoPoliza: number, idEstadoPoliza): Observable<any>;
  abstract updateRegistroPolizaSoloDatos(datos: RegistroSoloDatos, id: number): Observable<Boolean>;
  abstract modificarPolizaSocket(json): Observable<any>;
}
