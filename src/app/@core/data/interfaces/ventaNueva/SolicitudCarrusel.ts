export interface SolicitudCarrusel {
  id: number;
  idMotivoDesactivacion: number;
  idEstadoSolicitudCarrusel: number;
  idEmpleado: number;
  idEmpleadoSolicitante: number;
  idEmpleadoCoordinador: number;
  idEmpleadoRRHH: number;
  fechaRegistro: Date;
  fechaAutorizacionCoordinador: Date;
  fechaAutorizacionRrhh: Date;
}
