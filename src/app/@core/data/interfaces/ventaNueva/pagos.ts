import {Observable} from 'rxjs';

export interface Pagos {
  id: number;
  idRecibo: number;
  idFormaPago: number;
  idEstadoPago: number;
  idEmpleado: number;
  fechaPago: Date;
  cantidad: number;
  archivo: string;
  datos: string;
  numero?: number;
  recibosCantidad?: number;
  formapagoDescripcion?: string;
  estadoPagoDescripcion?: string;
  numeroTarjeta?: string;
  titular?: string;
}


export interface PagosComplemento {
  id: number;
  idReciboComplemento: number;
  idFormaPago: number;
  idEstadoPago: number;
  idEmpleado: number;
  cantidad: number;
  archivo: string;
  fechaRegistro: Date;
  fechaPago: Date;
  datos: string;
}

export abstract class PagosData {
  abstract get(): Observable<Pagos[]>;
  abstract getPagoById(id): Observable<Pagos>;
  abstract getPagoByIdADM(id): Observable<Pagos>;
  abstract getByIdRecibo(idRecibo): Observable<Pagos>;
  abstract getByIdReciboADM(idRecibo): Observable<Pagos>;
  abstract post(pago): Observable<Pagos>;
  abstract postRecibo(pago): Observable<Pagos>;
  abstract postOne(pago): Observable<Pagos>;
  abstract postOneComplemento(pago): Observable<PagosComplemento>;
  abstract postOneRetenciones(pago): Observable<Pagos>;
  abstract postOneCorrecciones(pago): Observable<Pagos>;
  abstract put(id, pago): Observable<Pagos>;
  abstract putArchivo(id, archivo: string): Observable<Pagos>;
}
