export interface CarruselCoaching {
  id: number;
  idEmpleado: number;
  contador: number;
  permisoNormal: number;
  permisoRemedial: number;
}
