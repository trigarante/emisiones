import {Observable} from 'rxjs';

export interface CarruselConfiguracion {
  id: number;
  idSubarea: number;
  idDiaSemana: number;
  leadsMaximos: number;
  fechaFin: string;
  antiguedadMinima: number;
  antiguedadMaxima: number;
  activo: number;
  fechaInicio: string;
  idArea: number;
  subarea: string;
  idSede: number;
  area: string;
  idEmpresa: number;
  sede: string;
  idGrupo: number;
  empresa: string;
  grupo: string;
  dia: string;
}

export abstract class CarruselConfiguracionData {
  abstract get(): Observable<CarruselConfiguracion[]>;
  abstract getRegistroById(idCarrusel): Observable<CarruselConfiguracion>;
  abstract post(Carrusel): Observable<CarruselConfiguracion>;
  abstract put(idCarrusel, Carrusel): Observable<CarruselConfiguracion>;
}
