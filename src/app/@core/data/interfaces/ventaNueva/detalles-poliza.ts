import {Observable} from 'rxjs';

export interface DetallesPoliza {
  id: number;
  idEmpleado: number;
  idProducto: number;
  idTipoPago: number;
  idProductoSocio: number;
  idFlujoPoliza: number;
  poliza: string;
  fechaInicio: string;
  primaNeta: string;
  fechaRegistro: string;
  archivo: string;
  cantidadPagos: number;
  tipoPago: string;
  pruductoSocio: string;
  datos: string;
  idEstadoPoliza: number;
  estado: string;
  idArea: number;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  idSede: number;
  idGrupo: number;
  idPuesto: number;
  idEmpresa: number;
  idSubarea: number;
  idCliente: number;
  idSolictud: number;
  descripcion: string;
  idSubRamo: number;
}
export interface DatosCliente {
  cp: string;
  edad: number;
  clave: string;
  marca: string;
  genero: string;
  modelo: string;
  paquete: string;
  servicio: string;
  descuento: number;
  movimiento: string;
  aseguradora: string;
  descripcion: string;
  numeroMotor: string;
  numeroPlacas: string;
  fechaNacimiento: string;
}
export abstract class DetallesPolizaData {
  abstract get(): Observable<DetallesPoliza>;
  abstract getById(id): Observable<DetallesPoliza>;

}
