import {Observable} from 'rxjs';
import {SolicitudesVN} from './solicitudes';

export interface AdminVnView {
  id: number;
  poliza: string;
  fechaInicio: string;
  primaNeta: number;
  fechaRegistro: string;
  archivo: string;
  cantidadPagos: number;
  archivoPago: string;
  datosPagos: string;
  nombreComercial: string;
  nombreEmpleado: string;
  numeroPagos: number;
  idEstadoRecibo: number;
  estadoRecibo: string;
  numeroSerie: string;
  idEstadoPoliza: number;
  estadoPoliza: string;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  correo: string;
  subarea: string;
  idCliente: number;
  idRecibo: number;
  fechaPromesaPago: number;
  nombreCliente: string;
  apellidoPaternoCliente: string;
  apellidoMaternoCliente: string;
  archivoSubido: number;
  carpetaCliente: string;
  tipoSubramo: string;
  estadoPago: string;
  telefonoMovil: string;
  cierre: number;
}

export abstract class AdminVnViewData {
  abstract getAllRegistroByIdUsuario(): Observable<AdminVnViewData[]>;
  abstract getAllRegistroByIdUsuarioIb(): Observable<AdminVnViewData[]>;
  abstract getRegistroById(idRegistro): Observable<AdminVnViewData>;
  // Sockets
  abstract postPolizaSocket(data): Observable<AdminVnViewData>;
  abstract updateClienteSockets(data): Observable<SolicitudesVN>;
  abstract updateNumeroSerieSockets(data): Observable<SolicitudesVN>;
  abstract updateArchivoSubidoSockets(data): Observable<SolicitudesVN>;
  abstract updatePagoSockets(data): Observable<SolicitudesVN>;
  abstract updateEstadoPolizaSockets(data): Observable<SolicitudesVN>;
}
