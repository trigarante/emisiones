import {Observable} from 'rxjs';
import * as moment from "moment";

export interface Recibos {
  id: number;
  idRegistro: number;
  idEmpleado: number;
  idEstadoRecibos: number;
  fechaVigencia: Date;
  numero: number;
  cantidad: number;
  estdo: string;
  primaNeta: string;
  poliza: string;
  estado: string;
  fechaLiquidacion: Date;
  nombreComercial?: string;
  area?: string;
  idEstadoPoliza: number;
  fechaRegistro: string;
  idFlujoPoliza?: number;
  fechaInicio: string;
  fechaPromesaPago: string;
  fechaPago: Date;
  datos: string;
  idFormaPago: number;
  activo: number;
  archivo: string;
  idPago: number;
  nombreEmpleadoPago: string;
  apellidoPaternoEmpleadoPago: string;
  apellidoMaternoEmpleadoPago: string;
}

export interface RecibosComplemento {
  id: number;
  idRegistroComplemento: number;
  idEstadoRecibo: number;
  idEmpleado: number;
  cantidad: number;
  numero: number;
  fechaRegistro: Date;
  fechaVigencia: Date;
  fechaLiquidacion: Date;
  activo: number;
}

export abstract class RecibosData {
  abstract get(): Observable<Recibos[]>;
  abstract post(recibos: any[]): Observable<string>;
  abstract postConId(recibos: any[], id: number): Observable<any>;
  abstract getReciboById(idRecibo): Observable<Recibos>;
  abstract put(idRecibo, Recibo): Observable<Recibos>;
  abstract getRecibosByIdRegistro(idRegistro: number): Observable<Recibos[]>;
  abstract getRecibosByIdRegistroComplemento(idRegistro: number): Observable<RecibosComplemento[]>;
  abstract getRecibosByIdRegistroConPago(idRegistro: number): Observable<Recibos[]>;
  abstract getRecibosByIdRegistroConNumero(idRegistro: number, numero: number): Observable<Recibos>;
  abstract getRecibosByIdRegistroADM(idRegistro: number): Observable<Recibos[]>;
  abstract putEstadoRecibo(idRecibo: number, idEstadoRecibo: number): Observable<any>;
  abstract putActivo(idRecibo: number);
  abstract putBajaSinPago(idRecibo: number);
  abstract putIdEmpleado(idEmpleado: number, idRegistro: number): Observable<any>;
  abstract getReciboPago(idRegistro): Observable<Recibos[]>;
}
