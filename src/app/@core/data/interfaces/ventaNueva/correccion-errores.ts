import {Observable} from 'rxjs';

export interface ErroresAutorizacion {
  id: number;
  idRegistro: number;
  idEstadoVerificacion: number;
  numero: number;
  verificadoCliente: number;
  verificadoProducto: number;
  verificadoRegistro: number;
  verificadoPago: number;
  verificadoInspeccion: number;
  fechaCreacion: Date;
  idErrorAutorizacion?: number;
  idErrorAnexo?: number;
  idEstadoCorreccion: number;
  idEmpleadoCorreccion: number;
  idEmpleado: number;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  poliza: string;
  idCliente: number;
  nombreCliente: string;
  apellidoPaternoCliente: string;
  apellidoMaternoCliente: string;
  carpetaCliente: string;
  carpetaRegistro: string;
  idPago: number;
  archivoPago: string;
  estadoVerificacion: string;
}

export interface ErroresVerificacion {
  id: number;
  idRegistro: number;
  idEstadoVerificacion: number;
  estadoVerificacion: string;
  numero: number;
  verificadoCliente: number;
  verificadoProducto: number;
  verificadoRegistro: number;
  verificadoPago: number;
  verificadoInspeccion: number;
  fechaCreacion: Date;
  idErrorAnexo?: number;
  idErrorAutorizacion?: number;
  idEstadoCorreccion: number;
  idEmpleadoCorreccion: number;
  idEmpleado: number;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  poliza: string;
  idCliente: number;
  nombreCliente: string;
  apellidoPaternoCliente: string;
  apellidoMaternoCliente: string;
  carpetaCliente: string;
  carpetaRegistro: string;
  idPago: number;
  archivoPago: string;
  fechaCreacionError?: Date;
  fechaPago?: Date;
  subarea: string;
  nombreComercial: string;
  horasRestantes: number;
}

export abstract class CorreccionErroresData {
  abstract getErroresDocumentosA (): Observable<ErroresAutorizacion[]>;
  abstract getErroresDatosA (): Observable<ErroresAutorizacion[]>;
  abstract getErroresDatosAR (): Observable<ErroresAutorizacion[]>;
  abstract getErroresDocumentosV (): Observable<ErroresVerificacion[]>;
  abstract getErroresDatosV (): Observable<ErroresVerificacion[]>;
  abstract getErroresDatosVR (): Observable<ErroresVerificacion[]>;
  // Venta Nueva
  abstract getErroresDocumentosVVn(): Observable<ErroresVerificacion[]>;
  abstract getErroresDatosVVn(): Observable<ErroresAutorizacion[]>;
  // Venta Nueva Ib
  abstract getErroresDocumentosVVnIb(): Observable<ErroresVerificacion[]>;
  abstract getErroresDatosVVnIb(): Observable<ErroresAutorizacion[]>;
  // Cobranza
  abstract getErroresDocumentosCobranzaV(): Observable<ErroresVerificacion[]>;
  abstract getErroresDatosCobranzaV(): Observable<ErroresAutorizacion[]>;
  // Subsecuentes
  abstract getErroresDocumentosSubsecuentesV(): Observable<ErroresVerificacion[]>;
  abstract getErroresDatosSubsecuentesV(): Observable<ErroresAutorizacion[]>;
  // Ecommerce
  abstract getErroresDocumentosEcommerceV(): Observable<ErroresVerificacion[]>;
  abstract getErroresDatosEcommerceV(): Observable<ErroresAutorizacion[]>;
  // Peru
  abstract getErroresDocumentosPeruV(): Observable<ErroresVerificacion[]>;
  abstract getErroresDatosPeruV(): Observable<ErroresAutorizacion[]>;
  // Peru
  abstract getErroresDocumentosGMV(): Observable<ErroresVerificacion[]>;
  abstract getErroresDatosGMV(): Observable<ErroresAutorizacion[]>;
  // Tradicional
  abstract getErroresDocumentosTradicionalV(): Observable<ErroresVerificacion[]>;
  abstract getErroresDatosTradicionalV(): Observable<ErroresAutorizacion[]>;
  // Retenciones
  abstract getErroresDocumentosRetencionesV(): Observable<ErroresVerificacion[]>;
  abstract getErroresDatosRetencionesV(): Observable<ErroresAutorizacion[]>;

  // Nuevo
  abstract getErroresDocumentosAFlujoPoliza (idFlujoPoliza: number): Observable<ErroresAutorizacion[]>;
  abstract getErroresDatosAFlujoPoliza (idFlujoPoliza: number): Observable<ErroresAutorizacion[]>;
  abstract getErroresDocumentosVFlujoPoliza (idFlujoPoliza: number): Observable<ErroresVerificacion[]>;
  abstract getErroresDatosVFlujoPoliza (idFlujoPoliza: number): Observable<ErroresVerificacion[]>;
  abstract getErroresDocumentosASubsecuentes (): Observable<ErroresAutorizacion[]>;
  abstract getErroresDatosASubsecuentes (): Observable<ErroresAutorizacion[]>;
  abstract getErroresDocumentosVSubsecuentes (): Observable<ErroresVerificacion[]>;
  abstract getErroresDatosVSubsecuentes (): Observable<ErroresVerificacion[]>;
  abstract getErroresDocumentosACobranza (): Observable<ErroresAutorizacion[]>;
  abstract getErroresDatosACobranza (): Observable<ErroresAutorizacion[]>;
  abstract getErroresDocumentosVCobranza (): Observable<ErroresVerificacion[]>;
  abstract getErroresDatosVCobranza (): Observable<ErroresVerificacion[]>;
  abstract getErroresDocumentosVReno (): Observable<ErroresVerificacion[]>;
  abstract getErroresDatosVReno (): Observable<ErroresVerificacion[]>;
}
