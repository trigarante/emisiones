import {Observable} from 'rxjs';

export interface ErroresAnexos {
  id: number;
  idAutorizacionRegistro: number;
  idEmpleado: number;
  idEstadoCorreccion: number;
  idTipoDocumento: number;
  idEmpleadoCorreccion: number;
  correcciones: string;
}

export abstract class ErroresAnexosData {
  abstract get(): Observable<ErroresAnexos[]>;
  abstract getByIdAutorizacionRegitro(idAutorizacionRegistro: number, idTipoDocumento: number): Observable<ErroresAnexos>;
  abstract post(error: ErroresAnexos): Observable<string>;
  abstract put(idError: number, error: ErroresAnexos): Observable<any>;
  abstract putEstadoCorreccion(idErrorAnexo: number): Observable<any>;
}
