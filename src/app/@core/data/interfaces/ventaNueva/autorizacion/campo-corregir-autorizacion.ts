import {Observable} from 'rxjs';

export interface CampoCorregirAutorizacion {
  id: number;
  idTabla: number;
  campo: string;
  descripcion: string;
  activo: boolean;
}

export abstract class CampoCorregirAutorizacionData {
  abstract getCamposDisponibles(idTabla: number): Observable<CampoCorregirAutorizacion[]>;
}
