import {Observable} from 'rxjs';

export interface AutorizacionRegistro {
  id: number;
  idRegistro: number;
  idEstadoVerificacion: number;
  numero: number;
  verificadoCliente: number;
  verificadoProducto: number;
  verificadoRegistro: number;
  verificadoPago: number;
  verificadoInspeccion: number;
  // Las variables que están abajo son exclusivas de la vista
  idEmpleado?: number;
  poliza?: string;
  idCliente?: number;
  nombreCliente?: string;
  apellidoPaternoCliente?: string;
  apellidoMaternoCliente?: string;
  carpetaCliente?: string;
  carpetaRegistro?: string;
  archivoPago?: string;
  estadoVerificacion?: string;
  idPago?: number;
  idProducto: number;
  idFlujoPoliza?: number;
}

export interface AuthRegistro {
  id: number;
  idEstadoVerificacion: number;
  idEmpleado: number;
  idSubarea: number;
  idFlujoPoliza: number;
  verificadoCliente: number;
  verificadoProducto: number;
  verificadoRegistro: number;
  verificadoPago: number;
}

export abstract class AutorizacionRegistroData {
  abstract get(): Observable<AutorizacionRegistro[]>;
  abstract getById(id: number): Observable<AutorizacionRegistro>;
  abstract getByIdIn(id: number): Observable<AutorizacionRegistro>;
  abstract post(registro): Observable<AutorizacionRegistro>;
  abstract put(id: number, verificacion: AutorizacionRegistro): Observable<AutorizacionRegistro>;
  abstract putIn(id: number, verificacion: AutorizacionRegistro): Observable<AutorizacionRegistro>;
  abstract documentoVerificado(idAutorizacionRegistro: number, documentoVerificado: number, estado: number): Observable<String>;
  abstract getIdFlujoPoliza(idFlujoPoliza: number): Observable<AutorizacionRegistro[]>;
  abstract getGastosMedicos(): Observable<AutorizacionRegistro[]>;
  abstract getSubsecuentes(): Observable<AutorizacionRegistro[]>;
  abstract getCobranza(): Observable<AutorizacionRegistro[]>;
  abstract getAutorizacionVn(): Observable<AutorizacionRegistro[]>;
  abstract getAutorizacionEcommerce(): Observable<AutorizacionRegistro[]>;
  abstract getTabla(estadoVerificacion: string): Observable<AutorizacionRegistro[]>;
  abstract getTablaIn(estadoVerificacion: string): Observable<AutorizacionRegistro[]>;
  abstract getAuth(): Observable<AuthRegistro[]>;
  abstract getTablaCobranza(estadoVerificacion: string): Observable<AutorizacionRegistro[]>;
  abstract getTablaCobranzaReno(estadoVerificacion: string): Observable<AutorizacionRegistro[]>;
  abstract getTablaSubsecuentes(estadoVerificacion: string): Observable<AutorizacionRegistro[]>;
  abstract getTablaSubsecuentesReno(estadoVerificacion: string): Observable<AutorizacionRegistro[]>;
  abstract getTablaSubsecuentesPeru(estadoVerificacion: string): Observable<AutorizacionRegistro[]>;
  abstract getTablaEcommerce(estadoVerificacion: string): Observable<AutorizacionRegistro[]>;
  abstract getVnById(id: number): Observable<AutorizacionRegistro>;
  abstract getEcommerceById(id: number): Observable<AutorizacionRegistro>;
  abstract documentoVerificadoVn(idAutorizacionRegistro: number, documentoVerificado: number, estado: number): Observable<String>;
  abstract documentoVerificadoVnIn(idAutorizacionRegistro: number, documentoVerificado: number, estado: number): Observable<String>;
  abstract documentoVerificadoSubsecuentes(idAutorizacionRegistro: number, documentoVerificado: number, estado: number): Observable<String>;
  abstract gerPeru(): Observable<AutorizacionRegistro[]>;
  abstract getPeruById(id: number): Observable<AutorizacionRegistro>;
  abstract getSubPeruById(id: number): Observable<AutorizacionRegistro>;
  abstract getSubsecuentesById(id: number): Observable<AutorizacionRegistro>;
  abstract getCobranzaById(id: number): Observable<AutorizacionRegistro>;
  abstract getSubsecuentesPeruById(id: number): Observable<AutorizacionRegistro>;
}
