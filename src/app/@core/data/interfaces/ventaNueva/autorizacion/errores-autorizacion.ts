import {Observable} from 'rxjs';

export interface ErroresAutorizacion {
  id: number;
  idAutorizacionRegistro: number;
  idEmpleado: number;
  idEstadoCorreccion: number;
  idTipoDocumento: number;
  idEmpleadoCorreccion: number;
  correcciones: string;
}

export abstract class ErroresAutorizacionData {
  abstract get(): Observable<ErroresAutorizacion[]>;
  abstract post(camposConErrores: ErroresAutorizacion): Observable<string>;
  abstract getCamposConErrores(idAutorizacionRegistro: number, idTipoDocumento: number): Observable<ErroresAutorizacion>;
  abstract putEstadoCorreccion(idErrorAutorizacion: number): Observable<any>;
}

