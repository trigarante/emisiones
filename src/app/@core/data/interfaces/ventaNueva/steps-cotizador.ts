import {Observable} from 'rxjs';

export interface StepsCotizador {
  idSolicitud: number;
  idProductoCliente: number;
  idCliente: number;
  idRegistro: number;
  recibos: number;
  pagos: number;
}

export abstract class StepsCotizadorData {
  abstract get(): Observable<StepsCotizador[]>;
  abstract getById(id: number): Observable<StepsCotizador>;
  abstract getByIdRegistro(idRegistro: number): Observable<StepsCotizador>;
}
