import {Observable} from 'rxjs';

export interface Inspecciones {
  id: number;
  idRegistro: number;
  idEstadoInspeccion: number;
  cp?: string;
  idColonia?: number;
  fechaInspeccion?: Date;
  kilometraje?: number;
  placas?: number;
  urlPreventa?: number;
  urlPostventa?: string;
  archivo?: string;
  archivoRegistro?: string;
  comentarios?: string;
  estado?: string;
  asenta?: string;
  poliza?: string;
  primaNeta?: number;
  calle?: string;
  numeroPlacas?: string;
  numInt: number;
  numExt: number;
}

export abstract class InspeccionesData {

  abstract get(): Observable<Inspecciones[]>;

  abstract post(Inspeccion): Observable<Inspecciones>;

  abstract postOne(Inspeccion): Observable<Inspecciones>;

  abstract getInspeccionById(idInspeccion): Observable<Inspecciones>;

  abstract put(idInspeccion, Inspeccion): Observable<Inspecciones>;

  abstract postOnePeru(Inspeccion): Observable<Inspecciones>;

}
