import {Observable} from 'rxjs';
export interface ValidacionTelefono {
  telefono: string;
}
export abstract class ValidacionTelefonoData {
  abstract post(telefono): Observable<ValidacionTelefono>;

}
