export interface Tarjeta {
  estatusFinanzas: string;
  primaNeta: string;
  estatusPago: string;
  numeroTarjeta: string;
  titular: string;
  carrier: string;
  bancoTarjeta: string;
  tipoTarjeta: string;
  mesesIntereses: string;
  codigo: string;
  fechaVencimiento: Date;
  notas: string;
  comentarios: string;
  notasFinanzas: string;
  registrarPoliza: string;
}
