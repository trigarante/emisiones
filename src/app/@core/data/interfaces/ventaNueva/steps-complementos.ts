export interface StepsComplementos {
  idRegistro: number;
  idRegistroComplemento: number;
  idReciboComplemento?: number;
  idPagoComplemento?: number;
}
