import {Observable} from 'rxjs';

export interface Solicitudes {
  id: number;
  idCotizacionAli: number;
  cotizo: string;
  idEmpleado: number;
  idEstadoSolicitud: number;
  idSubArea: number;
  idSubRamo: number;
  peticion: any;
  respuesta: any;
  estado: string;
  fechaSolicitud: string;
  comentarios: string;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  numeroProspecto: string;
  nombreProspecto: string;
  correoProspecto: string;
  idEtiquetaSolicitud: number;
  idSubetiquetaSolicitud: number;
  idFlujoSolicitud: number;
  etiquetaSolicitud: string;
  flujoSolicitud: string;
  idTipoContacto: number;
  idProducto: number;
}

export interface SolicitudesView {
  id: number;
  idCotizacionAli: number;
  idEmpleado: number;
  idEstadoSolicitud: number;
  fechaSolicitud: Date;
  comentarios: string;
  idCotizacion: number;
  peticion: string;
  respuesta: string;
  fechaCotizacion: Date;
  fechaActualizacion: Date;
  estado: string;
  activo: number;
  idUsuario: number;
  idBanco: number;
  idCandidato: number;
  idPuesto: number;
  idTipoPuesto: number;
  puestoDetalle: string;
  idSubarea: number;
  subArea: string;
  idPrecandidato: number;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  idProducto: number;
  idTipoContacto: number;
  idProspecto: number;
  idSubRamo: number;
  nombreProspecto: string;
  correoProspecto: string;
  numeroProspecto: string;
  idEtiquetaSolicitud: number;
  idFlujoSolicitud: number;
  etiquetaSolicitud: string;
  etiquetaSolicitudActivo: number;
  idLlamada: number;
}

export interface Solicitud {
  id: number;
  idCotizacionAli: number;
  idEmpleado: number;
  idEstadoSolicitud: number;
  idEtiquetaSolicitud: number;
  idSubetiquetaSolicitud: number;
  idFlujoSolicitud: number;
  comentarios: string;
  idFase: number;
  idSubarea: number;
}

export interface SolicitudesVN {
  id: number;
  idCotizacionAli: number;
  idEmpleado: number;
  fechaSolicitud: string;
  estado: string;
  idSubArea: number;
  subArea: string;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  idProducto: number;
  idProspecto: number;
  nombreProspecto: string;
  numeroProspecto: string;
  etiquetaSolicitud: string;
  idTipoPeticion: number;
  idSolicitudes: number;
}


export interface EtiquetaSolicitud {
  idEstadoSolicitud: number;
  idEtiquetaSolicitud: string;
  idSubetiquetaSolicitud: number;
  comentarios: string;
}

export abstract class SolicitudesVNData {
  abstract get(): Observable<Solicitudes[]>;
  abstract getTipo(tipo: string): Observable<Solicitudes[]>;
  abstract getByIdCotizacionAli(idCotizacionAli: number): Observable<Solicitudes>;
  abstract getSubsecuentes(tipo: string): Observable<Solicitudes[]>;
  abstract getSubsecuentesPeru(tipo: string): Observable<Solicitudes[]>;
  abstract getCobranza(tipo: string): Observable<Solicitudes[]>;
  abstract getFechas(tipo: string, fechaInicial: number, fechaFinal: number): Observable<SolicitudesVN[]>;
  abstract post(solicitud): Observable<Solicitudes>;
  abstract postWs(solicitud): Observable<Solicitudes>;
  abstract postCreateSolicitudWsSocket(solicitud): Observable<any>;
  abstract postSocket(json): Observable<JSON>;
  abstract put(idSolicitud, solicitud): Observable<Solicitudes>;
  abstract putSolicitud(idSolicitud, solicitud): Observable<string>;
  abstract updateEstadoSolicitud(idSolicitud: number, estadoSolicitud: number): Observable<any>;
  abstract updateEmpleadoSolicitud(idSolicitud: number, idEmpleado: number): Observable<any>;
  abstract getSolicitudesById(idSolicitud): Observable<Solicitudes>;
  abstract getSolicitudById(idSolicitud): Observable<Solicitud>;
  abstract getSolicitudByIdNode(idSolicitud): Observable<any>;
  abstract getSolicitudByIdSinJson(idSolicitud): Observable<Solicitudes>;
  abstract getSolicitudesPeru(tipo: string): Observable<Solicitudes[]>;
  abstract entryIdCliente(value);
  abstract returnIdCliente(): Observable<number>;
  abstract entryIdRegistroPoliza(value);
  abstract returnIdRegistroPoliza(): Observable<number>;
  abstract entryURLType(value);
  abstract returnentryURLType(): Observable<number>;
  abstract entryIdSolicitud(value);
  abstract returnIdSolicitud(): Observable<number>;
  abstract putEstadoEtiquetaSubetiqueta(json: any): Observable<any>;
  // Sockets
  abstract postSolicitudSocket(data): Observable<SolicitudesVN>;
  abstract putEstadoSolicitudSocket(data): Observable<SolicitudesVN>;
  abstract reasignarEmpleadoSocket(data): Observable<SolicitudesVN>;
  abstract removeSolicitudSockets(data): Observable<SolicitudesVN>;
  abstract updateProspectoSockets(data): Observable<SolicitudesVN>;
}
