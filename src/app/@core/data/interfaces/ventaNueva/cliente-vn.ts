import {Observable} from 'rxjs';
import {ClientePeru} from './cliente-peru';

export interface ClienteVn {
  id: number;
  idPais: number;
  personaMoral: number;
  nombre: string;
  paterno: string;
  materno: string;
  cp: number;
  calle: string;
  numInt: string;
  numExt: string;
  idColonia: number;
  colonia: string;
  genero: string;
  telefonoFijo: string;
  telefonoMovil: string;
  correo: string;
  fechaNacimiento: string;
  fechaRegistro: string;
  curp: string;
  rfc: string;
  contrasenaApp: string;
  nombrePaises: string;
  razonSocial: string;
  archivo: string;
  archivoSubido: number;
  dni: string;
  ruc: string;
  idColoniaPeru: number;
}

export abstract class ClientesData {
  abstract get(): Observable <ClienteVn[]>;
  abstract getFinanzas(): Observable<ClienteVn[]>;
  abstract getClienteFinanzas(): Observable<ClienteVn[]>;
  abstract getClienteFinanzasById(idClienteFinanzas): Observable<ClienteVn>;
  abstract getClienteById(idCliente): Observable <ClienteVn>;
  abstract getClienteByIdADM(idCliente): Observable <ClienteVn>;
  abstract getFinanzasClienteById(idCliente): Observable <ClienteVn>;
  abstract getClienteByIdEmision(idCliente): Observable <ClienteVn>;
  abstract getClientePeruById(idCliente: number): Observable<ClientePeru>;
  abstract curpExist(curp);
  abstract rfcExist(rfc);
  abstract post(cliente): Observable <ClienteVn>;
  abstract postClienteFinanzas(cliente): Observable <ClienteVn>;
  abstract postSocket(json): Observable<JSON>;
  abstract put(idCliente, cliente): Observable <ClienteVn>;
  abstract putArchivo(idCliente: number, idCarpetaDrive: string): Observable<any>;
  abstract putArchivoSubido(idCliente: number, idArchivoSubido: number): Observable<any>;
  abstract dniExist(dni: string): Observable<ClientePeru>;
  abstract getClienteByCellPhone(celular: string): Observable<ClienteVn[]>;
}

export abstract class ClienteVentaNUevaData {
  abstract get(): Observable<ClienteVn[]>;

  abstract post(clienteVentaNueva): Observable<ClienteVn>;

  abstract getClienteVentaNuevaById(idClienteVentaNueva): Observable<ClienteVn>;

  abstract put(idClienteVentaNueva, clienteVentaNueva): Observable<ClienteVn>;
}
