export interface MiEstadoCuenta {
  id: number;
  poliza: string;
  fechaInicio: string;
  fechaRegistro: string;
  idEmpleado: number;
  nombreEjecutivo: string;
  nombreComercial: string;
  idSubarea: number;
  subarea: string;
  comisionable: boolean;
  pagado: boolean;
  autorizado: boolean;
  verificado: boolean;
  verificadoCliente: number;
  verificadoProducto: number;
  verificadoRegistro: number;
  verificadoPago: number;
  aplicado: boolean;
  fechaAplicacionAseg: string;
  fechaAplicacion: string;
  fechaComision: string;
  fechaPago: string;
}
