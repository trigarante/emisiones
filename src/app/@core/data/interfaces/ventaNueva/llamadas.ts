import {Observable} from 'rxjs';

export interface Llamadas {
  numero: number;
  fechaRegristo: Date;
  fecha: Date;
  idSolicitud: number;
}

export abstract class LlamadasData {
  abstract get(): Observable<Llamadas[]>;

  abstract post(idUsuario, llamadas): Observable<Llamadas>;
}
