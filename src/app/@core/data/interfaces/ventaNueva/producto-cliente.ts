import {Observable} from 'rxjs';

export interface ProductoCliente {
  length: boolean;
  id: number;
  idCliente: number;
  idSolicitud: number;
  idCategoriaSocio: number;
  datos: any;
  idLlamadaEntrantes?: any;
  correo?: string;
  idEmision: number; // renovacion
}
export interface ProductoClienteOnline {
  length: boolean;
  id: number;
  idCliente: number;
  idSolictud: number;
  idCategoriaSocio: number;
  datos: any;
  idLlamadaEntrantes?: any;
  correo?: string;
  idEmision: number; // renovacion
  idSubRamo?: number;
}

export abstract class ProductoClienteData {
  abstract get(): Observable<ProductoCliente[]>;
  abstract getProductoClienteById(idProductoCliente): Observable<ProductoCliente>;
  abstract getProductoClienteByIdOnline(idProductoCliente): Observable<ProductoClienteOnline>;
  abstract getProductoClienteByidSolicitud(idSolicitud): Observable<ProductoCliente>;
  abstract getProductoClienteByIdNormal(idProductoCliente): Observable<ProductoCliente>;
  abstract getProductoClienteByIdADM(idProductoCliente): Observable<ProductoCliente>;
  abstract post(productoCliente): Observable<ProductoCliente>;
  abstract put(idProductoCliente, productoCliente): Observable<ProductoCliente>;
  abstract putCliente(idClienteViejo, idClienteNuevo): Observable<ProductoCliente>;
  abstract putProductoCliente(id, idLlamadaEntrantes);
  abstract putPD(idProductoCliente, idEmision): Observable<any>;
  abstract entryAseguradora(aseguradora);
  abstract returnAseguradora(): Observable<number>;
  abstract entryIdProductoCliente(idProductoCliente);
  abstract returnIdProductoCliente(): Observable<number>;
  abstract entryIdRecibo(idRecibo);
  abstract entryIdCliente(idRecibo);
  abstract returnIdCliente(): Observable<number>;
  abstract returnIdRecibo(): Observable<number>;
}
