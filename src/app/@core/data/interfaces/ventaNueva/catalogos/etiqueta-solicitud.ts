import {Observable} from 'rxjs';
export interface EtiquetaSolicitud {
  id: number;
  idEstadoSolicitud: number;
  descripcion: string;
  activo: number;
}
export abstract class EtiquetaSolicitudData {
  abstract get(): Observable<EtiquetaSolicitud[]>;
  abstract getActivo(): Observable<EtiquetaSolicitud[]>;
  abstract post(turno): Observable<EtiquetaSolicitud>;
  abstract getEtiquetaById(idTurno): Observable<EtiquetaSolicitud>;
  abstract put(idTurno, turno): Observable<EtiquetaSolicitud>;
  abstract getByEstadoSolicitud(idEstadoSolicitud: number): Observable<EtiquetaSolicitud[]>;
}

