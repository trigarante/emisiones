import {Observable} from 'rxjs';

export interface BancoVn {
  id: string;
  nombre: string;
  activo: number;
  idCarrier: number;
}

export interface BancoExterno {
  id_banco: string;
  nombre: string;
  abreviacion: number;
}


export abstract class BancoVnData {
  abstract get(): Observable<BancoVn[]>;

  abstract getActivos(activos: number): Observable<BancoVn[]>;

  abstract getBancoById(id): Observable<BancoVn>;

  abstract post(banco): Observable<BancoVn>;

  abstract put(id, banco): Observable<BancoVn>;

  abstract getexterno(): Observable<BancoExterno[]>;
}
