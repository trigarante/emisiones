import {Observable} from 'rxjs';

export interface TipoPagoVn {
  id: number;
  cantidadPagos: number;
  tipoPago: string;
  activo: number;
}

export abstract class TipoPagoData {
  abstract getAll(): Observable<TipoPagoVn[]>;
  abstract get(): Observable<TipoPagoVn[]>;
  abstract getAllReno(): Observable<any[]>;
  abstract getPeru(): Observable<TipoPagoVn[]>;
  abstract post(tipoPago): Observable<TipoPagoVn>;
  abstract getTipoPagoById(idTipoPago): Observable<TipoPagoVn>;
  abstract put(idTipoPago, tipoPago): Observable<TipoPagoVn>;
  abstract postSocket(json): Observable<JSON>;
}
