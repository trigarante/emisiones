import {Observable} from 'rxjs';

export interface EndosoFinanzas {
  id: number;
  idTipoEndosoFinanzas: number;
  cantidad: number;
  fechaEndoso: Date;
  idRegistro: number;
  activo: number;
  // Datos de la vista
  tipoEndoso?: string;
  poliza?: string;
  nombre?: string;
  apellidoPaterno?: string;
  apellidoMaterno?: string;
}

export abstract class EndosoFinanzaData {
  abstract get(): Observable<EndosoFinanzas[]>;
  abstract post(endososFinanzas): Observable<EndosoFinanzas>;
  abstract getEndosoFinanzasById(idEndosoFinanzas): Observable<EndosoFinanzas>;
  abstract put(idEndosoFinanzas, endososFinanzas): Observable<EndosoFinanzas>;
}
