import {Observable} from 'rxjs';

export interface TipoIngreso {
  id: number;
  descripcion?: string;
  ingresos: number;
  activo: number;

}

export abstract class TipoIngresosData {
  abstract get(): Observable<TipoIngreso[]>;

  abstract post(tipoIngreso: TipoIngreso): Observable<TipoIngreso>;

  abstract getTipoIngresoById(idTipoIngreso): Observable<TipoIngreso>;

  abstract put(idTipoIngresos, tipoIngresos): Observable<TipoIngreso>;
}
