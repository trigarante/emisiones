export interface ClavesSocios {
  id: number;
  idSocio: number;
  clave: string;
  activo: number;
}
