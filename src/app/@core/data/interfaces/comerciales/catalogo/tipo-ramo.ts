import {Observable} from 'rxjs';
export interface TipoRamo {
  id: number;
  tipo: string;
  activo: number;
}
export abstract class TipoRamo {
  abstract get(): Observable<TipoRamo[]>;
  abstract getByActivo(activo): Observable<TipoRamo[]>;
  abstract post(tipoRamo): Observable<TipoRamo>;
  abstract postSocket(json): Observable<JSON>;

  abstract getTipoRamoById(idTipoRamo): Observable<TipoRamo>;

  abstract put(idTipoRamo, tipoRamo): Observable<TipoRamo>;
}
