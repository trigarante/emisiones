import {Observable} from 'rxjs';
// import {Paises} from "../../catalogos/paises";

export interface TipoCometencia {
  id: number;
  tipo: string;
  activo: number;
}

export abstract class TipoCompetencia {
  abstract get(): Observable<TipoCometencia[]>;

  abstract post(tipoCompetencia): Observable<TipoCometencia>;
  abstract postSocket(json): Observable<JSON>;
  abstract getByActivo(activo): Observable<TipoCometencia[]>;

  abstract getTipoCompetenciaById(idTipoCompetencia): Observable<TipoCometencia>;

  abstract put(idTipoCompetencia, tipoCompetencia): Observable<TipoCometencia>;
}
