import {Observable} from 'rxjs';
export interface TipoSubramo {
  id: number;
  tipo: string;
  activo: number;
}
export abstract class TipoSubramo {
  abstract get(): Observable<TipoSubramo[]>;

  abstract post(tipoSubramo): Observable<TipoSubramo>;

  abstract getTipoSubramoById(idTipoSubramo): Observable<TipoSubramo>;

  abstract put(idTipoSubramo, tipoSubramo): Observable<TipoSubramo>;

  abstract getByActivo(activo): Observable<TipoSubramo[]>;

  /** Funcion que trae los id 1, 2, 9 **/
  abstract getVN(): Observable<TipoSubramo[]>;
}
