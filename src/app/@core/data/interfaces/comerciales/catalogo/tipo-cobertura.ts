import {Observable} from 'rxjs';

export interface TipoCobertura {
  id: number;
  cobertura: string;
  activo: number;
}

export abstract class TipoCobertura {
  abstract get(): Observable<TipoCobertura[]>;

  abstract getByActivo(activo): Observable<TipoCobertura[]>;

  abstract post(tipocobertura): Observable<TipoCobertura>;
  abstract postSocket(json): Observable<JSON>;

  abstract getTipoCoberturaById(idTipoCobertura): Observable<TipoCobertura>;

  abstract put(idTipoCobertura, tipocobertura): Observable<TipoCobertura>;
}
