import {Observable} from 'rxjs';

export interface TipoProducto {
  id: number;
  tipo: string;
  activo: number;
}

export abstract class TipoProducto {
  abstract get(): Observable<TipoProducto[]>;
  abstract getByActivo(activo): Observable<TipoProducto[]>;
  abstract post(tipoProducto): Observable<TipoProducto>;
  abstract postSocket(json): Observable<JSON>;

  abstract getTipoProducto(idTipoProducto): Observable<TipoProducto>;

  abstract put(idTipoProducto, tipoProducto): Observable<TipoProducto>;
}
