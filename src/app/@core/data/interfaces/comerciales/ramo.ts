import {Observable} from 'rxjs';
import {TipoRamo} from "./catalogo/tipo-ramo";
export interface Ramo {
  id: number;
  idSocio: number;
  tipoRamo: string;
  idTipoRamo: number;
  descripcion: string;
  prioridad: number;
  prioridades: string;
  activo: number;
  alias: string;
  idEstado: number;
  idEstadoSocio: number;
  estado: string;
  nombreComercial: string;
}
export abstract class Ramo {
  abstract get(): Observable<Ramo[]>;
  abstract post(ramo): Observable<Ramo>;
  abstract postSocket(json): Observable<JSON>;
  abstract getRamoById(idRamo): Observable<Ramo>;
  abstract getRamoByIdSocio(idSocio: number): Observable<Ramo[]>;
  abstract put(idRamo, ramo): Observable<Ramo>;
  abstract getTipoRamoById(idTipoRamo): Observable<TipoRamo>;
}
