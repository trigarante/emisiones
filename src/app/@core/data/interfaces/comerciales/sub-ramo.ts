import {Observable} from 'rxjs';
export interface SubRamo {
  id: number;
  idRamo: number;
  idTipoSubRamo: number;
  prioridad: number;
  descripcion: string;
  activo: number;
  idEstado: number;
  idEstadoSocio: number;
  alias: any;
  estado: any;
  nombreComercial: any;
  prioridades: any;
  idSocio: number;
  tipoSubRamo: any;
  tipoRamo: any;
}
export abstract class SubRamo {
  abstract get(): Observable<SubRamo[]>;
  abstract getSubramoById(idSubramo): Observable<SubRamo>;
  abstract getSubramosByIdRamo(idRamo: number): Observable<SubRamo[]>;
  abstract getSubramosByIdRamoAndIdTipoSubramo(idRamo: number, idTipoSubramo: number): Observable<SubRamo[]>;
  abstract post(subramo): Observable<SubRamo>;
  abstract postSocket(json): Observable<JSON>;
  abstract getTipoAndEstado(idTipoSubramo, idEstadoSocio): Observable<SubRamo[]>;
  abstract getTipoAndAlias(idTipoSubramo, alias): Observable<SubRamo>;
  abstract put(idSubramo, subramo): Observable<SubRamo>;
}
