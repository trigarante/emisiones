export interface AvanceEjecutivos {
  idEmpleado: number;
  ejecutivo: string;
  base: number;
  renovadas: number;
  reexpedidas: number;
  migradas: number;
  gestion: number;
  canceladas: number;
}
