export interface AvanceReno {
  poliza: string;
  fechaInicio: string;
  fechaFin: string;
  estado: string;
  estadoPago: string;
  primerLlamada: string;
  ultimaLlamada: string;
  ultimaEtiqueta: string;
  llamadas: number;
  ultimoComentario: string;
}
