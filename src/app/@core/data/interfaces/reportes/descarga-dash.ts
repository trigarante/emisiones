export interface DescargaDash {
  id: number;
  subarea: string;
  ejecutivo: string;
  llmadas: number;
}
