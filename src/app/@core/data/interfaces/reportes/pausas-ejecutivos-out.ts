export interface PausasEjecutivosOut {
  idEmpleado: number;
  empleado: string;
  idSubarea: number;
  subarea: string;
  pausa: string;
  tiempo: number;
}
