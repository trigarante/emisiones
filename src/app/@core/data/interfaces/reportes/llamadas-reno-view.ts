export interface LlamadasRenoView {
  id: number;
  estado: string;
  subarea: string;
  ejecutivo: string;
  numero: string;
  horaInicio: string;
  horaFinal: string;
  duracion: string;
  etiqueta: string;
  subEtiqueta: string;
}

