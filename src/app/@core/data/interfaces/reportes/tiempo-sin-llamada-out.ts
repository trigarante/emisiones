export interface TiempoSinLlamadaOut {
  id: number;
  idSubarea: number;
  empleado: string;
  subarea: string;
  tiempo: number;
}
