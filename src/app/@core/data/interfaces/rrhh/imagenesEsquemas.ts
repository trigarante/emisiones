export interface ImagenesEsquemas {
  id: number;
  idArea: number;
  links: string;
  activo: number;
}
