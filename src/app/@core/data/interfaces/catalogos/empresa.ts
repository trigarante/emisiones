export interface Empresa {
  id: number;
  nombre: string;
  codigo: string;
  activo: number;
}
