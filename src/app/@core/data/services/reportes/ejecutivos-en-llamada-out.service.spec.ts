import { TestBed } from '@angular/core/testing';

import { EjecutivosEnLlamadaOutService } from './ejecutivos-en-llamada-out.service';

describe('EjecutivosEnLlamadaOutService', () => {
  let service: EjecutivosEnLlamadaOutService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EjecutivosEnLlamadaOutService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
