import { Injectable } from '@angular/core';
import {environment} from "../../../../../environments/environment";
import {AsignacionCobranza} from "../../interfaces/reportes/asignacion-cobranza";
import {LlamadasRenoView} from "../../interfaces/reportes/llamadas-reno-view";
import {PNC} from "../../interfaces/reportes/pnc";
import {HttpClient} from "@angular/common/http";
import {LlamadasReno} from "../../interfaces/reportes/llamadas-reno";
import {AvanceEjecutivos} from "../../interfaces/reportes/avance-ejecutivo";
import {Observable} from "rxjs";
import {AvanceReno} from "../../interfaces/reportes/avance-reno";

@Injectable({
  providedIn: 'root'
})
export class DescargasService {

  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;
  }
  getPnc(fechaInicio: number, fechaFin: number): Observable<PNC[]> {
    return this.http.get<PNC[]>(this.baseURL + 'v1/descargas/pnc/' + fechaInicio + '/' + fechaFin);
  }
  getAvance(fechaInicio: number, fechaFin: number): Observable<AvanceReno[]> {
    return this.http.get<AvanceReno[]>(this.baseURL + 'v1/descargas/avance/' + fechaInicio + '/' + fechaFin);
  }
  getAvanceEjecutivos(fechaInicio: number, fechaFin: number): Observable<AvanceEjecutivos[]> {
    return this.http.get<AvanceEjecutivos[]>(this.baseURL + 'v1/descargas/avance-ejecutivos/' + fechaInicio + '/' + fechaFin);
  }
  getLlamadasReno(fechaInicio: number, fechaFin: number): Observable<LlamadasReno[]> {
    return this.http.get<LlamadasReno[]>(this.baseURL + 'v1/descargas/llamadas/' + fechaInicio + '/' + fechaFin);
  }
  getAsignacionCobranza(): Observable<AsignacionCobranza[]> {
    return this.http.get<AsignacionCobranza[]>(this.baseURL + 'v1/descargas/asignacion-cobranza');
  }
  getLlamadasRenoEntrantes(fechaInicio: number, fechaFin: number): Observable<LlamadasRenoView[]> {
    return this.http.get<LlamadasRenoView[]>(this.baseURL + 'v1/descargas/llamadas-reno-salida/' + fechaInicio + '/' + fechaFin);
  }
  getLlamadasRenoSalidas(fechaInicio: number, fechaFin: number): Observable<LlamadasRenoView[]> {
    return this.http.get<LlamadasRenoView[]>(this.baseURL + 'v1/descargas/llamadas-reno-entrada/' + fechaInicio + '/' + fechaFin);
  }
}
