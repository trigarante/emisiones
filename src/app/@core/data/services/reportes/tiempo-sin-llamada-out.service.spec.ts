import { TestBed } from '@angular/core/testing';

import { TiempoSinLlamadaOutService } from './tiempo-sin-llamada-out.service';

describe('TiempoSinLlamadaOutService', () => {
  let service: TiempoSinLlamadaOutService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TiempoSinLlamadaOutService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
