import { TestBed } from '@angular/core/testing';

import { ImagenesGacetaService } from './imagenes-gaceta.service';

describe('ImagenesGacetaService', () => {
  let service: ImagenesGacetaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ImagenesGacetaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
