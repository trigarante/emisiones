import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {ImagenesEsquemas} from "../../interfaces/rrhh/imagenesEsquemas";


@Injectable({
  providedIn: 'root',
})
export class ImagenesEsquemasService {
  url: string;

  constructor(private http: HttpClient) {
    this.url = environment.GLOBAL_SERVICIOS_OPERACIONES + 'v1/imagenes-esquema';
  }

  getByArea(idArea: number): Observable<ImagenesEsquemas> {
    return this.http.get<ImagenesEsquemas>(this.url + '/' + idArea);
  }

  getAreas(): Observable<any> {
    return this.http.get(this.url + '/areas/' + sessionStorage.getItem('Empleado'));
  }
}
