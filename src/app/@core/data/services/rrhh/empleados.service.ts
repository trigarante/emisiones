import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Empleados} from '../../interfaces/rrhh/empleados';
import {Observable} from 'rxjs';
import {environment} from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmpleadosService {

  private baseURL;
  private socketURL;
  title: any[] = [];
  private URL = environment.apiUrl + 'administracion-personal/empleados';
  private URL2 = environment.CORE_VN2;

  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }
  getPlazasHijo() {
    const headers = new HttpHeaders().set('id', sessionStorage.getItem('Empleado'));
    return this.http.get<any[]>(this.URL2 + `/administracion-personal/empleados/getPlazasHijo`, {headers});
  }
  get(): Observable<Empleados[]> {
    return this.http.get<Empleados[]>(this.baseURL + 'v1/empleados');
  }

  getfiltrado(): Observable<Empleados[]> {
    return this.http.get<Empleados[]>(this.baseURL + 'v1/empleados/get-empleados');
  }

  getEmpleadosBaja(): Observable<Empleados[]> {
    return this.http.get<Empleados[]>(this.baseURL + 'v1/empleados/get-empleados-baja');
  }

  saveEmpleado(empleados: Empleados, idPrecandidato, idEtapa): Observable<Empleados> {
    return this.http.post<Empleados>(this.baseURL + 'v1/empleados/' + idPrecandidato + '/' + idEtapa, empleados);
  }

  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'empleados/saveEmpleado', json); //
  }
  postSocketImss(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'empleadosimss/saveEmpleadoImss', json);
  }
  postSocketBaja(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'empleadosbaja/saveEmpleadoBaja', json);
  }

  saveFoto(foto, id): any {
    const archivo = new FormData();
    archivo.append('foto', foto, 'imagen empleado');
    return this.http.put(this.baseURL + 'v1/empleados/actualizar-foto/' + id, archivo);
  }

  saveDocumento(id, documentId, option): any {
    return this.http.put(this.baseURL + 'v1/empleados/documento/' + id + '/' + documentId + '/' + option
      + '/' + sessionStorage.getItem('Empleado'), null);
  }

  getFoto(id): Observable<ArrayBuffer> {
    return this.http.get(this.baseURL + 'v1/empleados/get-foto/' + id, { responseType: 'arraybuffer'});
  }

  getEmpleadoById(idEmpleados): Observable<Empleados> {
    return this.http.get<Empleados>(this.baseURL + 'v1/empleados/' + idEmpleados);
  }
  getEmpleadoEjecutivoById(idEmpleados): Observable<Empleados> {
    return this.http.get<Empleados>(this.baseURL + 'v1/empleados/empleado-ejecutivo/' + idEmpleados);
  }

  put(id, empleado, fechaIngreso): Observable<Empleados> {
    return this.http.put<Empleados>( this.baseURL + 'v1/empleados/' + id + '/' + fechaIngreso
      + '/' + sessionStorage.getItem('Empleado'), empleado);
  }
  putEstadoSolicitudBaja(id, empleado): Observable<Empleados> {
    return this.http.put<Empleados>( this.baseURL + 'v1/empleados/estado-solicitud-baja/' + id, empleado);
  }

  getBySubarea(idSubarea): Observable<Empleados[]> {
    return this.http.get<Empleados[]>(this.baseURL + 'v1/empleados/get-by-id-subarea/' + idSubarea);
  }

  bajaEmpleado(idPrecandidato, idEmpleado, datosBaja, fechaBaja): Observable<Empleados> {
    return this.http.put<Empleados>(this.baseURL + 'v1/empleados/baja-empleado/' + idPrecandidato + '/' + idEmpleado
      + '/' + fechaBaja + '/' + sessionStorage.getItem('Empleado'), datosBaja);
  }

  reactivarEmpleado(idEmpleado, idPrecandidato, fechaIngreso): Observable<Empleados> {
    return this.http.put<Empleados>(this.baseURL + 'v1/empleados/reactivar/' + idEmpleado + '/' + idPrecandidato + '/' +
      fechaIngreso + '/' + sessionStorage.getItem('Empleado'), idEmpleado);
  }

  editarRecontratableEmp(idEmpleado, recontratable): Observable<Empleados> {
    return this.http.put<Empleados>(this.baseURL + 'v1/empleados/recontrtar/' + idEmpleado + '/' + recontratable
      + '/' + sessionStorage.getItem('Empleado'), idEmpleado);
  }

  altaImss(idEmpleado, fechaIngreso, fechaAlta, empleado: Empleados): Observable<Empleados> {
    return this.http.put<Empleados>(this.baseURL + 'v1/empleados/alta-imss/' + idEmpleado + '/' + fechaIngreso + '/' + fechaAlta
      + '/' + sessionStorage.getItem('Empleado'), empleado);
  }

  finiquito(idEmpleado, fechaFiniquito, idEstadoFiniquito, montoFiniquito): Observable<Empleados> {
    return this.http.put<Empleados>(this.baseURL + 'v1/empleados/finiquito/' + idEmpleado + '/' + fechaFiniquito + '/' + idEstadoFiniquito +
      '/' + montoFiniquito + '/' + sessionStorage.getItem('Empleado'), idEmpleado);
  }

  getEjecutivosActivos(usuario: string): Observable<Empleados[]> {
    return this.http.get<Empleados[]>(this.baseURL + 'v1/empleados/ejecutivos-activos/' + usuario);
  }
  // getEmpleadobySubareabyidUsuario(idSubarea, idUsuario): Observable<Empleados> {
  // return this.http.get<Empleados>(this.baseURL + 'v1/empleados/idSubarea/' + idSubarea + '/idUsuario/' + idUsuario);
  // }

  getEmpleadosByEstado(estado: number): Observable<any[]> {
    const headers = new HttpHeaders().set('estado', estado.toString());
    return this.http.get<any[]>(this.baseURL + `v1/empleados/`, { headers });
  }
}
