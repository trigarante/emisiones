import { TestBed } from '@angular/core/testing';

import { VisualizacionesGacetaService } from './visualizaciones-gaceta.service';

describe('VisualizacionesGacetaService', () => {
  let service: VisualizacionesGacetaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VisualizacionesGacetaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
