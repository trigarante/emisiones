import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Gaceta} from '../../interfaces/rrhh/gaceta';
import {environment} from "../../../../../environments/environment";

@Injectable({
  providedIn: 'root',
})
export class GacetaService {
  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.CORE_ESCUCHA + '/desarrollo-organizacional/gaceta';
  }

  post(infoGaceta, imagenesGaceta): Observable<any> {
    const formData = new FormData();
    for (const file of imagenesGaceta) {
      formData.append('file', file, file.name);
    }
    formData.append('datos', JSON.stringify(infoGaceta));
    return this.http.post<any>(this.baseURL + '/subir-gaceta', formData);
  }

  getDelMes(): Observable<Gaceta[]> {
    return this.http.get<Gaceta[]>(this.baseURL + '/del-mes');
  }

}
