import { Injectable } from '@angular/core';
import Swal, {SweetAlertResult} from 'sweetalert2';
import {environment} from '../../../../../environments/environment';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class NotificacionesService {

  constructor(private snackBar: MatSnackBar) { }

  carga(texto?: string): Promise<any> {
    // @ts-ignore
    return Swal.fire({
      title: (texto || 'Actualizando información') + ', por favor espere',
      showConfirmButton: false,
      allowOutsideClick: !environment.production,
      onBeforeOpen: () => {
        Swal.showLoading();
      }
    });
  }

  exito(texto?: string): Promise<any> {
    return Swal.fire({
      icon: 'success',
      title: 'Correcto',
      showConfirmButton: false,
      timer: 3000,
      text: '¡' + (texto || 'Se actualizó la información') + ' con éxito!',
      allowOutsideClick: !environment.production,
    });
  }

  exitoPersonalizado(html: any): Promise<any> {
    return Swal.fire({
      icon: 'success',
      title: 'Correcto',
      html,
      allowOutsideClick: !environment.production,
    });
  }

  informacion(texto: string): Promise<any> {
    return Swal.fire({
      icon: 'info',
      title: 'Información',
      text: texto,
      allowOutsideClick: !environment.production,
    });
  }

  error(texto?: string): Promise<any> {
    return Swal.fire({
      title: 'Error',
      text: texto || 'Hubo un error, favor de intentar nuevamente',
      icon: 'error',
      allowOutsideClick: !environment.production,
    });
  }

  advertencia(texto: string, title?: string): Promise<any> {
    return Swal.fire({
      title: title || 'Advertencia',
      text: texto,
      icon: 'warning',
      timer: 3000,
      showConfirmButton: false,
      allowOutsideClick: !environment.production,
    });
  }

  pregunta(text: string, title?: string, confirmButtonText?: string, cancelButtonText?: string): Promise<SweetAlertResult> {
    return Swal.fire({
      title: title || 'IMPORTANTE',
      text,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: confirmButtonText || 'Si, continuar',
      cancelButtonText: cancelButtonText || 'No, cancelar',
      allowOutsideClick: !environment.production,
    });
  }

  snackbar(texto: string, esAzul: boolean, duracionEnSegundos?: number) {
    this.snackBar.open(texto, 'Cerrar', {duration: duracionEnSegundos * 1000 || 3500, horizontalPosition: 'right', verticalPosition: 'bottom',
      panelClass: ['mat-toolbar', esAzul ? 'mat-primary' : 'mat-warn']});
  }

  cerrar() {
    return Swal.close();
  }

}
