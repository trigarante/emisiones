import { TestBed } from '@angular/core/testing';

import { FlujoLlamadaService } from './flujo-llamada.service';

describe('FlujoLlamadaService', () => {
  let service: FlujoLlamadaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FlujoLlamadaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
