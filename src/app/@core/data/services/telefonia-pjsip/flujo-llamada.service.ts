import { Injectable } from '@angular/core';
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root',
})
export class FlujoLlamadaService {

  // 0 = disponible
  // 1 =  en llamada
  // 2 = tipificando
  flujoLlamadaEvents = new Subject<number>();

  esucharFlujoLlamad() {
    return this.flujoLlamadaEvents.asObservable();
  }

  available() {
    this.flujoLlamadaEvents.next(1);
  }

  unAvailable() {
    this.flujoLlamadaEvents.next(0);
  }
}
