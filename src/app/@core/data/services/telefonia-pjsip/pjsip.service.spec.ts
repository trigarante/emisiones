import { TestBed } from '@angular/core/testing';

import { PjsipService } from './pjsip.service';

describe('PjsipService', () => {
  let service: PjsipService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PjsipService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
