import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {RegistroClienteEmision} from '../../interfaces/emisiones/cliente-emision';

@Injectable({
  providedIn: 'root',
})
export class ClienteEmisionService {

  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  postInOneClienteEmision(data): Observable<RegistroClienteEmision> {
    return this.http.post<RegistroClienteEmision>(this.baseURL + 'v1/registro-cliente-emision/all', data);
  }
}
