import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {InspeccionesEmisiones, InspeccionesEmisionesData} from '../../interfaces/emisiones/inpecciones-emisiones';

@Injectable({
  providedIn: 'root',
})
export class InspeccionesEmisionesService extends InspeccionesEmisionesData {
  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
  }

  get(): Observable<InspeccionesEmisiones[]> {
    return this.http.get<InspeccionesEmisiones[]>(this.baseURL + 'v1/inspecciones-emisiones');
  }

  post(Inspeccion: InspeccionesEmisiones): Observable<InspeccionesEmisiones> {
    return this.http.post<InspeccionesEmisiones>(this.baseURL + 'v1/inspecciones-emisiones/', Inspeccion);
  }
  postOne(Inspeccion: InspeccionesEmisiones): Observable<InspeccionesEmisiones> {
    return this.http.post<InspeccionesEmisiones>(this.baseURL + 'v1/inspecciones-emisiones/all/', Inspeccion);
  }

  getInspeccionById(idInspeccion): Observable<InspeccionesEmisiones> {
    return this.http.get<InspeccionesEmisiones>(this.baseURL + 'v1/inspecciones-emisiones/' + idInspeccion);
  }

  put(idInspeccion, Inspeccion: InspeccionesEmisiones): Observable<InspeccionesEmisiones> {
    return this.http.put<InspeccionesEmisiones>(this.baseURL + 'v1/inspecciones-emisiones/' + idInspeccion + '/'
      + sessionStorage.getItem('Empleado') , Inspeccion);
  }
}
