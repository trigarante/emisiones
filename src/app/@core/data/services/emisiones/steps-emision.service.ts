import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import { Observable} from 'rxjs';
import {StepsEmisionView, StepsEmisionViewData} from '../../interfaces/emisiones/steps-emision.view';


@Injectable()
export class StepsEmisionService extends StepsEmisionViewData {

  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<StepsEmisionView[]> {
    return this.http.get<StepsEmisionView[]>(this.baseURL + 'v1/step-cotizador');
  }

  getById(idEmisiones: number): Observable<StepsEmisionView> {
    return this.http.get<StepsEmisionView>(this.baseURL + 'v1/step-emision/' + idEmisiones);
  }
  getByIdRegistro(idRegistro: number): Observable<StepsEmisionView> {
    return this.http.get<StepsEmisionView>(this.baseURL + 'v1/step-emision/registro/' + idRegistro);
  }
}
