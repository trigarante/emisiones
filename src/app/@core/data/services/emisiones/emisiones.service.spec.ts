import { TestBed } from '@angular/core/testing';

import { EmisionesService } from './emisiones.service';

describe('EmisionesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EmisionesService = TestBed.get(EmisionesService);
    expect(service).toBeTruthy();
  });
});
