import { Injectable } from '@angular/core';
import {Emisiones, EmisionesData, EmisionesViews} from '../../interfaces/emisiones/emisiones';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {AplicacionView} from '../../interfaces/finanzas/aplicacionView';

@Injectable({
  providedIn: 'root',
})
export class EmisionesService {
  private baseURL;
  private CORE_VN2;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.CORE_VN2 = environment.CORE_VN2;

  }

  // NEW

  getAllRegistrosEmision(): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.CORE_VN2 + 'emisiones/getAllEmision');
  }
  getDetallesPolizaEmisiones(id): Observable<any> {
    return this.http.get<any>(this.CORE_VN2 + 'emisiones/detalles-poliza-emisiones/' + id);
  }

  getRegistroEmision(idEmision: number, idRegistro: number): Observable<any> {
    return this.http.get<any>(this.CORE_VN2 + 'emisiones/all/' + idEmision + '/' + idRegistro);
  }

  // FIN NEW

  post(data): Observable<Emisiones> {
    return this.http.post<Emisiones>(this.baseURL + 'v1/emisiones', data);
  }

  postInOne(data): Observable<any> {
    return this.http.post<any>(this.baseURL + 'v1/registro-emision/all', data);
  }

  getAll(): Observable<EmisionesViews[]> {
    return this.http.get<EmisionesViews[]>(this.baseURL + 'v1/emisiones');
  }


  getAllEmisionesOutbound(): Observable<EmisionesViews[]> {
    return this.http.get<EmisionesViews[]>(this.baseURL + 'v1/emisiones/get-outbound');
  }

  getAllEmisionesOutboundPeru(): Observable<EmisionesViews[]> {
    return this.http.get<EmisionesViews[]>(this.baseURL + 'v1/emisiones/get-outbound-peru');
  }

  getAllEmisionesCargoAutomaticoInterno(): Observable<EmisionesViews[]> {
    return this.http.get<EmisionesViews[]>(this.baseURL + 'v1/emisiones/get-cargo-automatico-interno');
  }

  getAllEmisionesCargoAutomaticoExterno(): Observable<EmisionesViews[]> {
    return this.http.get<EmisionesViews[]>(this.baseURL + 'v1/emisiones/get-cargo-automatico-externo');
  }

  getById(id: number): Observable<EmisionesViews> {
    return this.http.get<EmisionesViews>(this.baseURL + `v1/emisiones/${id}`);
  }

  getRegistroEmisionById(idRegistro): Observable<any> {
    return this.http.get<any>(this.baseURL + 'v1/registro-emision/' + idRegistro);
  }


  getByIdTipoBandeja(idTipoBandeja: number): Observable<EmisionesViews[]> {
    return this.http.get<EmisionesViews[]>(this.baseURL + 'v1/emisiones/get-by-bandeja/' + idTipoBandeja);
  }

  getByIdTipoBandejaAndFecha(fecha1, fecha2, idTipoBandeja: number): Observable<EmisionesViews[]> {
    return this.http.get<EmisionesViews[]>(this.baseURL + `v1/emisiones/get-by-bandeja-fecha/${idTipoBandeja}/${fecha1}/${fecha2}`);
  }

  getFiltroFechaCobranzaRenovacion(fecha1, fecha2): Observable<EmisionesViews[]> {
    return this.http.get<EmisionesViews[]>(this.baseURL + `v1/emisiones/get-emision-en-proceso/${fecha1}/${fecha2}`);
  }

  getAllPendientes(fechaInicio,fechaFin): Observable<EmisionesViews[]> {
    return this.http.get<EmisionesViews[]>(this.baseURL + 'v1/emisiones/pendientes/' + fechaInicio + '/' + fechaFin);
  }

  getAllRegistros(): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURL + 'v1/emisiones/polizas-emisiones');
  }

  getAllBandejas(): Observable<any[]> {
    return this.http.get<any[]>(this.baseURL + 'v1/bandejas-renovacion');
  }

  getFiltroFechaEmisiones(fecha1, fecha2): Observable<any[]> {
    return this.http.get<any[]>(this.baseURL + 'v1/emisiones/filtro-fecha/' + fecha1 + '/' + fecha2);
  }

  getFiltroFechaARenovar(fecha1, fecha2): Observable<any[]> {
    return this.http.get<any[]>(this.baseURL + 'v1/emisiones/a-renovar/filtro-fecha/' + fecha1 + '/' + fecha2);
  }

  getFiltroColores(valor): Observable<any[]> {
    return this.http.get<any[]>(this.baseURL + 'v1/emisiones/color/' + valor);
  }

  putEmision(id: number, emisiones): Observable<any> {
    return this.http.put(this.baseURL + 'v1/emisiones/' + id, emisiones);
  }

  putEmisionModalRegistro(id: number, tipoRenovacion: number): Observable<any> {
    return this.http.put(this.baseURL + 'v1/emisiones/modalRegistro' + '/' + id + '/' + tipoRenovacion, null);
  }

  putDatosRegistroEmisiones(idRegistro: number, datos): Observable<any> {
    return this.http.put(this.baseURL + 'v1/datos-registro-emisiones/update-datos-registro-emision' + '/' + idRegistro, datos);
  }
  getCobranzaEmisionesRenovacion(): Observable<EmisionesViews[]> {
    return this.http.get<EmisionesViews[]>(this.baseURL + 'v1/emisiones/getCobranzaEmisionesReno/');
  }
}
