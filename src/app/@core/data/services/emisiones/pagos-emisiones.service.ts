import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../../environments/environment';
import {PagosEmisiones, PagosEmisionesData} from '../../interfaces/emisiones/pagos-emisiones';

@Injectable({
  providedIn: 'root',
})
export class PagosEmisionesService extends PagosEmisionesData {
  private baseURL;
  private baseURLADM;

  constructor(private http: HttpClient ) {
    super();
    this.baseURLADM = environment.GLOBAL_SERVICIOS;
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
  }

  get(): Observable<PagosEmisiones[]> {
    return this.http.get<PagosEmisiones[]>(this.baseURL + 'v1/pagos-emision');
  }

  getPagoById(id): Observable<PagosEmisiones> {
    return this.http.get<PagosEmisiones>(this.baseURL + 'v1/pagos-emision/' + id);
  }
  getPagoByIdADM(id): Observable<PagosEmisiones> {
    return this.http.get<PagosEmisiones>(this.baseURLADM + 'v1/pagos-emision/' + id);
  }

  getByIdRecibo(idRecibo): Observable<PagosEmisiones> {
    return this.http.get<PagosEmisiones>(this.baseURL + 'v1/pagos-emision/get-by-id-recibo/' + idRecibo);
  }
  getByIdReciboADM(idRecibo): Observable<PagosEmisiones> {
    return this.http.get<PagosEmisiones>(this.baseURLADM + 'v1/pagos-emision/get-by-id-recibo/' + idRecibo);
  }

  post( pago ): Observable<PagosEmisiones> {
    return this.http.post<PagosEmisiones>(this.baseURL + 'v1/pagos-emision', pago);
  }

  postOne( pago ): Observable<PagosEmisiones> {
    return this.http.post<PagosEmisiones>(this.baseURL + 'v1/pagos-emision/all', pago);
  }

  put( id, pago: PagosEmisiones ): Observable<PagosEmisiones> {
    return this.http.put<PagosEmisiones>(this.baseURL + 'v1/pagos-emision/' + id + '/' + sessionStorage.getItem('Empleado'), pago);
  }

  putArchivo( id, archivo: string ): Observable<PagosEmisiones> {
    return this.http.put<PagosEmisiones>(this.baseURL + 'v1/pagos-emision/update-archivo/' + id + '/' + archivo + '/' +
      sessionStorage.getItem('Empleado'), null);
  }
}
