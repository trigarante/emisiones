import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CargaExcelService {

  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.CARGA_EXCEL + 'excel';
  }

  postArchivoPDF(data: FileList, nombreArchivo: string): Observable<any> {
    const formData = new FormData();
    formData.append(nombreArchivo, data.item(0), nombreArchivo);
    return this.http.post<any>(this.baseURL + '/carga-pdf', formData,      {responseType: 'text' as 'json'});
  }
  postArchivoExcel(data: FileList, nombreArchivo: string): Observable<any> {
    const formData = new FormData();
    const idEmpleado = sessionStorage.getItem('Empleado');
    formData.append('excel', data.item(0), nombreArchivo);
    formData.append('idEmpleado', idEmpleado.toString());
    formData.append('fecha', new Date().toISOString().split('T')[0]);
    return this.http.post<any>(this.baseURL + `/carga-excel`, formData,      {responseType: 'text' as 'json'});
  }

  descargarExcel(): Observable<any> {
    return this.http.get(this.baseURL + '/descargar-excel', { responseType: 'blob'} );
  }
}
