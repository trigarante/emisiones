import { TestBed } from '@angular/core/testing';

import { ClienteEmisionService } from './cliente.service';

describe('ClienteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClienteEmisionService = TestBed.get(ClienteEmisionService);
    expect(service).toBeTruthy();
  });
});
