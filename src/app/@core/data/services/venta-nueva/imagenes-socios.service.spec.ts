import { TestBed } from '@angular/core/testing';

import { ImagenesSociosService } from './imagenes-socios.service';

describe('ImagenesSociosService', () => {
  let service: ImagenesSociosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ImagenesSociosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
