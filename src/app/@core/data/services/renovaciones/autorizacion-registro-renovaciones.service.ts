import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AutorizacionRegistroRenovaciones} from '../../interfaces/renovaciones/autorizacion-registro-renovaciones';
import {AutorizacionRegistro} from '../../interfaces/ventaNueva/autorizacion/autorizacion-registro';
import {VerificacionRegistroRenovaciones} from "../../interfaces/renovaciones/verificacion-registro-renovaciones";

@Injectable({
  providedIn: 'root',
})
export class AutorizacionRegistroRenovacionesService {
  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
  }

  getById(id: number): Observable<AutorizacionRegistroRenovaciones> {
    return this.http.get<AutorizacionRegistroRenovaciones>
    (this.baseURL + 'v1/autorizacion-renovaciones/get-by-id/' + id);
  }

  getIdTipoBandejaRenovacion(idTipoBandejaRenovacion: number): Observable<AutorizacionRegistroRenovaciones[]> {
    return this.http.get<AutorizacionRegistroRenovaciones[]>
    (this.baseURL + 'v1/autorizacion-renovaciones/' + idTipoBandejaRenovacion);
  }

  getTablaRenovacion(estadoVerificacion: string, tipoRenovacion: number): Observable<AutorizacionRegistroRenovaciones[]> {
    return this.http.get<AutorizacionRegistroRenovaciones[]>
    (this.baseURL + 'v1/autorizacion-renovaciones/tabla/' + estadoVerificacion + '/' + tipoRenovacion + '/'
      + sessionStorage.getItem('Empleado'));
  }

  put(id: number, autorizacion: VerificacionRegistroRenovaciones): Observable<VerificacionRegistroRenovaciones> {
    return this.http.put<VerificacionRegistroRenovaciones>(this.baseURL + 'v1/autorizacion-registro-vn/' + id, autorizacion);
  }

  post(idRegistro: number): Observable<AutorizacionRegistro> {
    return this.http.post<AutorizacionRegistro>(this.baseURL + 'v1/autorizacion-renovaciones/' + idRegistro + '/'
      + sessionStorage.getItem('Empleado'), null);
  }

  documentoVerificado(idAutorizacionRegistro: number, documentoVerificado: number, estado: number): Observable<String> {
    return this.http.put<string>(this.baseURL + 'v1/autorizacion-renovaciones/' + idAutorizacionRegistro + '/'
      + documentoVerificado + '/' + estado, null, {responseType: 'text' as 'json'});
  }
}
