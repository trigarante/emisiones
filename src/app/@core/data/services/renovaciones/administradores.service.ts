import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {RenovacionesDomiciliacion} from '../../interfaces/renovaciones/renovacionesDomiciliacion';

@Injectable({
  providedIn: 'root',
})
export class AdministradoresService {
  url: string;

  constructor(private http: HttpClient) {
    this.url = environment.GLOBAL_SERVICIOS_OPERACIONES + 'v1/administrador-renovacion/';
  }

  getDomiciliaciones(idTipoBandeja: number): Observable<RenovacionesDomiciliacion[]> {
    return this.http.get<RenovacionesDomiciliacion[]>(this.url + idTipoBandeja);
  }

  getFiltroFecha(fecha1, fecha2): Observable<RenovacionesDomiciliacion> {
    return this.http.get<RenovacionesDomiciliacion>(this.url + `filtro-fecha/${fecha1}/${fecha2}`);
  }

  getRenovacionesPorAutorizar(idTipoBandeja: number): Observable<RenovacionesDomiciliacion> {
    return this.http.get<RenovacionesDomiciliacion>(this.url + 'por-autorizar/' + idTipoBandeja);
  }

  findAllByCanceladas(idBandeja: number): Observable<RenovacionesDomiciliacion> {
    return this.http.get<RenovacionesDomiciliacion>(this.url + 'canceladas/' + idBandeja);
  }
}
