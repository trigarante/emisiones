import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {ErroresAutorizacion, ErroresVerificacion} from "../../interfaces/ventaNueva/correccion-errores";

@Injectable({
  providedIn: 'root',
})
export class CorreccionErroresRenoService {
  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
  }

  getErroresDocumentosVFlujoPoliza(idFlujoPoliza, tipoBandeja): Observable<any[]> {
    const usuario = sessionStorage.getItem('Usuario');
    return this.http.get<any[]>(this.baseURL + `v1/correcciones-reno/verificacion/anexos/${usuario}/${idFlujoPoliza}/${tipoBandeja}`);
  }

  getErroresDatosVFlujoPoliza(idFlujoPoliza, tipoBandeja): Observable<any[]> {
    return this.http.get<any[]>(this.baseURL + `v1/correcciones-reno/verificacion/datos/
    ${sessionStorage.getItem('Usuario')}/${idFlujoPoliza}/${tipoBandeja}`);
  }

  getErroresDatosAFlujoPoliza(idFlujoPoliza, tipoBandeja): Observable<any[]> {
    const usuario = sessionStorage.getItem('Usuario');
    return this.http.get<any[]>(
      this.baseURL + `v1/autorizacion-errores-reno/autorizacion/datos/${usuario}/${idFlujoPoliza}/${tipoBandeja}`);
  }

  getErroresDocumentosAFlujoPoliza(idFlujoPoliza, tipoBandeja): Observable<any[]> {
    const usuario = sessionStorage.getItem('Usuario');
    return this.http.get<any[]>(
      this.baseURL + `v1/autorizacion-errores-reno/autorizacion/anexos/${usuario}/${idFlujoPoliza}/${tipoBandeja}`);
  }

  getErroresDocumentosARenovacion(tipoBandeja: number): Observable<ErroresAutorizacion[]> {
    return this.http.get<ErroresAutorizacion[]>(this.baseURL + `v1/correcciones-reno/autorizacion/anexos/${tipoBandeja}/`
      + sessionStorage.getItem('Usuario'));
  }

  getErroresDatosRenovacionA(tipoBandeja): Observable<ErroresAutorizacion[]> {
    return this.http.get<ErroresAutorizacion[]>(this.baseURL + `v1/correcciones-reno/autorizacion/datos/${tipoBandeja}/`
      + sessionStorage.getItem('Usuario'));
  }

  getErroresDocumentosARenovacionV(tipoBandeja): Observable<ErroresVerificacion[]> {
    return this.http.get<ErroresVerificacion[]>(this.baseURL + `v1/correcciones-reno/verificacion/anexos/${tipoBandeja}/`
      + sessionStorage.getItem('Usuario'));
  }

  getErroresDatosRenovacionV(tipoBandeja): Observable<ErroresAutorizacion[]> {
    return this.http.get<ErroresAutorizacion[]>(this.baseURL + `v1/correcciones-reno/verificacion/datos/${tipoBandeja}/`
      + sessionStorage.getItem('Usuario'));
  }
}
