import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {IntentosPago} from '../../interfaces/renovaciones/intentosPago';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class IntentosPagoService {
  url: string;

  constructor(private http: HttpClient) {
    this.url = environment.GLOBAL_SERVICIOS_OPERACIONES + 'v1/intentos-pago/';
  }

  getByIdRecibo(idRecibo) {
    return this.http.get<IntentosPago>(this.url + 'get-by-recibo/' + idRecibo);
  }

  getByUltimoIntento(idRecibo) {
    return this.http.get<IntentosPago>(this.url + 'get-ultimo-intento/' + idRecibo);
  }

  post(intentosPago: IntentosPago): Observable<IntentosPago> {
    return this.http.post<IntentosPago>(this.url, intentosPago);
  }

  primerIntento(idRecibo: number): Observable<IntentosPago> {
    return this.http.post<IntentosPago>(this.url + idRecibo, null);
  }
}
