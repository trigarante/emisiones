import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Pagos} from '../../interfaces/ventaNueva/pagos';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PagoRenovacionService {
  url: string;

  constructor(private http: HttpClient) {
    this.url = environment.GLOBAL_SERVICIOS_OPERACIONES + 'v1/pago-renovacion/';
  }

  post(pago): Observable<Pagos> {
    return this.http.post<Pagos>(this.url, pago);
  }

  postPagoRenovacion(idIntentoPago: number, pago) {
    return this.http.post<Pagos>(this.url + idIntentoPago, pago);
  }
}
