import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {ProductoCliente} from '../../interfaces/ventaNueva/producto-cliente';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ProductoClienteRenovacionesService {
  baseURL: string;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES + 'v1/producto-cliente-renovaciones/';
  }

  get(): Observable<ProductoCliente[]> {
    return this.http.get<ProductoCliente[]>(this.baseURL);
  }

  getProductoClienteById(idProductoCliente): Observable<ProductoCliente> {
    return this.http.get<ProductoCliente>(this.baseURL + idProductoCliente);
  }

  post(productoCliente: ProductoCliente): Observable<ProductoCliente> {
    return this.http.post<ProductoCliente>(this.baseURL, productoCliente);
  }

  put(idProductoCliente, productoCliente: ProductoCliente): Observable<ProductoCliente> {
    return this.http.put<ProductoCliente>(this.baseURL + idProductoCliente
      + '/' + sessionStorage.Empleado, productoCliente);
  }
}
