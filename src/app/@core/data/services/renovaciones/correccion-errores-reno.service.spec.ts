import { TestBed } from '@angular/core/testing';

import { CorreccionErroresRenoService } from './correccion-errores-reno.service';

describe('CorreccionErroresRenoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CorreccionErroresRenoService = TestBed.get(CorreccionErroresRenoService);
    expect(service).toBeTruthy();
  });
});
