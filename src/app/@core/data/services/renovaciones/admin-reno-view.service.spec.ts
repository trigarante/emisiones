import { TestBed } from '@angular/core/testing';

import { AdminRenoViewService } from './admin-reno-view.service';

describe('AdminRenoViewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdminRenoViewService = TestBed.get(AdminRenoViewService);
    expect(service).toBeTruthy();
  });
});
