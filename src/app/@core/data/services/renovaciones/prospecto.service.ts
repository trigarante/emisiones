import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Prospecto} from '../../interfaces/ventaNueva/prospecto';
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root',
})
export class ProspectoService {
  url: string;

  constructor(private http: HttpClient) {
    this.url = environment.GLOBAL_SERVICIOS_OPERACIONES + 'v1/prospecto';
  }

  get(): Observable<Prospecto[]> {
    return this.http.get<Prospecto[]>(this.url);
  }
}
