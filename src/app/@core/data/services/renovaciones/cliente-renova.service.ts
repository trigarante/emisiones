import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {ClienteVn} from '../../interfaces/ventaNueva/cliente-vn';
import {ClientePeru} from '../../interfaces/ventaNueva/cliente-peru';

@Injectable({
  providedIn: 'root',
})
export class ClienteRenovaService {

  private baseURL;
  private baseURLOp;
  private  socketURL;
  constructor(private http: HttpClient) {
    // super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.baseURLOp = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<ClienteVn[]> {
    return this.http.get<ClienteVn[]>(this.baseURLOp + 'v1/cliente-renovacionn');
  }

  getClienteRenova(): Observable<ClienteVn[]> {
    return this.http.get<ClienteVn[]>(this.baseURL + 'v1/cliente-renovacionn/renovacion');
  }

  getClienteFinanzasById(idClienteFinanzas): Observable<ClienteVn> {
    return this.http.get<ClienteVn>(this.baseURL + 'v1/cliente-renovacionn/' + idClienteFinanzas);
  }

  post(cliente: ClienteVn): Observable<ClienteVn> {
    return this.http.post<ClienteVn>(this.baseURLOp + 'v1/cliente-renovacionn', cliente);
  }
  postClienteEmision(cliente: ClienteVn): Observable<ClienteVn> {
    return this.http.post<ClienteVn>(this.baseURL + 'v1/cliente-renovacionn', cliente);
  }

  getFinanzasClienteById(idCliente): Observable<ClienteVn> {
    return this.http.get<ClienteVn>(this.baseURL + 'v1/cliente-renovacionn/' + idCliente);
  }

  getClienteFinPeruById(idCliente: number): Observable<ClientePeru> {
    return this.http.get<ClientePeru>(this.baseURL + 'v1/cliente-renovacionn/peru/' + idCliente);
  }
}
