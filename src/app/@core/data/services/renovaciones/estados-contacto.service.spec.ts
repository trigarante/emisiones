import { TestBed } from '@angular/core/testing';

import { EstadosContactoService } from './estados-contacto.service';

describe('EstadosContactoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EstadosContactoService = TestBed.get(EstadosContactoService);
    expect(service).toBeTruthy();
  });
});
