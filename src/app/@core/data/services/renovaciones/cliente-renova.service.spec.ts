import { TestBed } from '@angular/core/testing';

import { ClienteRenovaService } from './cliente-renova.service';

describe('ClienteRenovaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClienteRenovaService = TestBed.get(ClienteRenovaService);
    expect(service).toBeTruthy();
  });
});
