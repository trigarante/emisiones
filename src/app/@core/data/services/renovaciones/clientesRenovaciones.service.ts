import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {ClienteRenovaciones} from '../../interfaces/renovaciones/clienteRenovaciones';

@Injectable({
  providedIn: 'root',
})
export class ClientesRenovacionesService {
  url: string;

  constructor(private http: HttpClient) {
    this.url = environment.GLOBAL_SERVICIOS_OPERACIONES + 'v1/cliente-renovacion/';
  }

  get(): Observable<ClienteRenovaciones[]> {
    return this.http.get<ClienteRenovaciones[]>(this.url);
  }

  curpExist(curp): Observable<ClienteRenovaciones> {
    return this.http.get<ClienteRenovaciones>(this.url + 'curp/' + curp);
  }

  rfcExist(rfc): Observable<ClienteRenovaciones> {
    return this.http.get<ClienteRenovaciones>(this.url + 'rfc/' + rfc);
  }

  getById(id: number): Observable<ClienteRenovaciones> {
    return this.http.get<ClienteRenovaciones>(this.url + id);
  }

  post(cliente) {
    return this.http.post<ClienteRenovaciones>(this.url, cliente);
  }

  put(idCliente, cliente: ClienteRenovaciones): Observable<ClienteRenovaciones> {
    return this.http.put<ClienteRenovaciones>(this.url + idCliente + '/' +
      + sessionStorage.getItem('Empleado') , cliente);
  }

  putArchivo(idCliente: number, idCarpetaDrive: string): Observable<any> {
    return this.http.put(this.url + 'update-archivo/' + idCliente + '/' + idCarpetaDrive, null);
  }

  putArchivoSubido(idCliente: number, idArchivoSubido: number): Observable<any> {
    return this.http.put(this.url + 'update-archivo-subido/' + idCliente + '/' + idArchivoSubido, null);
  }
}
