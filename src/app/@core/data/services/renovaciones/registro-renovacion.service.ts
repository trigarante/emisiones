import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {Registro} from '../../interfaces/ventaNueva/registro';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class RegistroRenovacionService {
  baseURL: string;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES + 'v1/registro-renovaciones/';
  }

  post(registro: any): Observable<Registro> {
    return this.http.post<any>(this.baseURL + 'all', registro);
  }

  updateArchivo(idRegistro: number, idCarpeta: string): Observable<any> {
    return this.http.put(this.baseURL + 'carpeta-drive/' + idRegistro + '/' + idCarpeta, null);
  }
}
