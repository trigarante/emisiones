import { TestBed } from '@angular/core/testing';

import { AutorizacionRegistroRenovService } from './autorizacion-registro-renov.service';

describe('AutorizacionRegistroRenovService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AutorizacionRegistroRenovService = TestBed.get(AutorizacionRegistroRenovService);
    expect(service).toBeTruthy();
  });
});
