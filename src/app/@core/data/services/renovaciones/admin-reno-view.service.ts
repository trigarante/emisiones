import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../../environments/environment";
import {Observable} from "rxjs";
import {AdminVnViewData} from "../../interfaces/ventaNueva/adminVnView";

@Injectable({
  providedIn: 'root',
})
export class AdminRenoViewService {
  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
  }

  getAllRegistroByIdUsuario(): Observable<AdminRenoView[]> {
    return this.http.get<AdminRenoView[]>(this.baseURL +
      'v1/admin-reno/getPolizas/' + sessionStorage.getItem('Empleado'));
  }
}
