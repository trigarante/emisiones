import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../../environments/environment";
import {Observable} from "rxjs";
import {AutorizacionRegistroRenov} from "../../interfaces/renovaciones/autorizacionRegistroRenov";

@Injectable({
  providedIn: 'root',
})
export class AutorizacionRegistroRenovService {
  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES + 'v1/autorizacion-registro-reno';
  }

  getTabla(estadoVerificacion: string): Observable<AutorizacionRegistroRenov[]> {
    return this.http.get<AutorizacionRegistroRenov[]>
    (this.baseURL  + '/tabla/' + estadoVerificacion + '/' + sessionStorage.getItem('Empleado'));
  }

  put(id: number, autorizacion): Observable<AutorizacionRegistroRenov> {
    return this.http.put<AutorizacionRegistroRenov>(this.baseURL + '/' + id, autorizacion);
  }

  getById(id: number): Observable<AutorizacionRegistroRenov> {
    return this.http.get<AutorizacionRegistroRenov>(this.baseURL + '/get-by-id/' + id);
  }

  documentoVerificado(idAutorizacionRegistro: number, documentoVerificado: number, estado: number): Observable<String> {
    return this.http.put<string>(this.baseURL + '/' + idAutorizacionRegistro + '/'
      + documentoVerificado + '/' + estado, null, {responseType: 'text' as 'json'});
  }
}
