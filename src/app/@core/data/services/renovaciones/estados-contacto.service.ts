import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class EstadosContactoService {
  private baseURL;
  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  getEstadosContacto(): Observable<any[]> {
    return this.http.get<any[]>(this.baseURL + 'v1/emisiones/estados-contacto');
  }

  getComentarios(idEmision: number): Observable<any[]> {
    return this.http.get<any[]>(this.baseURL + `v1/emisiones/estados-contacto-historial/${idEmision}`);
  }

  postComentarioEmision(comentario): Observable<any[]> {
    return this.http.post<any[]>( this.baseURL + 'v1/emisiones/comentarios-emision', comentario );
  }
}
