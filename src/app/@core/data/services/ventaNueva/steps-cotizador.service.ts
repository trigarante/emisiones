import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import { Observable} from 'rxjs';
import {StepsCotizador, StepsCotizadorData} from '../../interfaces/ventaNueva/steps-cotizador';


@Injectable()
export class StepsCotizadorService extends StepsCotizadorData {

  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
  }

  get(): Observable<StepsCotizador[]> {
    return this.http.get<StepsCotizador[]>(this.baseURL + 'v1/step-cotizador');
  }

  getById(idRegistro: number): Observable<StepsCotizador> {
    return this.http.get<StepsCotizador>(this.baseURL + 'v1/step-cotizador/' + idRegistro);
  }
  getByIdRegistro(idRegistro: number): Observable<StepsCotizador> {
    return this.http.get<StepsCotizador>(this.baseURL + 'v1/step-cotizador/registro/' + idRegistro);
  }
}
