import { TestBed } from '@angular/core/testing';

import { MiEstadoCuentaService } from './mi-estado-cuenta.service';

describe('MiEstadoCuentaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MiEstadoCuentaService = TestBed.get(MiEstadoCuentaService);
    expect(service).toBeTruthy();
  });
});
