import { Injectable } from '@angular/core';
import {ProductoClienteAuto, ProductoClienteAutoData} from '../../interfaces/ventaNueva/producto-cliente-auto';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()

export class ProductoClienteAutoService extends ProductoClienteAutoData {

  url: any;
  private baseURL2;

  constructor(private http: HttpClient) {
    super();
    this.url = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;

  }

  get(): Observable<ProductoClienteAuto[]> {
    return this.http.get<ProductoClienteAuto[]>(this.baseURL2 + 'v1/producto-cliente-auto');
  }

  getById(idProductoCliente): Observable<ProductoClienteAuto> {
    return this.http.get<ProductoClienteAuto>(this.baseURL2 + 'v1/producto-cliente-auto/' + idProductoCliente);
  }

}
