import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {CarruselEmpleadosData, CarruselEmpleados} from '../../interfaces/ventaNueva/carrusel-empleados';

@Injectable()
export class CarruselEmpleadosService extends CarruselEmpleadosData {

  private baseURL;
  private baseURL2;


  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;

  }

  get(): Observable<CarruselEmpleados[]> {
    return this.http.get<CarruselEmpleados[]>(this.baseURL2 + 'v1/carrusel-empleados');
  }

  getRegistroById(idCarrusel): Observable<CarruselEmpleados> {
    return this.http.get<CarruselEmpleados>(this.baseURL2 + 'v1/carrusel-empleados/' + idCarrusel);
  }
  post(Carrusel: CarruselEmpleados): Observable<CarruselEmpleados> {
    return this.http.post<CarruselEmpleados>(this.baseURL + 'v1/carrusel-empleados/', Carrusel);
  }

  put(idCarrusel, Carrusel: CarruselEmpleados): Observable<CarruselEmpleados> {
    return this.http.put<CarruselEmpleados>(this.baseURL + 'v1/carrusel-empleados/' + idCarrusel, Carrusel);
  }

}
