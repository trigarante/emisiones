import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Pagos, PagosComplemento, PagosData} from '../../interfaces/ventaNueva/pagos';
import {environment} from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PagosService extends PagosData {
  private baseURL;
  private baseURLADM;
  private baseURL2;


  constructor(private http: HttpClient ) {
    super();
    this.baseURLADM = environment.GLOBAL_SERVICIOS;
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;
  }

  get(): Observable<Pagos[]> {
    return this.http.get<Pagos[]>(this.baseURL2 + 'v1/pagos');
  }

  getPagoById(id): Observable<Pagos> {
    return this.http.get<Pagos>(this.baseURL2 + 'v1/pagos/' + id);
  }

  getPagoByIdADM(id): Observable<Pagos> {
    return this.http.get<Pagos>(this.baseURLADM + 'v1/pagos/' + id);
  }

  getByIdRecibo(idRecibo): Observable<Pagos> {
    return this.http.get<Pagos>(this.baseURL2 + 'v1/pagos/get-by-id-recibo/' + idRecibo);
  }

  getByIdReciboADM(idRecibo): Observable<Pagos> {
    return this.http.get<Pagos>(this.baseURLADM + 'v1/pagos/get-by-id-recibo/' + idRecibo);
  }

  post( pago ): Observable<Pagos> {
    return this.http.post<Pagos>(this.baseURL + 'v1/pagos', pago);
  }

  postRecibo( pago ): Observable<Pagos> {
    return this.http.post<Pagos>(this.baseURL + 'v1/pagos/recibo/' + sessionStorage.getItem('Empleado'), pago);
  }

  postOne( pago ): Observable<Pagos> {
    return this.http.post<Pagos>(this.baseURL + 'v1/pagos/all', pago);
  }

  postOneComplemento( pago ): Observable<PagosComplemento> {
    return this.http.post<PagosComplemento>(this.baseURL + 'v1/pagos-complemento/all', pago);
  }

  postOneRetenciones( pago ): Observable<Pagos> {
    return this.http.post<Pagos>(this.baseURL + 'v1/pagos/retenciones', pago);
  }

  postOneCorrecciones(pago): Observable<Pagos> {
    return this.http.post<Pagos>(this.baseURL + 'v1/pagos/correcciones-all', pago);
  }

  put( id, pago: Pagos ): Observable<Pagos> {
    return this.http.put<Pagos>(this.baseURL + 'v1/pagos/' + id + '/' + sessionStorage.getItem('Empleado'), pago);
  }

  putArchivo( id, archivo: string ): Observable<Pagos> {
    return this.http.put<Pagos>(this.baseURL + 'v1/pagos/update-archivo/' + id + '/' + archivo + '/' +
      sessionStorage.getItem('Empleado'), null);
  }
}
