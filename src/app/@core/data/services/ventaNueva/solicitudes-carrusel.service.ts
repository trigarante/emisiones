import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {SolicitudesCarrusel} from '../../interfaces/ventaNueva/SolicitudesCarrusel';

@Injectable({
  providedIn: 'root',
})
export class SolicitudesCarruselService {
  url: string;
  private baseURL2;


  constructor(private http: HttpClient) {
    this.url = environment.GLOBAL_SERVICIOS_OPERACIONES + 'v1/solicitudes-carrusel';
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura + 'v1/solicitudes-carrusel';

  }

  getByEstado(idEstadoSolicitudCarrusel: number): Observable<SolicitudesCarrusel[]> {
    return this.http.get<SolicitudesCarrusel[]>(this.baseURL2 + '/by-estado/' + idEstadoSolicitudCarrusel);
  }

  post(solicitud): Observable<any> {
    return this.http.post(this.url, solicitud);
  }

  updateArchivo(idSolicitud: number, archivo: string): Observable<any> {
    return this.http.put(this.url + '/update-archivo/' + idSolicitud + '/' + archivo, null);
  }

  updateCoordinador(solicitud): Observable<any> {
    return this.http.put(this.url + '/update-coordinador', solicitud);
  }

  updateRRHH(solicitud, permiso: number): Observable<any> {
    return this.http.put(this.url + '/update-rrhh/' + permiso + '/' + sessionStorage.getItem('Empleado'), solicitud);
  }
}
