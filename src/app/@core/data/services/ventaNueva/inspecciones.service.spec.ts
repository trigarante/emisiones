import { TestBed } from '@angular/core/testing';

import { InspeccionesService } from './inspecciones.service';

describe('InspeccionesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InspeccionesService = TestBed.get(InspeccionesService);
    expect(service).toBeTruthy();
  });
});
