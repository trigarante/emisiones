import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {
  CorreccionErroresData,
  ErroresAutorizacion, ErroresVerificacion,
} from '../../interfaces/ventaNueva/correccion-errores';

@Injectable()
export class CorreccionErroresService extends CorreccionErroresData {
  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
  }

  getErroresDatosA(): Observable<ErroresAutorizacion[]> {
    return this.http.get<ErroresAutorizacion[]>(this.baseURL + 'v1/correcciones/autorizacion/datos/'
      + sessionStorage.getItem('Usuario'));
  }

  getErroresDocumentosA(): Observable<ErroresAutorizacion[]> {
    return this.http.get<ErroresAutorizacion[]>(this.baseURL + 'v1/correcciones/autorizacion/anexos/'
      + sessionStorage.getItem('Usuario'));
  }

  getErroresDatosV(): Observable<ErroresVerificacion[]> {
    return this.http.get<ErroresVerificacion[]>(this.baseURL + 'v1/correcciones/verificacion/datos/'
      + sessionStorage.getItem('Usuario'));
  }

  getErroresDocumentosV(): Observable<ErroresVerificacion[]> {
    return this.http.get<ErroresVerificacion[]>(this.baseURL + 'v1/correcciones/verificacion/anexos/'
      + sessionStorage.getItem('Usuario'));
  }

  // Venta Nueva
  getErroresDocumentosVVn(): Observable<ErroresVerificacion[]> {
    return this.http.get<ErroresVerificacion[]>(this.baseURL + 'v1/errores-verificacion-vn/anexos/'
      + sessionStorage.getItem('Usuario'));
  }

  getErroresDatosVVn(): Observable<ErroresAutorizacion[]> {
    return this.http.get<ErroresAutorizacion[]>(this.baseURL + 'v1/errores-verificacion-vn/datos/'
      + sessionStorage.getItem('Usuario'));
  }

  // Venta Nueva Inbound
  getErroresDocumentosVVnIb(): Observable<ErroresVerificacion[]> {
    return this.http.get<ErroresVerificacion[]>(this.baseURL + 'v1/errores-verificacion-vn-in/anexos/'
      + sessionStorage.getItem('Usuario'));
  }

  getErroresDatosVVnIb(): Observable<ErroresAutorizacion[]> {
    return this.http.get<ErroresAutorizacion[]>(this.baseURL + 'v1/errores-verificacion-vn-in/datos/'
      + sessionStorage.getItem('Usuario'));
  }

  // Cobranza
  getErroresDocumentosCobranzaV(): Observable<ErroresVerificacion[]> {
    return this.http.get<ErroresVerificacion[]>(this.baseURL + 'v1/errores-verificacion-cobranza/anexos/'
      + sessionStorage.getItem('Usuario'));
  }

  getErroresDatosCobranzaV(): Observable<ErroresAutorizacion[]> {
    return this.http.get<ErroresAutorizacion[]>(this.baseURL + 'v1/errores-verificacion-cobranza/datos/'
      + sessionStorage.getItem('Usuario'));
  }

  // Subsecuentes
  getErroresDocumentosSubsecuentesV(): Observable<ErroresVerificacion[]> {
    return this.http.get<ErroresVerificacion[]>(this.baseURL + 'v1/errores-verificacion-subsecuentes/anexos/'
      + sessionStorage.getItem('Usuario'));
  }

  getErroresDatosSubsecuentesV(): Observable<ErroresAutorizacion[]> {
    return this.http.get<ErroresAutorizacion[]>(this.baseURL + 'v1/errores-verificacion-subsecuentes/datos/'
      + sessionStorage.getItem('Usuario'));
  }

  // Ecommerce
  getErroresDocumentosEcommerceV(): Observable<ErroresVerificacion[]> {
    return this.http.get<ErroresVerificacion[]>(this.baseURL + 'v1/errores-verificacion-ecommerce/anexos/'
      + sessionStorage.getItem('Usuario'));
  }

  getErroresDatosEcommerceV(): Observable<ErroresAutorizacion[]> {
    return this.http.get<ErroresAutorizacion[]>(this.baseURL + 'v1/errores-verificacion-ecommerce/datos/'
      + sessionStorage.getItem('Usuario'));
  }

  // Peru
  getErroresDocumentosPeruV(): Observable<ErroresVerificacion[]> {
    return this.http.get<ErroresVerificacion[]>(this.baseURL + 'v1/errores-verificacion-peru/anexos/'
      + sessionStorage.getItem('Usuario'));
  }

  getErroresDatosPeruV(): Observable<ErroresAutorizacion[]> {
    return this.http.get<ErroresAutorizacion[]>(this.baseURL + 'v1/errores-verificacion-peru/datos/'
      + sessionStorage.getItem('Usuario'));
  }

  // Gastos médicos
  getErroresDocumentosGMV(): Observable<ErroresVerificacion[]> {
    return this.http.get<ErroresVerificacion[]>(this.baseURL + 'v1/errores-verificacion-gm/anexos/'
      + sessionStorage.getItem('Usuario'));
  }

  getErroresDatosGMV(): Observable<ErroresAutorizacion[]> {
    return this.http.get<ErroresAutorizacion[]>(this.baseURL + 'v1/errores-verificacion-gm/datos/'
      + sessionStorage.getItem('Usuario'));
  }

  // Tradicional
  getErroresDocumentosTradicionalV(): Observable<ErroresVerificacion[]> {
    return this.http.get<ErroresVerificacion[]>(this.baseURL + 'v1/errores-verificacion-tradicional/anexos/'
      + sessionStorage.getItem('Usuario'));
  }

  getErroresDatosTradicionalV(): Observable<ErroresAutorizacion[]> {
    return this.http.get<ErroresAutorizacion[]>(this.baseURL + 'v1/errores-verificacion-tradicional/datos/'
      + sessionStorage.getItem('Usuario'));
  }

  // Retenciones
  getErroresDocumentosRetencionesV(): Observable<ErroresVerificacion[]> {
    return this.http.get<ErroresVerificacion[]>(this.baseURL + 'v1/errores-verificacion-retenciones/anexos/'
      + sessionStorage.getItem('Usuario'));
  }

  getErroresDatosRetencionesV(): Observable<ErroresAutorizacion[]> {
    return this.http.get<ErroresAutorizacion[]>(this.baseURL + 'v1/errores-verificacion-retenciones/datos/'
      + sessionStorage.getItem('Usuario'));
  }


  // Nuevo
  getErroresDatosAFlujoPoliza(idFlujoPoliza): Observable<ErroresAutorizacion[]> {
    return this.http.get<ErroresAutorizacion[]>(this.baseURL + 'v1/correcciones/autorizacion/datos/'
      + sessionStorage.getItem('Usuario') + '/' + idFlujoPoliza);
  }

  getErroresDocumentosAFlujoPoliza(idFlujoPoliza): Observable<ErroresAutorizacion[]> {
    return this.http.get<ErroresAutorizacion[]>(this.baseURL + 'v1/correcciones/autorizacion/anexos/'
      + sessionStorage.getItem('Usuario') + '/' + idFlujoPoliza);
  }

  getErroresDatosVFlujoPoliza(idFlujoPoliza): Observable<ErroresVerificacion[]> {
    return this.http.get<ErroresVerificacion[]>(this.baseURL + 'v1/correcciones/verificacion/datos/'
      + sessionStorage.getItem('Usuario') + '/' + idFlujoPoliza);
  }

  getErroresDocumentosVFlujoPoliza(idFlujoPoliza): Observable<ErroresVerificacion[]> {
    return this.http.get<ErroresVerificacion[]>(this.baseURL + 'v1/correcciones/verificacion/anexos/'
      + sessionStorage.getItem('Usuario') + '/' + idFlujoPoliza);
  }

  getErroresDatosASubsecuentes(): Observable<ErroresAutorizacion[]> {
    return this.http.get<ErroresAutorizacion[]>(this.baseURL + 'v1/correcciones-subsecuentes/autorizacion/datos/'
      + sessionStorage.getItem('Usuario'));
  }

  getErroresDocumentosASubsecuentes(): Observable<ErroresAutorizacion[]> {
    return this.http.get<ErroresAutorizacion[]>(this.baseURL + 'v1/correcciones-subsecuentes/autorizacion/anexos/'
      + sessionStorage.getItem('Usuario'));
  }

  getErroresDatosVSubsecuentes(): Observable<ErroresVerificacion[]> {
    return this.http.get<ErroresVerificacion[]>(this.baseURL + 'v1/correcciones-subsecuentes/verificacion/datos/'
      + sessionStorage.getItem('Usuario'));
  }

  getErroresDocumentosVSubsecuentes(): Observable<ErroresVerificacion[]> {
    return this.http.get<ErroresVerificacion[]>(this.baseURL + 'v1/correcciones-subsecuentes/verificacion/anexos/'
      + sessionStorage.getItem('Usuario'));
  }

  //
  getErroresDatosACobranza(): Observable<ErroresAutorizacion[]> {
    return this.http.get<ErroresAutorizacion[]>(this.baseURL + 'v1/correcciones-cobranza/autorizacion/datos/'
      + sessionStorage.getItem('Usuario'));
  }

  getErroresDocumentosACobranza(): Observable<ErroresAutorizacion[]> {
    return this.http.get<ErroresAutorizacion[]>(this.baseURL + 'v1/correcciones-cobranza/autorizacion/anexos/'
      + sessionStorage.getItem('Usuario'));
  }

  getErroresDatosVCobranza(): Observable<ErroresVerificacion[]> {
    return this.http.get<ErroresVerificacion[]>(this.baseURL + 'v1/correcciones-cobranza/verificacion/datos/'
      + sessionStorage.getItem('Usuario'));
  }

  getErroresDocumentosVCobranza(): Observable<ErroresVerificacion[]> {
    return this.http.get<ErroresVerificacion[]>(this.baseURL + 'v1/correcciones-cobranza/verificacion/anexos/'
      + sessionStorage.getItem('Usuario'));
  }

  // Reno
  getErroresDocumentosVReno(): Observable<ErroresVerificacion[]> {
    return this.http.get<ErroresVerificacion[]>(this.baseURL + 'v1/correcciones-reno/anexos/'
      + sessionStorage.getItem('Usuario'));
  }

  getErroresDatosVReno(): Observable<ErroresVerificacion[]> {
    return this.http.get<ErroresVerificacion[]>(this.baseURL + 'v1/correcciones-reno/datos/'
      + sessionStorage.getItem('Usuario'));
  }

  // Subsecuentes Renovaciones

  getErroresDatosAR(): Observable<ErroresAutorizacion[]> {
    return this.http.get<ErroresAutorizacion[]>(this.baseURL + 'v1/correcciones/autorizacion/datos/'
      + sessionStorage.getItem('Usuario'));
  }

  getErroresDatosVR(): Observable<ErroresVerificacion[]> {
    return this.http.get<ErroresVerificacion[]>(this.baseURL + 'v1/correcciones/verificacion/datos/'
      + sessionStorage.getItem('Usuario'));
  }
}
