import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {ErroresAutorizacion, ErroresAutorizacionData} from '../../../interfaces/ventaNueva/autorizacion/errores-autorizacion';

@Injectable()
export class ErroresAutorizacionService extends ErroresAutorizacionData {
  private baseURL;
  private baseURL2;


  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;

  }

  get(): Observable<ErroresAutorizacion[]> {
    return this.http.get<ErroresAutorizacion[]>(this.baseURL2 + 'v1/errores-autorizacion');
  }

  post(error: ErroresAutorizacion): Observable<string> {
    return this.http.post<string>(this.baseURL + 'v1/errores-autorizacion', error,
      {responseType: 'text' as 'json'});
  }

  getCamposConErrores(idAutorizacionRegistro: number, idTipoDocumento: number): Observable<ErroresAutorizacion> {
    return this.http.get<ErroresAutorizacion>(this.baseURL2 + 'v1/errores-autorizacion/' + idAutorizacionRegistro + '/'
      + idTipoDocumento);
  }

  putEstadoCorreccion(idErrorAutorizacion: number): Observable<any> {
    return this.http.put(this.baseURL + 'v1/errores-autorizacion/' + idErrorAutorizacion, null);
  }
}
