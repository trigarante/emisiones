import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../../../environments/environment";
import {Observable} from "rxjs";
import {CampoCorregir} from "../../../interfaces/verificacion/campo-corregir";
import {CampoCorregirAutorizacionData} from "../../../interfaces/ventaNueva/autorizacion/campo-corregir-autorizacion";

@Injectable({
  providedIn: 'root',
})
export class CampoCorregirAutorizacionService extends  CampoCorregirAutorizacionData {
  private baseURL;
  private baseURL2;


  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;

  }

  getCamposDisponibles(idTabla: number): Observable<CampoCorregir[]> {
    return this.http.get<CampoCorregir[]>(this.baseURL2 + 'v1/campo-corregir/' + idTabla);
  }
}
