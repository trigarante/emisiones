import { TestBed } from '@angular/core/testing';

import { TipoDocumentoAutorizacionService } from './tipo-documento-autorizacion.service';

describe('TipoDocumentoAutorizacionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TipoDocumentoAutorizacionService = TestBed.get(TipoDocumentoAutorizacionService);
    expect(service).toBeTruthy();
  });
});
