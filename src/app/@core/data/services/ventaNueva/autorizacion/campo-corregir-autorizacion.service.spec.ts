import { TestBed } from '@angular/core/testing';

import { CampoCorregirAutorizacionService } from './campo-corregir-autorizacion.service';

describe('CampoCorregirAutorizacionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CampoCorregirAutorizacionService = TestBed.get(CampoCorregirAutorizacionService);
    expect(service).toBeTruthy();
  });
});
