import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {
  AutorizacionRegistroData,
  AutorizacionRegistro,
  AuthRegistro,
} from '../../../interfaces/ventaNueva/autorizacion/autorizacion-registro';

@Injectable()
export class AutorizacionRegistroService extends AutorizacionRegistroData {

  private baseURL;
  private baseURL2;


  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;

  }

  get(): Observable<AutorizacionRegistro[]> {
    return this.http.get<AutorizacionRegistro[]>(this.baseURL2 + 'v1/autorizacion-registro/' + sessionStorage.getItem('Empleado'));
  }

  getById(id: number): Observable<AutorizacionRegistro> {
    return this.http.get<AutorizacionRegistro>(this.baseURL2 + 'v1/autorizacion-registro-vn/get-by-id/' + id);
  }

  getByIdIn(id: number): Observable<AutorizacionRegistro> {
    return this.http.get<AutorizacionRegistro>(this.baseURL2 + 'v1/autorizacion-registro-inbound/get-by-id/' + id);
  }

  put(id: number, autorizacion: AutorizacionRegistro): Observable<AutorizacionRegistro> {
    return this.http.put<AutorizacionRegistro>(this.baseURL + 'v1/autorizacion-registro-vn/' + id, autorizacion);
  }

  putIn(id: number, autorizacion: AutorizacionRegistro): Observable<AutorizacionRegistro> {
    return this.http.put<AutorizacionRegistro>(this.baseURL + 'v1/autorizacion-registro-inbound/' + id, autorizacion);
  }

  post(idRegistro: number): Observable<AutorizacionRegistro> {
    return this.http.post<AutorizacionRegistro>(this.baseURL + 'v1/autorizacion-registro/' + idRegistro + '/'
      + sessionStorage.getItem('Empleado'), null);
  }

  documentoVerificado(idAutorizacionRegistro: number, documentoVerificado: number, estado: number): Observable<String> {
    return this.http.put<string>(this.baseURL + 'v1/autorizacion-registro/' + idAutorizacionRegistro + '/'
      + documentoVerificado + '/' + estado, null, {responseType: 'text' as 'json'});
  }

  getIdFlujoPoliza(idFlujoPoliza: number): Observable<AutorizacionRegistro[]> {
    return this.http.get<AutorizacionRegistro[]>
    (this.baseURL2 + 'v1/autorizacion-registro/' + sessionStorage.getItem('Empleado') + '/' + idFlujoPoliza);
  }

  getGastosMedicos(): Observable<AutorizacionRegistro[]> {
    return this.http.get<AutorizacionRegistro[]>
    (this.baseURL2 + 'v1/autorizacion-registro/gm/' + sessionStorage.getItem('Empleado'));
  }

  getSubsecuentes(): Observable<AutorizacionRegistro[]> {
    return this.http.get<AutorizacionRegistro[]>
    (this.baseURL2 + 'v1/autorizacion-registro-subsecuente/' + sessionStorage.getItem('Empleado'));
  }

  getCobranza(): Observable<AutorizacionRegistro[]> {
    return this.http.get<AutorizacionRegistro[]>
    (this.baseURL2 + 'v1/autorizacion-registro-cobranza/' + sessionStorage.getItem('Empleado'));
  }

  getAutorizacionVn(): Observable<AutorizacionRegistro[]> {
    return this.http.get<AutorizacionRegistro[]>
    (this.baseURL2 + 'v1/autorizacion-registro-vn/' + sessionStorage.getItem('Empleado'));
  }

  getAutorizacionEcommerce(): Observable<AutorizacionRegistro[]> {
    return this.http.get<AutorizacionRegistro[]>
    (this.baseURL2 + 'v1/autorizacion-registro-ecommerce/' + sessionStorage.getItem('Empleado'));
  }

  getTabla(estadoVerificacion: string): Observable<AutorizacionRegistro[]> {
    return this.http.get<AutorizacionRegistro[]>
    (this.baseURL2 + 'v1/autorizacion-registro-vn/tabla/' + estadoVerificacion + '/' + sessionStorage.getItem('Empleado'));
  }

  getTablaIn(estadoVerificacion: string): Observable<AutorizacionRegistro[]> {
    return this.http.get<AutorizacionRegistro[]>
    (this.baseURL2 + 'v1/autorizacion-registro-inbound/tabla/' + estadoVerificacion + '/' + sessionStorage.getItem('Empleado'));
  }

  getAuth(): Observable<AuthRegistro[]> {
    return this.http.get<AuthRegistro[]>
    (this.baseURL2 + 'v1/autorizacion-registro-vn/auth/' + sessionStorage.getItem('Empleado'));
  }

  getTablaCobranza(estadoVerificacion: string): Observable<AutorizacionRegistro[]> {
    return this.http.get<AutorizacionRegistro[]>
    (this.baseURL2 + 'v1/autorizacion-registro-cobranza/tabla/' + estadoVerificacion + '/' + sessionStorage.getItem('Empleado'));
  }

  getTablaCobranzaReno(estadoVerificacion: string): Observable<AutorizacionRegistro[]> {
    return this.http.get<AutorizacionRegistro[]>
    (this.baseURL + 'v1/autorizacion-registro-cobranza-reno/tabla/' + estadoVerificacion + '/' + sessionStorage.getItem('Empleado'));
  }

  getTablaEcommerce(estadoVerificacion: string): Observable<AutorizacionRegistro[]> {
    return this.http.get<AutorizacionRegistro[]>
    (this.baseURL2 + 'v1/autorizacion-registro-ecommerce/tabla/' + estadoVerificacion + '/' + sessionStorage.getItem('Empleado'));
  }

  getTablaSubsecuentes(estadoVerificacion: string): Observable<AutorizacionRegistro[]> {
    return this.http.get<AutorizacionRegistro[]>
    (this.baseURL2 + 'v1/autorizacion-registro-subsecuente/tabla/' + estadoVerificacion + '/' + sessionStorage.getItem('Empleado'));
  }

  getTablaSubsecuentesReno(estadoVerificacion: string): Observable<AutorizacionRegistro[]> {
    return this.http.get<AutorizacionRegistro[]>
    (this.baseURL2 + 'v1/autorizacion-registro-subsecuente/tabla/SubReno/' + estadoVerificacion + '/' + sessionStorage.getItem('Empleado'));
  }

  getTablaSubsecuentesPeru(estadoVerificacion: string): Observable<AutorizacionRegistro[]> {
    return this.http.get<AutorizacionRegistro[]>
    (this.baseURL2 + 'v1/autorizacion-registro-subsecuente/tabla/SubPeru/' + estadoVerificacion + '/' + sessionStorage.getItem('Empleado'));
  }

  getVnById(id: number): Observable<AutorizacionRegistro> {
    return this.http.get<AutorizacionRegistro>(this.baseURL2 + 'v1/autorizacion-registro-vn/get-by-id/' + id);
  }

  getEcommerceById(id: number): Observable<AutorizacionRegistro> {
    return this.http.get<AutorizacionRegistro>(this.baseURL2 + 'v1/autorizacion-registro-ecommerce/get-by-id/' + id);
  }

  documentoVerificadoVn(idAutorizacionRegistro: number, documentoVerificado: number, estado: number): Observable<String> {
    return this.http.put<string>(this.baseURL + 'v1/autorizacion-registro-vn/' + idAutorizacionRegistro + '/'
      + documentoVerificado + '/' + estado, null, {responseType: 'text' as 'json'});
  }

  documentoVerificadoVnIn(idAutorizacionRegistro: number, documentoVerificado: number, estado: number): Observable<String> {
    return this.http.put<string>(this.baseURL + 'v1/autorizacion-registro-inbound/' + idAutorizacionRegistro + '/'
      + documentoVerificado + '/' + estado, null, {responseType: 'text' as 'json'});
  }

  documentoVerificadoSubsecuentes(idAutorizacionRegistro: number, documentoVerificado: number, estado: number): Observable<String> {
    return this.http.put<string>(this.baseURL + 'v1/autorizacion-registro-subsecuente/' + idAutorizacionRegistro + '/'
      + documentoVerificado + '/' + estado, null, {responseType: 'text' as 'json'});
  }

  gerPeru(): Observable<AutorizacionRegistro[]> {
    return this.http.get<AutorizacionRegistro[]>
    (this.baseURL2 + 'v1/autorizacion-registro/peru/' + sessionStorage.getItem('Empleado'));
  }

  getPeruById(id: number): Observable<AutorizacionRegistro> {
    return this.http.get<AutorizacionRegistro>(this.baseURL2 + 'v1/autorizacion-registro/peru/get-by-id/' + id);
  }

  getCobranzaById(idCliente: number): Observable<AutorizacionRegistro> {
    return this.http.get<AutorizacionRegistro>(this.baseURL2 + 'v1/autorizacion-registro-cobranza/getById/' + idCliente);
  }

  getSubsecuentesById(idCliente: number): Observable<AutorizacionRegistro> {
    return this.http.get<AutorizacionRegistro>(this.baseURL2 + 'v1/autorizacion-registro-subsecuente/get-by-id/' + idCliente);
  }

  getSubPeruById(id: number): Observable<AutorizacionRegistro> {
    return this.http.get<AutorizacionRegistro>(this.baseURL2 + 'v1/autorizacion-registro-subsecuente/get-by-id/sub-peru/' + id);
  }

  getSubsecuentesPeruById(idCliente: number): Observable<AutorizacionRegistro> {
    return this.http.get<AutorizacionRegistro>(this.baseURL2 +
      'v1/autorizacion-registro-subsecuente/get-by-id/sub-peru/' + idCliente);
  }
}
