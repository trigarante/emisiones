import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {ErroresAnexos, ErroresAnexosData} from '../../../interfaces/ventaNueva/autorizacion/errores-anexos';

@Injectable()
export class ErroresAnexosService extends ErroresAnexosData {
  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES + 'v1/errores-anexos/';
  }

  get(): Observable<ErroresAnexos[]> {
    return this.http.get<ErroresAnexos[]>(this.baseURL);
  }

  getByIdAutorizacionRegitro(idAutorizacionRegistro: number, idTipoDocumento: number): Observable<ErroresAnexos> {
    return this.http.get<ErroresAnexos>(
      this.baseURL + idAutorizacionRegistro + '/' + idTipoDocumento);
  }

  getComentarios(idVerificacionRegistro: number, idTipoDocumento: number): Observable<ErroresAnexos[]> {
    return this.http.get<ErroresAnexos[]>(this.baseURL + 'get-comentarios/' +
      idVerificacionRegistro + '/' + idTipoDocumento);
  }

  post(error: ErroresAnexos): Observable<string> {
    return this.http.post<string>(this.baseURL, error, {responseType: 'text' as 'json'});
  }

  put(idError: number, error: ErroresAnexos): Observable<any> {
    return this.http.put(this.baseURL, error);
  }

  putEstadoCorreccion(idErrorAnexo: number): Observable<any> {
    return this.http.put(this.baseURL + 'update-estado/' + idErrorAnexo, null);
  }
}
