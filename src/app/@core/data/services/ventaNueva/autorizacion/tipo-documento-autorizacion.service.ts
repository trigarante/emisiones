import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../../../environments/environment";
import {Observable} from "rxjs";
import {TipoDocumentos} from "../../../interfaces/verificacion/tipo-documentos";
import {TipoDocumentosAutorizacionData} from "../../../interfaces/ventaNueva/autorizacion/tipo-documento-autorizacion";

@Injectable()

export class TipoDocumentoAutorizacionService extends  TipoDocumentosAutorizacionData {
  private baseURL: string;
  private baseURL2;


  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;

  }

  get(idTabla: number): Observable<TipoDocumentos[]> {
    return this.http.get<TipoDocumentos[]>(this.baseURL2 + 'v1/tipo-documentos/' + idTabla);
  }
}
