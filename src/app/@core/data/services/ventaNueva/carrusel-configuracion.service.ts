import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {CarruselConfiguracion, CarruselConfiguracionData} from '../../interfaces/ventaNueva/carrusel-configuracion';

@Injectable({
  providedIn: 'root',
})
export class CarruselConfiguracionService extends CarruselConfiguracionData {

  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
  }

  get(): Observable<CarruselConfiguracion[]> {
    return this.http.get<CarruselConfiguracion[]>(this.baseURL + 'v1/carrusel-configuracion');
  }

  getRegistroById(idCarrusel): Observable<CarruselConfiguracion> {
    return this.http.get<CarruselConfiguracion>(this.baseURL + 'v1/carrusel-configuracion/' + idCarrusel);
  }
  post(Carrusel: CarruselConfiguracion): Observable<CarruselConfiguracion> {
    return this.http.post<CarruselConfiguracion>(this.baseURL + 'v1/carrusel-configuracion/', Carrusel);
  }

  put(idCarrusel, Carrusel: CarruselConfiguracion): Observable<CarruselConfiguracion> {
    return this.http.put<CarruselConfiguracion>(this.baseURL + 'v1/carrusel-configuracion/' + idCarrusel, Carrusel);
  }

}
