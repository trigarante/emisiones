import { TestBed } from '@angular/core/testing';

import { StepsComplementosService } from './steps-complementos.service';

describe('StepsComplementosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StepsComplementosService = TestBed.get(StepsComplementosService);
    expect(service).toBeTruthy();
  });
});
