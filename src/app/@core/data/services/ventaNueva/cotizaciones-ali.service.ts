import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {CotizacionesAli} from '../../interfaces/ventaNueva/cotizaciones-ali';

@Injectable()
export class CotizacionesAliService {

  private baseURL;
  private baseURL2;


  constructor(private http: HttpClient) {
    // super();
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES + 'v1/cotizacionesAli';
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura + 'v1/cotizacionesAli';

  }

  get(): Observable<CotizacionesAli[]> {
    return this.http.get<CotizacionesAli[]>(this.baseURL2);
  }

  getByIdCotizacion(idCotizacion: number): Observable<CotizacionesAli> {
    return this.http.get<CotizacionesAli>(this.baseURL2 + '/idCotizacion/' + idCotizacion);
  }

  post(cotizacionAli: CotizacionesAli): Observable<CotizacionesAli> {
    return this.http.post<CotizacionesAli>(this.baseURL, cotizacionAli);
  }

  getCotizacionAliById(id): Observable<CotizacionesAli> {
    return this.http.get<CotizacionesAli>(this.baseURL2 + '/' + id);
  }

  put(idCotizacionAli, cotizacionAli: CotizacionesAli): Observable<CotizacionesAli> {
    return this.http.put<CotizacionesAli>(this.baseURL + '/' + idCotizacionAli + '/'
      + sessionStorage.getItem('Empleado'), cotizacionAli);
  }
}
