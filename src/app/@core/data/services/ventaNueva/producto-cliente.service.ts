import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {
  ProductoCliente,
  ProductoClienteData,
  ProductoClienteOnline
} from '../../interfaces/ventaNueva/producto-cliente';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable()
export class ProductoClienteService extends ProductoClienteData {

  private baseURL;
  private baseURLADM;
  private baseURLNode;
  public aseguradora = new BehaviorSubject<number>(0);
  public idCliente = new BehaviorSubject<number>(0);
  public idProductoCliente = new BehaviorSubject<number>(0);
  public idRecibo = new BehaviorSubject<number>(0);
  private baseURL2;


  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.baseURLADM = environment.GLOBAL_SERVICIOS;
    this.baseURLNode = environment.SERVICIOS_NODE;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;

  }

  get(): Observable<ProductoCliente[]> {
    return this.http.get<ProductoCliente[]>(this.baseURL2 + 'v1/producto-cliente');
  }

  getProductoClienteById(idProductoCliente): Observable<ProductoCliente> {
    return this.http.get<ProductoCliente>(this.baseURL2 + 'v1/producto-cliente/' + idProductoCliente);
  }
  getProductoClienteByIdOnline(idProductoCliente): Observable<ProductoClienteOnline> {
    return this.http.get<ProductoClienteOnline>(this.baseURL2 + 'v1/producto-cliente/' + idProductoCliente);
  }
  getProductoClienteByIdADM(idProductoCliente): Observable<ProductoCliente> {
    return this.http.get<ProductoCliente>(this.baseURLADM + 'v1/producto-cliente/' + idProductoCliente);
  }

  getProductoClienteByIdNormal(idProductoCliente): Observable<ProductoCliente> {
    return this.http.get<ProductoCliente>(this.baseURL2 + 'v1/producto-cliente/model/' + idProductoCliente);
  }

  getProductoClienteByidSolicitud(idSolicitud): Observable<ProductoCliente> {
    return this.http.get<ProductoCliente>(this.baseURL2 + 'v1/producto-cliente/Solicitud/' + idSolicitud);
  }

  post(productoCliente: ProductoCliente): Observable<ProductoCliente> {
    return this.http.post<ProductoCliente>(this.baseURL + 'v1/producto-cliente', productoCliente);
  }

  put(idProductoCliente, productoCliente: ProductoCliente): Observable<ProductoCliente> {
    return this.http.put<ProductoCliente>(this.baseURL + 'v1/producto-cliente/' + idProductoCliente
      + '/' + sessionStorage.Empleado, productoCliente);
  }

  putCliente(id, idLlamadaEntrantes): Observable<ProductoCliente> {
    return this.http.put<ProductoCliente>(this.baseURL + 'venta-nueva/producto-clientes/' + id + '/'
      + idLlamadaEntrantes, null);
  }

  putProductoCliente(id, idLlamadaEntrante): Observable<string> {
    return this.http.put<string>(`${this.baseURLNode}/venta-nueva/producto-clientes`, {id: id, idLlamadaEntrantes: idLlamadaEntrante});
  }
  putPD(idProductoCliente, idEmision): Observable<any> {
    return this.http.put<any>(this.baseURL + 'v1/emisiones/pd/' + idProductoCliente + '/' + idEmision, null);
  }

  entryAseguradora(value) {
    this.aseguradora.next(value);
  }

  returnAseguradora(): Observable<number> {
    return this.aseguradora.asObservable();
  }

  entryIdProductoCliente(value) {
    this.idProductoCliente.next(value);
  }
  entryIdCliente(value) {
    this.idCliente.next(value);
  }
  returnIdCliente(): Observable<number> {
    return this.idCliente.asObservable();
  }

  returnIdProductoCliente(): Observable<number> {
    return this.idProductoCliente.asObservable();
  }

  entryIdRecibo(value) {
    this.idRecibo.next(value);
  }

  returnIdRecibo(): Observable<number> {
    return this.idRecibo.asObservable();
  }

}
