import { TestBed } from '@angular/core/testing';

import { ValidacionTelefonoService } from './validacion-telefono.service';

describe('ValidacionTelefonoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ValidacionTelefonoService = TestBed.get(ValidacionTelefonoService);
    expect(service).toBeTruthy();
  });
});
