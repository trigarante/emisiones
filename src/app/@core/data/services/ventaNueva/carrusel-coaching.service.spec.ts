import { TestBed } from '@angular/core/testing';

import { CarruselCoachingService } from './carrusel-coaching.service';

describe('CarruselCoachingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CarruselCoachingService = TestBed.get(CarruselCoachingService);
    expect(service).toBeTruthy();
  });
});
