import { Injectable } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {VerificacionRegistro} from '../../interfaces/verificacion/verificacion-registro';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class VerificacionesPendientesService {
  private baseUrl: string;
  private baseURL2;


  constructor(private http: HttpClient) {
    this.baseUrl = environment.GLOBAL_SERVICIOS_OPERACIONES + 'v1/verificaciones-pendientes';
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura + 'v1/verificaciones-pendientes';

  }

  get(): Observable<VerificacionRegistro[]> {
    return this.http.get<VerificacionRegistro[]>(this.baseURL2 + '/' + sessionStorage.getItem('Empleado'));
  }
}
