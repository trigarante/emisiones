import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {BehaviorSubject, Observable} from 'rxjs';
import {Recibos, RecibosComplemento, RecibosData} from '../../interfaces/ventaNueva/recibos';

@Injectable()
export class RecibosService extends RecibosData {

  private baseURL;
  private baseURLFN;
  private baseURL2;

  public idRecibo = new BehaviorSubject<number>(0);
  constructor(private http: HttpClient) {
    super();
    this.baseURLFN = environment.GLOBAL_SERVICIOS;
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;

  }

  get(): Observable<Recibos[]> {
    return this.http.get<Recibos[]>(this.baseURL2 + 'v1/recibovn');
  }

  post(recibos: Recibos[]): Observable<string> {
    return this.http.post<string>(this.baseURL + 'v1/recibovn/', recibos,
      {responseType: 'text' as 'json'});
  }

  postConId(recibos: any[], id: number): Observable<any> {
    return this.http.post<string>(this.baseURL + 'v1/recibovn/' + id, recibos,
      {responseType: 'text' as 'json'});
  }

  getReciboById(idRecibo): Observable<Recibos> {
    return this.http.get<Recibos>(this.baseURL2 + 'v1/recibovn/' + idRecibo);
  }

  getRecibosByIdRegistro (idRegistro: number): Observable<Recibos[]> {
    return this.http.get<Recibos[]>(this.baseURL2 + 'v1/recibovn/registro/' + idRegistro);
  }

  getRecibosByIdRegistroComplemento (idRegistro: number): Observable<RecibosComplemento[]> {
    return this.http.get<RecibosComplemento[]>(this.baseURL2 + 'v1/recibovn-complemento/registro/' + idRegistro);
  }
  getRecibosByIdRegistroConPago (idRegistro: number): Observable<Recibos[]> {
    return this.http.get<Recibos[]>(this.baseURLFN + 'v1/recibovn/pago/' + idRegistro);
  }
  getRecibosByIdRegistroConNumero (idRegistro: number, numero: number): Observable<Recibos> {
    return this.http.get<Recibos>(this.baseURLFN + 'v1/recibovn/pagoEndoso/' + idRegistro + '/' + numero );
  }

  getRecibosByIdRegistroADM (idRegistro: number): Observable<Recibos[]> {
    return this.http.get<Recibos[]>(this.baseURLFN + 'v1/recibovn/registro/' + idRegistro);
  }

  put(idRecibo, recibo: Recibos): Observable<Recibos> {
    return this.http.put<Recibos>(this.baseURL + 'v1/recibovn/' + idRecibo + '/'
      + sessionStorage.getItem('Empleado'), recibo);
  }

  entryIdRecibo(value) {
    this.idRecibo.next(value);
  }

  returnIdRecibo(): Observable<number> {
    return this.idRecibo.asObservable();
  }

  putActivo(idRecibo: number) {
    return this.http.put(this.baseURL + 'v1/recibovn/baja-recibos/' + idRecibo, null);
  }

  putBajaSinPago(idRecibo: number) {
    return this.http.put(this.baseURL + 'v1/recibovn/baja-recibos-sin-pago/' + idRecibo, null);
  }

  putEstadoRecibo(idRecibo: number, idEstadoRecibo: number): Observable<any> {
    return this.http.put(this.baseURL + 'v1/recibovn/estado-recibo/' + idRecibo + '/' + idEstadoRecibo, null);
  }

  putIdEmpleado(idEmpleado: number, idRegistro: number): Observable<any> {
    return this.http.put(this.baseURL + 'v1/recibovn/actualizar-recibos/' + idEmpleado + '/' + idRegistro, null);
  }

  getReciboPago (idRegistro: number): Observable<Recibos[]> {
    return this.http.get<Recibos[]>(this.baseURL2 + 'v1/recibos-pago/get-id-registro/' + idRegistro);
  }
}
