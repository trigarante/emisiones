import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../../environments/environment";
import {Observable} from "rxjs";
import {MiEstadoCuenta} from "../../interfaces/ventaNueva/mi-estado-cuenta";

@Injectable({
  providedIn: 'root',
})
export class MiEstadoCuentaService {

  private baseURL;
  private socketURL;
  private baseURL2;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.socketURL = environment.GLOBAL_SOCKET;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;

  }

  getAllRegistroByIdUsuarioRegistro(fechaInicio: number, fechaFin: number): Observable<MiEstadoCuenta[]> {
    return this.http.get<MiEstadoCuenta[]>(this.baseURL2 +
      'v1/estado-cuenta/getRegistro/' + sessionStorage.getItem('Empleado') + '/' +
    fechaInicio + '/' + fechaFin);
  }

  getAllRegistroByIdUsuarioAplicado(fechaInicio: number, fechaFin: number): Observable<MiEstadoCuenta[]> {
    return this.http.get<MiEstadoCuenta[]>(this.baseURL2 +
      'v1/estado-cuenta/getAplicado/' + sessionStorage.getItem('Empleado') + '/' +
      fechaInicio + '/' + fechaFin);
  }
}
