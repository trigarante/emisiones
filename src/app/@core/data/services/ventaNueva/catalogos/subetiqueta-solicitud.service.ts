import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {SubetiquetaSolicitud} from '../../../interfaces/ventaNueva/catalogos/subetiqueta-solicitud';

@Injectable({
  providedIn: 'root',
})
export class SubetiquetaSolicitudService {
  url: string;

  constructor(private http: HttpClient) {
    this.url = environment.SERVICIOS_DATOS;
  }
  getByEtiqueta(idEtiqueta: number): Observable<SubetiquetaSolicitud[]> {
    return this.http.get<SubetiquetaSolicitud[]>(`${this.url}/tipificaciones/subetiqueta/${idEtiqueta}`);
  }
}
