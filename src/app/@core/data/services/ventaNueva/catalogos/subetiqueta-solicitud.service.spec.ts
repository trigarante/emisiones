import { TestBed } from '@angular/core/testing';

import { SubetiquetaSolicitudService } from './subetiqueta-solicitud.service';

describe('SubetiquetaSolicitudService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SubetiquetaSolicitudService = TestBed.get(SubetiquetaSolicitudService);
    expect(service).toBeTruthy();
  });
});
