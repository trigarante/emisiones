import { Injectable } from '@angular/core';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {BancoExterno, BancoVn, BancoVnData} from '../../../interfaces/ventaNueva/catalogos/banco-vn';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class BancoVnService extends BancoVnData {

  private baseURL;
  private baseURL2;


  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;

  }

  get(): Observable<BancoVn[]> {
    return this.http.get<BancoVn[]>(this.baseURL2 + 'v1/bancos');
  }

  getActivos(activos: number): Observable<BancoVn[]> {
    return this.http.get<BancoVn[]>(this.baseURL2 + 'v1/bancos/activos/' + activos);
  }

  getBancoById(id): Observable<BancoVn> {
    return this.http.get<BancoVn>(this.baseURL2 + 'v1/bancos/' + id);
  }

  post(banco): Observable<BancoVn> {
    return this.http.post<BancoVn>(this.baseURL + 'v1/bancos', banco);
  }

  put(id, banco): Observable<BancoVn> {
    return this.http.put<BancoVn>(this.baseURL + 'v1/bancos' + id, banco);
  }

  getexterno(): Observable<BancoExterno[]> {
    return this.http.get<BancoExterno[]>( 'https://ws-qualitas.com/catalogos/bancos');
  }
}
