import { TestBed } from '@angular/core/testing';

import { FlujoSolicitudService } from './flujo-solicitud.service';

describe('FlujoSolicitudService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FlujoSolicitudService = TestBed.get(FlujoSolicitudService);
    expect(service).toBeTruthy();
  });
});
