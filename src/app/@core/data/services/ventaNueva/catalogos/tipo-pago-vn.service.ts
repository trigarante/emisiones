import {Injectable} from '@angular/core';
import {TipoPagoData, TipoPagoVn} from '../../../interfaces/ventaNueva/catalogos/tipo-pago-vn';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../../../environments/environment';

@Injectable()
export class TipoPagoVnService extends TipoPagoData {

  private baseURL;
  private socketURL;
  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<TipoPagoVn[]> {
    return this.http.get<TipoPagoVn[]>(this.baseURL + 'v1/tipo-pago');
  }

  getAllReno(): Observable<any[]> {
    return this.http.get<any[]>(this.baseURL + 'v1/tipo-pago/all-tipo-pago-reno');
  }

  getAll(): Observable<TipoPagoVn[]> {
    return this.http.get<TipoPagoVn[]>(this.baseURL + 'v1/tipo-pago/all');
  }

  getPeru(): Observable<TipoPagoVn[]> {
    return this.http.get<TipoPagoVn[]>(this.baseURL + 'v1/tipo-pago/peru');
  }

  post(tipoPago: TipoPagoVn): Observable<TipoPagoVn> {
    return this.http.post<TipoPagoVn>(this.baseURL + 'v1/tipo-pago', tipoPago);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'vntipopago/saveVnTipoPago', json);
  }

  getTipoPagoById(idTipoPago): Observable<TipoPagoVn> {
    return this.http.get<TipoPagoVn>(this.baseURL + 'v1/tipo-pago/' + idTipoPago);
  }

  put(idTipoPago, tipoPago: TipoPagoVn): Observable<TipoPagoVn> {
    return this.http.put<TipoPagoVn>(this.baseURL + 'v1/tipo-pago/' + idTipoPago + '/'
      + sessionStorage.getItem('Empleado'), tipoPago);
    /*+ sessionStorage.getItem('Empleado')*/
  }
}
