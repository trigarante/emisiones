import { Injectable } from '@angular/core';
import {EstadoInspeccion, EstadoInspeccionData} from '../../../interfaces/ventaNueva/catalogos/estado-inspeccion';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../../../environments/environment';

@Injectable()
export class EstadoInspeccionService extends EstadoInspeccionData {
  private baseURL;
  private baseURL2;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;

  }
  get(): Observable<EstadoInspeccion[]> {
    return this.http.get<EstadoInspeccion[]>(this.baseURL2 + 'v1/estado-inspeccion');
  }

  getActivos(): Observable<EstadoInspeccion[]> {
    return this.http.get<EstadoInspeccion[]>(this.baseURL2 + 'v1/estado-inspeccion/activos');
  }

  post(estadoInspeccion: EstadoInspeccion): Observable<EstadoInspeccion> {
    return this.http.post<EstadoInspeccion>(this.baseURL + 'v1/estado-inspeccion', estadoInspeccion);
  }

  getEstadoInsoeccionById(idEstadoInspeccion): Observable<EstadoInspeccion> {
    return this.http.get<EstadoInspeccion>(this.baseURL2 + 'v1/estado-inspeccion/' + idEstadoInspeccion);
  }

  put(idEstadoInspeccion, estadoInspeccion: EstadoInspeccion): Observable<EstadoInspeccion> {
    return this.http.put<EstadoInspeccion>(this.baseURL + 'v1/estado-inspeccion/' + idEstadoInspeccion, estadoInspeccion);
    // + sessionStorage.getItem('Empleado')
  }
}
