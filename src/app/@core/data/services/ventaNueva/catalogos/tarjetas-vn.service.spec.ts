import { TestBed } from '@angular/core/testing';

import { TarjetasVnService } from './tarjetas-vn.service';

describe('TarjetasVnService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TarjetasVnService = TestBed.get(TarjetasVnService);
    expect(service).toBeTruthy();
  });
});
