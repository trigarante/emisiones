import { TestBed } from '@angular/core/testing';

import { TarjetasBancoVnService } from './tarjetas-banco-vn.service';

describe('TarjetasBancoVnService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TarjetasBancoVnService = TestBed.get(TarjetasBancoVnService);
    expect(service).toBeTruthy();
  });
});
