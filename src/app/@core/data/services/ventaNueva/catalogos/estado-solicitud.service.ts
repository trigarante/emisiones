import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {EstadoSolicitud} from '../../../interfaces/ventaNueva/catalogos/estado-solicitud';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EstadoSolicitudService {

  private baseURL;
  private servicioNode;
  private baseURL2;


  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.servicioNode = environment.SERVICIOS_NODE;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;

  }

  get(): Observable<EstadoSolicitud[]> {
    return this.http.get<EstadoSolicitud[]>(this.baseURL2 + 'v1/etiqueta-solicitud');
  }

  // getNode(idSubarea: string): Observable<EstadoSolicitud[]> {
  //   return this.http.get<EstadoSolicitud[]>(this.servicioNode + '/venta-nueva/catalogos/estado-solicitud/' + idSubarea);
  // }

  getNode(idSubarea: string): Observable<EstadoSolicitud[]> {
    return this.http.get<EstadoSolicitud[]>(`${this.servicioNode}/tipificaciones/estado-solicitud`);
  }

  getNodeReno(idSubarea: string): Observable<EstadoSolicitud[]> {
    return this.http.get<EstadoSolicitud[]>(`${this.servicioNode}/tipificaciones/estado-solicitud-reno`);
  }

  getEstadosCobranza(): Observable<EstadoSolicitud[]> {
    return this.http.get<EstadoSolicitud[]>(`${this.servicioNode}/tipificaciones/estado-solicitud-cobranza`);
  }

  post(estadoSolicitud: EstadoSolicitud): Observable<EstadoSolicitud> {
    return this.http.post<EstadoSolicitud>(this.baseURL + 'v1/etiqueta-solicitud', estadoSolicitud);
  }

  getAreaById(idEstadoSolicitud): Observable<EstadoSolicitud> {
    return this.http.get<EstadoSolicitud>(this.baseURL2 + 'v1/etiqueta-solicitud/' + idEstadoSolicitud);
  }

  put(idEstadoSolicitud, estadoSolicitud: EstadoSolicitud) {
    return this.http.put<EstadoSolicitud>(this.baseURL + 'v1/etiqueta-solicitud/' + idEstadoSolicitud, estadoSolicitud);
  }

  getEtiquetaGM(): Observable<EstadoSolicitud[]> {
    return this.http.get<EstadoSolicitud[]>(`${this.servicioNode}/tipificaciones/estado-solicitud-gm`);
  }
}
