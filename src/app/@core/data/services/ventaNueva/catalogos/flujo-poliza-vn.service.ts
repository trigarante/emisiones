import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
// import {Global} from '../../global';
import {FlujoPolizaData, FlujoPolizaVn} from '../../../interfaces/ventaNueva/catalogos/flujo-poliza-vn';
import {Observable} from 'rxjs';
import {environment} from '../../../../../../environments/environment';

@Injectable()
export class FlujoPolizaVnService extends FlujoPolizaData {

  private baseURL;
  private socketURL;
  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<FlujoPolizaVn[]> {
    return this.http.get<FlujoPolizaVn[]>(this.baseURL + 'v1/flujo-poliza');
  }

  post(flujoPoliza: FlujoPolizaVn): Observable<FlujoPolizaVn> {
    return this.http.post<FlujoPolizaVn>(this.baseURL + 'v1/flujo-poliza', flujoPoliza);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'v1/vnflujopoliza/saveVnFlujoPoliza', json);
  }

  getFlujoPolizaById(idFlujoPoliza): Observable<FlujoPolizaVn> {
    return this.http.get<FlujoPolizaVn>(this.baseURL + 'v1/flujo-poliza/' + idFlujoPoliza);
  }

  put(idFlujoPoliza, flujoPoliza: FlujoPolizaVn): Observable<FlujoPolizaVn> {
    return this.http.put<FlujoPolizaVn>(this.baseURL + 'v1/flujo-poliza/' + idFlujoPoliza, flujoPoliza);
    // return this.http.put<FlujoPolizaVn>(this.baseURL + 'v1/flujo-poliza/' + idFlujoPoliza + '/'
    //   + sessionStorage.getItem('Empleado'), flujoPoliza);
    // + sessionStorage.getItem('Empleado'),
  }
}
