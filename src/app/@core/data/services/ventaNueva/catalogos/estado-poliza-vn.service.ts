import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Global} from '../../global';
import {EstadoPolizaVn, EstadoPolizaVnData} from '../../../interfaces/ventaNueva/catalogos/estado-poliza-vn';
import {Observable} from 'rxjs';
import {environment} from '../../../../../../environments/environment';

@Injectable()
export class EstadoPolizaVnService extends EstadoPolizaVnData {

  private baseURL;
  private socketURL;
  private baseURL2;
  private CORE_VN2;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.socketURL = environment.GLOBAL_SOCKET;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;
    this.CORE_VN2 = environment.CORE_VN2;

  }

  // NEW
  get(): Observable<EstadoPolizaVn[]> {
    return this.http.get<EstadoPolizaVn[]>(this.CORE_VN2 + 'estado-poliza');
  }

  // FIN


  post(estadoPoliza: EstadoPolizaVn): Observable<EstadoPolizaVn> {
    return this.http.post<EstadoPolizaVn>(this.baseURL + 'v1/estado-poliza', estadoPoliza);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'v1/vnestadopoliza/saveVnEstadoPoliza', json);
  }

  getEstadoPolizaById(idEstadoPoliza): Observable<EstadoPolizaVn> {
    return this.http.get<EstadoPolizaVn>(this.baseURL2 + 'v1/estado-poliza/' + idEstadoPoliza);
  }

  put(idEstadoPoliza, estadoPoliza: EstadoPolizaVn): Observable<EstadoPolizaVn> {
    return this.http.put<EstadoPolizaVn>(this.baseURL + 'v1/estado-poliza/' + idEstadoPoliza, estadoPoliza);
    // return this.http.put<EstadoPolizaVn>(this.baseURL + 'v1/estado-poliza/' + idEstadoPoliza + '/'
    //   + sessionStorage.getItem('Empleado'), estadoPoliza);
    // + sessionStorage.getItem('Empleado')
  }
}
