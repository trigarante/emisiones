import { TestBed } from '@angular/core/testing';

import { EstadoRecibosService } from './estado-recibos.service';

describe('EstadoRecibosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EstadoRecibosService = TestBed.get(EstadoRecibosService);
    expect(service).toBeTruthy();
  });
});
