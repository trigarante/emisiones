import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {Periodicidad, PeriodicidadData} from '../../../interfaces/ventaNueva/catalogos/periodicidad';

@Injectable({
  providedIn: 'root',
})
export class PeriodicidadService extends PeriodicidadData {
  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
  }

  get(): Observable<Periodicidad[]> {
    return this.http.get<Periodicidad[]>(this.baseURL + 'v1/periodicidad');
  }

  getById(idPeriodicidad: number): Observable<Periodicidad> {
    return this.http.get<Periodicidad>(this.baseURL + 'v1/periodicidad/' + idPeriodicidad);
  }
}
