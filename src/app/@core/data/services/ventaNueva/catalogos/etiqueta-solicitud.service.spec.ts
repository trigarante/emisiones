import { TestBed } from '@angular/core/testing';

import { EtiquetaSolicitudService } from './etiqueta-solicitud.service';

describe('EtiquetaSolicitudService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EtiquetaSolicitudService = TestBed.get(EtiquetaSolicitudService);
    expect(service).toBeTruthy();
  });
});
