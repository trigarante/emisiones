import { TestBed } from '@angular/core/testing';

import { ImagenesSociosService } from './imagenes-socios.service';

describe('ImagenesSociosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ImagenesSociosService = TestBed.get(ImagenesSociosService);
    expect(service).toBeTruthy();
  });
});
