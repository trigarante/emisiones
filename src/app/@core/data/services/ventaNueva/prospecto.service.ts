import {Injectable} from '@angular/core';
import {Prospecto, ProspectoData} from '../../interfaces/ventaNueva/prospecto';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class ProspectoService extends ProspectoData {

  private baseURL;
  private nodeURL;
  private baseURL2;


  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.nodeURL = environment.SERVICIOS_NODE;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;

  }

  get(): Observable<Prospecto[]> {
    return this.http.get<Prospecto[]>(this.baseURL2 + 'v1/prospectovn');
  }

  post(prospecto: Prospecto): Observable<Prospecto> {
    return this.http.post<Prospecto>(this.baseURL + 'v1/prospectovn/', prospecto);
  }

  getProspectoById(idProspecto): Observable<Prospecto> {
    return this.http.get<Prospecto>(this.baseURL2 + 'v1/prospectovn/' + idProspecto);
  }

  put(idProspecto, prospecto: Prospecto): Observable<Prospecto> {
    return this.http.put<Prospecto>(this.baseURL + 'v1/prospectovn/' + idProspecto + '/'
      + sessionStorage.getItem('Empleado'), prospecto);
  }

  validaCorreo(correo): Observable<Prospecto> {
    return this.http.get<Prospecto>(this.baseURL2 + 'v1/prospectovn/correo/' + correo);
  }

  getProspectoByPhone(telefono: string): Observable<Prospecto> {
    return this.http.get<Prospecto>(`${this.nodeURL}/venta-nueva/prospecto/${telefono}`);
  }

}
