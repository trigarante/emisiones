import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Registro, RegistroComplemento, RegistrosData, RegistroSoloDatos} from '../../interfaces/ventaNueva/registro';
import {Observable} from 'rxjs';

@Injectable()
export class RegistroService extends RegistrosData {

  private baseURLFN;
  private baseURL;
  private socketURL;
  private baseURL2;


  constructor(private http: HttpClient) {
    super();
    this.baseURLFN = environment.GLOBAL_SERVICIOS;
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.socketURL = environment.GLOBAL_SOCKET;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;

  }

  get(): Observable<Registro[]> {
    return this.http.get<Registro[]>(this.baseURL2 + 'v1/registro-vn');
  }

  updateRegistroPolizaSoloDatos(datos: RegistroSoloDatos, id: number): Observable<Boolean> {
    return this.http.put<Boolean>(this.baseURL + 'v1/registro-vn/update-datos/' + id, datos);
  }

  getRegistroById(idRegistro): Observable<Registro> {
    return this.http.get<Registro>(this.baseURL2 + 'v1/registro-vn/' + idRegistro);
  }

  getRegistroComplementoById(idRegistro): Observable<RegistroComplemento> {
    return this.http.get<RegistroComplemento>(this.baseURL2 + 'v1/registro-complemento-vn/' + idRegistro);
  }

  getRegistroRetenciones(idSolicitud): Observable<Registro> {
    return this.http.get<Registro>(this.baseURL2 + 'v1/registro-vn/retenciones/' + idSolicitud);
  }
  getRegistroEmisionById(idRegistro): Observable<Registro> {
    return this.http.get<Registro>(this.baseURL2 + 'v1/registro-emision/' + idRegistro);
  }
  getRegistroByIdADM(idRegistro): Observable<Registro> {
    return this.http.get<Registro>(this.baseURLFN + 'v1/registro-vn/' + idRegistro);
  }
  getRegistroFinanzasById(idRegistro): Observable<Registro> {
    return this.http.get<Registro>(this.baseURLFN + 'v1/registro-finanzas/' + idRegistro);
  }

  getNoSerieExistente(numSerie: string): Observable<string> {
    return this.http.get<string>(this.baseURL2 + 'v1/registro-vn/buscar-no-serie/' + numSerie,
      {responseType: 'text' as 'json'});
  }

  getNoSerieExistenteRenovaciones(numSerie: string): Observable<string> {
    return this.http.get<string>(this.baseURL2 + 'v1/registro-vn/buscar-no-serie/renovaciones/' + numSerie,
      {responseType: 'text' as 'json'});
  }

  getPolizaExistente(poliza: string, idSocio: string, fechaInicio: string): Observable<string> {
    const fecha = new Date(fechaInicio);

    return this.http.get<string>(this.baseURL2 + `v1/registro-vn/existe-poliza`,
      {responseType: 'text' as 'json',
        params: {
          poliza,
          idSocio,
          mes: (fecha.getUTCMonth() + 1).toString(),
          anio: (fecha.getUTCFullYear()).toString(),
      }});
  }

  post(registro: Registro): Observable<Registro> {
    return this.http.post<Registro>(this.baseURL + 'v1/registro-vn/', registro);
  }
  postRF(registro: Registro): Observable<Registro> {
    return this.http.post<Registro>(this.baseURLFN + 'v1/registro-finanzas/', registro);
  }
  postInOne(registro: any): Observable<Registro> {
    return this.http.post<any>(this.baseURL + 'v1/registro-vn/all', registro);
  }

  postInOneComplemento(registro: any): Observable<RegistroComplemento> {
    return this.http.post<any>(this.baseURL + 'v1/registro-complemento-vn/complemento-all', registro);
  }

  put(idRegistro, registro: Registro): Observable<Registro> {
    return this.http.put<Registro>(this.baseURL + 'v1/registro-vn/' + idRegistro + '/'
      + sessionStorage.getItem('Empleado'), registro);
  }
  putRF(idRegistro, registro: Registro): Observable<Registro> {
    return this.http.put<Registro>(this.baseURLFN + 'v1/registro-finanzas/' + idRegistro + '/'
      + sessionStorage.getItem('Empleado'), registro);
  }

  postAllFinanzas(registroInOneFinanzas): Observable<any> {
    return this.http.post<any>(this.baseURLFN + `v1/registro-finanzas/all-finanzas`, registroInOneFinanzas);
  }

  updateArchivo(idRegistro: number, idCarpeta: string): Observable<any> {
    return this.http.put(this.baseURL + 'v1/registro-vn/carpeta-drive/' + idRegistro + '/' + idCarpeta, null);
  }

  updateArchivoComplemento(idRegistro: number, idCarpeta: string): Observable<any> {
    return this.http.put(this.baseURL + 'v1/registro-complemento-vn/complemento/carpeta-drive/' + idRegistro + '/' + idCarpeta, null);
  }

  updateEstadoPoliza(idRegistro: number, estado: number): Observable<any> {
    return this.http.put(this.baseURL + 'v1/registro-vn/update-estado/' + idRegistro + '/' + estado, null);
  }

  updateFlujoPoliza(idRegistro: number, idFlujoPoliza: number): Observable<any> {
    return this.http.put(this.baseURL + 'v1/registro-vn/update-flujo/' + idRegistro + '/' + idFlujoPoliza, null);
  }

  updateFlujoAndEstadoPoliza(idRegistro: number, idFlujoPoliza: number, idEstadoPoliza): Observable<any> {
    return this.http.put(this.baseURL + 'v1/registro-vn/update-flujo-estado/' + idRegistro + '/' + idFlujoPoliza
        + '/' + idEstadoPoliza, null);
  }

  modificarPolizaSocket(json): Observable<any> {
    return this.http.put<any>(this.socketURL + 'porAplicar/putPorAplicar', json);
  }
}
