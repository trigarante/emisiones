import {Injectable} from '@angular/core';
import {
  AdminVnViewData,
} from '../../interfaces/ventaNueva/adminVnView';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {SolicitudesVN} from '../../interfaces/ventaNueva/solicitudes';

@Injectable()
export class AdminVnViewService extends AdminVnViewData {

  private baseURL;
  private baseURL2;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  getAllRegistroByIdUsuario(): Observable<AdminVnViewData[]> {
    return this.http.get<AdminVnViewData[]>(this.baseURL2 +
      'v1/admin-vn/getPolizas/' + sessionStorage.getItem('Empleado'));
  }

  getAllRegistroByIdUsuarioIb(): Observable<AdminVnViewData[]> {
    return this.http.get<AdminVnViewData[]>(this.baseURL2 +
      'v1/admin-vn-ib/getPolizas/' + sessionStorage.getItem('Empleado'));
  }

  getRegistroById(idRegistro): Observable<AdminVnViewData> {
    return this.http.get<AdminVnViewData>(this.baseURL2 + 'v1/admin-vn/' + idRegistro);
  }

  // Sockets
  postPolizaSocket(json): Observable<AdminVnViewData> {
    return this.http.post<AdminVnViewData>(this.socketURL + 'vnAdministradorPolizas/saveVnPoliza', json);
  }

  updateClienteSockets(json): Observable<SolicitudesVN> {
    return this.http.put<SolicitudesVN>(this.socketURL + 'vnAdministradorPolizas/updateCliente', json);
  }

  updateNumeroSerieSockets(json): Observable<SolicitudesVN> {
    return this.http.put<SolicitudesVN>(this.socketURL + 'vnAdministradorPolizas/updateNumeroSerie', json);
  }

  updateArchivoSubidoSockets(json): Observable<SolicitudesVN> {
    return this.http.put<SolicitudesVN>(this.socketURL + 'vnAdministradorPolizas/updateArchivoSubido', json);
  }

  updatePagoSockets(json): Observable<SolicitudesVN> {
    return this.http.put<SolicitudesVN>(this.socketURL + 'vnAdministradorPolizas/updatePago', json);
  }

  updateEstadoPolizaSockets(json): Observable<SolicitudesVN> {
    return this.http.put<SolicitudesVN>(this.socketURL + 'vnAdministradorPolizas/updateEstadoPoliza', json);
  }
}
