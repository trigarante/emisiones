import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {StepsComplementos} from '../../interfaces/ventaNueva/steps-complementos';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StepsComplementosService {
  baseUrl: string;
  private baseURL2;


  constructor(private http: HttpClient) {
    this.baseUrl = environment.GLOBAL_SERVICIOS_OPERACIONES + 'v1/steps-complementos';
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura + 'v1/steps-complementos';

  }

  getByIdRegistro(idRegistro: number): Observable<StepsComplementos> {
    return this.http.get<StepsComplementos>(this.baseURL2 + '/' + idRegistro);
  }
}
