import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import { ClienteVn} from '../../interfaces/ventaNueva/cliente-vn';
import {ClientePeru} from '../../interfaces/ventaNueva/cliente-peru';


@Injectable({
  providedIn: 'root',
})
export class ClienteVnService  {

  private nodeURL;
  private baseURL;
  private baseURLOp;
  private socketURL;
  private baseURL2;
  private CORE_VN2;


  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.baseURLOp = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.socketURL = environment.GLOBAL_SOCKET;
    this.nodeURL = environment.SERVICIOS_NODE;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;
    this.CORE_VN2 = environment.CORE_VN2;


  }

  // NEW
  getClienteByIdEmision(idCliente): Observable<ClienteVn> {
    return this.http.get<ClienteVn>(this.CORE_VN2 + 'cliente/emision-cliente/' + idCliente);
  }

  // FIN NEW

  get(): Observable<ClienteVn[]> {
    return this.http.get<ClienteVn[]>(this.baseURL2 + 'v1/cliente');
  }

  getFinanzas(): Observable<ClienteVn[]> {
    return this.http.get<ClienteVn[]>(this.baseURL + 'v1/cliente/finanzas');
  }

  getClienteFinanzas(): Observable<ClienteVn[]> {
    return this.http.get<ClienteVn[]>(this.baseURL + 'v1/cliente-finanzas/finanzas');
  }

  getClienteFinanzasById(idClienteFinanzas): Observable<ClienteVn> {
    return this.http.get<ClienteVn>(this.baseURL + 'v1/cliente/finanzas/' + idClienteFinanzas);
  }

  post(cliente: ClienteVn): Observable<ClienteVn> {
    return this.http.post<ClienteVn>(this.baseURLOp + 'v1/cliente', cliente);
  }

  postClienteFinanzas(cliente: ClienteVn): Observable<ClienteVn> {
    return this.http.post<ClienteVn>(this.baseURL + 'v1/cliente-finanzas', cliente);
  }

  postSocket(json: JSON): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'vnClientes/saveVnClientes', json);
  }

  getClienteById(idCliente): Observable<ClienteVn> {
    return this.http.get<ClienteVn>(this.baseURL2 + 'v1/cliente/' + idCliente);
  }

  getClienteByIdADM(idCliente): Observable<ClienteVn> {
    return this.http.get<ClienteVn>(this.baseURL + 'v1/cliente/' + idCliente);
  }

  getFinanzasClienteById(idCliente): Observable<ClienteVn> {
    return this.http.get<ClienteVn>(this.baseURL + 'v1/cliente-finanzas/' + idCliente);
  }



  getClientePeruById(idCliente: number): Observable<ClientePeru> {
    return this.http.get<ClientePeru>(this.baseURL2 + 'v1/cliente/peru/' + idCliente);
  }

  getClienteFinPeruById(idCliente: number): Observable<ClientePeru> {
    return this.http.get<ClientePeru>(this.baseURL + 'v1/cliente-finanzas/peru/' + idCliente);
  }

  curpExist(curp) {
    return this.http.get(this.baseURL + 'v1/cliente/curp/' + curp);
  }

  rfcExist(rfc) {
    return this.http.get(this.baseURL + 'v1/cliente/rfc/' + rfc);
  }

  dniExist(dni: string): Observable<ClientePeru> {
    return this.http.get<ClientePeru>(this.baseURL + 'v1/cliente/dni/' + dni);
  }

  put(idCliente, cliente: ClienteVn): Observable<ClienteVn> {
    return this.http.put<ClienteVn>(this.baseURL + 'v1/cliente/' + idCliente + '/' +
      +sessionStorage.getItem('Empleado'), cliente);
  }

  putArchivo(idCliente: number, idCarpetaDrive: string): Observable<any> {
    return this.http.put(this.baseURL + 'v1/cliente/update-archivo/' + idCliente + '/' + idCarpetaDrive, null);
  }

  putArchivoSubido(idCliente: number, idArchivoSubido: number): Observable<any> {
    return this.http.put(this.baseURL + 'v1/cliente/update-archivo-subido/' + idCliente + '/' + idArchivoSubido, null);
  }
  getClienteByCellPhone(celular: string): Observable<ClienteVn[]> {
    return this.http.get<ClienteVn[]>(`${this.nodeURL}/venta-nueva/cliente/${celular}`);
  }

}
