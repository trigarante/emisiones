import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Llamadas, LlamadasData} from '../../interfaces/ventaNueva/llamadas';
import {Observable} from 'rxjs';
import {environment} from '../../../../../environments/environment';

@Injectable()
export class LlamadasService extends LlamadasData {
  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.LLAMADAS_MYCC;
  }
  get(): Observable<Llamadas[]> {
    return this.http.get<Llamadas[]>(this.baseURL + 'callback');
  }
  post(idUsuario, llamadas: Llamadas): Observable<Llamadas> {
   return this.http.post<Llamadas>(this.baseURL + 'callback/usuario/' + idUsuario, llamadas);
}
}
