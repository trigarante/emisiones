import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {ContactosCampana} from '../../../interfaces/reportes/contactos-campana';
import {ContactosProveedor} from "../../../interfaces/reportes/contactos-proveedor";

@Injectable({
  providedIn: 'root',
})
export class ContactosCampanaService {

  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }
  getContacto(idTipoSubarea: number, fechaInicio: number, fechaFin: number): Observable<ContactosCampana[]> {
    return this.http.get<ContactosCampana[]>(this.baseURL + 'v1/contactos-campana/' +
      idTipoSubarea + '/' + fechaInicio + '/' + fechaFin);
  }

  getContactoProveedor(fechaInicio: number, fechaFin: number): Observable<ContactosProveedor[]> {
    return this.http.get<ContactosProveedor[]>(this.baseURL + 'v1/contactos-campana/proveedor/fecha/' + fechaInicio + '/' + fechaFin);
  }
}
