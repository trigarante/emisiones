import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {CobranzaPendiente} from '../../../interfaces/reportes/cobranza-pendiente';
import {Leads} from '../../../interfaces/reportes/leads';
import {NotasMesa} from '../../../interfaces/reportes/notas-mesa';
import {PNA} from '../../../interfaces/reportes/pna';
import {PNC} from '../../../interfaces/reportes/pnc';
import {PolizasInicioVigencia} from '../../../interfaces/reportes/polizas-inicio-vigencia';
import {ProductividadEjecutivo} from "../../../interfaces/reportes/productividad-ejecutivo";


@Injectable({
  providedIn: 'root',
})
export class DescargasService {

  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }
  getCobranza(fechaInicio: number, fechaFin: number): Observable<CobranzaPendiente[]> {
    return this.http.get<CobranzaPendiente[]>(this.baseURL + 'v1/descargas/cobranza/' + fechaInicio + '/' + fechaFin);
  }
  getLeads(fechaInicio: number, fechaFin: number): Observable<Leads[]> {
    return this.http.get<Leads[]>(this.baseURL + 'v1/descargas/leads/' + fechaInicio + '/' + fechaFin);
  }
  getNotas(fechaInicio: number, fechaFin: number): Observable<NotasMesa[]> {
    return this.http.get<NotasMesa[]>(this.baseURL + 'v1/descargas/notas/' + fechaInicio + '/' + fechaFin);
  }
  getPna(fechaInicio: number, fechaFin: number): Observable<PNA[]> {
    return this.http.get<PNA[]>(this.baseURL + 'v1/descargas/pna/' + fechaInicio + '/' + fechaFin);
  }
  getPnc(fechaInicio: number, fechaFin: number): Observable<PNC[]> {
    return this.http.get<PNC[]>(this.baseURL + 'v1/descargas/pnc/' + fechaInicio + '/' + fechaFin);
  }
  getInicioVigencia(fechaInicio: number, fechaFin: number): Observable<PolizasInicioVigencia[]> {
    return this.http.get<PolizasInicioVigencia[]>(this.baseURL + 'v1/descargas/inicio/' + fechaInicio + '/' + fechaFin);
  }
  getProductividad(fechaInicio: number, fechaFin: number): Observable<ProductividadEjecutivo[]> {
    return this.http.get<ProductividadEjecutivo[]>(this.baseURL + 'v1/productividad/' +
      0 + '/' + 0 + '/' + fechaInicio + '/' + fechaFin);
  }
}
