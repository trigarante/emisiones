import { TestBed } from '@angular/core/testing';

import { ContactosCampanaService } from './contactos-campana.service';

describe('ContactosCampanaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContactosCampanaService = TestBed.get(ContactosCampanaService);
    expect(service).toBeTruthy();
  });
});
