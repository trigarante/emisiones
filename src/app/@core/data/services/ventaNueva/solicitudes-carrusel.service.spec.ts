import { TestBed } from '@angular/core/testing';

import { SolicitudesCarruselService } from './solicitudes-carrusel.service';

describe('SolicitudesCarruselService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SolicitudesCarruselService = TestBed.get(SolicitudesCarruselService);
    expect(service).toBeTruthy();
  });
});
