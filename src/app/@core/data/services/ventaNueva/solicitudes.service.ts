import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Solicitud, Solicitudes, SolicitudesVN, SolicitudesVNData} from '../../interfaces/ventaNueva/solicitudes';
import {BehaviorSubject, Observable} from 'rxjs';


@Injectable({
  providedIn: 'root',
})
export class SolicitudesvnService extends SolicitudesVNData {
  private baseURL;
  private nodeURL;
  private baseURLNode;
  public idCliente = new BehaviorSubject<number>(0);
  public idRegistroPoliza = new BehaviorSubject<number>(0);
  public  urlPostventa = new BehaviorSubject<number>(0);
  private socketURL;
  private baseURL2;


  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.baseURLNode = environment.SERVICIOS_NODE + '/venta-nueva/solicitudes';
    this.socketURL = environment.GLOBAL_SOCKET;
    this.nodeURL = environment.SERVICIOS_NODE;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;

  }

  get(): Observable<Solicitudes[]> {
    return this.http.get<Solicitudes[]>(this.baseURL2 + 'v1/solicitudes-vn/' + sessionStorage.getItem('Usuario'));
  }

  getTipo(tipo: string): Observable<Solicitudes[]> {
    return this.http.get<Solicitudes[]>(this.baseURL2 + 'v1/solicitudes-vn/tipo/' + tipo + '/' +
      sessionStorage.getItem('Usuario'));
  }

  getGM(tipo: string): Observable<Solicitudes[]> {
    return this.http.get<Solicitudes[]>(this.baseURL2 + 'v1/solicitudes-gm/tipo/' + tipo + '/' + sessionStorage.getItem('Usuario'));
  }

  getByIdCotizacionAli(idCotizacionAli: number): Observable<Solicitudes> {
    return this.http.get<Solicitudes>(this.baseURL2 + 'v1/solicitudes-vn/idCotizacionAli/' + idCotizacionAli);
  }

  getSubsecuentes(tipo: string): Observable<Solicitudes[]> {
    return this.http.get<Solicitudes[]>(this.baseURL2 + 'v1/solicitudes-vn-subsecuentes/tipo/' + tipo + '/' +
      sessionStorage.getItem('Usuario'));
  }

  getSubsecuentesPeru(tipo: string): Observable<Solicitudes[]> {
    return this.http.get<Solicitudes[]>(this.baseURL2 + 'v1/solicitudes-subsecuentes-peru/' + tipo + '/' +
      sessionStorage.getItem('Usuario'));
  }

  getCobranza(tipo: string): Observable<Solicitudes[]> {
    return this.http.get<Solicitudes[]>(this.baseURL2 + 'v1/solicitudes-vn-cobranza/tipo/' + tipo + '/' +
      sessionStorage.getItem('Usuario'));
  }

  getFechas(tipo: string, fechaInicial: number, fechaFinal: number): Observable<SolicitudesVN[]> {
    return this.http.get<SolicitudesVN[]>(this.baseURL2 + 'v1/solicitudes-vn/fecha/' + tipo + '/' +
      sessionStorage.getItem('Usuario') + '/' + fechaInicial + '/' + fechaFinal);
  }

  getSolicitudesPeru(tipo: string): Observable<Solicitudes[]> {
    return this.http.get<Solicitudes[]>(this.baseURL2 + 'v1/solicitudes-vn/get-peru/' + tipo + '/' +
      sessionStorage.getItem('Usuario'));
  }

  post(solicitud: Solicitudes): Observable<Solicitudes> {
    return this.http.post<Solicitudes>(this.baseURL + 'v1/solicitudes-vn' , solicitud);
  }
  postWs(solicitud: Solicitudes): Observable<Solicitudes> {
    return this.http.post<Solicitudes>(this.baseURL + 'v1/solicitudes-vn/solicitud-ws' , solicitud);
  }

  postCreateSolicitudWsSocket(solicitud): Observable<any> {
    return this.http.post<any>(this.socketURL + 'solicitudes-ws/postCreateSolicitudWs' , solicitud);
  }
  getSolicitudByIdNode(idSolicitud): Observable<any> {
    return this.http.get<any>(`${this.nodeURL}/solicitudesVn/solicitudes/${idSolicitud}`);
  }

  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'vnsolicitudes/saveVnSolicitudes', json);
  }

  getSolicitudesById(idSolicitud): Observable<Solicitudes> {
    return this.http.get<Solicitudes>(this.baseURL2 + 'v1/solicitudes-vn/get-by-id/' + idSolicitud);
  }

  getSolicitudById(idSolicitud): Observable<Solicitud> {
    return this.http.get<Solicitud>(this.baseURL2 + 'v1/solicitudes-vn/get-solicitud-by-id/' + idSolicitud);
  }

  getSolicitudByIdSinJson(idSolicitud): Observable<Solicitudes> {
    return this.http.get<Solicitudes>(this.baseURL2 + 'v1/solicitudes-vn/get-by-id-sin-json/' + idSolicitud);
  }

  put(idSolicitud, solicitud): Observable<Solicitudes> {
    return this.http.put<Solicitudes>(this.baseURL + 'v1/solicitudes-vn/' + idSolicitud + '/'
      + sessionStorage.getItem('Empleado') , solicitud);
  }

  putSolicitud (idSolicitud, solicitud): Observable<string> {
    return this.http.put<string>(`${this.nodeURL}/llamada-salidas/solicitud/${idSolicitud}`, solicitud);
  }

  updateEstadoSolicitud(idSolicitud: number, estadoSolicitud: number): Observable<any> {
    return this.http.put(this.baseURL + 'v1/solicitudes-vn/update-estado/' + idSolicitud + '/' + estadoSolicitud, null);
  }

  updateEmpleadoSolicitud(idSolicitud: number, idEmpleado: number): Observable<any> {
    return this.http.put(this.baseURL + 'v1/solicitudes-vn/update-empleado/' + idSolicitud + '/' + idEmpleado
      + '/' + sessionStorage.getItem('Empleado'), null);
  }

  putEstadoEtiquetaSubetiqueta(json: any): Observable<any> {
    return this.http.put(this.baseURLNode, json);
  }

  entryIdCliente(value) {
    this.idCliente.next(value);
  }

  returnIdCliente(): Observable<number> {
    return this.idCliente.asObservable();
  }

  entryIdRegistroPoliza(value) {
    this.idRegistroPoliza.next(value);
  }

  returnIdRegistroPoliza(): Observable<number> {
    return this.idRegistroPoliza.asObservable();
  }

  entryURLType(value) {
    this.urlPostventa.next(value);
  }

  returnentryURLType(): Observable<number> {
    return this.urlPostventa.asObservable();
  }

  entryIdSolicitud(value) {
    this.idCliente.next(value);
  }

  returnIdSolicitud(): Observable<number> {
    return this.idCliente.asObservable();
  }

  // sockets
  postSolicitudSocket(json): Observable<SolicitudesVN> {
    return this.http.post<SolicitudesVN>(this.socketURL + 'vnsolicitudes/saveVnSolicitudes', json);
  }

  putEstadoSolicitudSocket(json): Observable<SolicitudesVN> {
    return this.http.put<SolicitudesVN>(this.socketURL + 'vnsolicitudes/updateEstadoSolicitud', json);
  }

  reasignarEmpleadoSocket(json): Observable<SolicitudesVN> {
    return this.http.put<SolicitudesVN>(this.socketURL + 'vnsolicitudes/updateEmpleado', json);
  }

  removeSolicitudSockets(json): Observable<SolicitudesVN> {
    return this.http.put<SolicitudesVN>(this.socketURL + 'vnsolicitudes/removeSolicitud', json);
  }

  updateProspectoSockets(json): Observable<SolicitudesVN> {
    return this.http.put<SolicitudesVN>(this.socketURL + 'vnsolicitudes/updateProspecto', json);
  }
}
