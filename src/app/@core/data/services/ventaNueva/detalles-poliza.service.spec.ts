import { TestBed } from '@angular/core/testing';

import { DetallesPolizaService } from './detalles-poliza.service';

describe('DetallesPolizaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DetallesPolizaService = TestBed.get(DetallesPolizaService);
    expect(service).toBeTruthy();
  });
});
