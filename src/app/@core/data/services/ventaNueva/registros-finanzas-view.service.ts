import { Injectable } from '@angular/core';
import {RegistrosFinanzasView, RegistrosFinanzasViewData} from '../../interfaces/ventaNueva/registrosFinanzasView';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class RegistrosFinanzasViewService extends RegistrosFinanzasViewData {
  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<RegistrosFinanzasView[]> {
    return this.http.get<RegistrosFinanzasView[]>(this.baseURL + 'v1/correccion-polizas-vn');
  }

  getRegistrosFinanzasViewById(idRegistrosFinanzasView): Observable<RegistrosFinanzasView> {
    return this.http.get<RegistrosFinanzasView>(this.baseURL + 'v1/correccion-polizas-vn/' + idRegistrosFinanzasView);
  }
}
