import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../../../environments/environment";
import {Observable} from "rxjs";
import {
  RegistroCorreccion,
  RegistroCorreccionData, RegistroCorreccionSoloDatos,
} from "../../../interfaces/ventaNueva/correccionPolizas/registro-correccion";

@Injectable()
export class RegistroCorreccionService extends RegistroCorreccionData {

  private baseURLFN;
  private baseURL;
  private socketURL;
  private baseURL2;


  constructor(private http: HttpClient) {
    super();
    this.baseURLFN = environment.GLOBAL_SERVICIOS;
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.socketURL = environment.GLOBAL_SOCKET;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;
  }

  get(): Observable<RegistroCorreccion[]> {
    return this.http.get<RegistroCorreccion[]>(this.baseURL2 + 'v1/registro-correccion-vn');
  }

  updateRegistroPolizaSoloDatos(datos: RegistroCorreccionSoloDatos, id: number): Observable<Boolean> {
    return this.http.put<Boolean>(this.baseURL + 'v1/registro-correccion-vn/update-datos/' + id, datos);
  }

  getRegistroById(idRegistro): Observable<RegistroCorreccion> {
    return this.http.get<RegistroCorreccion>(this.baseURL2 + 'v1/registro-correccion-vn/' + idRegistro);
  }
  getRegistroByIdADM(idRegistro): Observable<RegistroCorreccion> {
    return this.http.get<RegistroCorreccion>(this.baseURLFN + 'v1/registro-correccion-vn/' + idRegistro);
  }
  getRegistroFinanzasById(idRegistro): Observable<RegistroCorreccion> {
    return this.http.get<RegistroCorreccion>(this.baseURLFN + 'v1/registro-finanzas/' + idRegistro);
  }

  getNoSerieExistente(numSerie: string): Observable<string> {
    return this.http.get<string>(this.baseURL2 + 'v1/registro-vn/buscar-no-serie/' + numSerie,
      {responseType: 'text' as 'json'});
  }

  post(registro: RegistroCorreccion): Observable<RegistroCorreccion> {
    return this.http.post<RegistroCorreccion>(this.baseURL + 'v1/registro-correccion-vn/', registro);
  }
  postRF(registro: RegistroCorreccion): Observable<RegistroCorreccion> {
    return this.http.post<RegistroCorreccion>(this.baseURLFN + 'v1/registro-finanzas/', registro);
  }
  postInOne(registro: any): Observable<RegistroCorreccion> {
    return this.http.post<any>(this.baseURL + 'v1/registro-correccion-vn/all', registro);
  }

  put(idRegistro, registro: RegistroCorreccion): Observable<RegistroCorreccion> {
    return this.http.put<RegistroCorreccion>(this.baseURL + 'v1/registro-correccion-vn/' + idRegistro + '/'
      + sessionStorage.getItem('Empleado'), registro);
  }
  putRF(idRegistro, registro: RegistroCorreccion): Observable<RegistroCorreccion> {
    return this.http.put<RegistroCorreccion>(this.baseURLFN + 'v1/registro-finanzas/' + idRegistro + '/'
      + sessionStorage.getItem('Empleado'), registro);
  }

  updateArchivo(idRegistro: number, idCarpeta: string): Observable<any> {
    return this.http.put(this.baseURL + 'v1/registro-correccion-vn/carpeta-drive/' + idRegistro + '/' + idCarpeta, null);
  }

  updateEstadoPoliza(idRegistro: number, estado: number): Observable<any> {
    return this.http.put(this.baseURL + 'v1/registro-correccion-vn/update-estado/' + idRegistro + '/' + estado, null);
  }

  updateFlujoPoliza(idRegistro: number, idFlujoPoliza: number): Observable<any> {
    return this.http.put(this.baseURL + 'v1/registro-correccion-vn/update-flujo/' + idRegistro + '/' + idFlujoPoliza, null);
  }

  updateFlujoAndEstadoPoliza(idRegistro: number, idFlujoPoliza: number, idEstadoPoliza): Observable<any> {
    return this.http.put(this.baseURL + 'v1/registro-correccion-vn/update-flujo-estado/' + idRegistro + '/' + idFlujoPoliza
      + '/' + idEstadoPoliza, null);
  }

  modificarPolizaSocket(json): Observable<any> {
    return this.http.put<any>(this.socketURL + 'porAplicar/putPorAplicar', json);
  }
}
