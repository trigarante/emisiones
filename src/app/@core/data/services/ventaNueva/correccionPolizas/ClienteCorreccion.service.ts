import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../../../environments/environment";
import {Observable} from "rxjs";
import {ClientePeru} from "../../../interfaces/ventaNueva/cliente-peru";
import {
  ClienteCorreccion,
  ClienteCorreccionData,
} from "../../../interfaces/ventaNueva/correccionPolizas/ClienteCorreccion";

@Injectable()
export class ClienteCorreccionService extends ClienteCorreccionData {

  private baseURL;
  private baseURLOp;
  private  socketURL;
  private baseURL2;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.baseURLOp = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.socketURL = environment.GLOBAL_SOCKET;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;

  }

  get(): Observable<ClienteCorreccion[]> {
    return this.http.get<ClienteCorreccion[]>(this.baseURL2 + 'v1/cliente-correccion');
  }

  getFinanzas(): Observable<ClienteCorreccion[]> {
    return this.http.get<ClienteCorreccion[]>(this.baseURL + 'v1/cliente-correccion/finanzas');
  }
  getClienteFinanzas(): Observable<ClienteCorreccion[]> {
    return this.http.get<ClienteCorreccion[]>(this.baseURL + 'v1/cliente-finanzas/finanzas');
  }

  getClienteFinanzasById(idClienteFinanzas): Observable<ClienteCorreccion> {
    return this.http.get<ClienteCorreccion>(this.baseURL + 'v1/cliente-correccion/finanzas/' + idClienteFinanzas);
  }

  post(cliente: ClienteCorreccion): Observable<ClienteCorreccion> {
    return this.http.post<ClienteCorreccion>(this.baseURLOp + 'v1/cliente-correccion', cliente);
  }
  postClienteFinanzas(cliente: ClienteCorreccion): Observable<ClienteCorreccion> {
    return this.http.post<ClienteCorreccion>(this.baseURL + 'v1/cliente-finanzas', cliente);
  }
  postSocket(json: JSON): Observable<JSON> {
    return  this.http.post<JSON>(this.socketURL + 'vnClientes/saveVnClientes', json);
  }

  getClienteById(idCliente): Observable<ClienteCorreccion> {
    return this.http.get<ClienteCorreccion>(this.baseURL2 + 'v1/cliente-correccion/' + idCliente);
  }
  getClienteByIdADM(idCliente): Observable<ClienteCorreccion> {
    return this.http.get<ClienteCorreccion>(this.baseURL + 'v1/cliente-correccion/' + idCliente);
  }
  getFinanzasClienteById(idCliente): Observable<ClienteCorreccion> {
    return this.http.get<ClienteCorreccion>(this.baseURL + 'v1/cliente-finanzas/' + idCliente);
  }

  getClientePeruById(idCliente: number): Observable<ClientePeru> {
    return this.http.get<ClientePeru>(this.baseURL2 + 'v1/cliente-correccion/peru/' + idCliente);
  }
  getClienteFinPeruById(idCliente: number): Observable<ClientePeru> {
    return this.http.get<ClientePeru>(this.baseURL + 'v1/cliente-finanzas/peru/' + idCliente);
  }

  curpExist(curp) {
    return this.http.get(this.baseURL + 'v1/cliente-correccion/curp/' + curp);
  }

  rfcExist(rfc) {
    return this.http.get(this.baseURL + 'v1/cliente-correccion/rfc/' + rfc);
  }

  dniExist(dni: string): Observable<ClientePeru> {
    return this.http.get<ClientePeru>(this.baseURL + 'v1/cliente-correccion/dni/' + dni);
  }

  put(idCliente, cliente: ClienteCorreccion): Observable<ClienteCorreccion> {
    return this.http.put<ClienteCorreccion>(this.baseURL + 'v1/cliente-correccion/' + idCliente + '/' +
      + sessionStorage.getItem('Empleado') , cliente);
  }

  putCliente(idCliente, cliente: ClienteCorreccion): Observable<ClienteCorreccion> {
    return this.http.put<ClienteCorreccion>(this.baseURL + 'v1/cliente-correccion/cliente/' + idCliente + '/' +
      + sessionStorage.getItem('Empleado') , cliente);
  }

  putArchivo(idCliente: number, idCarpetaDrive: string): Observable<any> {
    return this.http.put(this.baseURL + 'v1/cliente-correccion/update-archivo/' + idCliente + '/' + idCarpetaDrive, null);
  }
  putArchivoSubido(idCliente: number, idArchivoSubido: number): Observable<any> {
    return this.http.put(this.baseURL + 'v1/cliente-correccion/update-archivo-subido/' + idCliente + '/' + idArchivoSubido, null);
  }

}
