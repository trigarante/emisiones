import { TestBed } from '@angular/core/testing';

import { RegistrosFinanzasViewService } from './registros-finanzas-view.service';

describe('RegistrosFinanzasViewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RegistrosFinanzasViewService = TestBed.get(RegistrosFinanzasViewService);
    expect(service).toBeTruthy();
  });
});
