import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {DetallesPoliza, DetallesPolizaData} from '../../interfaces/ventaNueva/detalles-poliza';

@Injectable({
  providedIn: 'root',
})
export class DetallesPolizaService extends DetallesPolizaData {

  private baseURL;
  private baseURL2;


  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;

  }
  get(): Observable<DetallesPoliza> {
    return this.http.get<DetallesPoliza>(this.baseURL2 + 'v1/aplicaciones-view');
  }
  getById(id): Observable<DetallesPoliza> {
    return this.http.get<DetallesPoliza>(this.baseURL2 + 'v1/aplicaciones-view/' + id);
  }
}
