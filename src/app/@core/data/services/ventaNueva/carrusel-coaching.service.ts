import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {CarruselCoaching} from '../../interfaces/ventaNueva/carrusel-coaching';

@Injectable({
  providedIn: 'root',
})
export class CarruselCoachingService {

  private baseURL;
  private baseURL2;


  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;

  }

  get(): Observable<CarruselCoaching[]> {
    return this.http.get<CarruselCoaching[]>(this.baseURL2 + 'v1/carrusel-coaching');
  }

  getRegistroByIdEmpleado(idEmpleado): Observable<CarruselCoaching> {
    return this.http.get<CarruselCoaching>(this.baseURL2 + 'v1/carrusel-coaching/empleado/' + idEmpleado);
  }

  getRegistroById(idCarrusel): Observable<CarruselCoaching> {
    return this.http.get<CarruselCoaching>(this.baseURL2 + 'v1/carrusel-coaching/' + idCarrusel);
  }
  post(Carrusel: CarruselCoaching): Observable<CarruselCoaching> {
    return this.http.post<CarruselCoaching>(this.baseURL + 'v1/carrusel-coaching/', Carrusel);
  }

  put(idCarrusel, Carrusel: CarruselCoaching): Observable<CarruselCoaching> {
    return this.http.put<CarruselCoaching>(this.baseURL + 'v1/carrusel-coaching/' + idCarrusel, Carrusel);
  }
}
