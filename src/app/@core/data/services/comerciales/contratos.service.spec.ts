import { TestBed, getTestBed } from '@angular/core/testing';
import { ContratosService } from './contratos.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../matchers';

fdescribe('ContratosService', () => {
  let injector: TestBed;
  let service: ContratosService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 1,
    idSocio: 7,
    fechaInicio: Date.now(),
    fechaFin: Date.now(),
    referencia: 'CONTRATO UNOS',
    activo: 0,
    idEstado: 1,
    idEstadoSocio: 1,
    diasRestantes: 2,
  },
    {
      id: 'contratossocio',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [ContratosService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(ContratosService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/contratos-socio');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/contratos-socio', '/1');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/contratos-socio/1568991961088/1568991961088');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/contratos-socio', '/1/74');
      });
    });
  }


});
