import {Injectable} from '@angular/core';
import { ProductoSocios} from '../../interfaces/comerciales/producto-socios';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ProductoSociosService  {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<ProductoSocios[]> {
    return this.http.get<ProductoSocios[]>(this.baseURL + 'v1/productos-socio');
  }

  getProductoSocioByIdSubRamo(idSubramo: number): Observable<ProductoSocios[]> {
    return this.http.get<ProductoSocios[]>(this.baseURL + 'v1/productos-socio/id-subramo/' + idSubramo);
  }

  getProductoSocioByIdSubRamoByNombre(idSubramo: number, nombre: string): Observable<ProductoSocios[]> {
    return this.http.get<ProductoSocios[]>(this.baseURL + 'v1/productos-socio/id-subramoNombre/' + idSubramo + '/'
      + nombre);
  }

  getProductoSociosById(idProductoSocios): Observable<ProductoSocios> {
    return this.http.get<ProductoSocios>(this.baseURL + 'v1/productos-socio/' + idProductoSocios);
  }

  post(productoSocios: ProductoSocios): Observable<ProductoSocios> {
    return this.http.post<ProductoSocios>(this.baseURL + 'v1/productos-socio', productoSocios);
  }

  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'producto/saveProducto', json);
  }

  put(idProductoSocios, productoSocios: ProductoSocios): Observable<ProductoSocios> {
    return this.http.put<ProductoSocios>(this.baseURL + 'v1/productos-socio/' + idProductoSocios + '/'
      + sessionStorage.getItem('Empleado') , productoSocios);
  }
}
