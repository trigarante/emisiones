import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {SociosComercial} from '../../interfaces/comerciales/socios-comercial';

@Injectable({
  providedIn: 'root',
})
export class SociosService  {
  private baseURL;
  private nodeURL;
  private baseURLNode;
  private socketURL;
  private baseURL2;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.baseURLNode = environment.SERVICIOS_NODE + '/comerciales/socios';
    this.socketURL = environment.GLOBAL_SOCKET;
    this.nodeURL = environment.SERVICIOS_NODE;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;
  }

  get(): Observable<SociosComercial[]> {
    return this.http.get<SociosComercial[]>(this.baseURL2 + 'v1/socios');
  }

  getGM(): Observable<SociosComercial[]> {
    return this.http.get<SociosComercial[]>(this.baseURL + 'v1/socios/gm');
  }

  getSociosByIdPais(idPais: number): Observable<SociosComercial[]> {
    return this.http.get<SociosComercial[]>(this.baseURL + 'v1/socios/get-pais/' + idPais);
  }

  post(socios: SociosComercial): Observable<SociosComercial> {
    return this.http.post<SociosComercial>(this.baseURL + 'v1/socios', socios);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'socios/saveSocios', json);
  }



  getIdByAlias(alias): Observable<SociosComercial> {
    return this.http.get<SociosComercial>(this.baseURL + 'v1/socios/alias/' + alias);
  }

  put(idSocios, socios: SociosComercial): Observable<SociosComercial> {
    return this.http.put<SociosComercial>(this.baseURL + 'v1/socios/' + idSocios + '/'
      + sessionStorage.getItem('Empleado'), socios);
  }

  getActivos(activos: number): Observable<SociosComercial[]> {
    return this.http.get<SociosComercial[]>(this.baseURL + 'v1/socios/activos/' + activos);
  }
}
