import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../../../environments/environment";

@Injectable({
  providedIn: 'root',
})
export class AseguradoraMesService {
  private url = environment.CORE_EXTERNO + '/v1/aseguradora_recomendada';


  constructor(private http: HttpClient) {
  }

  authenticate(): any {
    return this.http.post(`${environment.CORE_EXTERNO}/v1/authenticate`, {
      tokenData: "mHf/0x8xqWmYlrjaRWECOzmkksnuNDZv1fBvMLjpI2g="});
  }

  obtenerAseguradora() {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${sessionStorage.getItem('core-externo')}`,
    });
    return this.http.get(`${this.url}/obtenerAseguradora`, {headers});
  }

  desactivar() {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${sessionStorage.getItem('core-externo')}`,
    });
    return this.http.put(`${this.url}/desactivar`, {}, {headers});
  }

  activar() {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${sessionStorage.getItem('core-externo')}`,
    });
    return this.http.put(`${this.url}/activar`, {}, {headers});
  }

  getAseguradoras() {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${sessionStorage.getItem('core-externo')}`,
    });
    return this.http.get(`${this.url}/getNames`, {headers});
  }

  setAseguradoraMes(aseguradora: string, fecha: string) {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${sessionStorage.getItem('core-externo')}`,
    });
    return this.http.get(`${this.url}/setAseguradora?aseguradora=${aseguradora}&fechaExpiracion=${fecha}`, {headers});
  }

}
