import {getTestBed, TestBed} from '@angular/core/testing';
import { ConveniosService } from './convenios.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../matchers';

fdescribe('ConveniosService', () => {
  let injector: TestBed;
  let service: ConveniosService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 1,
    idSubCategoria: 1,
    idSubRamo: 1,
    cantidad: 1.0,
    activo: 0,
    idEstado: 2,
    idEstadoSocio: 2,
    estado: 'INACTIVO',
    idRamo: 30,
    idTipoSubRamo: 1,
    tipoSubRamo: 'AUTO',
    tipoRamo: 'SEGUROS',
    nombreComercial: 'KK',
    alias: 'KK',
    idTipoDivisa: 1,
    detalle: 'TEST',
    idCategoria: 1,
  },
    {
      id: 'convenios',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [ConveniosService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(ConveniosService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/convenios');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/convenios', '/1');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/convenios');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/convenios', '/1/74');
      });
    });
  }
});
