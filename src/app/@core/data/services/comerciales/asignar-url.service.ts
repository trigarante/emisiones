import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {AsignarUrl} from '../../interfaces/comerciales/asignar-url';

@Injectable({
  providedIn: 'root',}
)

export class AsignarUrlService extends AsignarUrl {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<AsignarUrl[]> {
    return this.http.get<AsignarUrl[]>(this.baseURL + 'v1/url-socios');
  }

  post(asignarUrl: AsignarUrl): Observable<AsignarUrl> {
    return this.http.post<AsignarUrl>(this.baseURL + 'v1/url-socios', asignarUrl);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'url/saveUrl/1', json);
  }

  getAsignarUrlById(idAsignarUrl): Observable<AsignarUrl> {
    return this.http.get<AsignarUrl>(this.baseURL + 'v1/url-socios/' + idAsignarUrl);
  }

  put(idAsignarUrl, asignarUrl): Observable<AsignarUrl> {
    return this.http.put<AsignarUrl>(this.baseURL + 'v1/url-socios/' + idAsignarUrl + '/'
      + sessionStorage.getItem('Empleado') , asignarUrl);
  }
}
