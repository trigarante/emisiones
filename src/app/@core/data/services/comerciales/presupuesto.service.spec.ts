import { getTestBed, TestBed } from '@angular/core/testing';
import { PresupuestoService } from './presupuesto.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../matchers';
fdescribe('Presupuesto Service', () => {
  let injector: TestBed;
  let service: PresupuestoService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const url = environment.GLOBAL_SERVICIOS;

  const body = [{
    id: 1,
    idSocio: 1,
    presupuesto: '1000001',
    idEstadoSocio: 1,
    alias: 'MORAL',
    nombreComercial: 'testing',
    estado: 'ACTIVO',
    idEstado: 3,
    anio: 'testing',
    activo: 0,
  },
    {
      id: 'presupuestosocio',
    }];
    Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [PresupuestoService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(PresupuestoService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });

  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/presupuesto-socio');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/presupuesto-socio', '/1');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/presupuesto-socio');
      }); /*
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/presupuesto-socio', '/1/74');
      });*/
    });
  }
});
