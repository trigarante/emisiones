import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {EjecutivoCuenta} from '../../interfaces/comerciales/ejecutivo-cuenta';
import {DatosEjecutivoCuentaComponent} from "../../../../modulos/comercial/modals/datos-ejecutivo-cuenta/datos-ejecutivo-cuenta.component";



@Injectable({
  providedIn: 'root',}
)

export class EjecutivoCuentaService  {

  private baseURL;
  private nodeURL;
  private baseURLNode;
  private socketURL;
  private baseURL2;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.baseURLNode = environment.SERVICIOS_NODE + '/comerciales/ejecutivo-cuenta';
    this.socketURL = environment.GLOBAL_SOCKET;
    this.nodeURL = environment.SERVICIOS_NODE;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;
  }

  get(): Observable<EjecutivoCuenta[]> {
    return this.http.get<EjecutivoCuenta[]>(this.baseURL + 'v1/ejecutivos-cuentas');
  }

  post(ejecutivoCuenta: EjecutivoCuenta): Observable<EjecutivoCuenta> {
    return this.http.post<EjecutivoCuenta>(this.baseURL + 'v1/ejecutivos-cuentas', ejecutivoCuenta);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'contactos/saveContacto/1', json);
  }

   getEjectutivoCuentaById(idEjecutivoCuenta): Observable<DatosEjecutivoCuentaComponent> {
    return this.http.get<DatosEjecutivoCuentaComponent>(this.baseURL + 'v1/ejecutivos-cuentas/' + idEjecutivoCuenta);
   }

  put(idEjecutivoCuenta, ejecutivoCuenta: EjecutivoCuenta): Observable<EjecutivoCuenta> {
    return this.http.put<EjecutivoCuenta>(this.baseURL + 'v1/ejecutivos-cuentas/' + idEjecutivoCuenta + '/'
      + sessionStorage.getItem('Empleado'), ejecutivoCuenta);
  }
}
