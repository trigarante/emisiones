import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {SubRamo} from '../../interfaces/comerciales/sub-ramo';

@Injectable({
  providedIn: 'root',
})
export class SubRamoService  {

  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<SubRamo[]> {
    return this.http.get<SubRamo[]>(this.baseURL + 'v1/subramo');
  }

  getSubramoById(idSubRamo): Observable<SubRamo> {
    return this.http.get<SubRamo>(this.baseURL + 'v1/subramo/' + idSubRamo);
  }

  getSubramosByIdRamo(idRamo: number): Observable<SubRamo[]> {
    return this.http.get<SubRamo[]>(this.baseURL + 'v1/subramo/id-ramo/' + idRamo);
  }

  getSubramosByIdRamoAndIdTipoSubramo(idRamo: number, idTipoSubramo: number): Observable<SubRamo[]> {
    return this.http.get<SubRamo[]>(this.baseURL + 'v1/subramo/id-ramo-gm/' + idRamo + '/' + idTipoSubramo);
  }

  post(subramo: SubRamo): Observable<SubRamo> {
    return this.http.post<SubRamo>(this.baseURL + 'v1/subramo', subramo);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'subramo/saveSubRamo/1', json);
  }

  getTipoAndEstado(idTipoSubramo, idEstadoSocio): Observable<SubRamo[]> {
    return this.http.get<SubRamo[]>(this.baseURL + 'v1/subramo/tipo/' + idTipoSubramo + '/' + idEstadoSocio);
  }

  getTipoAndAlias(idTipoSubramo, alias): Observable<SubRamo> {
    return this.http.get<SubRamo>(this.baseURL + 'v1/subramo/alias/' + idTipoSubramo + '/' + alias);
  }

  put(idSubRamo, subramo: SubRamo): Observable<SubRamo> {
    return this.http.put<SubRamo>(this.baseURL + 'v1/subramo/' + idSubRamo + '/'
      + sessionStorage.getItem('Empleado'), subramo);
  }
}
