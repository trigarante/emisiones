import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {TipoSubramo} from '../../../interfaces/comerciales/catalogo/tipo-subramo';

@Injectable({
  providedIn: 'root',
})
export class TipoSubramoService{
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS + 'v1/tipo-subramo';
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<TipoSubramo[]> {
    return this.http.get<TipoSubramo[]>(this.baseURL);
  }

  getByActivo(activo: number): Observable<TipoSubramo[]> {
    return this.http.get<TipoSubramo[]>(this.baseURL + '/activos/' + activo);
  }

  post(tipoSubramo: TipoSubramo): Observable<TipoSubramo> {
    return this.http.post<TipoSubramo>(this.baseURL , tipoSubramo);
  }

  getTipoSubramoById(idTipoSubramo): Observable<TipoSubramo> {
    return this.http.get<TipoSubramo>(this.baseURL + '/' + idTipoSubramo);
  }

  put(idTipoSubramo, tipoSubramo: TipoSubramo): Observable<TipoSubramo> {
    return this.http.put<TipoSubramo>(this.baseURL + '/' + idTipoSubramo + '/'
      + sessionStorage.getItem('Empleado'), tipoSubramo);
  }

  getVN(): Observable<TipoSubramo[]> {
    return this.http.get<TipoSubramo[]>(this.baseURL + '/getVN/');
  }
}
