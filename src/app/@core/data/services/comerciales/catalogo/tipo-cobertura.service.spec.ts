import { TestBed } from '@angular/core/testing';

import { TipoCoberturaService } from './tipo-cobertura.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../../matchers';

fdescribe('TipoCoberturaService', () => {
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 1,
    cobertura: 'TOTAL01--',
    activo: 0,
  },
    {
      id: 'tipocobertura',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [TipoCoberturaService, HttpClient],
  }));
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/tipo-cobertura');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/tipo-cobertura', '/1');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/tipo-cobertura');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/tipo-cobertura', '/1/74');
      });
    });
  }
});
