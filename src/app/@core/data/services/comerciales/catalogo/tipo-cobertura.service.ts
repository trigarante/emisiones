import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {TipoCobertura} from '../../../interfaces/comerciales/catalogo/tipo-cobertura';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TipoCoberturaService  {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<TipoCobertura[]> {
    return this.http.get<TipoCobertura[]>(this.baseURL + 'v1/tipo-cobertura');
  }

  getByActivo(activo: number): Observable<TipoCobertura[]> {
    return this.http.get<TipoCobertura[]>(this.baseURL + 'v1/tipo-cobertura/activos/' + activo);
  }

  post(tipocobertura: TipoCobertura): Observable<TipoCobertura> {
    return this.http.post<TipoCobertura>(this.baseURL + 'v1/tipo-cobertura', tipocobertura);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'tipocobertura/saveTipoCobertura', json);
  }

  getTipoCoberturaById(idTipoCobertura): Observable<TipoCobertura> {
    return this.http.get<TipoCobertura>(this.baseURL + 'v1/tipo-cobertura/' + idTipoCobertura);
  }

  put(idTipoCobertura, tipocobertura: TipoCobertura): Observable<TipoCobertura> {
    return this.http.put<TipoCobertura>(this.baseURL + 'v1/tipo-cobertura/' + idTipoCobertura + '/'
      + sessionStorage.getItem('Empleado') , tipocobertura);
  }
}
