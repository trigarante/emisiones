import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {SociosBaja} from '../../../interfaces/comerciales/catalogo/socios-baja';

@Injectable({
  providedIn: 'root',
})
export class SociosBajaService  {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<SociosBaja[]> {
    return this.http.get<SociosBaja[]>(this.baseURL + 'v1/estado-socio');
  }

  getByActivo(activo: number): Observable<SociosBaja[]> {
    return this.http.get<SociosBaja[]>(this.baseURL + 'v1/estado-socio/activos/' + activo);
  }

  post(baja: SociosBaja): Observable<SociosBaja> {
    return this.http.post<SociosBaja>(this.baseURL + 'v1/estado-socio', baja);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON> (this.socketURL + 'bajasocios/saveBajaSocios', json);
  }

  getSocioBajaById(idSocio): Observable<SociosBaja> {
    return this.http.get<SociosBaja>(this.baseURL + 'v1/estado-socio/' + idSocio);
  }

  put(idSocio, sociobaja: SociosBaja): Observable<SociosBaja> {
    return this.http.put<SociosBaja>(this.baseURL + 'v1/estado-socio/' + idSocio + '/'
      + sessionStorage.getItem('Empleado'), sociobaja);
  }
}
