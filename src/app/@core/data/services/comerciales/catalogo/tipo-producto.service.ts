import {Injectable} from '@angular/core';
import {TipoProducto} from '../../../interfaces/comerciales/catalogo/tipo-producto';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root',
})
export class TipoProductoService extends TipoProducto {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<TipoProducto[]> {
    return this.http.get<TipoProducto[]>(this.baseURL + 'v1/tipo-producto');
  }

  getByActivo(activo: number): Observable<TipoProducto[]> {
    return this.http.get<TipoProducto[]>(this.baseURL + 'v1/tipo-producto/activos/' + activo);
  }

  post(tipoProducto: TipoProducto): Observable<TipoProducto> {
    return this.http.post<TipoProducto>(this.baseURL + 'v1/tipo-producto', tipoProducto);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'tipoproducto/saveTipoProducto', json);
  }

  getTipoProducto(idTipoProducto): Observable<TipoProducto> {
    return this.http.get<TipoProducto>(this.baseURL + 'v1/tipo-producto/' + idTipoProducto);
  }

  put(idTipoProducto, tipoProducto: TipoProducto): Observable<TipoProducto> {
    return this.http.put<TipoProducto>(this.baseURL + 'v1/tipo-producto/' + idTipoProducto + '/'
      + sessionStorage.getItem('Empleado'), tipoProducto);
  }
}
