import { TestBed } from '@angular/core/testing';

import { AseguradoraMesService } from './aseguradora-mes.service';

describe('AseguradoraMesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AseguradoraMesService = TestBed.get(AseguradoraMesService);
    expect(service).toBeTruthy();
  });
});
