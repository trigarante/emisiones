import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class CandidatoService {
  private baseURL = environment.apiUrl + 'atraccion-talento/candidato';
  private simuladoresURL = environment.simuladoresUrl + 'atraccion-talento/candidatos';

  constructor(
    private http: HttpClient
  ) {
  }

  crearCandidato(dataCandidato): Observable<any> {
    return this.http.post<any>(this.baseURL, dataCandidato);
  }

  darSeguimiento(dataCandidato): Observable<any> {
    return this.http.post<any>(this.baseURL + '/seguimiento', dataCandidato);
  }

  getCandidatosData(): Observable<any[]> {
    return this.http.get<any[]>(this.baseURL + '/getAll');
  }

  getCandidatosByEstado(estado: number): Observable<any[]> {
    const headers = new HttpHeaders().set('estado', estado.toString());
    return this.http.get<any[]>(this.baseURL + '/getByEstado', { headers });
  }

  getCandidatoById(idCandidato): Observable<any> {
    return this.http.get<any>(this.baseURL + `/get/${idCandidato}`);
  }

  uploadDocumentsCandidato(files: FileList, idcandidato, nombreArchivo, tipoArchivo, autorizacionDirecta?) {
    const formData = new FormData();
    // @ts-ignore
    formData.append('file', files.item(0));
    formData.append('nombreArchivo', nombreArchivo );
    formData.append('idCandidato', idcandidato.toString() );
    formData.append('tipoArchivo', tipoArchivo.toString() );
    if (autorizacionDirecta) {
      formData.append('autorizacionDirecta', '1');
    }

    return this.http.put(this.baseURL + `/subir-archivo/`, formData,
      {responseType: 'text' as 'json'});
  }

  uploadDocumentsEmpleado(files: FileList, idEmpleado, nombreArchivo, tipoDocumento) {
    const formData = new FormData();
    // @ts-ignore
    formData.append('file', files.item(0));
    formData.append('nombreArchivo', nombreArchivo );

    return this.http.post(this.baseURL + `/subir-archivo-empleado/${idEmpleado}/${tipoDocumento}`, formData,
      {responseType: 'text' as 'json'});
  }

  uploadExpedienteEmpleado(files: FileList, idEmpleado, nombreArchivo, tipoDocumento) {
    const formData = new FormData();
    // @ts-ignore
    formData.append('file', files.item(0));
    formData.append('nombreArchivo', nombreArchivo );
    formData.append('idEmpleado', idEmpleado );
    formData.append('tipoDocumento', tipoDocumento );

    return this.http.post(this.baseURL + `/expediente-empleado`, formData,
      {responseType: 'text' as 'json'});
  }

  updateDocumentsCandidato(files: FileList, idcandidato, nombreArchivo, tipoArchivo, autorizacionDirecta?) {
    const formData = new FormData();
    // @ts-ignore
    formData.append('file', files.item(0));
    formData.append('nombreArchivo', nombreArchivo );
    formData.append('idCandidato', idcandidato.toString() );
    formData.append('tipoArchivo', tipoArchivo.toString() );
    if (autorizacionDirecta) {
      formData.append('autorizacionDirecta', '1');
    }

    return this.http.put(this.baseURL + `/actualizar-archivo/`, formData,
      {responseType: 'text' as 'json'});
  }

  getDocumentos(archivoId) {
    const headers = new HttpHeaders().append('archivoId', archivoId);
    return this.http.get<any>(this.baseURL + `/obtener-archivo`, {headers});
    }

  getCoaching(): Observable<any> {
    return this.http.get<any>(this.baseURL + '/getAproboSimuladores');
  }

  getCandidatosCoaching(): Observable<any> {
    return this.http.get<any>(this.simuladoresURL + '/getCandidatosLibres');
  }

  updateEmail(id: number, email: string): Observable<any> {
    const headers = new HttpHeaders().append('id', id.toString());
    return this.http.post<any>(this.simuladoresURL, {email}, { headers });
  }

  getCoachingAnEstado(estado: number): Observable<any> {
    return this.http.get<any>(this.baseURL + '/getAproboSimuladoresAndEstado/' + estado);
  }

  getByIdEtapa(idEtapa: number): Observable<any> {
    return this.http.get<any>(this.baseURL + '/getByIdEtapa/' + idEtapa);
  }

  getByIdEtapaAndEstado(idEtapa: number, estado: number): Observable<any> {
    return this.http.get<any>(this.baseURL + '/getByIdEtapaAndEstado/' + idEtapa + '/' + estado);
  }

  getByIdEtapaAndEstadoAndIdCandidato(idEtapa: number, id: number): Observable<any> {
    return this.http.get<any>(this.baseURL + '/getByIdEtapaAndEstadoAndEmpleado/' + idEtapa + '/' + id);
  }

  put(data): Observable<any> {
    return this.http.put(this.baseURL, data);
  }
  putPreempleados(data): Observable<any> {
    return this.http.put(this.baseURL + '/preempleados', data);
  }

  getDetalles(id: number): Observable<any> {
    const headers = new HttpHeaders().append('id', id.toString());
    return this.http.get<any>(this.baseURL + '/detalles', {headers});
  }
  getVerificarDocumentos(): Observable<any> {
    return this.http.get<any>(this.baseURL + '/verificar-documento');
  }
}

