import { Injectable } from '@angular/core';
import {environment} from '../../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EstadoAsistenciaService {

  private url = environment.CORE_VN2 + 'administracion-personal/estado-asistencia';

  constructor(private http: HttpClient) { }

  getActivos(): Observable<any[]> {
    return this.http.get<any[]>(this.url);
  }
}
