import { TestBed } from '@angular/core/testing';

import { TipoJustificacionService } from './tipo-justificacion.service';

describe('TipoJustificacionService', () => {
  let service: TipoJustificacionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipoJustificacionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
