import { Injectable } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Departamento} from '../../interfaces/catalogos/departamento';

@Injectable({
  providedIn: 'root'
})
export class DepartamentoService {

  private url = environment.CORE_VN2 + 'capital-humano/departamento';

  constructor(
    private http: HttpClient
  ) {
  }
  getDepartamentoByIdArea(idArea: number): Observable<Departamento[]> {
    return this.http.get<Departamento[]>(this.url + 'getByIdArea/' + idArea);
  }
  getDepartamentos(): Observable<any[]> {
    return this.http.get<any[]>(this.url);
  }

  getAreas(): Observable<unknown[]> {
    return this.http.get<unknown[]>(`${this.url}/areas`);
  }

  getByIdPadre(idPadre: number): Observable<unknown[]> {
    const headers = new HttpHeaders().set('id', idPadre.toString());
    return this.http.get<unknown[]>(`${this.url}/getByIdPadre`, {headers});
  }
}
