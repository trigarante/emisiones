import { Injectable } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JustificacionesService {
  private baseURL = environment.CORE_VN2 + 'administracion-personal/justificaciones';
  private doc = environment.CORE_DOCUMENTOS + '/administracion-personal/justificaciones';
  constructor(
    private http: HttpClient
  ) { }

  getByArchivo(idArchivo: string): Observable<any> {
    const headers = new HttpHeaders().set('id', idArchivo);
    return this.http.get<any>(`${this.doc}/obtenerArchivo`, { headers });
  }

  crearSolicitudIncapacidad(files: FileList, data, nombreArchivo): Observable<any> {
    const formData = new FormData();
    // @ts-ignore
    formData.append('file', files.item(0));
    formData.append('nombreArchivo', nombreArchivo );
    formData.append('datos', JSON.stringify(data) );
    return this.http.post<any>(this.baseURL, formData);
  }

  getByEmpleadoAndEstado(idEstado: number, permiso): Observable<any> {
    const headers = new HttpHeaders().set('idempleado', sessionStorage.getItem('Empleado')).set('idestado', idEstado.toString())
      .set('permiso', permiso.toString());
    return this.http.get<any>(`${this.baseURL}/byEmpleadoAndEstado`, { headers });
  }

  getByEstado(idEstado: number): Observable<any> {
    const headers = new HttpHeaders().set('idestado', idEstado.toString());
    return this.http.get<any>(`${this.baseURL}/byEstado`, { headers });
  }

  updateEstatus(data): Observable<any> {
    return this.http.put<any>(`${this.baseURL}/actualizarEstatus`, data);
  }

  getByMismoFolio(id, folio) {
    const headers = new HttpHeaders().set('id', id.toString()).set('folio', folio.toString());
    return this.http.get<any>(`${this.baseURL}/byMismoFolio`, { headers });
  }
}
