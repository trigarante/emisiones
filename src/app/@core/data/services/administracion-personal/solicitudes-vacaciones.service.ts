import { Injectable } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SolicitudesVacacionesService {
  private baseURL = environment.CORE_VN2 + 'administracion-personal/solicitud-vacaciones';
  constructor(
    private http: HttpClient
  ) { }

  // NEW
  getCantidadVacaciones(idEmpleado): Observable<any[]> {
    return this.http.get<any[]>(this.baseURL + `/cantidad-vacaciones/${idEmpleado}`);
  }

  post(data) {
    return this.http.post(this.baseURL, data);
  }

  getByEmpleado(): Observable<any> {
    const headers = new HttpHeaders().append('id', sessionStorage.getItem('Empleado'));
    return this.http.get(this.baseURL + '/byEmpleado', {headers});
  }

  getByDepartamento(idEstado: number): Observable<any> {
    const headers = new HttpHeaders().set('idempleado', sessionStorage.getItem('Empleado')).set('idestado', idEstado.toString());
    return this.http.get(this.baseURL + '/byDepartamento', {headers});
  }
  getByDepartamento2(idEstado: number): Observable<any> {
    const headers = new HttpHeaders().set('idempleado', sessionStorage.getItem('Empleado')).set('idestado', idEstado.toString());
    return this.http.get(this.baseURL + '/byDepartamento2', {headers});
  }

  update(idSolicitudVacaciones, data): Observable<any> {
    return this.http.put<any>(this.baseURL + `/${idSolicitudVacaciones}`, data);
  }
  // FIN

  getSolicitudesVacaciones(): Observable<any[]> {
    return this.http.get<any[]>(this.baseURL + `/solicitudes-completas`);
  }

  getAutorizarVacaciones(): Observable<any[]> {
    return this.http.get<any[]>(this.baseURL + `/autorizar-completas`);
  }

  getSolicitudVacacionesById(id): Observable<any> {
    return this.http.get<any>(this.baseURL + `/autorizar-completas/${id}`);
  }

  updateSolicitudVacaciones(idSolicitudVacaciones, data): Observable<any> {
    return this.http.put<any>(this.baseURL + `/${idSolicitudVacaciones}`, data);
  }

  createSolicitudVacaciones(data): Observable<any> {
    return this.http.post<any>(this.baseURL, data);
  }
}
