import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TipoJustificacionService {

  private baseURL = environment.CORE_VN2 + 'administracion-personal/tipo-justificacion';
  constructor(
    private http: HttpClient
  ) { }

  getActivos() {
    return this.http.get(this.baseURL);
  }
}
