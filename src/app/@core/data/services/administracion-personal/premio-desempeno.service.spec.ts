import { TestBed } from '@angular/core/testing';

import { PremioDesempenoService } from './premio-desempeno.service';

describe('PremioDesempenoService', () => {
  let service: PremioDesempenoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PremioDesempenoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
