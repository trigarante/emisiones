import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import {Observable} from 'rxjs';
import {Empresa} from '../../../interfaces/catalogos/empresa';

@Injectable({
  providedIn: 'root'
})
export class EmpresaServiceService {

  private baseURL = environment.CORE_VN2 + 'administracion-personal/empresa/';
  constructor(
    private http: HttpClient
  ) { }

  getEmpresa(): Observable<Empresa[]> {
    return this.http.get<Empresa[]>(this.baseURL + 'getAll');
  }

  getActivos(): Observable<[Empresa]> {
    return this.http.get<[Empresa]>(this.baseURL + 'getAll');
  }
}
