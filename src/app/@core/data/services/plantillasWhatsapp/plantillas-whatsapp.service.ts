import { Injectable } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PlantillasWhatsappService {
  private baseURL;
  constructor(private http: HttpClient) {
     this.baseURL = environment.SERVICIOS_NODE;
    this.baseURL = environment.SERVICIOS_NODE_WHATSAPP;
  }

  getPlantillaWhatsappById(idPlantilla): Observable<any> {
    return this.http.get<any>(this.baseURL + `plantillas-comercial-whatsapp/obtener-plantilla/${idPlantilla}`);
  }

  getPlantillasWhatsapp(): Observable<any[]> {
    return this.http.get<any[]>(this.baseURL + `plantillas-comercial-whatsapp/obtener-plantillas`);
  }

  getTipoSubarea(): Observable<any[]> {
    return this.http.get<any[]>(this.baseURL + `plantillas-comercial-whatsapp/tipo-subareas`);
  }

  postPlantillasWhatsapp(plantilla): Observable<any> {
    return this.http.post<any>(this.baseURL + 'plantillas-comercial-whatsapp/agregar-plantilla', plantilla);
  }

  putPlantillaWhatsapp(idPlantilla, plantilla): Observable<any> {
    return this.http.put<any>(this.baseURL + `plantillas-comercial-whatsapp/actualizar-plantilla/${idPlantilla}`, plantilla);
  }

  actualizarEstado(idPlantilla, plantilla): Observable<any> {
    return this.http.put<any>(this.baseURL + `plantillas-comercial-whatsapp/actualizar-estado/${idPlantilla}`, plantilla);
  }
}
