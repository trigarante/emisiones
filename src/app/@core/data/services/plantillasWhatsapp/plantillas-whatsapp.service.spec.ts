import { TestBed } from '@angular/core/testing';

import { PlantillasWhatsappService } from './plantillas-whatsapp.service';

describe('PlantillasWhatsappService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlantillasWhatsappService = TestBed.get(PlantillasWhatsappService);
    expect(service).toBeTruthy();
  });
});
