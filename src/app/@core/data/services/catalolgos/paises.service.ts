import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
// import {Paises, PaisesData} from '../../interfaces/catalogos/paises';
// import {Area} from '../../interfaces/catalogos/area';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';


interface Paises {
  activo: number;
}

interface Area {
}

@Injectable()
export class PaisesService {
  private baseURL;
  private socketURL;
  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Paises[]> {
    return this.http.get<Paises[]>(this.baseURL + 'v1/paises');
  }

  post(paises: Paises): Observable<Paises> {
    return this.http.post<Paises>(this.baseURL + 'v1/paises', paises);
  }
  getByActivo(activo: number): Observable<Paises[]> {
    return this.http.get<Paises[]>(this.baseURL + 'v1/paises/activos/' + activo);
  }

  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(environment.GLOBAL_SOCKET + 'paises/savePaises', json);
  }

  getPaisesById(idPaises): Observable<Area> {
    return this.http.get<Area>(this.baseURL + 'v1/paises/' + idPaises);
  }

  put(idPaises, paises: Paises): Observable<Paises> {
    return this.http.put<Paises>(this.baseURL + 'v1/paises/' + idPaises + '/'
      + sessionStorage.getItem('Empleado'), paises);
  }

}
