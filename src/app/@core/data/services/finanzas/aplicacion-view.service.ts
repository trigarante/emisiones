import { Injectable } from '@angular/core';
import {AplicacionView, AplicacionViewData, ModificarDatosPoliza} from '../../interfaces/finanzas/aplicacionView';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {Registro} from '../../interfaces/ventaNueva/registro';


@Injectable({
  providedIn: 'root',
})
export class AplicacionViewService {
  private baseURL;
  private baseURLFN;
  private socketURL;
  private baseURLNode;

  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.baseURLFN = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
    this.baseURLNode = environment.SERVICIOS_DATOS;
  }

  get(): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURL + 'v1/aplicaciones-view');
  }

  getCanceladas(): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURL + 'v1/aplicaciones-view/canceladas');
  }

  getAplicacionViewById(idAplicacionView): Observable<AplicacionView> {
    return this.http.get<AplicacionView>(this.baseURLNode + '/finanzas/aplicaciones/' + idAplicacionView);
  }

  getAllRegistroByIdUsuario(): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURL +
      'v1/aplicaciones-view/getRegistroByIdUsuario/' + sessionStorage.getItem('Usuario'));
  }

  getAllRegistroByIdUsuarioReno(): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURL +
      'v1/aplicaciones-view/adminPolizasReno/' + sessionStorage.getItem('Usuario'));
  }

  getAllRegistroByIdUsuarioAnIddFlujoPoliza(idFlujoPoliza: number): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURL +
      'v1/aplicaciones-view/getRegistroByIdUsuario/' + sessionStorage.getItem('Usuario') + `/${idFlujoPoliza}`);
  }

  getAllRegistroGM(idFlujoPoliza: number): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURL +
      'v1/aplicaciones-view/getRegistroGMByIdUsuario/' + sessionStorage.getItem('Usuario') + `/${idFlujoPoliza}`);
  }

  getRegistroConValidacion(idFlujoPoliza: number): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURL + 'v1/aplicaciones-view/registro-validacion/' +
      idFlujoPoliza + '/' + sessionStorage.getItem('Empleado'));
  }

  getRegistroConValidacionPeru(): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURL + 'v1/admin-view-peru/registro-validacion/' +
      + sessionStorage.getItem('Empleado'));
  }

  getRegistroConValidacionCobranza(): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURL + 'v1/admin-cobranza/registro-validacion/' +
      sessionStorage.getItem('Empleado'));
  }

  getRegistroConValidacionGastosMedicos(): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURL + 'v1/aplicaciones-view/registro-validacion-gastos-medicos' + '/' +
      sessionStorage.getItem('Empleado'));
  }

  crearAutorizacionnGastosMedicos(): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURL + 'v1/aplicaciones-view/crear-autorizacion-gastos-medicos' + '/' +
      Number(sessionStorage.getItem('Empleado')));
  }

  getRegistroConValidacionHogar(): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURL + 'v1/aplicaciones-view/registro-validacion-hogar/74');
  }

  getRegistroConValidacionPyme(): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURL + 'v1/aplicaciones-view/registro-validacion-pyme/74');
  }

  getRegistroConValidacionVida(): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURL + 'v1/aplicaciones-view/registro-validacion-vida/74');
  }

  getAplicacionFechaInicio(): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURLFN + 'v1/aplicaciones-view/aplicacion');
  }

  getRegistroConValidacionSubsecuentes(): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURL + 'v1/aplicaciones-view/registro-validacion/7/' +
      sessionStorage.getItem('Empleado'));
  }

  getReciboAPagar(idRegistro): Observable<number> {
    return this.http.get<number>(this.baseURL + 'v1/aplicaciones-view/pagarSiguienteRecibo/' + idRegistro);
  }

  getFinanzas (): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURLFN + 'v1/aplicaciones-view/finanzas');
  }

  getAllTabla(tabla: string): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURL +
      'v1/aplicaciones-view/' + tabla);
  }

  getAllCobranza(): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURL +
      'v1/aplicacionesview-cobranza/cobranza');
  }

  getTablaByIdUsuario(tabla: string): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURL +
      'v1/aplicaciones-view/' + tabla + '/' + sessionStorage.getItem('Usuario'));
  }

  // getEmisiones(): Observable<AplicacionView[]> {
  //   return this.http.get<AplicacionView[]>(this.baseURL + 'v1/aplicaciones-view/emisiones');
  // }

  getEmisiones(): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURL + 'v1/aplicaciones-view/emisiones-renovar');
  }

  getCobranzaByIdUsuario(): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURL +
      'v1/aplicacionesview-cobranza/cobranza/' + sessionStorage.getItem('Usuario'));
  }

  getAllTablaSinAplicar(tabla: string): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURLFN +
      'v1/aplicaciones-view/' + tabla + '/sin-aplicar');
  }

  getAllCobranzaSinAplicar(): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURL +
      'v1/aplicacionesview-cobranza/cobranza/sin-aplicar');
  }

  getEndosos(): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURLFN + 'v1/aplicaciones-view-endoso');
  }

  getCobranza(): Observable<AplicacionView[]> {
      return this.http.get<AplicacionView[]>(this.baseURL +
        'v1/aplicacionesview-cobranza/cobranza/' + sessionStorage.getItem('Usuario'));
  }

  putSocketPorAplicar(idRegistro, registro: Registro): Observable<Registro> {
    return this.http.put<Registro>(this.socketURL + 'porAplicar/putPorAplicar', idRegistro + registro);
  }

  putModificarPoliza(idRegistro, registro: ModificarDatosPoliza): Observable<ModificarDatosPoliza> {
    return this.http.put<ModificarDatosPoliza>( this.baseURL + 'v1/datos-poliza-por-aplicar/actualizar-datos-poliza/' +
    idRegistro, registro);
  }

  getPeru(): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURL + 'v1/admin-vn');
  }

  getRegistroConValidacionVn(): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURL + 'v1/admin-vn/registro-validacion/' + sessionStorage.getItem('Empleado'));
  }

  getRegistroConValidacionEcommerce(): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURL + 'v1/admin-ecommerce/registro-validacion/' + sessionStorage.getItem('Empleado'));
  }

  getAllRegistroByIdUsuarioPeru(): Observable<AplicacionView[]> {
    return this.http.get<AplicacionView[]>(this.baseURL +
      'v1/admin-view-peru/getRegistroByIdUsuario/' + sessionStorage.getItem('Usuario'));
  }

  cierreFinanzas(fechaCierre: number): Observable<any> {
    return  this.http.get<any>(this.baseURLFN +
      'v1/aplicaciones-view/cierre/' + fechaCierre);
  }
}
