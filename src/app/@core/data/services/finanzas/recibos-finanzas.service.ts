import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {RecibosFinanzas} from '../../interfaces/finanzas/RecibosFinanzas';

@Injectable({
  providedIn: 'root',
})
export class RecibosFinanzasService {
  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  getRecibosFinanzasByIdRegistro (idRegistro: number): Observable<RecibosFinanzas[]> {
    return this.http.get<RecibosFinanzas[]>(this.baseURL + 'v1/recibo-finanzas/registro/' + idRegistro);
  }

  getReciboFinanzasById(idRecibo): Observable<RecibosFinanzas> {
    return this.http.get<RecibosFinanzas>(this.baseURL + 'v1/recibo-finanzas/' + idRecibo);
  }

  putReciboFinanzas(recibo: RecibosFinanzas): Observable<RecibosFinanzas> {
    return this.http.put<RecibosFinanzas>(this.baseURL + 'v1/recibo-finanzas/' + sessionStorage.getItem('Empleado'), recibo);
  }

}
