import { TestBed } from '@angular/core/testing';

import { GestionIngresosService } from './gestion-ingresos.service';

describe('GestionIngresosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GestionIngresosService = TestBed.get(GestionIngresosService);
    expect(service).toBeTruthy();
  });
});
