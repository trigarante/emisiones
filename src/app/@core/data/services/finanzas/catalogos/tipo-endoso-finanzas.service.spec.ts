import { TestBed } from '@angular/core/testing';

import { TipoEndosoFinanzasService } from './tipo-endoso-finanzas.service';

describe('TipoEndosoFinanzasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TipoEndosoFinanzasService = TestBed.get(TipoEndosoFinanzasService);
    expect(service).toBeTruthy();
  });
});
