import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {TipoEndosoFinanzasData, TipoEndosoFinanzas} from '../../../interfaces/finanzas/catalogos/tipo-endoso-finanzas';

@Injectable()
export class TipoEndosoFinanzasService extends TipoEndosoFinanzasData {

  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<TipoEndosoFinanzas[]> {
    return this.http.get<TipoEndosoFinanzas[]>(this.baseURL + 'v1/tipo-endoso-finanzas');
  }

  post(tipoEndososFinanzas: TipoEndosoFinanzas): Observable<TipoEndosoFinanzas> {
    return this.http.post<TipoEndosoFinanzas>(this.baseURL + 'v1/tipo-endoso-finanzas', tipoEndososFinanzas);
  }

  getTipoEndosoFinanzasById(idTipoEndosoFinanzas): Observable<TipoEndosoFinanzas> {
    return this.http.get<TipoEndosoFinanzas>(this.baseURL + 'v1/tipo-endoso-finanzas/' + idTipoEndosoFinanzas);
  }

  put(idTipoEndosoFinanzas, tipoEndososFinanzas: TipoEndosoFinanzas): Observable<TipoEndosoFinanzas> {
    return this.http.put<TipoEndosoFinanzas>(this.baseURL + 'v1/tipo-endoso-finanzas/' + idTipoEndosoFinanzas, tipoEndososFinanzas);
  }
}
