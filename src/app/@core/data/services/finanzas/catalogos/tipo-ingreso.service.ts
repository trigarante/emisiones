import { Injectable } from '@angular/core';
import {TipoIngreso, TipoIngresosData} from '../../../interfaces/finanzas/catalogos/tipoIngreso';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class TipoIngresoService extends TipoIngresosData {
  private baseURL;
  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }
  get(): Observable<TipoIngreso[]> {
    return this.http.get<TipoIngreso[]>(this.baseURL + 'v1/tipo-ingreso');
  }

  post(tipoIngreso: TipoIngreso ): Observable<TipoIngreso> {
    return this.http.post<TipoIngreso>(this.baseURL + 'v1/tipo-ingreso', tipoIngreso);
  }

  getTipoIngresoById(idTipoIngresos): Observable<TipoIngreso> {
    return this.http.get<TipoIngreso>(this.baseURL + 'v1/tipo-ingreso/' + idTipoIngresos);
  }

  put(idTipoIngresos, tipoIngreso: TipoIngreso): Observable<TipoIngreso> {
    return this.http.put<TipoIngreso>(this.baseURL + 'v1/tipo-ingreso/' + idTipoIngresos , tipoIngreso);
  }
}
