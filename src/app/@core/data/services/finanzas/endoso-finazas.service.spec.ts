import { TestBed } from '@angular/core/testing';

import { EndosoFinazasService } from './endoso-finazas.service';

describe('EndosoFinazasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EndosoFinazasService = TestBed.get(EndosoFinazasService);
    expect(service).toBeTruthy();
  });
});
