import { TestBed } from '@angular/core/testing';

import { AplicacionViewService } from './aplicacion-view.service';

describe('AplicacionViewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AplicacionViewService = TestBed.get(AplicacionViewService);
    expect(service).toBeTruthy();
  });
});
