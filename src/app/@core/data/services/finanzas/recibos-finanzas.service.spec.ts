import { TestBed } from '@angular/core/testing';

import { RecibosFinanzasService } from './recibos-finanzas.service';

describe('RecibosFinanzasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RecibosFinanzasService = TestBed.get(RecibosFinanzasService);
    expect(service).toBeTruthy();
  });
});
