import { Injectable } from '@angular/core';
import {GestionIngresos, GestionIngresosData} from '../../interfaces/finanzas/gestionIngresos';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';


@Injectable()
export class GestionIngresosService extends GestionIngresosData {
  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;

  }
  get(): Observable<GestionIngresos[]> {
    return this.http.get<GestionIngresos[]>(this.baseURL + 'v1/gestion-ingreso');
  }

  post(gestionIngresos: GestionIngresos ): Observable<GestionIngresos> {
    return this.http.post<GestionIngresos>(this.baseURL + 'v1/gestion-ingreso', gestionIngresos);
  }

  getGestionIngresosById(idGestionIngresos): Observable<GestionIngresos> {
    return this.http.get<GestionIngresos>(this.baseURL + 'v1/gestion-ingreso/' + idGestionIngresos);
  }

  put(idGestionIngresos, gestionIngresos: GestionIngresos): Observable<GestionIngresos> {
    return this.http.put<GestionIngresos>(this.baseURL + 'v1/gestion-ingreso/' + idGestionIngresos + '/'
      + sessionStorage.getItem('Empleado') , gestionIngresos);
  }
}
