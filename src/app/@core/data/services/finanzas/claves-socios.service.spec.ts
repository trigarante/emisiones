import { TestBed } from '@angular/core/testing';

import { ClavesSociosService } from './claves-socios.service';

describe('ClavesSociosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClavesSociosService = TestBed.get(ClavesSociosService);
    expect(service).toBeTruthy();
  });
});
