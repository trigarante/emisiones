import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {ClavesSocios} from '../../interfaces/finanzas/claves-socios';

@Injectable({
  providedIn: 'root',
})
export class ClavesSociosService {
  baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = environment.GLOBAL_SERVICIOS + 'v1/claves-socios';
  }

  getByIdSocio (idSocio: number): Observable<ClavesSocios[]> {
    return this.http.get<ClavesSocios[]>(this.baseUrl + '/id-socio/' + idSocio);
  }
}
