import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {EndosoFinanzaData, EndosoFinanzas} from '../../interfaces/finanzas/endoso-finanzas';

@Injectable()
export class EndosoFinazasService extends EndosoFinanzaData {

  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }
  get(): Observable<EndosoFinanzas[]> {
    return this.http.get<EndosoFinanzas[]>(this.baseURL + 'v1/endosos-finanzas');
  }
  post(endososFinanzas: EndosoFinanzas): Observable<EndosoFinanzas> {
    return this.http.post<EndosoFinanzas>(this.baseURL + 'v1/endosos-finanzas', endososFinanzas);
  }

  getEndosoFinanzasById(idEndosoFinanzas): Observable<EndosoFinanzas> {
    return this.http.get<EndosoFinanzas>(this.baseURL + 'v1/endosos-finanzas/' + idEndosoFinanzas);
  }

  put(idEndosoFinanzas, endososFinanzas: EndosoFinanzas): Observable<EndosoFinanzas> {
    return this.http.put<EndosoFinanzas>(this.baseURL + 'v1/endosos-finanzas/' + idEndosoFinanzas ,  endososFinanzas);
  }
}
