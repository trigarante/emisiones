import { Injectable } from '@angular/core';
import Menu from '../interfaces/menu/menu';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  permisos = JSON.parse(window.localStorage.getItem('User'));

  menu: Menu[] = [
    {
      // nuevosPermisos
      icon: 'arrow_right',
      name: 'Emisiones',
      alias: 'emisiones',
      permiso: true,
      children: [
        {rutas: 'emisiones/administracion-polizas', icon: '', name: 'Pólizas por Emitir', permiso: this.permisos.EMI?.polizasEmitir},
        {rutas: 'emisiones/pendientes-renovar', icon: '', name: 'Pólizas Emitidas',permiso: this.permisos.EMI?.polizasEmitidas},

      ]
    }
  ];

  getMenu(): Menu[]{
    return this.menu;
  }

}
