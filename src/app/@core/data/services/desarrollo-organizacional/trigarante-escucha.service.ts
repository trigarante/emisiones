import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class TrigaranteEscuchaService {
  url = environment.CORE_ESCUCHA + '/desarrollo-organizacional/';

  constructor(
    private http: HttpClient
  ) { }

  getActivos(activo: number): Observable<unknown[]> {
    const headers = new HttpHeaders().append('activo', activo.toString());
    return this.http.get<unknown[]>(`${this.url}tema-escucha/getByActivos`, {headers});
  }
  getByIdTema(idtema: number): Observable<unknown[]> {
    const headers = new HttpHeaders().append('idtema', idtema.toString());
    return this.http.get<unknown[]>(`${this.url}motivo-escucha/getByTema`, {headers});
  }

  post(data) {
    return this.http.post<any>(this.url + 'trigarante-escucha', data);
  }
  put(id, data) {
    return this.http.put<any>(this.url, {id, ...data});
  }
}
