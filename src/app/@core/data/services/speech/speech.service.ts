import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {SpeechInterface, SpeechInterfaceView} from '../../interfaces/speech/speech.interface';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SpeechService {
  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.SERVICIOS_NODE;
  }

  createSpeech(speech: SpeechInterface): Observable<SpeechInterface> {
    return this.http.post<SpeechInterface>(`${this.baseURL}/auditoria/speech/crearSpeech`, speech);
  }

  getAllSpeeches(): Observable<SpeechInterfaceView[]> {
    return this.http.get<SpeechInterfaceView[]>(`${this.baseURL}/auditoria/speech`);
  }

  getSpeechPorAutorizar(): Observable<SpeechInterfaceView[]> {
    return this.http.get<SpeechInterfaceView[]>(`${this.baseURL}/auditoria/speech/porAutorizar`);
  }

  getSpeechAutorizados(): Observable<SpeechInterfaceView[]> {
    return this.http.get<SpeechInterfaceView[]>(`${this.baseURL}/auditoria/speech/autorizados`);
  }

  getSpeechInhabilitados(): Observable<SpeechInterfaceView[]> {
    return this.http.get<SpeechInterfaceView[]>(`${this.baseURL}/auditoria/speech/inhabilitados`);
  }

  putSpeech(json): Observable<any> {
    return this.http.put<any[]>(`${this.baseURL}/auditoria/speech/actualizarSpeech`, json);
  }

  getByIdSpeeh(id): Observable<any> {
    return this.http.get<any>(`${this.baseURL}/auditoria/speech/byIdSpeech/${id}`);
  }

  bajaSpeech(idSpeech): Observable<string> {
    return this.http.put<string>(`${this.baseURL}/auditoria/speech/bajaSpeech/${idSpeech}`, null);
  }

  getSpeechBySubArea(idSubarea: string): Observable<SpeechInterface[]> {
    return this.http.get<SpeechInterface[]>(`${this.baseURL}/speech/bySubarea/${idSubarea}`);
  }
  putSpeechAutorizacion(json): Observable<any> {
    return this.http.put<any[]>(`${this.baseURL}/auditoria/speech/autorizar`, json);
  }
  putSpeechDesautorizacion(json): Observable<any> {
    return this.http.put<any[]>(`${this.baseURL}/auditoria/speech/desautorizar`, json);
  }
  putSpeechInhabilitado(json): Observable<any> {
    return this.http.put<any[]>(`${this.baseURL}/auditoria/speech/inhabilitar`, json);
  }
}
