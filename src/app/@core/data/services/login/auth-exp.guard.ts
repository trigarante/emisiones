// import { Injectable } from '@angular/core';
// import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
// import { Observable } from 'rxjs';
//
// @Injectable({
//   providedIn: 'root'
// })
// export class AuthExp implements CanActivate, CanLoad {
//
//   constructor(
//     private router: Router
//   ) { }
//
//   private validarFecha = (): boolean => {
//     const helper = new JwtHelperService();
//
//     const decodedToken = helper.decodeToken(window.localStorage.getItem('token'));
//     const fechaToken = moment(decodedToken.exp * 1000).toDate();
//     if( fechaToken.getDate() > new Date().getDate() ){
//       this.router.navigateByUrl('/login');
//       return false;
//     }
//
//     return true;
//   }
//
//   canLoad(route: Route, segments: UrlSegment[]): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
//     return this.validarFecha();
//   }
//
//   canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
//     return this.validarFecha();
//   }
// }
