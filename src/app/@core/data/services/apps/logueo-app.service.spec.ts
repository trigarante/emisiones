import { TestBed } from '@angular/core/testing';

import { LogueoAppService } from './logueo-app.service';

describe('LogueoAppService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LogueoAppService = TestBed.get(LogueoAppService);
    expect(service).toBeTruthy();
  });
});
