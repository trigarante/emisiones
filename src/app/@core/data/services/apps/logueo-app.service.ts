import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../../environments/environment";
import {Observable} from "rxjs";
// import {PlantillasWhatsApp} from "../../interfaces/whatsapp/plantillasTipoSubarea";

@Injectable({
    providedIn: 'root',
})
export class LogueoAppService {
    private urlApps;

    constructor(
        private http: HttpClient,
    ) {
        this.urlApps = environment.CORE_APP_V1;
    }

    getLogueosApps(): Observable<any[]> {
        return this.http.get<any[]>(this.urlApps + '/logueos-apps');
    }

    getDescargasPoliza(): Observable<any[]> {
        return this.http.get<any[]>(this.urlApps + '/descargas-polizas');
    }

    getSeguimientoMail(): Observable<any[]> {
        return this.http.get<any[]>(this.urlApps + '/seguimiento-mail');
    }

    getFullReporte(): Observable<any[]> {
        return this.http.get<any[]>(this.urlApps + '/reporte');
    }

    getReporteById(idEmpresa): Observable<any[]> {
        return this.http.get<any[]>(this.urlApps + '/reporte/' + idEmpresa);
    }

    getEmpresas(): Observable<any[]> {
        return this.http.get<any[]>(this.urlApps + '/empresas')
    }
}
