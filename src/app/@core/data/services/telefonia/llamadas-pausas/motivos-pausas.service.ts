import { Injectable } from '@angular/core';
import {HttpBackend, HttpClient} from '@angular/common/http';
import {environment} from "../../../../../../environments/environment";
import {MotivosPausa} from "../../../interfaces/telefonia/llamadas-pausas/motivos-pausas";
const base_url = environment.SERVICIOS_NUEVA_LOGICA;

@Injectable({
  providedIn: 'root'
})
export class MotivosPausasService {

  private http: HttpClient;
  constructor(
    // private http: HttpClient
    private handler: HttpBackend
  ) {
    this.http = new HttpClient(handler);
  }

  getMotivosPausas() {
    return this.http.get<MotivosPausa[]>(`${base_url}/llamadas-pausas/motivos-pausa`);
  }
}
