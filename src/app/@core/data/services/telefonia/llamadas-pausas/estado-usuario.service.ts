import { Injectable } from '@angular/core';
import {HttpBackend, HttpClient} from '@angular/common/http';
import {environment} from "../../../../../../environments/environment";
import {EstadoUsuario} from "../../../interfaces/telefonia/llamadas-pausas/estadoUsuario";
const base_url = environment.SERVICIOS_NUEVA_LOGICA;

@Injectable({
  providedIn: 'root'
})
export class EstadoUsuarioService {

  private http: HttpClient;
  constructor(
    // private http: HttpClient
    private handler: HttpBackend
  ) {
    this.http = new HttpClient(handler);
  }

  postInicioPausa(json) {
    return this.http.post<EstadoUsuario[]>(`${base_url}/llamadas-pausas/estado-usuario-inicio`, json);
  }

  putFinPausa(idEstadoUsuario) {
    return this.http.put<EstadoUsuario[]>(`${base_url}/llamadas-pausas/estado-usuario-fin/${idEstadoUsuario}`, null);
  }

}
