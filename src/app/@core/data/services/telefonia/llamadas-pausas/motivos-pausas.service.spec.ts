import { TestBed } from '@angular/core/testing';

import { MotivosPausasService } from './motivos-pausas.service';

describe('MotivosPausasService', () => {
  let service: MotivosPausasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MotivosPausasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
