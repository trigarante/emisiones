import { TestBed } from '@angular/core/testing';

import { ContactoTransferenciaService } from './contacto-transferencia.service';

describe('ContactoTransferenciaService', () => {
  let service: ContactoTransferenciaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ContactoTransferenciaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
