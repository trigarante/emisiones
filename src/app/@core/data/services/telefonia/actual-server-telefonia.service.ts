import { Injectable } from '@angular/core';
import {environment} from "../../../../../environments/environment";
import {HttpBackend, HttpClient} from '@angular/common/http';
const base_url = environment.SERVICIOS_PJSIP_VN;
const hostName: string = window.location.hostname;

@Injectable({
  providedIn: 'root'
})
export class ActualServerTelefoniaService {

  private http: HttpClient;
  constructor(
    // private http: HttpClient
    private handler: HttpBackend
  ) {
    this.http = new HttpClient(handler);
  }

  getCurrentServer() {
    return this.http.get<string>(`${base_url}/currentServer`, {
      headers: {
        'plataforma': hostName,
      },
    });
  }
}
