import { Injectable } from '@angular/core';
import {environment} from "../../../../../environments/environment";
import {HttpBackend, HttpClient} from '@angular/common/http';
const base_url = environment.SERVICIOS_NUEVA_LOGICA;
const extension = localStorage.getItem('extension');
const idEmpleado = sessionStorage.getItem('Empleado');

@Injectable({
  providedIn: 'root'
})
export class LlamadasOutService {
  private http: HttpClient;
  constructor(
    // private http: HttpClient
    private handler: HttpBackend
  ) {
    this.http = new HttpClient(handler);
  }

  iniciarLlamada(numero: string, idTipoLlamada: number, idSolicitud?: number, idRegistro?: number) {
    return this.http.post(`${base_url}/llamada-salidas/inicio-llamada/${extension}`, {idTipoLlamada, numero, idSolicitud, idRegistro, idEmpleado});
  }

  finLlamada(idLlamada) {
    return this.http.put(`${base_url}/llamada-salidas/fin-llamada/${idLlamada}`, null);
  }

  tipificarLlamada(idLlamada, idSubEtiqueta) {
    return this.http.put(`${base_url}/llamada-salidas/tipificar-llamada/${idLlamada}`, {idSubEtiqueta});
  }
}
