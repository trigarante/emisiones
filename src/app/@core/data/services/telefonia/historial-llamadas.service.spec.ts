import { TestBed } from '@angular/core/testing';

import { HistorialLlamadasService } from './historial-llamadas.service';

describe('HistorialLlamadasService', () => {
  let service: HistorialLlamadasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HistorialLlamadasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
