import { Injectable } from '@angular/core';
import {HttpBackend, HttpClient} from '@angular/common/http';
import {environment} from "../../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class HistorialLlamadasService {
  private baseURL;
  private http: HttpClient;
  constructor(
    // private http: HttpClient
    private handler: HttpBackend
  ) {
    this.baseURL = environment.SERVICIOS_NUEVA_LOGICA;
    this.http = new HttpClient(handler);
  }

  getHistorialLlamadas(idSolicitud: string) {
    return this.http.get<any[]>(`${this.baseURL}/llamada-salidas/historial-llamada/${idSolicitud}`);
  }
}
