import { Injectable } from '@angular/core';
import {environment} from "../../../../../environments/environment";
import {HttpBackend, HttpClient} from '@angular/common/http';
const base_url = environment.SERVICIOS_NUEVA_LOGICA;
const idEmpleado = sessionStorage.getItem('Empleado');

@Injectable({
  providedIn: 'root'
})
export class SolicitudesvnTelefoniaService {

  private http: HttpClient;
  constructor(
    // private http: HttpClient
    private handler: HttpBackend
  ) {
    this.http = new HttpClient(handler);
  }

  asignarEmpleadoSolicitud(idSolicitud) {
    return this.http.put(`${base_url}/solicitudes/asignar-empleado/${idSolicitud}/${idEmpleado}`, null);
  }

  getSolicitudes(fechaInicio, fechaFin) {
    return this.http.get(`${base_url}/solicitudes/empleado/${idEmpleado}/${fechaInicio}/${fechaFin}`);
  }

  getSolicitudById(idSolicitud) {
    return this.http.get(`${base_url}/solicitudes/solicitud-id/${idSolicitud}`);
  }
}
