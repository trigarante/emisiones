import { Injectable } from '@angular/core';
import {environment} from "../../../../../environments/environment";
import {HttpBackend, HttpClient} from '@angular/common/http';
const base_url = environment.SERVICIOS_NUEVA_LOGICA;
@Injectable({
  providedIn: 'root'
})
export class NuevoNumeroService {

  private http: HttpClient;
  constructor(
    // private http: HttpClient
    private handler: HttpBackend
  ) {
    this.http = new HttpClient(handler);
  }

  numeroSolicitud(idSolicitud, numero) {
    return this.http.post(`${base_url}/agregar-numero/solicitud/${idSolicitud}`, {
      numero
    });
  }

  getNumerosSolicitud(idSolicitud) {
    return this.http.get(`${base_url}/agregar-numero/numeros-solicitud/${idSolicitud}`);
  }
}
