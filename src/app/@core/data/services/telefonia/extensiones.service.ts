import { Injectable } from '@angular/core';
import {environment} from "../../../../../environments/environment";
import {HttpBackend, HttpClient} from '@angular/common/http';
import {Extensiones} from "../../interfaces/telefonia/extensiones";
import {Observable} from "rxjs";
const base_url = environment.SERVICIOS_TELEFONIA;

@Injectable({
  providedIn: 'root'
})
export class ExtensionesService {
  private http: HttpClient;
  constructor(
    // private http: HttpClient
    private handler: HttpBackend
  ) {
    this.http = new HttpClient(handler);
  }

  getContactos(): Observable<Extensiones[]> {
    return this.http.get<Extensiones[]>(`${base_url}/transferencias`);
  }
}
