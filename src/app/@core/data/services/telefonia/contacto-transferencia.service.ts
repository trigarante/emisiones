import { Injectable } from '@angular/core';
import {HttpBackend, HttpClient} from '@angular/common/http';
import {environment} from "../../../../../environments/environment";
import {Observable} from "rxjs";
import {TransferenciaTelefonia} from "../../interfaces/telefonia/transferenciaTelefonia/transferenciaTelefonia";

@Injectable({
  providedIn: 'root',
})
export class ContactoTransferenciaService {
  private baseURL;
  private http: HttpClient;
  constructor(
    // private http: HttpClient
    private handler: HttpBackend
  ) {
    this.baseURL = environment.SERVICIOS_NODE;
    this.http = new HttpClient(handler);
  }

  getContactos(): Observable<TransferenciaTelefonia[]> {
    return this.http.get<TransferenciaTelefonia[]>(`${this.baseURL}/transferencias`);
  }
}
