import { Injectable } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpBackend, HttpClient} from '@angular/common/http';
const baseUrl = environment.SERVICIOS_NUEVA_LOGICA;

@Injectable({
  providedIn: 'root'
})
export class EstadoUsuarioTelefoniaService {

  private http: HttpClient;
  constructor(
    // private http: HttpClient
    private handler: HttpBackend
  ) {
    this.http = new HttpClient(handler);
  }
  activarUsuario(idUser) {
    return this.http.put(`${baseUrl}/estado-u-telefonia/estado-activo/${idUser}`, null);
  }

  initUsuario(idUser) {
    return this.http.post(`${baseUrl}/estado-u-telefonia/inicio-usuario/${idUser}`, null);
  }

  usuarioInactivo(idUsuario) {
    return this.http.put(`${baseUrl}/estado-u-telefonia/estado-inactivo/${idUsuario}`, null);
  }

  usuarioPausa(idUsuario) {
    return this.http.put(`${baseUrl}/estado-u-telefonia/estado-pausado/${idUsuario}`, null);
  }

  getEstadoUsuario(idUsuario) {
    return this.http.get(`${baseUrl}/estado-u-telefonia/estado-by-usuario/${idUsuario}`);
  }

  cambiarEstado(idUsuario, idEstadoSesion) {
    return this.http.put(`${baseUrl}/estado-u-telefonia/cambiar-estado/${idUsuario}/${idEstadoSesion}`, null);
  }

  inicioPausa(idUsuario, idMotivoPausa) {
    return this.http.post(`${baseUrl}/estado-u-telefonia/inicio-pausa/${idUsuario}/${idMotivoPausa}`, null);
  }

  finPausa(idPausaUsuario) {
    return this.http.put(`${baseUrl}/estado-u-telefonia/fin-pausa/${idPausaUsuario}`, null);
  }
}
