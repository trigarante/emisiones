import { Injectable } from '@angular/core';
import {HttpBackend, HttpClient} from '@angular/common/http';
import {environment} from './../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {NuevoNumero} from "../../../interfaces/telefonia/nuevo-numero/nuevoNumero";

@Injectable({
  providedIn: 'root',
})
export class AgendaLlamadasService {
  private baseURL;
  private telefoniaDatos;
  private urlPjsip;
  private http: HttpClient;

  constructor(
    // private http: HttpClient
    private handler: HttpBackend
  ) {
    this.http = new HttpClient(handler);
    this.baseURL = environment.SERVICIOS_NODE;
    this.telefoniaDatos = environment.SERVICIOS_DATOS;
    this.urlPjsip = environment.SERVICIOS_PJSIP;
  }

  // put(id, json): Observable<JSON> {
  //   return this.http.put<JSON>(`${this.baseURL}/callcenter/llamadasalida/adendarLlamada/` + id, json,
  //   );
  // }

  post(json): Observable<JSON> {
    return this.http.post<JSON>(`${this.baseURL}/callcenter/agendarLlamadas`, json);
  }
  cambiaraAtendida(idAgenda): Observable<any> {
    return this.http.put<any>(`${this.baseURL}/callcenter/agendarLlamadas/${idAgenda}` , null);
  }

  crearNumero(idRegistro, idSolicitud, numero): Observable<any> {
    return this.http.post<any>(`${this.telefoniaDatos}/supervisores/add-number-registro` , {
      idRegistro,
      idSolicitud,
      numero,
    });
  }

  getNumerosByIdRegistro(idRegistro): Observable<NuevoNumero[]> {
    return this.http.get<NuevoNumero[]>(`${this.telefoniaDatos}/supervisores/get-numeros/${idRegistro}`);
  }
  getNumerosByIdSolicitud(idSolicitud): Observable<NuevoNumero[]> {
    return this.http.get<NuevoNumero[]>(`${this.telefoniaDatos}/supervisores/contactos/${idSolicitud}`);
  }

  agregarNumero(idSolicitud, numero): Observable<{id: number; idSolicitud: number; numero: string}> {
    return this.http.post<{id: number; idSolicitud: number; numero: string}>(`${this.urlPjsip}/llamada-salidas/add-number`, {
      idSolicitud,
      numero,
    });
  }

  getNumerosPjsip(idSolicitud): Observable<[{id: number; idSolicitud: number; numero: string}]> {
    return this.http.get<[{id: number; idSolicitud: number; numero: string}]>(
      `${this.urlPjsip}/llamada-salidas/numeroByidSolicitud/${idSolicitud}`);
  }
}

