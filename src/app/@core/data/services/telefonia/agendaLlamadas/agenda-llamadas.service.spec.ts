import { TestBed } from '@angular/core/testing';

import { AgendaLlamadasService } from './agenda-llamadas.service';

describe('AgendaLlamadasService', () => {
  let service: AgendaLlamadasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AgendaLlamadasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
