import {EventEmitter, Injectable} from '@angular/core';
import {environment} from "../../../../../environments/environment";
import {HttpBackend, HttpClient} from '@angular/common/http';
const base_url = environment.SERVICIOS_NUEVA_LOGICA;
@Injectable({
  providedIn: 'root'
})
export class PeticioneAmiService {

  private http: HttpClient;
  constructor(
    // private http: HttpClient
    private handler: HttpBackend
  ) {
    this.http = new HttpClient(handler);
  }

  public respSolicitud: EventEmitter<any> = new EventEmitter<any>();

  getSolicitud(extension: string, numero: string) {
    return this.http.get(`${base_url}/ami/asterisk-ami/${extension}/${numero}`);
  }
}
