import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ToolbarComponent} from './toolbar/toolbar.component';
import {LoginComponent} from './login/login.component';
import {RouterModule} from '@angular/router';
import {TelefoniaModule} from "../modulos/telefonia/telefonia.module";
import {TrigaranteEscuchaModalComponent} from "./toolbar/modals/trigarante-escucha-modal/trigarante-escucha-modal.component";
import { MotivoPausaComponent } from './toolbar/modals/motivo-pausa/motivo-pausa.component';
import {MaterialModule} from "../modulos/material.module";
import { ImagenesGacetaComponent } from './toolbar/modals/imagenes-gaceta/imagenes-gaceta.component';
import { GacetasDelMesComponent } from './toolbar/modals/gacetas-del-mes/gacetas-del-mes.component';
import {FlexLayoutModule} from "@angular/flex-layout";
import {ReactiveFormsModule} from "@angular/forms";
import { ProcedimientosButtonModalComponent } from './toolbar/modals/procedimientos-button-modal/procedimientos-button-modal.component';
import { ImagenesEsquemasComponent } from './toolbar/modals/imagenes-esquemas/imagenes-esquemas.component';
import { MatSidenavModule } from '@angular/material/sidenav';


@NgModule({
  declarations: [
    ToolbarComponent,
    LoginComponent,
    TrigaranteEscuchaModalComponent,
    MotivoPausaComponent,
    ImagenesGacetaComponent,
    GacetasDelMesComponent,
    ProcedimientosButtonModalComponent,
    ImagenesEsquemasComponent,
  ],
    imports: [
        CommonModule,
        TelefoniaModule,
        FlexLayoutModule,
        RouterModule,
        MaterialModule,
        ReactiveFormsModule,
        RouterModule,
        MatSidenavModule
    ],
  exports: [
    ToolbarComponent,
    LoginComponent,
  ]
})
export class ThemeModule { }
