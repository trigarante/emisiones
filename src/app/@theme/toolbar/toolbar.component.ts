import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';
import {MatDialog, } from '@angular/material/dialog';
import {MenuService} from "../../@core/data/services/menu.service";
import {AuthService} from "../../@core/AuthService";
import {TrigaranteEscuchaModalComponent} from "./modals/trigarante-escucha-modal/trigarante-escucha-modal.component";
import {MatSnackBar} from "@angular/material/snack-bar";
import {LlamadaEntranteComponent} from "../../modulos/telefonia/modals/llamada-entrante/llamada-entrante.component";
import {WebrtcService} from "../../@core/data/services/telefonia/webrtc.service";
import {Subscription} from "rxjs";
import {PeticioneAmiService} from "../../@core/data/services/telefonia/peticione-ami.service";
import {MotivoPausaComponent} from "./modals/motivo-pausa/motivo-pausa.component";
import {ActualServerTelefoniaService} from "../../@core/data/services/telefonia/actual-server-telefonia.service";
import {ImagenesGacetaComponent} from "./modals/imagenes-gaceta/imagenes-gaceta.component";
import {VisualizacionesGacetaService} from "../../@core/data/services/rrhh/visualizaciones-gaceta.service";
import {GacetasDelMesComponent} from "./modals/gacetas-del-mes/gacetas-del-mes.component";
import {Usuario} from "../../models/usuario.model";
import {EstadoUsuarioService} from "../../@core/data/services/sesiones/estado-usuario.service";
import {SesionUsuarioService} from "../../@core/data/services/sesiones/sesion-usuario.service";
import {NotificacionesService} from "../../@core/data/services/others/notificaciones.service";
import {ProcedimientosButtonModalComponent} from "./modals/procedimientos-button-modal/procedimientos-button-modal.component";
import {EstadoUsuarioTelefoniaService} from "../../@core/data/services/telefonia/estado-usuario-telefonia.service";
import {SnackPausaComponent} from "../../modulos/telefonia/modals/snack-pausa/snack-pausa.component";
import { MatSidenav } from '@angular/material/sidenav';
import { animate, style, transition, trigger, state } from '@angular/animations';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
  animations:[
    trigger('openClose',[
      state('close', style({
        width: 0
      })),
      state('open', style({
        width: '*'
      })),
      transition('open <=> close',[
        animate('0.25s')
      ])
    ]),
    trigger('openCloseSub',[
      state('closeSub', style({
        width: 0
      })),
      state('openSub', style({
        width: '*',
      })),
      transition('openSub <=> closeSub',[
        animate('0.25s')
      ])
    ]),
    trigger('slideMenu',[
      transition(':enter',[
        style({opacity: 0}),
        animate('0.4s', style({opacity:1}))
      ]),
    ]),
  ],
})
export class ToolbarComponent implements OnInit, OnDestroy {
  permisos = JSON.parse(window.localStorage.getItem('User'));
  @ViewChild('sidenav') sidenav: MatSidenav;
  public menuOpen = false;
  public submenuOpen = false;
  public menuAlias: string;
  public subMenuAlias: string;
  public men: any[];
  public anio = new Date().getFullYear();
  public aparecer: boolean = true;
  private interval: any;
  public extension = localStorage.getItem('extension');
  public extStatus = '#F55C47';
  private pjsipEvents: Subscription;
  public conectado = false;
  public estados = [{id: 1, descripcion: 'EN LINEA'}, {id: 2, descripcion: 'EN PAUSA'}];
  public initialValue: number;
  public telefoniaServer: string;
  public visualizarGaceta:boolean;
  public usuario: Usuario =  new Usuario(localStorage.getItem('extension'), Number(sessionStorage.getItem('Usuario')), 1);
  private idSesion = Number(localStorage.getItem('sesion'));
  private idEstadoUsuario = localStorage.getItem('estadousuario');
  nameButtonActive: string;
  nameButton: string;
  subButtonActivatedID: number;
  buttonActivatedID: number;
  nameButtonSub: string;
  nameButtonClickSub: string;
  color: string;
  constructor(
    private router: Router,
    public dialog: MatDialog,
    private authService: AuthService,
    private menu: MenuService,
    private snakBar: MatSnackBar,
    private webRTCService: WebrtcService,
    private amiService: PeticioneAmiService,
    private servidorTelefonia: ActualServerTelefoniaService,
    private estadoUsuarioService: EstadoUsuarioService,
    private sesionUsuarioService: SesionUsuarioService,
    private notificacionesService: NotificacionesService,
    private telefoniaUsuariosService: EstadoUsuarioTelefoniaService,
    private visualizacionesGacetaService: VisualizacionesGacetaService,
  ) {
  }

  ngOnInit(): void {
    this.men = this.menu.getMenu();
    this.getGaceta();
    window.onbeforeunload = data => {
      this.abandonoPagina();
    };
    // this.evitarRecargar();
  }


  ngOnDestroy() {
    if (this.pjsipEvents) {
      this.pjsipEvents.unsubscribe();
    }
  }
  abandonoPagina = async () => {
    this.telefoniaUsuariosService.usuarioInactivo(this.usuario.id).subscribe();
  }

  evitarRecargar() {
    /****** Estas lineas evitan que se recarge la pagina. Si estas desarrollando comentalas y al final descomentalas ***/
    window.addEventListener("keyup", disableF5);
    window.addEventListener("keydown", disableF5);
    window.addEventListener("beforeunload", disableRefersh);
    function disableF5(e) {
      if ((e.which || e.keyCode) === 116) e.preventDefault();
    }
    function disableRefersh(e) {
      return e.returnValue = 'No recargues';
    }
    /*********************************************************************************************************************************/
  }

  esucharTelefoniaEventos() {
    if (this.extension.length > 0 && this.extension !== 'null') {
      this.servidorTelefonia.getCurrentServer().subscribe((resp) => {
        this.webRTCService.currentServer = resp;
        this.telefoniaServer = `https://${resp}:8089/httpstatus`;
      });
      this.evaluarEstadoU();
      this.pjsipEvents = this.webRTCService.listenEvents().subscribe((resp) => {
        switch (resp.evento) {
          case 'connected':
          {
            this.conectado = true;
            this.extStatus = '#4AA96C';
            break;
          }
          case 'disconnected':
          {
            this.telefoniaUsuariosService.usuarioInactivo(this.usuario.id).subscribe();
            this.conectado = false;
            this.extStatus = '#F55C47';
            break;
          }
          case 'i_new_call':
            this.amiService.getSolicitud(this.extension, resp.numero).subscribe((response) => {
              this.amiService.respSolicitud.emit(response);
            });
            this.snakBar.openFromComponent(LlamadaEntranteComponent, {
              data: {
                numero: resp.numero
              },
            });
            break;
        }
      });
    }
  }

  evaluarEstadoU() {
    setTimeout(() => {
      this.telefoniaUsuariosService.getEstadoUsuario(this.usuario.id).subscribe((resp: number) => {
        this.usuario.estadoUsuario = resp;
        if (resp === 2) {
          this.snakBar.openFromComponent(SnackPausaComponent, {
            data: {
              idPausaUsuario: sessionStorage.getItem('pu'),
            }
          });
        }
      });
    }, 500);
  }
  async logOut() {
    if (this.menuOpen) {
      this.menuOpen = false;
    }
    const tkn = await Swal.fire({
      title: '¿Desea cerrar sesión?',
      text: '',
      icon: 'question',
      showConfirmButton: true,
      showCancelButton: true,
      cancelButtonColor: '#C33764',
      cancelButtonText: 'No',
      confirmButtonColor: '#1D2671',
      confirmButtonText: 'Si',
      allowOutsideClick: false,
      allowEscapeKey: false,
    });

    if (tkn.isConfirmed){
      this.telefoniaUsuariosService.usuarioInactivo(this.usuario.id).subscribe();
      this.webRTCService.sipUnRegister();
      this.authService.logOut();
      this.router.navigate(['login']);
    }
  }

  irInicio() {
    if (this.menuOpen) {
      this.menuOpen = false;
    }
    this.router.navigate(['/modulos/dashboard']);
  }

  trigaranteEscucha() {
    if (this.menuOpen) {
      this.menuOpen = false;
    }
    if (this.dialog.openDialogs.length === 0) {
      this.dialog.open(TrigaranteEscuchaModalComponent, {
        width: '500px',
      });
    }
  }

  conectarExtension() {
    this.aparecer = false;
    this.telefoniaUsuariosService.activarUsuario(this.usuario.id).subscribe();
    this.usuario.estadoUsuario = 1;
    this.webRTCService.sipRegister(this.usuario.id);
    this.interval = setInterval(() => {
      this.aparecer = true;
      clearInterval(this.interval);
    }, 3000);
    setTimeout(() => {
      this.webRTCService.sipCall('call-audio', '*47');
    }, 500);
  }

  motivoPausa() {
    this.telefoniaUsuariosService.usuarioPausa(this.usuario.id).subscribe();
    this.usuario.estadoUsuario = 2;
    this.dialog.open(MotivoPausaComponent, {
      data: this.usuario.id,
      width: '500px',
      disableClose: true,
    }).afterClosed().subscribe((resp) => {
      if (!resp.pauso) {
        this.usuario.estadoUsuario = 1;
      } else {
        this.snakBar.openFromComponent(SnackPausaComponent, {
          data: {
            idPausaUsuario: resp.idPausaUsuario,
            tiempo: resp.tiempo,
          }
        }).afterDismissed().subscribe(() => {
          this.usuario.estadoUsuario = 1;
        });
      }
    });
  }
  getGaceta() {
    this.visualizacionesGacetaService.getByIdEmpleado().subscribe({
      next: data => {
        if (!data) {
          this.visualizarGaceta = true;
          this.mostrarImagenes();
        }
      },
    });
  }


  mostrarImagenes() {
    this.dialog.open(ImagenesGacetaComponent, {
      disableClose: this.visualizarGaceta,
      data: this.visualizarGaceta,
    });
  }
  gacetasDelMes() {
    this.dialog.open(GacetasDelMesComponent, {
      panelClass: 'custom-dialog-Gaceta',
      height: '95%',
    });
  }

  procedimientosSteps(){
    this.dialog.open(ProcedimientosButtonModalComponent, {
      panelClass: 'custom-dialog-ProcedimientosModal'});
  }


  whatsappMessages(){
    this.notificacionesService.advertencia('No hay mensajes por responder');
  }

  /* Codigo para el funcionamiento del nuevo menu */
  //  función para cerrar el sidenav y todos los submenus abiertos
  closeSidenav(): void{
    this.sidenav.close();
    this.menuOpen = false;
    this.submenuOpen = false;
  }

  // función para abrir submenu hijo
  openMenu(indexMenu: number, alias: string): void{
    if(this.menuOpen === false){
      this.menuOpen = true;
    }
    else{
      if(indexMenu === this.buttonActivatedID){
        this.menuOpen = false;
      }
      else{
        this.menuOpen = true;
      }
      this.submenuOpen = false;
    }
    this.buttonActivatedID = indexMenu;
    this.menuAlias = alias;
  }

  // función para abrir submenu nieto
  openSubMenu(indexSubMenu: number, alias:string): void{
    if(this.submenuOpen === false){
      this.submenuOpen = true;
    }
    else{
      if(this.subButtonActivatedID === indexSubMenu){
        this.submenuOpen = false;
      }
      else{
        this.submenuOpen = true;
      }
    }
    this.subButtonActivatedID = indexSubMenu;
    this.subMenuAlias = alias;
  }
}
