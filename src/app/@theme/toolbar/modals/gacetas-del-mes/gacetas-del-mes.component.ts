import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";
import {GacetaService} from "../../../../@core/data/services/rrhh/gaceta.service";

import {ImagenesGacetaService} from "../../../../@core/data/services/rrhh/imagenes-gaceta.service";

@Component({
  selector: 'app-gacetas-del-mes',
  templateUrl: './gacetas-del-mes.component.html',
  styleUrls: ['./gacetas-del-mes.component.scss']
})
export class GacetasDelMesComponent implements OnInit {
  imagenes: any;
  contador = 0;
  labels = [];
  noHayGacetas = false;

  constructor(
    public dialogRef: MatDialogRef<GacetasDelMesComponent>,
    public gacetaService: GacetaService,
    public imagenesGacetaService: ImagenesGacetaService,
  ) { }

  ngOnInit() {
    this.getGacetasDelMes();
  }

  getGacetasDelMes() {
    this.gacetaService.getDelMes().subscribe({
      next: data => {
        if (data.length === 0) {
          this.noHayGacetas = true;
        } else {
          this.labels = data;
        }
      },
    });
  }

  dismiss() {
    this.dialogRef.close();
  }

  moverContador(incremento: boolean) {
    if (incremento) {
      this.contador++;
    } else {
      this.contador--;
    }
  }

  redireccionar() {
    if (this.imagenes[this.contador].link)
      window.open(this.imagenes[this.contador].link);
  }

  getImagenes($event) {
    this.imagenes = undefined;
    this.imagenesGacetaService.getByIdGaceta(this.labels[$event.index].id).subscribe({
      next: data => { this.imagenes = data; },
    });
  }
}

