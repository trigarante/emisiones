import { Component, Inject, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import Swal from 'sweetalert2';
import {MotivosPausa} from "../../../../@core/data/interfaces/telefonia/llamadas-pausas/motivos-pausas";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {MotivosPausasService} from "./../../../../@core/data/services/telefonia/llamadas-pausas/motivos-pausas.service"
import {MatSnackBar} from "@angular/material/snack-bar";
import {SesionUsuarioService} from "../../../../@core/data/services/sesiones/sesion-usuario.service";
import {FlujoLlamadaService} from "../../../../@core/data/services/telefonia-pjsip/flujo-llamada.service";
import {PjsipService} from "../../../../@core/data/services/telefonia-pjsip/pjsip.service";
import {EstadoUsuarioService} from "../../../../@core/data/services/sesiones/estado-usuario.service";
import {WebrtcService} from "../../../../@core/data/services/telefonia/webrtc.service";

@Component({
  selector: 'app-motivo-pausa',
  templateUrl: './motivo-pausa.component.html',
  styleUrls: ['./motivo-pausa.component.scss']
})
export class MotivoPausaComponent implements OnInit {
  public motivos: MotivosPausa[];
  motivoPausaForm: FormGroup;
  idEstadoUsuario;
  creacionPoliza;
  public aparecer = false;
  // grupo: any = localStorage.getItem('Grupo');

  constructor(public dialogRef: MatDialogRef<MotivoPausaComponent>,
              private motivosPausaService: MotivosPausasService,
              private fb: FormBuilder,
              private snackBar: MatSnackBar,
              private estadoUsuarioService: EstadoUsuarioService,
              private flujoLlamada: FlujoLlamadaService,
              private sesionUsuarioService: SesionUsuarioService,
              private webRtcService: WebrtcService,
              @Inject(MAT_DIALOG_DATA) public data) {
    sessionStorage.setItem('idEstadoSesion', '2');
    this.webRtcService.sipCall('call-audio', '*46');
  }

  ngOnInit() {
    this.motivoPausaForm = this.fb.group({
      'motivos': new FormControl(2, Validators.required),
    });
    this.getMotivos();
    this.habilitarbotonCancelar();
  }

  habilitarbotonCancelar() {
    setTimeout(() => {this.aparecer = true;}, 5000);
  }
  getMotivos() {
    this.motivosPausaService.getMotivosPausas().subscribe((resp: any) => {
      console.log(resp);
      this.motivos = resp;
    });
  }
  enviar() {
    this.swalCargando();
    // this.sipml5Service.sipCall('call-audio', this.prefijoPausa);
    const idSesion = Number(localStorage.getItem('sesion'));
    const idMotivo = this.motivoPausaForm.get('motivos').value.id;
    const tiempo = this.motivoPausaForm.get('motivos').value.tiempo;
    const estado = {
      idSesionUsuario: idSesion,
      idEstadoSesion: 2,
      idMotivosPausa: idMotivo,
    };
    this.estadoUsuarioService.post(estado).subscribe((resp: any) => {
      this.dialogRef.close(resp.estadoUsuario.id);
      localStorage.setItem('estadousuario', resp.estadoUsuario.id.toString());
    }, () => {}, () => {
      // sessionStorage.setItem('idEstadoSesion', '2');
      // window.location.reload();
      this.alertasEstadoUsuario(tiempo);
    });

  }
  swalCargando() {
    Swal.fire({
      title: 'Se está actualizando la informacón, espere un momento.',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
  }
  setValue(v) {
    this.creacionPoliza = v;
  }
  close() {
    const idSesion = Number(localStorage.getItem('sesion'));
    this.sesionUsuarioService.put(idSesion, 1).subscribe();
    sessionStorage.setItem('idEstadoSesion', '1');
    this.webRtcService.sipCall('call-audio', '*47');
    this.dialogRef.close();
  }

  alertasEstadoUsuario(tiempo) {
    if (this.creacionPoliza !== 24) {
      const texto = `Cuentas con <b> ${this.segudosAhoras(tiempo)} </b>`;
      let timerInterval;
      let timerIntervalOut;
      Swal.fire({
        title: 'Tus llamadas se encuentran en pausa.',
        html: texto,
        icon: 'success',
        confirmButtonText: 'RECONECTAR AHORA',
        allowOutsideClick: false,
        onBeforeOpen: () => {
          timerIntervalOut = setTimeout(() => {
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Excediste tu tiempo de descanso!',
              confirmButtonText: 'RECONECTAR AHORA',
              onClose: () => {
                clearInterval(timerInterval);
                this.webRtcService.sipCall('call-audio', '*47');
                this.flujoLlamada.available();
                this.cambiarEstadoUsuario(1);
                clearTimeout(timerIntervalOut);
              },
            }).then((result) => {
              if (result.value) {
                this.webRtcService.sipCall('call-audio', '*47');
                this.flujoLlamada.available();
                this.cambiarEstadoUsuario(1);
              }
            });
          }, (tiempo * 1000) + 1000);
          timerInterval = setInterval(() => {
            const content = Swal.getContent();
            const b = content.querySelector('b');
            try {
              b.innerText = `${this.segudosAhoras(tiempo)}`;
              tiempo -= 1;
            } catch (e) { }
          }, 1000);
          setTimeout(() => {
            clearInterval(timerInterval);
          }, (tiempo * 1000) + 1000);
        },
        onClose: () => {
          clearInterval(timerInterval);
          this.cambiarEstadoUsuario(1);
          this.webRtcService.sipCall('call-audio', '*47');
          this.flujoLlamada.available();
          clearTimeout(timerIntervalOut);
        },
      }).then((result) => {
        if (result.value) {
          this.cambiarEstadoUsuario(1);
          this.webRtcService.sipCall('call-audio', '*47');
          this.flujoLlamada.available();
        }
      });
    } else {
      Swal.fire( {
        title: 'No olvides finalizar tu pausa',
        icon: 'warning',
        timer: 2000,
        showConfirmButton: false,
      });
      const snack = this.snackBar.open('No olvides finalizar tu pausa', 'Finalizar pausa', {
        horizontalPosition: 'right',
        verticalPosition: 'top',
      });
      snack.afterDismissed().subscribe(() => {
        this.cambiarEstadoUsuario(1);
        this.webRtcService.sipCall('call-audio', '*47');
        this.flujoLlamada.available();
      });
    }
  }

  segudosAhoras(segundos) {
    let hour: any = Math.floor(segundos / 3600);
    hour = (hour < 10) ? '0' + hour : hour;
    let minute: any = Math.floor((segundos / 60) % 60);
    minute = (minute < 10) ? '0' + minute : minute;
    let second: any = segundos % 60;
    second = (second < 10) ? '0' + second : second;
    return hour + ':' + minute + ':' + second;
  }

  cambiarEstadoUsuario(idEstadoSesion) {
    this.swalCargando();
    const idSesion = Number(localStorage.getItem('sesion'));
    this.sesionUsuarioService.put(idSesion, idEstadoSesion).subscribe();
    if (idEstadoSesion === 1) {
      if (!this.idEstadoUsuario) {
        this.idEstadoUsuario = localStorage.getItem('estadousuario');
      }
      this.estadoUsuarioService.put(this.idEstadoUsuario).subscribe({
        complete: () => {
          Swal.fire({
            title: 'Te encuentras de nuevo en linea!',
            icon: 'success',
            timer: 2000,
            showConfirmButton: false,
          });
        },
      });
      sessionStorage.setItem('idEstadoSesion', '1');
    }
  }
}
