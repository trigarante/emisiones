import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImagenesGacetaComponent } from './imagenes-gaceta.component';

describe('ImagenesGacetaComponent', () => {
  let component: ImagenesGacetaComponent;
  let fixture: ComponentFixture<ImagenesGacetaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImagenesGacetaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImagenesGacetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
