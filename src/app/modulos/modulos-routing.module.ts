import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ModulosComponent} from './modulos.component';
import {AuthGuard} from '../@core/data/services/login/auth.guard';
import {DashboardComponent} from "./Dashboard/dashboard/dashboard.component";


const routes: Routes = [{
  path: '',
  component: ModulosComponent,
  children: [
    {
      path: 'dashboard',
      loadChildren: () => import('./Dashboard/dashboard.module').then(m => m.DashboardModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'telefonia',
      // loadChildren: './venta-nueva/venta-nueva.module#VentaNuevaModule',
      loadChildren: () => import('./telefonia/telefonia.module').then(m => m.TelefoniaModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'emisiones',
      loadChildren: () => import('./emisiones/emisiones.module').then(m => m.EmisionesModule),
      canActivate: [AuthGuard]
    },
    {
      path: '**',
      component: DashboardComponent,
      canActivate: [AuthGuard]
    },
  ]

}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModulosRoutingModule {
}
