import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {TelefoniaComponent} from "./telefonia.component";
import {HistorialLlamadasComponent} from "./historial-llamadas/historial-llamadas.component";
import {EnLlamadasOutComponent} from  "./en-llamadas-out/en-llamadas-out.component"
import {MonitoreoOutComponent} from "./monitoreo-out/monitoreo-out.component";

const routes: Routes = [
  {
  path: '',
  component: TelefoniaComponent,
  children: [
    {
      path: 'historial-llamadas/:idSolicitud',
      component: HistorialLlamadasComponent,
    },
    {
      path: 'llamadas-out',
      component: EnLlamadasOutComponent,
    },
    {
      path: 'monitoreo',
      component: MonitoreoOutComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TelefoniaRoutingModule { }
