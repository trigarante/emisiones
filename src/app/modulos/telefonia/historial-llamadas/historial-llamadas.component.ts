import { Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {HistorialLlamadasService} from "../../../@core/data/services/telefonia/historial-llamadas.service";
import {ActivatedRoute, Params} from "@angular/router";

@Component({
  selector: 'app-historial-llamadas',
  templateUrl: './historial-llamadas.component.html',
  styleUrls: ['./historial-llamadas.component.scss']
})
export class HistorialLlamadasComponent implements OnInit  {
  displayedColumns: string[] = ['idLlamada', 'idAsterisk', 'idSolicitud', 'fechaInicio', 'fechaFinal'];
  dataSource: any;
  idSolicitud: string;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private historialLlamadasService: HistorialLlamadasService,
              private route: ActivatedRoute) {
    this.route.params.subscribe((params: Params) => {
      this.idSolicitud = params.idSolicitud;
    });
  }

  ngOnInit(): void {
    this.historialLlamadasService.getHistorialLlamadas(this.idSolicitud).subscribe(result => {
      this.dataSource = new MatTableDataSource<any>(result);
      this.dataSource.paginator = this.paginator;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
