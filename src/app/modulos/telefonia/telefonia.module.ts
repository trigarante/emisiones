import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TelefoniaRoutingModule } from './telefonia-routing.module';
import { LlamadaEntranteComponent } from './modals/llamada-entrante/llamada-entrante.component';
import {RouterModule} from "@angular/router";
import { DialogoLlamadaComponent } from './modals/dialogo-llamada/dialogo-llamada.component';
import {MaterialModule} from "../material.module";
import { AgregarNumeroComponent } from './modals/agregar-numero/agregar-numero.component';
import { LlamadaSalidaComponent } from './modals/llamada-salida/llamada-salida.component';
import { TipificarComponent } from './modals/tipificar/tipificar.component';
import { HistorialLlamadasComponent } from './historial-llamadas/historial-llamadas.component';
import {TelefoniaComponent} from "./telefonia.component";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatTableModule} from "@angular/material/table";
import {EnLlamadasOutComponent} from "./en-llamadas-out/en-llamadas-out.component";
import { LlamadasOutComponent } from './modals/llamadas-out/llamadas-out.component';
import { MonitoreoOutComponent } from './monitoreo-out/monitoreo-out.component';


@NgModule({
  declarations: [
    TelefoniaComponent,
    LlamadaEntranteComponent,
    DialogoLlamadaComponent,
    AgregarNumeroComponent,
    LlamadaSalidaComponent,
    TipificarComponent,
    HistorialLlamadasComponent,
    EnLlamadasOutComponent,
    LlamadasOutComponent,
    MonitoreoOutComponent,
  ],
  imports: [
    CommonModule,
    TelefoniaRoutingModule,
    MaterialModule,
    RouterModule,
    FlexLayoutModule,
    MatTableModule,


    // ReactiveFormsModule,
  ],
  exports: [
    LlamadaEntranteComponent,
    LlamadaSalidaComponent,
    AgregarNumeroComponent,
    EnLlamadasOutComponent,
    MaterialModule,
  ],
  entryComponents: [
    LlamadaEntranteComponent,
    DialogoLlamadaComponent,
    AgregarNumeroComponent,
    LlamadaSalidaComponent,
    TipificarComponent,
    EnLlamadasOutComponent,
    LlamadasOutComponent,

  ]
})
export class TelefoniaModule { }
