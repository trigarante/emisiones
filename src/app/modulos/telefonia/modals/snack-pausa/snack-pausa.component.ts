import {Component, Inject, OnInit} from '@angular/core';
import {MAT_SNACK_BAR_DATA, MatSnackBarRef} from '@angular/material/snack-bar';
import {EstadoUsuarioTelefoniaService} from "../../../../@core/data/services/telefonia/estado-usuario-telefonia.service";
import {WebrtcService} from "../../../../@core/data/services/telefonia/webrtc.service";

@Component({
  selector: 'app-snack-pausa',
  templateUrl: './snack-pausa.component.html',
  styleUrls: ['./snack-pausa.component.scss']
})
export class SnackPausaComponent implements OnInit {
  segundos: any;
  mensaje: string;
  interval: any;
  constructor(@Inject(MAT_SNACK_BAR_DATA) public data,
              private matSnackRef: MatSnackBarRef<SnackPausaComponent>,
              private webRTC: WebrtcService,
              private telefoniaUsuario: EstadoUsuarioTelefoniaService) {
    this.setMensaje();
  }

  ngOnInit(): void {
    if (this.data.tiempo) {
      this.startInterval();
    }
  }

  startInterval() {
    this.interval = setInterval(() => {
      this.data.tiempo --;
      if (this.data.tiempo === 0) {
        clearInterval(this.interval);
        return this.mensaje = 'Excediste tu tiempo de pausa';
      }
      this.setMensaje();
    }, 1000);
  }

  setMensaje() {
    if (this.data.tiempo) {
      this.segundos = new Date(this.data.tiempo * 1000).toISOString().substr(11, 8);
      this.mensaje = `Cuentas con ${this.segundos}`;
    } else {
      this.mensaje = 'Te encuentras en pausa';
    }
  }

  finalizarPausa() {
    this.webRTC.sipCall('call-audio', '*47');
    clearInterval(this.interval);
    this.telefoniaUsuario.activarUsuario(sessionStorage.getItem('Usuario')).subscribe();
    this.telefoniaUsuario.finPausa(this.data.idPausaUsuario).subscribe();
    this.matSnackRef.dismiss();
  }

}
