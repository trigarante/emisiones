import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subscription} from "rxjs";
import {Extensiones} from "../../../../@core/data/interfaces/telefonia/extensiones";
import {FormControl} from "@angular/forms";
import {WebrtcService} from "../../../../@core/data/services/telefonia/webrtc.service";
import {MAT_SNACK_BAR_DATA, MatSnackBarRef} from "@angular/material/snack-bar";
import {ExtensionesService} from "../../../../@core/data/services/telefonia/extensiones.service";
import {MatDialog} from "@angular/material/dialog";
import {SolicitudesvnTelefoniaService} from "../../../../@core/data/services/telefonia/solicitudesvn-telefonia.service";
import {map, startWith} from "rxjs/operators";
import {DialogoLlamadaComponent} from "../dialogo-llamada/dialogo-llamada.component";
import {LlamadasOutService} from "../../../../@core/data/services/telefonia/llamadas-out.service";
import {TipificarComponent} from "../tipificar/tipificar.component";

@Component({
  selector: 'app-llamada-salida',
  templateUrl: './llamada-salida.component.html',
  styleUrls: ['./llamada-salida.component.scss']
})
export class LlamadaSalidaComponent implements OnInit, OnDestroy {
  private llamadaEvents: Subscription;
  public options: Extensiones[];
  filteredOptions: Observable<Extensiones[]>;
  myControl = new FormControl();
  public transferir = false;
  public estadoMute: boolean = true;
  public holdStatus: boolean = true;
  holdAudio = false;
  public counter = 0;
  private intervalId: any;
  public dialogoData: any;
  private idLlamada: number;

  constructor(private webRTCService: WebrtcService,
              private snackBarRef: MatSnackBarRef<LlamadaSalidaComponent>,
              private matDialog: MatDialog,
              private extensionesService: ExtensionesService,
              private dialogService: MatDialog,
              private llamadasOutService: LlamadasOutService,
              private SolicitudesService: SolicitudesvnTelefoniaService,
              @Inject(MAT_SNACK_BAR_DATA) public data: any) { }

  ngOnInit(): void {
    this.llamar();
    this.getEvents();
    this.getNumerosTransferencia();
    this.getSolicitud();
  }

  getSolicitud() {
    this.SolicitudesService.getSolicitudById(this.data.idSolicitud).subscribe((resp) => {
      this.dialogoData = resp;
      this.abrirDialogo();
    });
  }

  ngOnDestroy() {
    this.llamadaEvents.unsubscribe();
    clearInterval(this.intervalId);
  }

  llamar() {
    this.webRTCService.sipCall('call-audio', this.data.numero);
    this.iniciarContador();
    this.llamadasOutService.iniciarLlamada(this.data.numero, this.data.idTipoLlamada, this.data.idSolicitud, this.data.idRegistro)
      .subscribe((resp: any) => {
        this.idLlamada = resp.id;
      });
  }

  getEvents() {
    this.llamadaEvents = this.webRTCService.listenEvents().subscribe((resp) => {
      switch (resp.evento) {
        case 'disconnected':
        case 'terminated':
          this.finalizarLlamada();
          this.llamadaEvents.unsubscribe();
          this.snackBarRef.dismiss();
          break;
      }
    });
  }

  finalizarLlamada() {
    this.llamadasOutService.finLlamada(this.idLlamada).subscribe();
    this.matDialog.open(TipificarComponent, {
      data: {
        idLlamada: this.idLlamada,
      }
    });
  }
  getNumerosTransferencia() {
    this.extensionesService.getContactos().subscribe((resp) => {
      this.options = resp;
      this.filteredOptions = this.myControl.valueChanges
        .pipe(
          startWith(''),
          map(value => {
            return this._filter(value);
          }),
        );
    });
  }

  private _filter(value: string): Extensiones[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.nombreCampania.toLowerCase().includes(filterValue));
  }

  transfer(extension: string) {
    this.webRTCService.sipTransfer(extension);
    this.transferir = true;
    this.llamadaEvents.unsubscribe();
    this.snackBarRef.dismiss();
  }

  mute(estadoMute) {
    this.estadoMute = (estadoMute === 1) ? false : true;
    this.webRTCService.sipToggleMute();
  }
  hold(holdState) {
    this.holdStatus = (holdState === 1) ? false : true;
    this.webRTCService.sipToggleMute();
    this.holdAudio = holdState === 1;
  }

  abrirDialogo() {
    if (this.dialogoData) {
      this.dialogService.open(DialogoLlamadaComponent, {
        data: this.dialogoData,
        width: '550px'
      });
    }
  }

  colgar() {
    this.webRTCService.sipHangUp();
  }

  iniciarContador() {
    this.intervalId = setInterval(() => {
      this.counter = this.counter + 1;
    }, 1000);
  }

}
