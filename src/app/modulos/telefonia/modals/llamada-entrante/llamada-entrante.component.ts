import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {WebrtcService} from "../../../../@core/data/services/telefonia/webrtc.service";
import {MAT_SNACK_BAR_DATA, MatSnackBarRef} from "@angular/material/snack-bar";
import {Observable, Subscription} from "rxjs";
import {ExtensionesService} from "../../../../@core/data/services/telefonia/extensiones.service";
import {Extensiones} from "../../../../@core/data/interfaces/telefonia/extensiones";
import {FormControl} from "@angular/forms";
import {map, startWith} from "rxjs/operators";
import {PeticioneAmiService} from "../../../../@core/data/services/telefonia/peticione-ami.service";
import {MatDialog} from "@angular/material/dialog";
import {DialogoLlamadaComponent} from "../dialogo-llamada/dialogo-llamada.component";
import {SolicitudesvnTelefoniaService} from "../../../../@core/data/services/telefonia/solicitudesvn-telefonia.service";

@Component({
  selector: 'app-llamada-entrante',
  templateUrl: './llamada-entrante.component.html',
  styleUrls: ['./llamada-entrante.component.scss']
})
export class LlamadaEntranteComponent implements OnInit, OnDestroy {
  private llamadaEvents: Subscription;
  public solicitudSubs: Subscription;
  public options: Extensiones[];
  filteredOptions: Observable<Extensiones[]>;
  myControl = new FormControl();
  public transferir = false;
  public estadoMute: boolean = true;
  public holdStatus: boolean = true;
  holdAudio = false;
  public counter = 0;
  private intervalId: any;
  public dialogoData: any;
  private idSolicitud: any;


  constructor(private webRtcService: WebrtcService,
              private snackBarRef: MatSnackBarRef<LlamadaEntranteComponent>,
              private extensionesService: ExtensionesService,
              private dialogService: MatDialog,
              private peticionesAmiService: PeticioneAmiService,
              private solicitudesVn: SolicitudesvnTelefoniaService,
              @Inject(MAT_SNACK_BAR_DATA) public data: any) { }

  ngOnInit(): void {
    this.getEvents();
    this.getSolicitud();
    this.getNumerosTransferencia();

  }

  getSolicitud() {
    this.solicitudSubs = this.peticionesAmiService.respSolicitud.subscribe((resp) => {
      this.asignarSolicitud(resp.id);
      this.idSolicitud = resp.id;
      this.dialogoData = resp;
      this.abrirDialogo();
    });
  }

  asignarSolicitud(idSolicitud) {
    if(idSolicitud) {
      this.solicitudesVn.asignarEmpleadoSolicitud(idSolicitud).subscribe();
    }
  }

  ngOnDestroy() {
    this.llamadaEvents.unsubscribe();
    this.solicitudSubs.unsubscribe();
    clearInterval(this.intervalId);
  }

  contestar() {
    this.webRtcService.sipCall('call-audio', null);
    this.iniciarContador();
  }

  colgar() {
    this.webRtcService.sipHangUp();
    this.snackBarRef.dismiss();
  }

  getEvents() {
    this.llamadaEvents = this.webRtcService.listenEvents().subscribe((resp) => {
      switch (resp.evento) {
        case 'disconnected':
        case 'terminated':
          this.snackBarRef.dismiss();
          this.llamadaEvents.unsubscribe();
          break;
      }
    });
  }

  getNumerosTransferencia() {
    this.extensionesService.getContactos().subscribe((resp) => {
      this.options = resp;
      this.filteredOptions = this.myControl.valueChanges
        .pipe(
          startWith(''),
          map(value => {
            return this._filter(value);
          }),
        );
    });
  }

  private _filter(value: string): Extensiones[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.nombreCampania.toLowerCase().includes(filterValue));
  }

  transfer(extension: string) {
    this.webRtcService.sipTransfer(extension);
    this.transferir = true;
    this.llamadaEvents.unsubscribe();
    this.snackBarRef.dismiss();
  }

  mute(estadoMute) {
    this.estadoMute = (estadoMute === 1) ? false : true;
    this.webRtcService.sipToggleMute();
  }
  hold(holdState) {
    this.holdStatus = (holdState === 1) ? false : true;
    this.webRtcService.sipToggleMute();
    this.holdAudio = holdState === 1;
  }

  iniciarContador() {
    this.intervalId = setInterval(() => {
      this.counter = this.counter + 1;
    }, 1000);
  }

  abrirDialogo() {
    if (this.dialogoData) {
      this.dialogService.open(DialogoLlamadaComponent, {
        data: this.dialogoData,
        width: '550px'
      });
    }
  }

}
