import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {LlamadasOutService} from "../../../../@core/data/services/telefonia/llamadas-out.service";
import {NotificacionesService} from "../../../../@core/data/services/others/notificaciones.service";

@Component({
  selector: 'app-tipificar',
  templateUrl: './tipificar.component.html',
  styleUrls: ['./tipificar.component.scss']
})
export class TipificarComponent implements OnInit {
  estadoSolcitudCreateForm: FormGroup;
  etiquetaSolicitud = [
    {id: 1, descripcion: 'ETIQUETA 1'},
    {id: 2, descripcion: 'ETIQUETA 2'}
  ];

  subetiquetas = [
    {id: 1, descripcion: 'SUBETIQUETA 1'},
    {id: 2, descripcion: 'SUBETIQUETA 2'}
  ];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private dialogRef: MatDialogRef<TipificarComponent>,
    private notificacionesService: NotificacionesService,
    private llamadasOutService: LlamadasOutService,
    private fb: FormBuilder) { }

  ngOnInit(): void {
    this.estadoSolcitudCreateForm = this.fb.group({
      'idEtiquetaSolicitud': new FormControl('', Validators.compose([Validators.required])),
      'idSubetiquetaSolicitud': new FormControl('', Validators.required),
      'comentarios': new FormControl(''),
    });
  }

  tipificarLlamada() {
    const idSubetiqueta = this.estadoSolcitudCreateForm.value.idSubetiquetaSolicitud;
    this.llamadasOutService.tipificarLlamada(this.data.idLlamada, idSubetiqueta).subscribe({
      complete: () => {
        this.notificacionesService.carga('Tipificando llamada');
        this.notificacionesService.exito('Llamada tipificada correctamente');
        this.dialogRef.close();
      }
    });
  }

}
