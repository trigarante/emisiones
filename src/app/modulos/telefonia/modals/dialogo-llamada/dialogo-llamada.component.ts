import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {WebrtcService} from "../../../../@core/data/services/telefonia/webrtc.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-dialogo-llamada',
  templateUrl: './dialogo-llamada.component.html',
  styleUrls: ['./dialogo-llamada.component.scss']
})
export class DialogoLlamadaComponent implements OnInit, OnDestroy{

  llamadaEvents: Subscription;

  constructor(@Inject(MAT_DIALOG_DATA) public data,
              public matDialogRef: MatDialogRef<DialogoLlamadaComponent>,
              private webRtcService: WebrtcService) { }

  ngOnInit() {
    this.llamadaEvents = this.webRtcService.listenEvents().subscribe((resp) => {
      switch (resp.evento) {
        case 'disconnected':
        case 'terminated':
          this.matDialogRef.close();
          break;
      }
    })
  }

  ngOnDestroy() {
    this.llamadaEvents.unsubscribe();
  }

}
