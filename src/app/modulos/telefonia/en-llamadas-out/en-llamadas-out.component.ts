import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { GetTestDataService } from '../../../@core/data/services/get-test-data.service'
import {MatTableDataSource} from "@angular/material/table";
import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {MatDialog} from "@angular/material/dialog";
import {LlamadasOutComponent} from "../modals/llamadas-out/llamadas-out.component";
import {interval, Subscription} from "rxjs";
import moment from "moment";
interface llamadaE{
  extension: number;
  fechaInicio: any;
  id: number;
  idAsterisk: String;
  idSubarea: number;
  nombre: string;
  numero: number;
  subarea: string;
  acciones : any;
  tiempo?: any;
}

@Component({
  selector: 'app-en-llamadas-out',
  templateUrl: './en-llamadas-out.component.html',
  styleUrls: ['./en-llamadas-out.component.scss']
})
export class EnLlamadasOutComponent implements OnInit, OnDestroy{

  displayColums: string [] = ['id', 'nombre', 'extension', 'fechaInicio', 'numero', 'campania', 'tiempo', 'acciones'];
  dataSource: any;
  timerPeticion: Subscription;
  pausas: llamadaE[];
  counter = 15;
  interval: any;
  extension = localStorage.getItem('extension');

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private llamadas: GetTestDataService,
              public dialog: MatDialog) {
  }

  openDialog(nombre : string, extension : any):void {
    this.dialog.open(LlamadasOutComponent, {
      width: '550px',
      data: {nombre: nombre, extension : extension},
    });
  }

  ngOnInit() {
    this.intervalo();
    this.getLlamadas();
    this.timerPeticion = interval(15000).subscribe(() => {
      this.getLlamadas();
    });

  }

  ngOnDestroy() {
    this.timerPeticion.unsubscribe();
    clearInterval(this.interval);
  }

  intervalo() {
    this.interval = setInterval(() => {
      this.counter = this.counter - 1;
      if (this.counter === 0) {
        this.counter = 15;
      }
    }, 1000);
  }

  getLlamadas() {
   let resp = this.llamadas.getTestData();
    resp.subscribe((report : any[]) => {
      this.pausas = report;
      this.pausas.forEach((llamada) => {
        const fecha1 = moment();
        const fecha2 = moment(llamada.fechaInicio);
        llamada.tiempo = this.segudosAhoras(fecha1.diff(fecha2, 'seconds'));
      });
     this.dataSource = new MatTableDataSource(this.pausas);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.counter = 15;
    });

  }

  segudosAhoras(segundos) {
    let hour: any = Math.floor(segundos / 3600);
    hour = (hour < 10) ? '0' + hour : hour;
    let minute: any = Math.floor((segundos / 60) % 60);
    minute = (minute < 10) ? '0' + minute : minute;
    let second: any = segundos % 60;
    second = (second < 10) ? '0' + second : second;
    return hour + ':' + minute + ':' + second;
  }


}
