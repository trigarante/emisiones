import {NgModule} from '@angular/core';
import {EmisionesComponent} from './emisiones.component';
import {CommonModule} from '@angular/common';
import {RouterModule} from "@angular/router";
import {EmisionesRoutingModule} from './emisiones-routing.module';
import {MatTableModule} from "@angular/material/table";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatButtonModule} from "@angular/material/button";
import {MatCardModule} from "@angular/material/card";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatDialogModule} from "@angular/material/dialog";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {MatMenuModule} from "@angular/material/menu";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatSortModule} from "@angular/material/sort";
import {MatFormFieldModule} from "@angular/material/form-field";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatTabsModule} from "@angular/material/tabs";
import {MatBadgeModule} from "@angular/material/badge";
import { AdministracionPolizasComponent } from './administracion-polizas/administracion-polizas.component';
import {MatExpansionModule} from "@angular/material/expansion";
import { StepCotizadorEmisionesComponent } from './step-cotizador-emisiones/step-cotizador-emisiones.component';
import {MatStepperModule} from "@angular/material/stepper";
import { DetallePolizaComponent } from './detalle-poliza/detalle-poliza.component';
import {MatListModule} from "@angular/material/list";
import { OptEstadosComponent } from './modals/opt-estados/opt-estados.component';
import {MatRadioModule} from "@angular/material/radio";
import { DatosRegistroPolizaComponent } from './modals/datos-registro-poliza/datos-registro-poliza.component';
import { PendientesRenovarComponent } from './pendientes-renovar/pendientes-renovar.component';
import {MatSelectModule} from "@angular/material/select";
import { RegistroEmisionesComponent } from './modals/registro-emisiones/registro-emisiones.component';
import {MatCheckboxModule} from "@angular/material/checkbox";
import { ClientesComponent } from './clientes/clientes.component';
import { DetallesClienteComponent } from './modals/detalles-cliente/detalles-cliente.component';
import { CrearClienteEmisionComponent } from './modals/crear-cliente-emision/crear-cliente-emision.component';
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {FlexLayoutModule} from "@angular/flex-layout";



@NgModule({
  declarations: [
    EmisionesComponent,
    AdministracionPolizasComponent,
    StepCotizadorEmisionesComponent,
    DetallePolizaComponent,
    OptEstadosComponent,
    DatosRegistroPolizaComponent,
    PendientesRenovarComponent,
    RegistroEmisionesComponent,
    ClientesComponent,
    DetallesClienteComponent,
    CrearClienteEmisionComponent,



  ],
    imports: [
        EmisionesRoutingModule,
        RouterModule,
        CommonModule,
        MatTableModule,
        MatTabsModule, MatToolbarModule, MatTooltipModule,
        MatButtonModule,
        MatCardModule,
        MatDatepickerModule,
        MatDialogModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatBadgeModule,
        MatPaginatorModule,
        MatSortModule,
      FlexLayoutModule,
        MatFormFieldModule, FormsModule, MatExpansionModule, ReactiveFormsModule, MatStepperModule, MatListModule, MatRadioModule, MatSelectModule, MatCheckboxModule, MatSlideToggleModule,
    ]
})
export class EmisionesModule { }
