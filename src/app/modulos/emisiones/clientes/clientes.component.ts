import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {FiltrosTablasService} from '../../../@core/filtros-tablas.service';
import {ClienteRenovaService} from "../../../@core/data/services/renovaciones/cliente-renova.service";
 import {CrearClienteEmisionComponent} from '../modals/crear-cliente-emision/crear-cliente-emision.component';
 import {DetallesClienteComponent} from '../modals/detalles-cliente/detalles-cliente.component';
import {ClienteEmision} from '../../../@core/data/interfaces/emisiones/cliente-emision';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.scss'],
})
export class ClientesComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  cols = ['detalles', 'curp', 'rfc', 'nombre', 'telefono', 'acciones'];
  dataSource: MatTableDataSource<ClienteEmision>;
  filterValues = {};
  filterSelectObj = [];
  data: any;
  permisos = JSON.parse(window.localStorage.getItem('User'));
  constructor(
    private clienteEmisionService: ClienteRenovaService,
    private snackBar: MatSnackBar,
    private filtrosTablasService: FiltrosTablasService,
    private dialog: MatDialog) {
    this.filterSelectObj = this.filtrosTablasService.createFilterSelect([ 'curp', 'rfc', 'nombre', 'telefono']);
  }

  ngOnInit() {
    this.getClientes(false);
  }
  ngOnDestroy() {
    this.filtrosTablasService.filterSelect = [];
  }
  getClientes (reload: boolean) {
    this.clienteEmisionService.getClienteRenova().subscribe(
      data => {
        this.dataSource = new MatTableDataSource(data.reverse());
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.filterSelectObj.filter((o) => {
          o.options = this.filtrosTablasService.getFilterObject(this.data, o.columnProp);
        });
        this.dataSource.filterPredicate = this.filtrosTablasService.createFilter();
      },
      () => {
        this.snackBar.open('Hubo un error al actualizar los datos, inténtalo nuevamente', '',
          {duration: 2000, horizontalPosition: 'right', verticalPosition: 'top', panelClass: ['mat-toolbar', 'mat-accent']});
      },
      () => {
        if (reload) {
          this.snackBar.open('Datos actualizados con éxito', '',
            {duration: 2000, horizontalPosition: 'right', verticalPosition: 'top', panelClass: ['mat-toolbar', 'mat-primary']});
        }
      });
  }
  filterChange(filter, event) {
    this.filterValues[filter.columnProp] = event.value.trim().toLowerCase();
    this.dataSource.filter = JSON.stringify(this.filterValues);
  }


  // Lineas para los filtros
  resetFilters() {
    this.filterValues = {}
    this.filterSelectObj.forEach((value, key) => {
      value.modelValue = undefined;
    })
    this.dataSource.filter = "";
  }
   detallesCliente (cliente: ClienteEmision) {
   this.dialog.open(DetallesClienteComponent, {width: '450px', data: cliente});
   }
  //
  crearCliente () {
    this.dialog.open(CrearClienteEmisionComponent, { width: '450px'}).afterClosed().subscribe(clienteCreado => {
      if (clienteCreado)
        this.getClientes(false);
    });
   }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
