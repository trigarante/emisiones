import { Component, OnInit } from '@angular/core';
import {AplicacionView, AplicacionViewData} from '../../../@core/data/interfaces/finanzas/aplicacionView';
import {DatosCliente} from '../../../@core/data/interfaces/ventaNueva/detalles-poliza';
import {ClientesData, ClienteVn} from '../../../@core/data/interfaces/ventaNueva/cliente-vn';
import {ActivatedRoute} from '@angular/router';
import * as moment from 'moment';
import {EmisionesService} from '../../../@core/data/services/emisiones/emisiones.service';
import {NotificacionesService} from "../../../@core/data/services/others/notificaciones.service";
import {AplicacionViewService} from "../../../@core/data/services/finanzas/aplicacion-view.service";
import {DetallesPolizaService} from "../../../@core/data/services/ventaNueva/detalles-poliza.service";
import {ClienteVnService} from "../../../@core/data/services/ventaNueva/cliente-vn.service";

@Component({
  selector: 'app-detalle-poliza',
  templateUrl: './detalle-poliza.component.html',
  styleUrls: ['./detalle-poliza.component.scss'],
})
export class DetallePolizaComponent implements OnInit {
  idPoliza: number = this.rutaActiva.snapshot.params.idPoliza;
  detallesPoliza: any;
  datoCliente: DatosCliente;
  dCliente: ClienteVn;
  permisos = JSON.parse(window.localStorage.getItem('User'));

  constructor(
              private rutaActiva: ActivatedRoute,
              private emisionesService: EmisionesService,
              private detallesPolizaService: DetallesPolizaService,
              private notificacionesService: NotificacionesService,
              private clienteService:ClienteVnService,

              ) { }

  getById() {
    this.emisionesService.getDetallesPolizaEmisiones(this.idPoliza).subscribe({ next: result => {
        this.detallesPoliza = result;

        this.datoCliente = JSON.parse(this.detallesPoliza.datos);
        this.detallesPoliza.fechaRegistro = moment(this.detallesPoliza.fechaRegistro).format('DD/MM/YYYY');
        this.detallesPoliza.fechaInicio = moment(this.detallesPoliza.fechaInicio).format('DD/MM/YYYY');
      }, error: () => {

        this.notificacionesService.error('Favor de intentarlo mas tarde o contactar al área de TI')
      }, complete: () => {
        this.clienteService.getClienteByIdEmision(this.detallesPoliza.idCliente).subscribe(
          resp => {
            this.dCliente = resp;
          },
        );
      },
    });
    // this.detallesPolizaService.getAplicacionViewById(this.idPoliza).subscribe({ next: result => {
    //     this.detallesPoliza = result;
    //
    //     this.datoCliente = JSON.parse(this.detallesPoliza.datos);
    //     this.detallesPoliza.fechaRegistro = moment(this.detallesPoliza.fechaRegistro).format('DD/MM/YYYY');
    //     this.detallesPoliza.fechaInicio = moment(this.detallesPoliza.fechaInicio).format('DD/MM/YYYY');
    //   }, error: () => {
    //     Swal.fire({
    //       title: 'Hubo un error',
    //       text: 'Favor de intentarlo mas tarde o contactar al área de TI',
    //       type: 'error',
    //     });
    //   }, complete: () => {
    //     this.clienteService.getClienteByIdEmision(this.detallesPoliza.idCliente).subscribe(
    //       resp => {
    //         this.dCliente = resp;
    //       },
    //     );
    //   },
    // });
  }

  ngOnInit() {
    this.getById();
  }

  cerrarWindow() {
    window.close();
  }
}
