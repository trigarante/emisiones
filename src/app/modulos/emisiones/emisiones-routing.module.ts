import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {EmisionesComponent} from './emisiones.component';
 import {AdministracionPolizasComponent} from './administracion-polizas/administracion-polizas.component';
 import {DetallePolizaComponent} from './detalle-poliza/detalle-poliza.component';
//  import {StepCotizadorComponent} from './step-cotizador/step-cotizador.component';
// import {EmisionesProcesoComponent} from './emisiones-proceso/emisiones-proceso.component';
import {PendientesRenovarComponent} from './pendientes-renovar/pendientes-renovar.component';
 import {StepCotizadorEmisionesComponent} from './step-cotizador-emisiones/step-cotizador-emisiones.component';
 import {ClientesComponent} from './clientes/clientes.component';
// import {RegistroPolizaCliComponent} from './registro-poliza-cli/registro-poliza-cli.component';
// import {StepCotizadorOnlineComponent} from "../venta-nueva/steps-online/step-cotizador-online.component";
// import {PendientesGuard} from "../../@core/data/services/auth-varificaciones/pendientes.guard";
// import {StepOnlineComponent} from "./step-online/step-online.component";

const permisos: any = JSON.parse(window.localStorage.getItem('User'));
const routes: Routes = [
  {
    path: '',
    component: EmisionesComponent,
    children: [
      {
        path: 'administracion-polizas',
        component: AdministracionPolizasComponent,

      },
      {
        path: 'detalles-poliza-emision/:idPoliza',
        component: DetallePolizaComponent,
      },
      // {
      //   path: 'step-cotizador-emision',
      //   component: StepCotizadorComponent,
      // },
      {
        path: 'steps-cotizador-emisiones',
        component: StepCotizadorEmisionesComponent,
      },
      // {
      //   path: 'emisiones-proceso',
      //   component: EmisionesProcesoComponent,
      // },
      {
        path: 'pendientes-renovar',
        component: PendientesRenovarComponent,

      },
      {
        path: 'clientes',
        component: ClientesComponent,
      },
      // {
      //   path: 'registro-poliza-cli/:idCliente',
      //   component: RegistroPolizaCliComponent,
      // },
      // {
      //   path: 'step-online/:id_solicitud',
      //   component: StepOnlineComponent,
      //   // canActivate: [PendientesGuard],
      // },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmisionesRoutingModule { }
