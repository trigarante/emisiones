import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {NotificacionesService} from '../../../@core/data/services/others/notificaciones.service';
import {FiltrosTablasService} from '../../../@core/filtros-tablas.service';
import {GetTestDataService} from '../../../@core/data/services/get-test-data.service';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {FormControl} from '@angular/forms';
import {WebrtcService} from '../../../@core/data/services/telefonia/webrtc.service';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
// import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter} from '@angular/material-moment-adapter';
import * as _moment from 'moment';
import {Subscription} from 'rxjs';
import {default as _rollupMoment, Moment} from 'moment';
 import {AplicacionView} from '../../../@core/data/interfaces/finanzas/aplicacionView';
 import {EmisionesService} from '../../../@core/data/services/emisiones/emisiones.service';
// import {CargaExcelService} from "../../../@core/data/services/emisiones/carga-excel.service";
import {OptEstadosComponent} from '../modals/opt-estados/opt-estados.component';
// import {ClienteModalComponent} from '../modals/cliente-modal/cliente-modal.component';
// import { saveAs } from 'file-saver';
import {Router} from '@angular/router';
import Swal from "sweetalert2";
import {CargaExcelService} from "../../../@core/data/services/emisiones/carga-excel.service";
import {DomSanitizer} from '@angular/platform-browser';
// tslint:disable-next-line:no-duplicate-imports

const moment = _rollupMoment || _moment;
const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-administracion-polizas',
  templateUrl: './administracion-polizas.component.html',
  styleUrls: ['./administracion-polizas.component.scss'],
  providers: [
    // {provide: DateAdapter
    //   useClass: MomentDateAdapter,
    //   deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    // },

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class AdministracionPolizasComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  filterValues = {};
  filterSelectObj = [];
  dataSource: MatTableDataSource<AplicacionView>;
  loading: boolean;
  /** Arreglo para declarar las cabeceras de la tabla. */
  displayedColumns: string[]  = ['detalle', 'poliza', 'nombre', 'correo', 'empleado', 'socio', 'idSubarea', 'estado', 'telefonoFijo',
    'fechaInicio', 'numeroSerie', 'acciones'];

  rangeDates = [
    new Date(new Date().setDate(new Date().getDate() - 15)),
    new Date(new Date().setDate(new Date().getDate() + 1)),
  ];
  date = new FormControl(moment());
  date2 = new FormControl(moment());
  panelOpenState = false;
  permisos = JSON.parse(window.localStorage.getItem('User'));
  activo: boolean;
  filesToUpload: FileList;
  link: string;
  webRtcEvents: Subscription;
  private data: any;
  errPolizas: any;
  fileUrl: any;
  error = false;


  constructor(public dialog: MatDialog,
              private snackBar: MatSnackBar,
              private emisionesService: EmisionesService,
              private notificacionesService: NotificacionesService,
              private filtrosTablasService: FiltrosTablasService,
              private testDataService: GetTestDataService,
              private cargaExcelService: CargaExcelService,
              private router: Router,
              private notificaciones: NotificacionesService,
              private sanitizer: DomSanitizer)
  {
    this.loading = true;
    // this.link = 'https://docs.google.com/spreadsheets/d/19jRHc0ymWDmkdnRFeAN2rVwwt7zl9QXDcQXj1xEtQoc/edit?usp=sharing';
    // plantilla con tipificacion
    // this.link = 'https://docs.google.com/spreadsheets/d/1W6jJVXg1ExsJoFNcRqdxX6-KpJRV5-zF/edit?usp=sharing&ouid=100027419103988686740&rtpof=true&sd=true';
    // plantilla con prima total
    // this.link = 'https://docs.google.com/spreadsheets/d/1XkP6iZeHAQN0GxaGXfFYi7xkVZvyDlBi/edit?usp=sharing&ouid=100027419103988686740&rtpof=true&sd=true';
    this.link = 'https://docs.google.com/spreadsheets/d/1CZiySx1M_DOZqvYNTXjM9yjE-IsTlhVAuqd4wYxd7bY/edit?usp=sharing';
  }
  ngOnInit() {

  }

  ngOnDestroy() {
    this.filtrosTablasService.filterSelect = [];
    if (this.webRtcEvents) { this.webRtcEvents.unsubscribe(); }
  }

  chosenYearHandler(normalizedYear: Moment) {
    this.date.setValue(normalizedYear);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: any) {
    this.date.setValue(normalizedMonth);
    datepicker.close();
  }



  almacenarExcel() {
    this.filesToUpload = (<HTMLInputElement>event.target).files;
    const extensionesDeArchivoAceptadas = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    const extensionValida: Boolean = extensionesDeArchivoAceptadas.includes(this.filesToUpload.item(0).type);

    Swal.fire({
      title: 'Se está subiendo el archivo',
      allowOutsideClick: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });

    if (extensionValida) {
      // this.filesToUpload = (<HTMLInputElement>event.target).files;
      this.cargaExcelService.postArchivoExcel(this.filesToUpload, this.filesToUpload[0].name).subscribe({
        next: value => {
          this.errPolizas = value;
        },
        error: err => {
          Swal.fire({
            icon: 'error',
            title: 'Algo salió mal...',
          });
          this.filesToUpload = null;
        },
        complete: () => {
          console.log(JSON.parse(this.errPolizas));
          if (JSON.parse(this.errPolizas).message !== 'error') {
            Swal.fire({
              icon: 'success',
              title: 'Archivo subido con éxito',
            });
          } else {
            this.error = true;
            const err = [];
            JSON.parse(this.errPolizas).data.forEach(datos => {
              err.push(datos.poliza);
            });
            const data = JSON.stringify(JSON.parse(this.errPolizas).data);
            const blob = new Blob([data], { type: 'application/json' });
            this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
            const polizas = `
            <p>Las siguientes pólizas no se registraron correctamente..</p>
            <section style="width: 100%; max-height: 80px; height: auto; text-align: left; overflow-y: auto; padding: 10px 15px;">
            <b>${err.toString().replace(',', ', ')}</b>
            </section>`;
            Swal.fire({
              icon: 'warning',
              allowOutsideClick: false,
              allowEscapeKey: false,
              allowEnterKey: false,
              title: '¡Atención!',
              text: 'Las siguientes pólizas no se registraron correctamente..',
              html: polizas,
            });
          }
          this.filesToUpload = null;
        },
      });
    } else {
      Swal.fire({
        icon: 'error',
        title: '¡Archivo no válido!',
      });
    }
   }
}
