import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-emisiones',
  template: `
      <router-outlet></router-outlet>
  `,
})
export class EmisionesComponent {}
