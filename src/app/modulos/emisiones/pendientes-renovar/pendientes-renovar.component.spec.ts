import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PendientesRenovarComponent } from './pendientes-renovar.component';

describe('PendientesRenovarComponent', () => {
  let component: PendientesRenovarComponent;
  let fixture: ComponentFixture<PendientesRenovarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PendientesRenovarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PendientesRenovarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
