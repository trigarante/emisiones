import {Component,  OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {NotificacionesService} from '../../../@core/data/services/others/notificaciones.service';
import {FiltrosTablasService} from '../../../@core/filtros-tablas.service';
import {GetTestDataService} from '../../../@core/data/services/get-test-data.service';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {FormControl} from '@angular/forms';
import { EmisionesViews} from '../../../@core/data/interfaces/emisiones/emisiones';

import {WebrtcService} from '../../../@core/data/services/telefonia/webrtc.service';
// import {SubirArchivoClienteRenoComponent} from '../../renovaciones/modals/subir-archivo-cliente-reno/subir-archivo-cliente-reno.component';
 import {RegistroEmisionesComponent} from '../modals/registro-emisiones/registro-emisiones.component';
import {MatDatepicker} from '@angular/material/datepicker';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter} from '@angular/material-moment-adapter';
import * as _moment from 'moment';
import {default as _rollupMoment, Moment} from 'moment';
import {FormBuilder,  FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AplicacionViewService} from "../../../@core/data/services/finanzas/aplicacion-view.service";
import {EmisionesService} from "../../../@core/data/services/emisiones/emisiones.service";
import {AplicacionView} from "../../../@core/data/interfaces/finanzas/aplicacionView";
import {map} from "rxjs/operators";
import {D} from "@angular/cdk/keycodes";
import {XlsxService} from "../../../@core/data/services/reportes/xlsx.service";

const moment = _rollupMoment || _moment;
const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-pendientes-renovar',
  templateUrl: './pendientes-renovar.component.html',
  styleUrls: ['./pendientes-renovar.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class PendientesRenovarComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  emisiones: EmisionesViews[];
  permisos = JSON.parse(window.localStorage.getItem('User'));
  filterValues = {};
  filterSelectObj = [];
  dataSource: any;
  loading: boolean;

  displayedColumns: string[]  = ['detalle', 'poliza', 'idEmpleadoRenovacion', 'polizaVieja', 'primaNeta', 'primaNetaAnterior', 'primaTotal', 'porcentaje',
    'fechaInicio', 'tipoPago', 'estadoRenovacion', 'nombreComercial', 'tipificacion', 'etiqueta' , 'acciones'];
  idRecibo: number;
  rangeDates =  [
    new Date(new Date().setDate(new Date().getDate() - 15)),
    new Date(new Date().setDate(new Date().getDate() + 1)),
  ];
  max: any = new Date();
  cantidadPorciento: number;
  primaNeta: number;
  primaNetaAnterior: number;
  date = new FormControl(moment());
  date2 = new FormControl(moment());
  coloresForm: FormGroup;
  colores = [
    {color: 'DECREMENTO DE LA PRIMA NETA', valor: 1, cc: '#1b5900'},
    {color: 'INCREMENTO HASTA 10%', valor: 2, cc: '#d32f2f'},
    {color: 'INCREMENTO MAS DEL 10%', valor: 3, cc: '#111218'},
  ];

  activo: boolean;
  private webRtcEvents: any;
  private data: any;


  constructor(
    private fb: FormBuilder,
    private xlsxService: XlsxService,
    private administracionPolizaService: AplicacionViewService,
    private emisionesService: EmisionesService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private notificacionesService: NotificacionesService,
    private filtrosTablasService: FiltrosTablasService,
    private testDataService: GetTestDataService,
    private router: Router,
    private webRTCService: WebrtcService) {
    this.filterSelectObj = this.filtrosTablasService.createFilterSelect([{id: 'nombreCliente' , nombre: 'CLIENTE'},
      {id: 'nombreEmpleadoRenovaciones' , nombre: 'EMPLEADO'},
      {id: 'correo' , nombre: 'CORREO'},
      {id: 'telefonoMovil' , nombre: 'TELEFONO'},
      {id: 'poliza' , nombre: 'POLIZA'},
      {id: 'polizaVieja' , nombre: 'POLIZA VIEJA'},
      {id: 'primaNeta' , nombre: 'PRIMA'},
      {id: 'primaNetaAnterior' , nombre: 'PRIMA ANTERIOR'},
      {id: 'variacion' , nombre: 'VARIACION'},
      {id: 'tipoPago' , nombre: 'TIPO PAGO'},
      {id: 'estadoRenovacion' , nombre: 'ESTADO'},
      {id: 'nombreComercial' , nombre: 'SOCIO'},]);
    this.loading = true;
  }

  ngOnInit() {
    this.getEmisiones(false);
    this.date.setValue('');
    this.date2.setValue('');
    this.coloresForm = this.fb.group({
      'colores': new FormControl(''),
    });
  }
  ngOnDestroy() {
    this.filtrosTablasService.filterSelect = [];
    if (this.webRtcEvents) { this.webRtcEvents.unsubscribe(); }
  }

  chosenYearHandler(normalizedYear: Moment) {
    this.date.setValue(normalizedYear);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: any) {
    this.date.setValue(normalizedMonth);
    datepicker.close();
  }

  chosenYearHandler2(normalizedYear2: Moment) {
    this.date2.setValue(normalizedYear2);
  }

  chosenMonthHandler2(normalizedMonth2: Moment, datepicker: any) {
    this.date2.setValue(normalizedMonth2);
    datepicker.close();
  }

  detallePoliza(idRegistro) {
    window.open('/modulos/emisiones/detalles-poliza-emision' + '/' + idRegistro);
  }

  filtroFecha() {
    // console.log(moment(this.date.value).format('MM-DD-YYYY'));
    if (this.date.value !== '' && this.date2.value !== '') {
      // this.emisionesService.getFiltroFechaARenovar(moment(this.date.value).format(), moment(this.date2.value).format())
      this.emisionesService.getFiltroFechaARenovar(moment(this.date.value).format(), moment(this.date2.value).format()).pipe(map(res => {
        return res.filter(other => other.idEstadoRenovacion !== 2);
      })).subscribe(polizas => {
        this.emisiones = polizas;
        this.dataSource = new MatTableDataSource(); //
        this.dataSource.sort = this.sort; //
        this.dataSource.paginator = this.paginator; //

        if (this.dataSource.data.length === 0) {
          // Swal.fire({
          //   type: 'info',
          //   title: 'Oops, no tienes registros por mostrar',
          // });
        }
      }, () => {
        this.snackBar.open('Hubo un error al cargar la información', '',
          {duration: 2000, horizontalPosition: 'right', verticalPosition: 'top', panelClass: ['mat-toolbar', 'mat-warn']});
      }, () => {
        // @ts-ignore
        if (this.emisiones.length !== 0) {
          this.snackBar.open('Datos actualizados con éxito', '',
            {duration: 2000, horizontalPosition: 'right', verticalPosition: 'top', panelClass: ['mat-toolbar', 'mat-primary']});
        }
        this.porcentaje();
      });
    }
  }

  getFiltroColores (valor) {
    this.emisionesService.getFiltroColores(valor).subscribe(result => {
      this.emisiones = result;
      this.dataSource = new MatTableDataSource(); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //

      if (this.dataSource.data.length === 0) {
        // Swal.fire({
        //   type: 'info',
        //   title: 'Oops, no tienes registros por mostrar',
        // });
      }
    }, () => {
      this.snackBar.open('Hubo un error al cargar la información', '',
        {duration: 2000, horizontalPosition: 'right', verticalPosition: 'top', panelClass: ['mat-toolbar', 'mat-warn']});
    }, () => {
      // @ts-ignore
      if (this.emisiones.length !== 0) {
        this.snackBar.open('Datos actualizados con éxito', '',
          {duration: 2000, horizontalPosition: 'right', verticalPosition: 'top', panelClass: ['mat-toolbar', 'mat-primary']});
      }
    });
  }

  getEmisiones(reload: boolean) {
    const hoy = moment();
    const fechaInicio = (this.rangeDates[0].toISOString().split('T')[0]);
    const fechaFin = (this.rangeDates[1].toISOString().split('T')[0]);
    this.emisionesService.getAllPendientes(fechaInicio,fechaFin).subscribe(polizas => {
      this.emisiones = polizas;
      this.dataSource = new MatTableDataSource(this.emisiones); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
      this.filterSelectObj.filter((o) => {
        o.options = this.filtrosTablasService.getFilterObject(polizas, o.columnProp);
      });
      this.dataSource.filterPredicate = this.filtrosTablasService.createFilter();

      if (this.dataSource.data.length === 0) {
        // Swal.fire({
        //   type: 'info',
        //   title: 'Oops, no tienes registros por mostrar',
        // });
      }
    }, () => {
      this.snackBar.open('Hubo un error al cargar la información', '',
        {duration: 2000, horizontalPosition: 'right', verticalPosition: 'top', panelClass: ['mat-toolbar', 'mat-warn']});
    }, () => {
      if (reload && this.emisiones.length !== 0) {
        this.coloresForm.controls['colores'].setValue('');
        this.snackBar.open('Datos actualizados con éxito', '',
          {duration: 2000, horizontalPosition: 'right', verticalPosition: 'top', panelClass: ['mat-toolbar', 'mat-primary']});
      }
      this.porcentaje();
    });
  }
  filterChange(filter, event) {
    this.filterValues[filter.columnProp] = event.value.trim().toLowerCase();
    this.dataSource.filter = JSON.stringify(this.filterValues);
  }


  // Lineas para los filtros
  resetFilters() {
    this.filterValues = {}
    this.filterSelectObj.forEach((value, key) => {
      value.modelValue = undefined;
    })
    this.dataSource.filter = "";
  }


  registroEmisiones(element) {
    this.dialog.open(RegistroEmisionesComponent, {
      // width: '500px',
      data: element,
    });
  }

  subirArchivo(idCliente, idRegistro) {
    // this.dialog.open(SubirArchivoClienteRenoComponent, {
    //   data: {
    //     idCliente: idCliente,
    //     idRegistro: idRegistro,
    //   },
    // }).afterClosed().subscribe( reload => {
    //   if (reload) {
    //     // if (this.idTipo === 1) { this.getEmisiones(false); } else { this.getEmisiones(false); }
    //     this.getEmisiones(reload);
    //   }
    // } );
  }

  porcentaje() {
    // @ts-ignore
    for (let i = 0; i < this.emisiones.length; i++) {
      this.primaNeta = this.emisiones[i].primaNeta;
      this.primaNetaAnterior = this.emisiones[i].primaNetaAnterior;
      this.cantidadPorciento = ((this.primaNeta * 100) / (this.primaNetaAnterior)) - 100;
      this.cantidadPorciento = Number(this.cantidadPorciento.toFixed(2));
    }
  }
  descargar() {

      const columns = ['poliza', 'idEmpleadoRenovacion', 'polizaVieja', 'primaNeta', 'primaNetaAnterior', 'variacion',
        'fechaInicio', 'tipoPago', 'estadoRenovacion', 'nombreComercial'];
      this.xlsxService.imprimirXLS(this.emisiones, columns, 'Dashboard');
  }

}
