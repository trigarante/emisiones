import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatStepper} from '@angular/material/stepper';
import {EmisionesViews} from '../../../@core/data/interfaces/emisiones/emisiones';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-step-cotizador-emisiones',
  templateUrl: './step-cotizador-emisiones.component.html',
  styleUrls: ['./step-cotizador-emisiones.component.scss'],
})
export class StepCotizadorEmisionesComponent implements OnInit, OnDestroy {
  @ViewChild('stepper') stepper: MatStepper;
  recibos: boolean;
  // Variables para el step de cliente
  // idCliente: number;
  idCliente: number = null;
  permisos = JSON.parse(window.localStorage.getItem('User'));
  // Variables para el step de producto cliente
  idProductoCliente: number = undefined;
  // Variables para el step de registro poliza
  idRegistro: number = undefined;
  stepsIdentificados: boolean = false;
  datos: any;

  idClienteE: number = 0;
  idProductoClienteE: number = 0;
  idRegistroE: number = 0;
  idRecibosE: number = 0;
  idPagosE: number = 0;
  actRecibos = false;
  pagoFlag: boolean;

  constructor(private route: ActivatedRoute, private router: Router,
              private _formBuilder: FormBuilder) {}

  ngOnInit() {
    // this.route.queryParams.subscribe(params => {
    //   if (params['idCliente'])
    //     this._idCliente = params['idCliente'];
    // });
    // this.getEmision();
    this.getStepActual();
  }

  ngOnDestroy() {
    window.sessionStorage.removeItem('idCliente');
    window.sessionStorage.removeItem('idProductoCliente');
    window.sessionStorage.removeItem('idRegistro');
    window.sessionStorage.removeItem('idPagos');
    window.sessionStorage.removeItem('idRecibos');
  }

  activarProductoCliente(value: any) {
    this.idProductoClienteE = 1;
    this.idCliente = value.idCliente;
    this.stepper.next();
  }

  prodCliente() {
    this.stepper.next();
  }

  nextSteper(num: number) {
    this.idRecibosE = 1;
    this.stepper.next();
  }

  stepInspeccion() {
    this.stepper.selectedIndex = 5;
  }

  activarRegistroPoliza(value: number) {
    this.idRegistroE = 1;
    this.idProductoCliente = value;
  }

  activarRecibos(idRegistro: number) {
    this.idRegistroE = idRegistro;
    this.idRegistroE = Number(window.sessionStorage.getItem('idRegistro'));
    this.idRegistro = +window.sessionStorage.getItem('idRegistro');
    if (idRegistro === Number(window.sessionStorage.getItem('idRegistro'))) {
      this.actRecibos = true;
      this.idRecibosE = 1;
      this.stepper.next();
    }
  }

  ultimoStep(step: number) {
    switch (step) {
      case 1 :
        this.stepper.selectedIndex = 4;
        break;
      case 2:
        this.stepper.selectedIndex = 5;
        break;
    }

  }

  getStepActual() {
    this.idClienteE = Number(window.sessionStorage.getItem('idCliente'));
    this.idProductoClienteE = Number(window.sessionStorage.getItem('idProductoCliente'));
    this.idRegistroE = Number(window.sessionStorage.getItem('idRegistro'));
    this.idRecibosE = Number(window.sessionStorage.getItem('idRecibos'));
    this.idPagosE = Number(window.sessionStorage.getItem('idPagos'));
    this.idCliente = +window.sessionStorage.getItem('idCliente');
    this.idProductoCliente = +window.sessionStorage.getItem('idProductoCliente');
    this.idRegistro = +window.sessionStorage.getItem('idRegistro');

    if (this.idProductoCliente) {
      this.idRegistroE === 0 ? this.stepper.selectedIndex = 2 : this.stepper.selectedIndex = 3;
      if (this.idRecibosE !== 0) {
        this.pagoFlag = true;
      }
    } else {
      this.pagoFlag = false;
      /*if (this.idProductoCliente === 0 && this.idClienteE === 0) {
        this.stepper.selectedIndex = 0;
      } else {
        this.idProductoClienteE = 1;
        this.stepper.selectedIndex = 1;
      }*/
    }

    if (this.pagoFlag) {
      this.idPagosE === 0 ? this.stepper.selectedIndex = 4 : this.stepper.selectedIndex = 5;
    }
  }


}
