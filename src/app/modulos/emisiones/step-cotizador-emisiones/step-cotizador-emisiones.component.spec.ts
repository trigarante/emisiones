import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StepCotizadorEmisionesComponent } from './step-cotizador-emisiones.component';

describe('StepCotizadorEmisionesComponent', () => {
  let component: StepCotizadorEmisionesComponent;
  let fixture: ComponentFixture<StepCotizadorEmisionesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StepCotizadorEmisionesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StepCotizadorEmisionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
