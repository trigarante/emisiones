import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroEmisionesComponent } from './registro-emisiones.component';

describe('RegistroEmisionesComponent', () => {
  let component: RegistroEmisionesComponent;
  let fixture: ComponentFixture<RegistroEmisionesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistroEmisionesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroEmisionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
