import {Component, Inject, OnInit} from '@angular/core';
import {Registro, RegistrosData} from '../../../../@core/data/interfaces/ventaNueva/registro';
import {FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {TipoPagoData, TipoPagoVn} from '../../../../@core/data/interfaces/ventaNueva/catalogos/tipo-pago-vn';
import {Periodicidad} from '../../../../@core/data/interfaces/ventaNueva/catalogos/periodicidad';
import {SociosComercial} from '../../../../@core/data/interfaces/comerciales/socios-comercial';
import {Ramo} from '../../../../@core/data/interfaces/comerciales/ramo';
import {SubRamo} from '../../../../@core/data/interfaces/comerciales/sub-ramo';
import { ProductoSocios} from '../../../../@core/data/interfaces/comerciales/producto-socios';
import * as moment from 'moment';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Emisiones, EmisionesData} from '../../../../@core/data/interfaces/emisiones/emisiones';
import {
  EstadoPolizaVn,
  EstadoPolizaVnData,
} from '../../../../@core/data/interfaces/ventaNueva/catalogos/estado-poliza-vn';
import {ProductoClienteData} from '../../../../@core/data/interfaces/ventaNueva/producto-cliente';
import {RecibosData} from '../../../../@core/data/interfaces/ventaNueva/recibos';
import {PeriodicidadService} from '../../../../@core/data/services/ventaNueva/catalogos/periodicidad.service';
import {map} from 'rxjs/operators';
import Swal from 'sweetalert2';
import {EmisionesService} from '../../../../@core/data/services/emisiones/emisiones.service';
import {TipoSubramoService} from "../../../../@core/data/services/comerciales/catalogo/tipo-subramo.service";
import {SociosService} from "../../../../@core/data/services/comerciales/socios.service";
import {RamoService} from "../../../../@core/data/services/comerciales/ramo.service";
import {SubRamoService} from "../../../../@core/data/services/comerciales/sub-ramo.service";
import {ProductoSociosService} from "../../../../@core/data/services/comerciales/producto-socios.service";

@Component({
  selector: 'app-registro-emisiones',
  templateUrl: './registro-emisiones.component.html',
  styleUrls: ['./registro-emisiones.component.scss'],
})
export class RegistroEmisionesComponent implements OnInit {

  idEmision: number;
  idRegistro: number;
  registro: Registro;
  idRecibo: number;
  registroForm: FormGroup = new FormGroup({
    'idTipoPago': new FormControl('', Validators.required),
    'idSocio': new FormControl('', Validators.required),
    'idTipoBandejaRenovacion': new FormControl('', Validators.required),
    'idRamo': new FormControl('', Validators.required),
    'idSubRamo': new FormControl('', Validators.required),
    'idProductoSocio': new FormControl('', Validators.required),
    'poliza': new FormControl('', Validators.required),
    'fechaInicio': new FormControl('', Validators.required),
    'oficina': new FormControl(''),
    'primaNeta': new FormControl('', [Validators.required, Validators.pattern('[0-9.]+')]),
    // 'primaNetaAnterior': new FormControl(''),
    'estado': new FormControl(''),
    'primerPago': new FormControl('', [Validators.required, Validators.pattern('[0-9.]+')]),
    'periodo': new FormControl('', Validators.required),
  });
  numeroGuion: RegExp = /[Aa-zZ0-9-]/;
  desactivarBotones: boolean = false;
  tipoPagos: TipoPagoVn[];
  periodicidad: Periodicidad[];
  socios: SociosComercial[];
  socio: SociosComercial;
  ramos: Ramo[];
  subRamos: SubRamo[];
  productosSocio: ProductoSocios[];
  urlPreventa: any;
  date = moment();
  filesToUpload: FileList;
  archivoValido: boolean = false;
  numeroPagos: number;
  instrucciones = 'INTRODUCE LOS SIGUIENTES DATOS CORRECTAMENTE:';
  /** FUNCIONA CON EL LOADER DE LOS BOTONES QUE LLEVAN UNA PETICIÓN **/
  gruposCargados: boolean = false;

  btnLoad: boolean = false;
  cantidad: number;
  bandejas: any[];
  cantidadPorciento: number;
  estados = [{id: 1, estado: 'ACTIVA'}, {id: 2, estado: 'CANCELADA'}];
  emisiones: any;
  dataAll = [];
  estadoPoliza: EstadoPolizaVn[];
  loader = true;
  loaderR = true;
  loaderSR = true;
  loaderPS = true;
  loaderBandeja = true;
  loaderButton = false;

  constructor(
    public dialogRef: MatDialogRef<RegistroEmisionesComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private emisionesService: EmisionesService,
    private subramoServices: TipoSubramoService,
    private productoSocioService: ProductoSociosService,
    private tipoPagoService: TipoPagoData,
    private estadoPolizaService: EstadoPolizaVnData,
    private registroService: RegistrosData,
    private productoClienteService: ProductoClienteData,
    private recibosService: RecibosData,
    private sociosService: SociosService,
    private ramoService: RamoService,
    private subRamoService: SubRamoService,

    private periodicidadService: PeriodicidadService) {
  }

  ngOnInit() {
    this.productoClienteService.returnAseguradora().subscribe(
      result => {
        this.urlPreventa = result;
      });
    this.getTipoPago();
    this.getPeriodicidad();
    this.getSocios();
    this.getBandejas();
    this.getRegistroPoliza();
    this.getActivoCancelado();
  }

  getActivoCancelado() {
    this.estadoPolizaService.get().pipe(map( data => {
      return data.filter(da => da.id === 21 || da.id === 9);
    })).subscribe({
      next: data => {
        this.estadoPoliza = data;
      },
    });
  }

  getTipoPago() {
    this.tipoPagoService.getAllReno().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.tipoPagos = result;
    });
  }

  getPeriodicidad() {
    this.periodicidadService.get().subscribe(res => {
      this.periodicidad = res;
    });
  }

  getPeriodicidadFilter() {
    this.periodicidad = this.periodicidad.filter(perio => perio.id !== 1);
    this.registroForm.controls['periodo'].setValue('');
  }

  getSocios() {
    this.sociosService.get().pipe(map(data => {
      return data.filter(res => res.activo === 1);
    })).subscribe( data => {
      this.socios = data;
    });
    // this.sociosService.get().pipe(map(data => {
    //   return data.filter(result => result.activo === 1);
    // })).subscribe( data => {this.socios = data; } );
  }

  getBandejas() {
    this.emisionesService.getAllBandejas().subscribe(data => {
      this.bandejas = data;
    });
  }

  getRamo(idSocio: number, idRamo?: number) {
    this.socio = this.socios.filter( socios => socios.id === this.registroForm.controls.idSocio.value)[0];

    if (!this.socio.expresionRegular) {
      this.registroForm.controls.poliza.setValidators(Validators.required);
    } else {
      if (this.socio.id !== 2 && this.socio.id !== 8) {
        this.registroForm.controls.poliza.setValidators(Validators.compose(
          [Validators.required, Validators.pattern(`${this.socio.expresionRegular}`)]));
      } else {
        const arrExp = this.socio.expresionRegular.split(',');
        const expresionesRegulares =
          this.socio.id === 2 ? `${arrExp[0]}|${arrExp[1].trim()}|${arrExp[2].trim()}` : `${arrExp[0]}|${arrExp[1].trim()}`;
        this.registroForm.controls.poliza.setValidators(Validators.compose(
          [Validators.required, Validators.pattern(expresionesRegulares)]));
      }
    }

    this.registroForm.controls.poliza.updateValueAndValidity();
    if (idRamo === undefined) {
      this.registroForm.controls['idRamo'].setValue('');
      this.registroForm.controls['idSubRamo'].setValue('');
      this.registroForm.controls['idProductoSocio'].setValue('');
      this.ramos = [];
      this.subRamos = [];
      this.productosSocio = [];
    }

    this.ramoService.getRamoByIdSocio(idSocio).subscribe( {next: data => {
        this.ramos = data;
        if (this.ramos.length === 0) {
          this.mensajeErrorCatalogos('ramos');
        }
      }, complete: () => {
        if (idRamo !== undefined) {
          this.registroForm.controls['idRamo'].setValue(idRamo);
          this.loaderR = false;
        }
      }});
  }

  getSubRamo(idRamo: number, idSubRamo?: number) {
    if (idSubRamo === undefined) {
      this.registroForm.controls['idSubRamo'].setValue('');
      this.registroForm.controls['idProductoSocio'].setValue('');
      this.subRamos = [];
      this.productosSocio = [];
    }

    this.subRamoService.getSubramosByIdRamo(idRamo).subscribe( {next: data => {
        this.subRamos = data;
        if (this.subRamos.length === 0) {
          this.mensajeErrorCatalogos('subramos');
        }
      }, complete: () => {
        if (idSubRamo !== undefined) {
          this.registroForm.controls['idSubRamo'].setValue(idSubRamo);
          this.loaderSR = false;
        }
      }});
  }

  getProductoSocio(subRamo: number, idProductoSocio?: number) {
    if (idProductoSocio === undefined) {
      this.registroForm.controls['idProductoSocio'].setValue('');
      this.productosSocio = [];
    }

    this.productoSocioService.getProductoSocioByIdSubRamo(subRamo).subscribe({next: result => {
        this.productosSocio = result;
        if (this.productosSocio.length === 0) {
          this.mensajeErrorCatalogos('productos socio');
        }
      }, complete: () => {
        if (idProductoSocio !== undefined) {
          this.registroForm.controls['idProductoSocio'].setValue(idProductoSocio);
          this.loaderPS = false;
        }
      }});
  }

  mensajeErrorCatalogos(catalogo: string) {
    // Swal.fire({
    //   title: 'Sin elementos disponibles',
    //   text: '¡Por favor agregue ' + catalogo + ' en el catálogo correspondiente para poder continuar!',
    //   type: 'error',
    // });
  }

  getRegistroPoliza() {
    this.emisionesService.getRegistroEmision(this.data.id, this.data.idRegistro).subscribe({
      next: value => {
        this.dataAll = value;
        this.registroForm.controls['idTipoPago'].setValue(this.dataAll[1].idTipoPago);
        this.registroForm.controls['idSocio'].setValue(this.dataAll[1].idSocio);
        this.registroForm.controls['idProductoSocio'].setValue(this.dataAll[1].idProductoSocio);
        this.registroForm.controls['poliza'].setValue(this.dataAll[1].poliza);
        this.registroForm.controls['idProductoSocio'].setValue(this.dataAll[1].idProductoSocio);
        this.registroForm.controls['idTipoBandejaRenovacion'].setValue(this.dataAll[1].idTipoBandejaRenovacion);
        this.registroForm.controls['fechaInicio'].setValue(this.dataAll[1].fechaInicio);
        this.registroForm.controls['primaNeta'].setValue(this.dataAll[1].primaNeta);
        this.registroForm.controls['oficina'].setValue(this.dataAll[1].oficina);
        this.registroForm.controls['estado'].setValue(this.dataAll[1].idEstadoPoliza);
        this.registroForm.controls['periodo'].setValue(this.dataAll[1].idPeriodicidad);
        this.registroForm.controls['primaNeta'].setValue(this.dataAll[1].primaNeta);
        this.registroForm.controls['primerPago'].setValue(this.dataAll[1].primerPago);
        this.getRamo(this.dataAll[1].idSocio, this.dataAll[0].idRamo);
        this.getSubRamo(this.dataAll[0].idRamo, this.dataAll[0].idSubramo);
        this.getProductoSocio(this.dataAll[0].idSubramo, this.dataAll[0].idProductoSocio);
        this.registroForm.controls['idTipoPago'].value !== 1 ? this.setMontoSubsecuente() : false;
        if (this.dataAll[0].archivo.length > 3) {
          this.instrucciones = 'YA NO PUEDES EDITAR LAS OPCIONES INGRESADAS';
          // this.desactivarBotones = true;
          // this.getMontoPagos();
        }
      },
      complete: () => {
        this.loader = false;
        this.loaderBandeja = false;
      },
    });
  }

  setMontoSubsecuente() {
    this.registroForm.addControl('pagoMensual', new FormControl('', [Validators.required, Validators.pattern('[0-9]+')]));
    this.registroForm.controls['pagoMensual'].setValue(this.dataAll[1].montoSubsecuente);
  }

  getNumeroPagos(): number {
    if (this.tipoPagos !== undefined && this.registroForm.controls['idTipoPago'].valid) {
      this.numeroPagos = this.tipoPagos.find(arr => arr.id === this.registroForm.controls['idTipoPago'].value).cantidadPagos;
      if (this.numeroPagos > 1) {
        this.registroForm.addControl('pagoMensual', new FormControl('',
          [Validators.required, Validators.pattern('[0-9.]+')], this.setErrorMensual.bind(this)));
      } else {
        if (this.registroForm.contains('pagoMensual')) {
          this.registroForm.removeControl('pagoMensual');
        }
      }
      return this.numeroPagos;
    } else {
      return 0;
    }
  }

  getMontoPagos() {
    this.recibosService.getRecibosByIdRegistro(this.data.idRegistro).subscribe( data => {
      if (data && data.length > 0) {
        if (data.length > 1) {
          this.registroForm.addControl('pagoMensual', new FormControl('', [Validators.required, Validators.pattern('[0-9]+')]));
          this.registroForm.controls['pagoMensual'].setValue(this.dataAll[1].montoSubsecuente);
        }
      }
    });
  }

  modificarDatosRegistro() {
    this.loaderButton = true;
    const tipoPago = this.registroForm.controls['idTipoPago'].value;
    const datosRegistro: any = {
      ...this.registroForm.value,
      'idRegistro': this.dataAll[1].idRegistro,
      'idEstadoPoliza':  this.registroForm.controls['estado'].value,
      'idPeriodicidad':  this.registroForm.controls['periodo'].value,
      'montoSubsecuente':  tipoPago !== 1 ? this.registroForm.controls['pagoMensual'].value : null,
    };
    this.emisionesService.putDatosRegistroEmisiones(this.dataAll[0].id, datosRegistro).subscribe({
      next: value => {},
      error: err => {
        this.loaderButton = false;
      },
      complete: () => {
      //   Swal.fire({
      //     title: 'Datos actualizados',
      //     text: '¡Los datos han sido subidos exitosamente!',
      //     type: 'success',
      //   })
      // .then(() => {
      //     this.dialogRef.close();
      //   });
      //   this.loaderButton = false;
       },
    });
  }

  // subirADrive (tipoArchivo: string, idRegistroPoliza: number) {
  //   let idCarpeta: string;
  //   Swal.fire({
  //     title: 'Subiendo la poliza ingresada',
  //     allowOutsideClick: false,
  //     onBeforeOpen: () => {
  //       Swal.showLoading();
  //     },
  //   });
  //   this.driveService.post(this.filesToUpload, tipoArchivo, this.registro.idCliente, idRegistroPoliza).subscribe({
  //     next: result => {
  //       idCarpeta = result;
  //     }, error: error => {
  //       let texto: string;
  //
  //       if (error.status !== 417 || error.status !== 400) {
  //         texto = 'Hubo un error, favor de contactar al departamento de TI o intenta volver a guardar la información';
  //       } else {
  //         texto = error.error;
  //       }
  //       Swal.fire({
  //         title: 'Error',
  //         text: texto,
  //         type: 'error',
  //       });
  //     }, complete: () => {
  //       this.registroService.updateArchivo(idRegistroPoliza, idCarpeta).subscribe({
  //         complete: () => {
  //           this.desactivarBotones = true;
  //           // this.crearRecibo(idRegistroPoliza);
  //         },
  //       });
  //     }});
  // }

  almacenarArchivo() {
    const extensionesDeArchivoAceptadas = ['application/pdf'];
    this.filesToUpload = (<HTMLInputElement>event.target).files;
    const extensionValida: Boolean = extensionesDeArchivoAceptadas.includes(this.filesToUpload.item(0).type);
    const tamanioArchivo = this.filesToUpload.item(0).size * .000001;
    if (this.filesToUpload.length === 1) {
      // El servicio en Spring solo acepta a lo más 7 MB para subir archivos
      if (extensionValida && tamanioArchivo < 7) {
        // Swal.fire({
        //   title: 'Archivos listos',
        //   text: '¡El archivo está listo para ser guardado!',
        //   type: 'success',
        // });
        this.archivoValido = true;
      } else {
        let titulo: string;
        let texto: string;

        if (!extensionValida) {
          titulo = 'Extensión no soportada';
          texto = 'Solo puedes subir archivos pdf';
        } else {
          titulo = 'Archivo demasiado grande';
          texto = 'Los archivos que subas deben pesar menos de 7 MB';
        }

        // Swal.fire({
        //   title: titulo,
        //   text: texto,
        //   type: 'error',
        // });
      }
    }
  }

  validarCampos(): boolean {
    if (this.desactivarBotones) {
      return true;
    } else {
      if (this.registroForm.valid && this.archivoValido) {
        return false;
      } else {
        return true;
      }
    }
  }

  setErrorMensual(): Promise<ValidationErrors | null> {
    return new Promise(() => {
      if (Number(this.registroForm.controls['primerPago'].value) >= Number(this.registroForm.controls['pagoMensual'].value)) {
        this.registroForm.controls['pagoMensual'].setErrors(null);
      } else {
        this.registroForm.controls['pagoMensual'].setErrors({'incorrect': true});
      }
    });
  }

}
