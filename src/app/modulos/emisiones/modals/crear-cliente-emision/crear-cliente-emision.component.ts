import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSlideToggle} from '@angular/material/slide-toggle';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';
import {ClienteRenovaService} from '../../../../@core/data/services/renovaciones/cliente-renova.service';

@Component({
  selector: 'app-crear-cliente-emision',
  templateUrl: './crear-cliente-emision.component.html',
  styleUrls: ['./crear-cliente-emision.component.scss'],
})
export class CrearClienteEmisionComponent implements OnInit {

  @ViewChild(MatSlideToggle) toogle: MatSlideToggle;
  textoToogle: string = 'Activa si el cliente es MORAL';
  clienteForm = new FormGroup({
    nombre: new FormControl('', Validators.compose([Validators.required,
      Validators.pattern('[A-Z ]+')])),
    paterno: new FormControl('', Validators.compose([Validators.required,
      Validators.pattern('[A-Z ]+')])),
    materno: new FormControl('', Validators.compose([Validators.required,
      Validators.pattern('[A-Z ]+')])),
    calle: new FormControl('POR CORREGIR'),
    numExt: new FormControl('POR CORREGIR'),
    telefonoMovil: new FormControl('POR CORREGIR'),
    correo: new FormControl('POR CORREGIR'),
    idPais: new FormControl(1),
    razonSocial: new FormControl(1),
  });

  constructor(public dialogRef: MatDialogRef<CrearClienteEmisionComponent>,
              private clienteEmisionService: ClienteRenovaService,
              private router: Router) { }

  ngOnInit() {
  }

  cambioTipoCliente() {
    if (this.toogle.checked) {
      this.textoToogle = 'Desactiva si el cliente es FÍSICO';
      this.clienteForm.controls['razonSocial'].setValue(2);
      this.clienteForm.controls['paterno'].clearValidators();
      this.clienteForm.controls['materno'].clearValidators();
    } else {
      this.textoToogle = 'Activa si el cliente es MORAL';
      this.clienteForm.controls['razonSocial'].setValue(1);
      this.clienteForm.controls['paterno'].setValidators([Validators.required, Validators.pattern('[A-Z ]+')]);
      this.clienteForm.controls['materno'].setValidators([Validators.required, Validators.pattern('[A-Z ]+')]);
    }
    this.clienteForm.controls['paterno'].updateValueAndValidity();
    this.clienteForm.controls['materno'].updateValueAndValidity();
  }

  crearCliente () {
    let idCliente: number;
    Swal.fire({
      title: 'Se está creando el cliente',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });

    this.clienteEmisionService.postClienteEmision(this.clienteForm.value).subscribe(
      data => { idCliente = data.id; },
      () => {
        // Swal.fire({
        //   title: 'Hubo un error creando el cliente, vuelve a intentarlo',
        //   type: 'error',
        // });
      },
      () => {
        // Swal.fire({
        //   title: 'Cliente creado exitosamente, ¿deseas registrar ahora?',
        //   type: 'success',
        //   showCancelButton: true,
        //   cancelButtonText: 'No',
        //   confirmButtonText: 'Si',
        //   reverseButtons: true,
        //   allowOutsideClick: true,
        //   allowEscapeKey: true,
        //   showCloseButton: true,
        //   backdrop: true,
        // }).then(resp => {
        //   if (String(resp.dismiss) === 'cancel') {
        //     this.dismiss(true);
        //   } else {
        //     this.dismiss(false);
        //     this.router.navigate(['/modulos/emisiones/registro-poliza-cli/' + idCliente]);
        //   }
        // });
      });
  }

  dismiss(clienteCreado: boolean): void {
    this.dialogRef.close(clienteCreado);
  }

}
