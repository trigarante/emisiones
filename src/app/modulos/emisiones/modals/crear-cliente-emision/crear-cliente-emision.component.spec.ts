import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearClienteEmisionComponent } from './crear-cliente-emision.component';

describe('CrearClienteEmisionComponent', () => {
  let component: CrearClienteEmisionComponent;
  let fixture: ComponentFixture<CrearClienteEmisionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrearClienteEmisionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearClienteEmisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
