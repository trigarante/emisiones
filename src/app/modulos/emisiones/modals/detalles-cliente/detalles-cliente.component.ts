import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ClienteEmision} from '../../../../@core/data/interfaces/emisiones/cliente-emision';

@Component({
  selector: 'app-detalles-cliente',
  templateUrl: './detalles-cliente.component.html',
  styleUrls: ['./detalles-cliente.component.scss'],
})
export class DetallesClienteComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DetallesClienteComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ClienteEmision) { }

  ngOnInit() {
  }

  dismiss () {
    this.dialogRef.close();
  }

}
