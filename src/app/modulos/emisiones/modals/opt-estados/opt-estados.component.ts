import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {DatosRegistroPolizaComponent} from '../datos-registro-poliza/datos-registro-poliza.component';

@Component({
  selector: 'app-opt-estados',
  templateUrl: './opt-estados.component.html',
  styleUrls: ['./opt-estados.component.scss'],
})
export class OptEstadosComponent implements OnInit {

  datos: any;

  constructor(
    public dialogRef: MatDialogRef<OptEstadosComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialog: MatDialog,
  ) { }

  ngOnInit() {
  }

  quesRenovacion() {
    this.dialog.open(DatosRegistroPolizaComponent, {
      data: {
        datos: this.data,
        tipo: 1,
      },
    }).afterClosed().subscribe(reload => this.salir(reload));
  }

  quesReexpedir() {
    this.dialog.open(DatosRegistroPolizaComponent, {
      data: {
        datos: this.data,
        tipo: 2,
      },
    }).afterClosed().subscribe(reload => this.salir(reload));
  }

  quesMigrar() {
    this.dialog.open(DatosRegistroPolizaComponent, {
      data: {
        datos: this.data,
        tipo: 3,
      },
    }).afterClosed().subscribe(reload => this.salir(reload));
  }

  salir(reload: boolean) {
    this.dialogRef.close(reload);
  }

}
