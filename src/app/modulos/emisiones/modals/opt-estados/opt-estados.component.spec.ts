import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OptEstadosComponent } from './opt-estados.component';

describe('OptEstadosComponent', () => {
  let component: OptEstadosComponent;
  let fixture: ComponentFixture<OptEstadosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OptEstadosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OptEstadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
