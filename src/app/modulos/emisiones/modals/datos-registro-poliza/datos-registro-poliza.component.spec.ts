import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosRegistroPolizaComponent } from './datos-registro-poliza.component';

describe('DatosRegistroPolizaComponent', () => {
  let component: DatosRegistroPolizaComponent;
  let fixture: ComponentFixture<DatosRegistroPolizaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatosRegistroPolizaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosRegistroPolizaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
