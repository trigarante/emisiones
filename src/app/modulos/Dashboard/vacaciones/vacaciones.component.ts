import {Component, OnInit, ViewChild} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {SolicitudesVacacionesService} from '../../../@core/data/services/administracion-personal/solicitudes-vacaciones.service';
import {NotificacionesService} from '../../../@core/data/services/others/notificaciones.service';
import moment from 'moment';
import Swal from "sweetalert2";

@Component({
  selector: 'app-vacaciones',
  templateUrl: './vacaciones.component.html',
  styleUrls: ['./vacaciones.component.scss']
})
export class VacacionesComponent implements OnInit {
  tabActual = 0;
  permisos = JSON.parse(window.localStorage.getItem('User')).VAC;
  constructor() { }

  ngOnInit(): void {
  }

  tabClick(event) {
    this.tabActual =  event.index;
    console.log(this.tabActual);
  }
}
