import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {EmpleadosService} from '../../../../@core/data/services/rrhh/empleados.service';
import {ImprimirXlsxService} from '../../../../@core/data/services/imprimirXLSX/imprimir-xlsx.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog} from '@angular/material/dialog';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';
import {SolicitudesVacacionesService} from '../../../../@core/data/services/administracion-personal/solicitudes-vacaciones.service';
import {PageEvent} from '@angular/material/paginator';
import {AsistenciasService} from '../../../../@core/data/services/administracion-personal/asistencias/asistencias.service';
import {TrigaranteEscuchaService} from '../../../../@core/data/services/desarrollo-organizacional/trigarante-escucha.service';
import {EstadoAsistenciaService} from '../../../../@core/data/services/administracion-personal/asistencias/estado-asistencia.service';
import {CancelarVacacionesComponent} from '../modal/cancelar-vacaciones/cancelar-vacaciones.component';

@Component({
  selector: 'app-vacacion-adp',
  templateUrl: './vacacion-adp.component.html',
  styleUrls: ['./vacacion-adp.component.scss']
})
export class VacacionADPComponent implements OnInit {

  activados = 1;
  solicitudesPendientes = [];
  solicitudesCompletas = [];
  datos: any[] = [];
  paginator = [];
  length = 0;
  pageSize = 10;
  dataSource = new MatTableDataSource([]);


  hijos;
  estados;
  formulario: FormGroup = new FormGroup({
    datos: new FormArray([]),
  });
  empleado = +sessionStorage.getItem('Empleado');
  yaRegistro = false;
  fecha = new Date();

  constructor(
    private dialog: MatDialog,
    private trigaranteEscuchaService: TrigaranteEscuchaService,
    private notificaciones: NotificacionesService,
    private imprimirXlsxService: ImprimirXlsxService,


    private empleadoService: EmpleadosService,
    private estadoAsistenciaService: EstadoAsistenciaService,
    private asistenciasService: AsistenciasService,
    private fb: FormBuilder,
    private notificacionesService: NotificacionesService,

    private vacacionesService: SolicitudesVacacionesService
  ) { }

  ngOnInit(): void {
    this.getSolicitudes();
  }

  /** Get inicial **/
  getSolicitudes() {
    const op = this.vacacionesService.getByDepartamento(2);
    op.subscribe( values => {
      this.dataSource = new MatTableDataSource<any>(values);
      if (values.length === 0) { this.notificaciones.informacion(`No tienes solicitudes pendientes`); }
      // this.updateDatos();
      this.onPage( { pageIndex: 0, pageSize: this.pageSize, length: this.datos.length });
      this.length = values.length;
    });
  }

  async finalizar(opcion, id) {
    const respuesta = await this.notificacionesService.pregunta(`¿Quieres ${opcion === 1 ? 'autorizar' : 'rechazar'} esta solicitud de vacación?`);
    if (respuesta.isConfirmed) {
      this.notificacionesService.carga();
      this.vacacionesService.update(id, {
        idEmpleadoSupervisor: +sessionStorage.getItem('Empleado'),
        fechaFinalizacion: new Date(),
        idEstadoVacacion: opcion === 1 ? 2 : 3,
      }).subscribe(() => this.notificacionesService.exito().then(() => this.getSolicitudes()));
    }
  }

  abrirDialog(id) {
    console.log(id);
    this.dialog.open(CancelarVacacionesComponent, {
      data: id,
      width: '500px',
      disableClose: true,
    }).afterClosed().subscribe(reload => {
      if (reload) { this.getSolicitudes(); }
    });
  }

  /** Cambiar el paginador **/
  onPage(event: PageEvent) {
    const start = event.pageIndex * event.pageSize;
    const final = event.pageIndex === 0 ? start + event.pageSize : start + event.pageSize + 1;
    this.paginator = this.dataSource.data.slice(start, final);
  }

  /** Actualizar información **/
  updateDatos() {
    this.datos = this.activados === 1 ? this.solicitudesPendientes : this.solicitudesCompletas ;
    this.dataSource = new MatTableDataSource(this.datos);
    this.onPage( { pageIndex: 0, pageSize: this.pageSize, length: this.datos.length });
  }

  /** Si modifican un dato en los modals **/
  update() {
    this.activados === 1 ? this.solicitudesPendientes = [] : this.solicitudesCompletas = [];
    this.getSolicitudes();
  }

  /** Filtro **/
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.datos = this.dataSource.filteredData;
    this.onPage( { pageIndex: 0, pageSize: this.pageSize, length: this.datos.length });
  }

  get getDatosArray() {
    return this.formulario.get('datos') as FormArray;
  }

  limpiar() {
    this.getDatosArray.clear();
    this.hijos = null;
  }
  async guardar() {
    const respuesta = await this.notificacionesService.pregunta('¿Quieres guardar la información?');
    if (respuesta.isConfirmed) {
      this.notificacionesService.carga();
      this.asistenciasService.post(this.formulario.get('datos').value).subscribe(() => {
        this.notificacionesService.exito().then(() => this.limpiar());
      });
    }
  }
}
