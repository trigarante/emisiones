import { Component, Input, OnInit } from '@angular/core';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SolicitudesVacacionesService} from '../../../../@core/data/services/administracion-personal/solicitudes-vacaciones.service';

@Component({
  selector: 'app-solicitar-vacacion',
  templateUrl: './solicitar-vacacion.component.html',
  styleUrls: ['./solicitar-vacacion.component.scss']
})
export class SolicitarVacacionComponent implements OnInit {
  rangeDates =  [
    new Date(new Date().setDate(new Date().getDate() + 7)),
    new Date(new Date().setDate(new Date().getDate() + 60)),
  ];
  fecha = new Date();
  inicio;
  fin;
  empleado;
  diasCorrespondientes;
  formulario = new FormGroup({
    idEmpleado: new FormControl(+sessionStorage.getItem('Empleado')),
    fechaInicio: new  FormControl(null, Validators.required),
    fechaFinal: new  FormControl(null, Validators.required),
    cantidadDias: new  FormControl(null),
  });

  constructor(
    private notificacionesService: NotificacionesService,
    private vacacionesService: SolicitudesVacacionesService
  ) { }

  ngOnInit(): void {
    this.getCantidadVacaciones();
  }

  getCantidadVacaciones() {
    this.vacacionesService.getCantidadVacaciones(+sessionStorage.getItem('Empleado')).subscribe({
      next: value => {
        this.empleado = value;
        this.diasCorrespondientes = (this.empleado.anios === 0) ? 0 :
          this.empleado.json.filter(dato => dato.anio <= this.empleado.anios).reverse()[0].dias;
      }
    });
  }

  async guardar() {
    const respuesta = await this.notificacionesService.pregunta('¿Quieres guardar la información?');
    if (respuesta.isConfirmed) {
      this.notificacionesService.carga();
      this.vacacionesService.post(this.formulario.getRawValue()).subscribe({
        next: res => {
          if (!res){
            this.notificacionesService.advertencia('Elegiste una fecha previamente autorizada, por favor revisa tus fechas');
          } else {
            this.notificacionesService.exito().then(() => {
              this.getCantidadVacaciones();
              this.formulario.reset();
              this.formulario.controls.idEmpleado.setValue(+sessionStorage.getItem('Empleado'));
            });
          }
        }
      });
    }
  }

  calcularDias() {
    if (this.formulario.controls.fechaFinal.value !== null){
      let inicio = new Date(this.formulario.controls.fechaInicio.value); // Fecha inicial
      const fin = new Date(this.formulario.controls.fechaFinal.value); // Fecha final
      const timeDiff = Math.abs(fin.getTime() - inicio.getTime());
      let diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)) + 1; // Días entre las dos fechas
      let sabado = 0;
      let domingo = 0;

      for (let i = 0; i < diffDays; i++) {
        // 0 => Domingo - 6 => Sábado
        if (inicio.getDay() === 0) {
          domingo++;
        } else if (inicio.getDay() === 6) {
          sabado ++;
        }
        inicio.setDate(inicio.getDate() + 1);
      }
      switch (this.empleado.trabajaSabado){
        case 1:
          diffDays = diffDays - domingo;
          break;
        case 2:
          diffDays = diffDays - sabado;
          break;
        case 0:
          diffDays = diffDays - domingo;
          diffDays = diffDays - sabado;
          break;

      }
      // diffDays = diffDays - domingo;
      // if (!this.empleado.trabajaSabado) {
      //   diffDays = diffDays - sabado;
      // }
      if (diffDays > this.empleado.cantidadVacaciones) {
        this.notificacionesService.error('No tienes tantos días disponibles');
        this.formulario.controls.fechaInicio.setValue(null);
        this.formulario.controls.fechaFinal.setValue(null);
      } else {
        console.log(diffDays);
        this.formulario.controls.cantidadDias.setValue(diffDays);
      }
    }
    // this.formulario.controls.fechaInicio.setValue(this.formulario.controls.fechaInicio.value.toISOString().split('T')[0]);
    // this.formulario.controls.fechaFinal.setValue(this.formulario.controls.fechaFinal.value.toISOString().split('T')[0]);
  }
  borrar(){
    this.formulario.controls.fechaInicio.setValue(null);
    this.formulario.controls.fechaFinal.setValue(null);
  }
}
