import {Component, Inject, OnInit} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {TrigaranteEscuchaService} from '../../../../../@core/data/services/desarrollo-organizacional/trigarante-escucha.service';
import {NotificacionesService} from '../../../../../@core/data/services/others/notificaciones.service';
import {DepartamentoService} from '../../../../../@core/data/services/administracion-personal/departamento.service';

@Component({
  selector: 'app-asignar-area',
  templateUrl: './asignar-area.component.html',
  styleUrls: ['./asignar-area.component.scss']
})
export class AsignarAreaComponent implements OnInit {

  permisos = JSON.parse(window.localStorage.getItem('User')).RH;
  idArea = new FormControl('', Validators.required);
  areas;
  respaldo;

  constructor(
    private departamentoService: DepartamentoService,
    private notificacionesService: NotificacionesService,
    private trigaranteEscuchaService: TrigaranteEscuchaService,
    public dialogRef: MatDialogRef<AsignarAreaComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) {}

  ngOnInit(): void {
    this.idArea.setValue(this.data.idArea);
    this.getAreas();
  }

  getAreas(){
    this.departamentoService.getAreas().subscribe(data => this.respaldo = this.areas = data);
  }

  registrar() {
    this.notificacionesService.carga();
    this.trigaranteEscuchaService.put(this.data.id, {idArea: this.idArea.value}).subscribe({
      next: () => this.notificacionesService.exito().then(() => {this.cerrar(true); }),
      error: () => this.notificacionesService.error().then(() => this.cerrar(false))
    });
  }

  cerrar(reload: boolean){
    this.dialogRef.close(reload);
  }
  onKey(value) {
    this.areas = this.search(value);
  }

  search(value: string) {
    const filter = value.toLowerCase();
    return this.respaldo.filter(area => area.nombre.toLowerCase().startsWith(filter));
  }

}
