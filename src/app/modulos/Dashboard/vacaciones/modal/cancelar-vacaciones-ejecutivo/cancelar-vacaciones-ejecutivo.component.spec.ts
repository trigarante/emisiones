import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelarVacacionesEjecutivoComponent } from './cancelar-vacaciones-ejecutivo.component';

describe('CancelarVacacionesEjecutivoComponent', () => {
  let component: CancelarVacacionesEjecutivoComponent;
  let fixture: ComponentFixture<CancelarVacacionesEjecutivoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CancelarVacacionesEjecutivoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelarVacacionesEjecutivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
