import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {NotificacionesService} from '../../../../../@core/data/services/others/notificaciones.service';
import {SolicitudesVacacionesService} from '../../../../../@core/data/services/administracion-personal/solicitudes-vacaciones.service';

@Component({
  selector: 'app-autorizar-vacaciones',
  templateUrl: './autorizar-vacaciones.component.html',
  styleUrls: ['./autorizar-vacaciones.component.scss']
})
export class AutorizarVacacionesComponent implements OnInit {

  comentarios = new FormControl('', Validators.compose([Validators.required]));

  constructor(
    public dialogRef: MatDialogRef<AutorizarVacacionesComponent>,
    private notificaciones: NotificacionesService,
    private vacacionesService: SolicitudesVacacionesService,
    @Inject(MAT_DIALOG_DATA) public data,
  ) { }

  ngOnInit(): void {
  }

  guardar() {
    console.log(this.data);
    this.notificaciones.carga(`Se están guardando los comentarios`);
    this.vacacionesService.update(this.data, {
      idEmpleadoSupervisor: +sessionStorage.getItem('Empleado'),
      fechaFinalizacion: new Date(),
      idEstadoVacacion: 3,
      comentarios: this.comentarios.value,
    }).subscribe(() => this.notificaciones.exito().then(() => this.cerrar(true)));
  }

  cerrar(reload: boolean) {
    this.notificaciones.cerrar();
    this.dialogRef.close(reload);
  }

}
