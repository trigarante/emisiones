import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CandidatoService} from '../../../../../@core/data/services/atraccion-talento/candidato/candidato.service';
import {NotificacionesService} from '../../../../../@core/data/services/others/notificaciones.service';
import {TipoJustificacionService} from '../../../../../@core/data/services/administracion-personal/tipo-justificacion.service';
import {JustificacionesService} from '../../../../../@core/data/services/administracion-personal/justificaciones.service';
import {EmpleadosService} from '../../../../../@core/data/services/rrhh/empleados.service';
import {MotivoJustificacionService} from '../../../../../@core/data/services/administracion-personal/motivo-justificacion.service';

@Component({
  selector: 'app-solicitar-justificante',
  templateUrl: './solicitar-justificante.component.html',
  styleUrls: ['./solicitar-justificante.component.scss']
})
export class SolicitarJustificanteComponent implements OnInit {

  file = {
    name: null,
    data: null
  };
  motivos: any[] = [];
  empleados: any[] = [];
  respaldo: any[] = [];
  tipoJustificacion: any[] = [];
  max = new Date();
  form = new FormGroup({
    idEmpleado: new FormControl(null, Validators.required),
    idEmpleadoSolicitante: new FormControl(+ sessionStorage.getItem('Empleado')),
    fechaInicio: new FormControl(null, Validators.required),
    fechaFin: new FormControl(null, Validators.required),
    comentarios: new FormControl(null, Validators.required),
    idMotivoJustificacion: new FormControl(null, Validators.required),
  });
  input = new FormControl();

  constructor(
    private candidatoService: CandidatoService,
    private notificaciones: NotificacionesService,
    private justificaciones: JustificacionesService,
    private matRef: MatDialogRef<SolicitarJustificanteComponent>,
    private motivoJustificacion: MotivoJustificacionService,
    private empleadosService: EmpleadosService,
    private tipoJustificacionService: TipoJustificacionService,
    @Inject(MAT_DIALOG_DATA) public data,
  ){ }

  ngOnInit(): void {
    this.motivoJustificacion.getAll().subscribe((motivos: any) => this.motivos = motivos);
    this.empleadosService.getPlazasHijo().subscribe((data: any) => this.empleados = this.respaldo = data);
  }

  async onSubmit() {
    this.notificaciones.carga('Enviando solicitud');
    this.justificaciones.crearSolicitudIncapacidad(this.file.data, this.form.getRawValue(), this.file.name)
      .subscribe(async () => {
        await this.notificaciones.exito('Se ha enviado tu solicitud').then(() => this.matRef.close(true));
      }, () => {
        this.notificaciones.error('Ocurrio un error al momento de crear tu solicitud');
      });
    // const pregunta = await this.notificaciones.pregunta('¿Es correcta la información?');
    // if (pregunta.isConfirmed) {
    //   this.matRef.close({...this.form.getRawValue(), archivo: this.file.data.item(0)});
    // }
  }

  onKey(value) {
    const filter = value.toLowerCase();
    this.empleados = this.respaldo.filter(empleado => empleado.nombreEmpleado.toLowerCase().startsWith(filter));
  }

  subirDocumento(files: FileList) {
    if (files.length > 0) {
      this.file.data = files;
      this.file.name = files.item(0).name;
    }
  }
  cerrar(){
    this.matRef.close();
  }
}
