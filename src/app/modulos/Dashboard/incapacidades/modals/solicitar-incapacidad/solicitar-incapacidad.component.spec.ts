import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitarIncapacidadComponent } from './solicitar-incapacidad.component';

describe('SolicitarIncapacidadComponent', () => {
  let component: SolicitarIncapacidadComponent;
  let fixture: ComponentFixture<SolicitarIncapacidadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SolicitarIncapacidadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitarIncapacidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
