import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {NotificacionesService} from '../../../../../@core/data/services/others/notificaciones.service';
import {JustificacionesService} from '../../../../../@core/data/services/administracion-personal/justificaciones.service';

@Component({
  selector: 'app-rechazar-justificante',
  templateUrl: './rechazar-justificante.component.html',
  styleUrls: ['./rechazar-justificante.component.scss']
})
export class RechazarJustificanteComponent implements OnInit {

  comentarios = new FormControl('', Validators.compose([Validators.required]));

  constructor(
    public dialogRef: MatDialogRef<RechazarJustificanteComponent>,
    private notificaciones: NotificacionesService,
    private justificaciones: JustificacionesService,
    @Inject(MAT_DIALOG_DATA) public data,
  ) { }

  ngOnInit(): void {
    console.log(this.data);
  }

  guardar() {
    console.log(this.data);
    this.notificaciones.carga(`Se están guardando los comentarios`);
    this.justificaciones.updateEstatus({
      id: this.data,
      idEmpleadoAutorizo: +sessionStorage.getItem('Empleado'),
      idEstadoJustificacion: 1,
      comentariosAdp: this.comentarios.value,
    }).subscribe(() => this.notificaciones.exito().then(() => this.cerrar(true)));
  }

  cerrar(reload: boolean) {
    this.notificaciones.cerrar();
    this.dialogRef.close(reload);
  }
}
