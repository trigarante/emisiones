import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RechazarJustificanteComponent } from './rechazar-justificante.component';

describe('RechazarJustificanteComponent', () => {
  let component: RechazarJustificanteComponent;
  let fixture: ComponentFixture<RechazarJustificanteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RechazarJustificanteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RechazarJustificanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
