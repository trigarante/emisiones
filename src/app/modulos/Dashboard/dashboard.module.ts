import { MatPaginatorIntl } from '@angular/material/paginator';
import { MatTabsModule } from '@angular/material/tabs';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import {DashboardComponent} from './dashboard/dashboard.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {RouterModule} from '@angular/router';
import {MatSelectModule} from '@angular/material/select';
import {getSpanishPaginatorIntl} from '../../traduccion-paginator';
import {MaterialModule} from '../material.module';
import {LeftComponent} from './left/left.component';
import {RightComponent} from './right/right.component';
import {IncapacidadesComponent} from './incapacidades/incapacidades.component';
import {NominaComponent} from './nomina/nomina.component';
import {VacacionesComponent} from './vacaciones/vacaciones.component';
import {VisualizarKpiComponent} from './visualizar-kpi/visualizar-kpi.component';
import {SolicitarIncapacidadComponent} from './incapacidades/modals/solicitar-incapacidad/solicitar-incapacidad.component';
import { DashHeaderComponent } from './dash-header/dash-header.component';
import { TabsComponent } from './tabs/tabs.component';
import { DashPjsipComponent } from './dash-pjsip/dash-pjsip.component';
import { DashRenoPjsipComponent } from './dash-reno-pjsip/dash-reno-pjsip.component';
import { VacacionesSolicitadasComponent } from './vacaciones/vacaciones-solicitadas/vacaciones-solicitadas.component';
import { SolicitarVacacionComponent } from './vacaciones/solicitar-vacacion/solicitar-vacacion.component';
import { AutorizarVacacionesComponent } from './vacaciones/modal/autorizar-vacaciones/autorizar-vacaciones.component';
import { AsignarAreaComponent } from './vacaciones/modal/asignar-area/asignar-area.component';
import { VacacionADPComponent } from './vacaciones/vacacion-adp/vacacion-adp.component';
import { CancelarVacacionesComponent } from './vacaciones/modal/cancelar-vacaciones/cancelar-vacaciones.component';
import { AsistenciasComponent } from './asistencias/asistencias.component';
import { AsistenciaComponent } from './asistencias/asistencia/asistencia.component';
import { VerAsistenciaComponent } from './asistencias/ver-asistencia/ver-asistencia.component';
import {FlexModule} from '@angular/flex-layout';
import {UltimosMovimientosComponent} from './ultimo-movimiento/ultimos-movimientos/ultimos-movimientos.component';
import { JustificantesComponent } from './incapacidades/justificantes/justificantes.component';
import { RechazarJustificanteComponent } from './incapacidades/modals/rechazar-justificante/rechazar-justificante.component';
import { SolicitarJustificanteComponent } from './incapacidades/modals/solicitar-justificante/solicitar-justificante.component';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { CancelarVacacionesEjecutivoComponent } from './vacaciones/modal/cancelar-vacaciones-ejecutivo/cancelar-vacaciones-ejecutivo.component';
import { UltimoMovimientoComponent } from './ultimo-movimiento/ultimo-movimiento.component';
@NgModule({
    declarations: [
        DashboardComponent,
        LeftComponent,
        RightComponent,
        IncapacidadesComponent,
        NominaComponent,
        VacacionesComponent,
        VisualizarKpiComponent,
        SolicitarIncapacidadComponent,
        DashHeaderComponent,
        TabsComponent,
        DashPjsipComponent,
        DashRenoPjsipComponent,
        VacacionesSolicitadasComponent,
        SolicitarVacacionComponent,
        AutorizarVacacionesComponent,
        AsignarAreaComponent,
        VacacionADPComponent,
        CancelarVacacionesComponent,
        AsistenciasComponent,
        AsistenciaComponent,
        VerAsistenciaComponent,
        UltimosMovimientosComponent,
        JustificantesComponent,
        RechazarJustificanteComponent,
        SolicitarJustificanteComponent,
        CancelarVacacionesEjecutivoComponent,
        UltimoMovimientoComponent
    ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    RouterModule,
    MaterialModule,
    FlexModule,
    NgxExtendedPdfViewerModule
  ],
  providers: [
    {provide: MatPaginatorIntl, useValue: getSpanishPaginatorIntl()},
    MatSelectModule,
    MatTabsModule,
    MatExpansionModule
  ]
})
export class DashboardModule { }
