import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';
import {MatDialog} from '@angular/material/dialog';
import {EmpleadosService} from '../../../../@core/data/services/rrhh/empleados.service';
import {AsistenciasService} from '../../../../@core/data/services/administracion-personal/asistencias/asistencias.service';
import {SolicitarIncapacidadComponent} from '../../incapacidades/modals/solicitar-incapacidad/solicitar-incapacidad.component';
import {EstadoAsistenciaService} from '../../../../@core/data/services/administracion-personal/asistencias/estado-asistencia.service';

@Component({
  selector: 'app-asistencia',
  templateUrl: './asistencia.component.html',
  styleUrls: ['./asistencia.component.scss']
})
export class AsistenciaComponent implements OnInit {

  hijos;
  estados;
  estadosCompletos;
  fecha = new Date();
  formulario: FormGroup = new FormGroup({
    datos: new FormArray([]),
  });
  empleado = +sessionStorage.getItem('Empleado');
  yaRegistro = false;
  faltan = [];
  proceso = true;

  constructor(
    private empleadoService: EmpleadosService,
    private estadoAsistenciaService: EstadoAsistenciaService,
    private asistenciasService: AsistenciasService,
    private fb: FormBuilder,
    private notificacionesService: NotificacionesService,
    private matDialog: MatDialog,
  ) { }
  get getDatosArray() {
    return this.formulario.get('datos') as FormArray;
  }
  ngOnInit(): void {
    this.revisarFaltantes();
    this.getDatosArray.clear();
    this.hijos = null;
    this.getAsistencias();
    this.getHijos();
    // this.getRegistroAsistencia();
  }
  revisarFaltantes(){
    this.asistenciasService.getEmpleadosFaltantes().subscribe( data => {
      this.yaRegistro = data.length > 0 ? false : true;
      console.log(data);
      // if (!this.yaRegistro){
      //   window.alert('Toma asistencia');
      // }
    });
  }
  // getRegistroAsistencia(tomada, i) {
  //   console.log(tomada.get('idEmpleado').value);
  //   this.asistenciasService.getRegistroAsistenciaXempleado(tomada.get('idEmpleado').value).subscribe({
  //     next: data => {
  //       if (data) {
  //         // this.yaRegistro = true;
  //         this.getDatosArray.at(i).get('tomada').setValue(1);
  //       } else {
  //         // this.yaRegistro = false;
  //         // this.getDatosArray.clear();
  //         // this.hijos = null;
  //         // this.getHijos();
  //         // this.getAsistencias();
  //       }
  //     }
  //   });
  //   return tomada.get('tomada').value;
  // }

  getHijos() {
    // if (this.hijos.length > this.getDatosArray.length) {
    //   this.proceso = true;
    // }
    this.proceso = true;
    this.getDatosArray.clear();
    this.hijos = null;
    this.empleadoService.getPlazasHijo().subscribe({
      next: data => {
        // this.proceso = true;
        if (data.length > 0) {
          this.hijos = data;
          // this.getDatosArray.clear();
          data.map(hijo => {
            const idEstadoAsistenciaCons = hijo.idVacacion ? 13 : 1;
            const bloquear = hijo.idVacacion ? 1 : 0;
            this.asistenciasService.getRegistroAsistenciaXempleado(hijo.id).subscribe({
              next: val => {
                this.proceso = true;
                this.getDatosArray.push(this.fb.group({
                  idEmpleadoSupervisor: +this.empleado,
                  idEmpleado: hijo.id,
                  idEstadoAsistencia: hijo.idVacacion ? idEstadoAsistenciaCons : val ? val.idEstadoAsistencia : 1,
                  nombre: hijo.nombre,
                  turno: hijo.turno,
                  bloquear,
                  tomada: val ? 1 : 0,
                  justificante: null
                }));
                console.log(this.getDatosArray.length);
                // if (this.hijos.length > this.getDatosArray.length) {
                //   this.getDatosArray.clear();
                // } else
                if ( this.hijos.length === this.getDatosArray.length){
                  setTimeout(() => this.proceso = false, this.hijos.length > 10 ? 315 : 260 );
                  // this.getDatosArray.clear();
                }
              },
              complete: () => {
                // this.proceso = false;
                // this.formulario.get('datos').value.forEach(empl => {
                //   // this.faltan = this.faltan.length > 0 ? [] : this.faltan ;
                //   if ( empl.tomada === 0) this.faltan.push(empl.idEmpleado);
                //   this.yaRegistro = this.faltan.length > 0 ? false : true;
                // });
              }
            });
            // this.getDatosArray.push(this.fb.group({
            //   idEmpleadoSupervisor: +this.empleado,
            //   idEmpleado: hijo.id,
            //   idEstadoAsistencia,
            //   nombre: hijo.nombre,
            //   bloquear,
            //   tomada: 0,
            //   justificante: null
            // }));
          });
        } else {
          this.yaRegistro = true;
        }
      },
      // complete: () => this.proceso = false,
      error: () => {
        // this.proceso = false;
        this.notificacionesService.error();
      }
    });
    this.revisarFaltantes();
    // console.log(this.getDatosArray.length);
    // if (this.hijos.length === this.getDatosArray.length) {
    //   this.proceso = false;
    // }
  }
  habilitar(){
    this.proceso = false;
  }
  limpiar() {
    this.getDatosArray.clear();
    this.hijos = null;
    this.yaRegistro = true;
    this.getHijos();
  }
  getAsistencias() {
    this.estadoAsistenciaService.getActivos().subscribe(data => {
      const quitadas = [5, 7, 8, 10];
      const filtrados = data.filter(edo => !quitadas.includes(edo.id));
      console.log(filtrados);
      this.estados = filtrados;
      this.estadosCompletos = data;
    } );
  }

  async guardar(indice) {
    console.log(indice);
    console.log(this.formulario.get('datos').value[indice]);
    const respuesta = await this.notificacionesService.pregunta('¿Quieres guardar la información?');
    if (respuesta.isConfirmed) {
      this.notificacionesService.carga();
      this.asistenciasService.post(this.formulario.get('datos').value[indice]).subscribe(() => {
        this.notificacionesService.exito().then(() => this.limpiar());
      });
    }
  }
test(){
  this.formulario.get('datos').value.forEach(empl => {
    if ( empl.tomada === 0) this.faltan.push(empl.idEmpleado);
    // console.log(empl);
  });
  console.log(this.faltan);
  // this.yaRegistro = this.faltan.length > 0 ? false : true;
  }
  validarJustificacion(idEstadoAsistencia, id, index) {
    // Arreglo de motivos justificacion
    // const array = [5, 7, 8, 10];
    // if (array.includes(idEstadoAsistencia)) {
    //   this.matDialog.open(SolicitarIncapacidadComponent, {
    //     width: '800px',
    //     data: {asistencia: 1, id, idEstadoAsistencia}
    //   }).afterClosed().subscribe(data => {
    //     if (data) {
    //       this.getDatosArray.at(index).get('justificante').setValue(data);
    //     }else {
    //       this.getDatosArray.at(index).get('idEstadoAsistencia').setValue(1);
    //     }
    //   });
    // } else {
      if (this.getDatosArray.at(index).get('justificante').value) {
        this.getDatosArray.at(index).get('justificante').setValue(null);
      }
    // }
  }

}
