import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizarKpiComponent } from './visualizar-kpi.component';

describe('VisualizarKpiComponent', () => {
  let component: VisualizarKpiComponent;
  let fixture: ComponentFixture<VisualizarKpiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualizarKpiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizarKpiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
