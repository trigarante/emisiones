import Swal, { SweetAlertResult } from 'sweetalert2';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Notificaciones {

  constructor() {}

  carga(texto: string): Promise<SweetAlertResult<unknown>> {
    // @ts-ignore
    return Swal.fire({
      title: texto + ', por favor espere',
      showConfirmButton: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      }
    });
  }

  exito(texto: string): Promise<any> {
    return Swal.fire({
      icon: 'success',
      title: 'Correcto',
      text: '¡' + texto + ' con éxito!',
    });
  }

  error(texto: string): Promise<any> {
    return Swal.fire({
      title: 'Error',
      text: texto,
      icon: 'error'
    });
  }

  advertencia(texto: string): Promise<any> {
    return Swal.fire({
      title: 'Advertencia',
      text: texto,
      icon: 'warning'
    });
  }

  pregunta(text: string, title?: string): Promise<SweetAlertResult> {
    return Swal.fire({
      title: title || 'Advertencia',
      text,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, continuar',
      cancelButtonText: 'No, cancelar',
      reverseButtons: true,
    });
  }

}
