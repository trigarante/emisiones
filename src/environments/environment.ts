// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'https://reno-core.trigarante2022.com/v1/',
  CORE_DOCUMENTOS: 'https://documentos-core.trigarante2022.com/v1',
  // CORE_DOCUMENTOS: 'http://localhost:8000/',
  nodeLogin: 'https://login-core.trigarante2022.com/login',
  simuladoresUrl: 'https://simulador.mark-43.net/',
  landingUrl:  'https://reclutamiento.trigarante2020.com',
  CORE_RRHH: 'https://rrhh.mark-43.net/',
  SERVICIOS_TELEFONIA: 'https://telefonia.mark-43.net/api',
  SERVICIOS_DATOS: 'https://telefonia-datos.mark-43.net/api',
  CARGA_EXCEL: 'http://localhost:8080/',
  // CARGA_EXCEL: 'https://emisiones-core.trigarante2022.com/',
  SERVICIOS_NUEVA_LOGICA: 'http://localhost:8000/api',
  SERVICIOS_NODE: 'https://telefonia.mark-43.net/api',
  SERVICIOS_DATOS_MONITOREO: 'https://telefonia-datos.mark-43.net',
  SERVICIOS_PJSIP: 'https://telefonia-pruebas.mark-43.net/pjsip',
  SERVICIOS_PJSIP_VN: 'https://telefonia-pruebas.mark-43.net/api',
  GLOBAL_SERVICIOS_OPERACIONES: 'https://operaciones.mark-43.net/mark43-service/',
  GLOBAL_SERVICIOS_OPERACIONES_Lectura: 'https://operacion-lectura.mark-43.net/mark43-service/',
  GLOBAL_SERVICIOS: 'https://reno-core.trigarante2022.com/',
  // GLOBAL_SERVICIOS: 'http://localhost:8081/',
  // 'http://192.168.42.217:5000/mark43-service/',
  GLOBAL_SOCKET: 'https://socket.mark-43.net/',
  GLOBAL_SERVICIOS_DOCUMENTOS: 'https://documentos.mark-43.net/mark43-service/',
  GLOBAL_SERVICIOS_DOCUMENTOS_OPERACION: 'https://operacionesdo.mark-43.net/mark43-service/',
  CORE_EXTERNO: undefined,
  SERVICIOS_NODE_WHATSAPP: 'https://wavy.mark-43.net/',
  // CORE_VN2: 'http://localhost:8000/',
  CORE_VN2: 'https://reno-core.trigarante2022.com/v1/',
  CORE_DASH: 'https://dash-core.trigarante2022.com/v1',
  CORE_ESCUCHA: 'https://escucha-core.trigarante2022.com',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *s
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
